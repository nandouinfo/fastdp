package com.nd.fastdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 超出 “com.nd.fastdp” 之外的包需要扫描时请添加至 下方 scanBasePackages 中
 */
@SpringBootApplication(scanBasePackages = {"com.nd.fastdp"})
public class FastdpStarter extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(FastdpStarter.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FastdpStarter.class);
    }
}
