/*
Navicat MySQL Data Transfer

Source Server         : dev
Source Server Version : 50729
Source Host           : localhost:10421
Source Database       : fastdp

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2021-04-26 11:36:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fdp_base_material
-- ----------------------------
DROP TABLE IF EXISTS `fdp_base_material`;
CREATE TABLE `fdp_base_material` (
  `id` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT '' COMMENT '名称',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `remark` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `type` varchar(64) DEFAULT '' COMMENT '素材类型',
  `create_time` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `create_user` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人名称',
  `create_dept` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门编码',
  `create_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT '0' COMMENT '修改时间',
  `modify_user` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改用户编码',
  `modify_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改用户名称',
  `modify_dept` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='素材';

-- ----------------------------
-- Records of fdp_base_material
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_base_objcode
-- ----------------------------
DROP TABLE IF EXISTS `fdp_base_objcode`;
CREATE TABLE `fdp_base_objcode` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '名称',
  `type` varchar(200) DEFAULT '' COMMENT '类型',
  `prefix` varchar(100) NOT NULL DEFAULT '' COMMENT '前缀',
  `start_val` bigint(100) DEFAULT '0' COMMENT '起始值',
  `total` bigint(100) DEFAULT '1' COMMENT '数量',
  `used_count` bigint(100) DEFAULT '0' COMMENT '已使用数量',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `has_desc` tinyint(1) DEFAULT '0' COMMENT '是否有描述信息',
  `desc_back_color` varchar(20) DEFAULT '' COMMENT '描述信息背景色',
  `salt` varchar(200) DEFAULT '' COMMENT '盐值',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='一物一码';

-- ----------------------------
-- Records of fdp_base_objcode
-- ----------------------------
INSERT INTO `fdp_base_objcode` VALUES ('bbfade4ceeef599e7540c61b06ee0c70', '灭火器', '1', 'MHQ', '20210426001', '100', '0', '2', '1', '#ffb800', 'A610784178EB5B60694DB888B99B9316', '', '0', '1619401654816', '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '1619401654816', '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');

-- ----------------------------
-- Table structure for fdp_base_objcode_detail
-- ----------------------------
DROP TABLE IF EXISTS `fdp_base_objcode_detail`;
CREATE TABLE `fdp_base_objcode_detail` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `obj_code_id` varchar(64) DEFAULT '' COMMENT '一物一码ID',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '名称',
  `no` varchar(200) NOT NULL DEFAULT '' COMMENT '编号',
  `path` varchar(255) DEFAULT '' COMMENT '地址',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `used_time` bigint(20) DEFAULT NULL COMMENT '使用时间',
  `used_user` varchar(64) DEFAULT '' COMMENT '使用人',
  `used_user_name` varchar(50) DEFAULT '' COMMENT '使用用户名称',
  `used_dept` varchar(64) DEFAULT '' COMMENT '使用部门',
  `used_dept_name` varchar(50) DEFAULT '' COMMENT '使用部门名称',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='一物一码明细';

-- ----------------------------
-- Records of fdp_base_objcode_detail
-- ----------------------------
INSERT INTO `fdp_base_objcode_detail` VALUES ('030dacbfbbf9e95f3b5a0b479b8e8f65', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426057', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('0370a1c8c1b419413d0988c803beba3e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426008', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('07892d25e561084689b138f793979f3c', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426006', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('07fe62f1e98f02c8cb9d495a3076a10a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426029', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('0838c1db3d769009dd04beb45bd37c24', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426060', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('0cbf053c828f36ebeacd8e392631070e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426093', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('12d66bd70069a73ab6058908a5bd9f1f', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426071', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('1313e8080cd455ee4ef463a5f5af33ca', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426047', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('13167585ff3e97cb01066b29d2387b96', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426031', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('15bdaef642b36db1e231379c712aa642', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426087', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('15e357608925020fc71888f72311e7ed', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426034', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('1fdcb66447ef6da7087f36887c188de2', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426088', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('249b08e6cbc98a1be15953bab301961f', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426078', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('2529628daf57f22d94be32b4f7896e9d', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426052', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('25dee53e1de7a3585131b240a0169297', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426012', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('2616c20cd0176a53bcddfc1d92679952', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426090', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('27e296ab1b16704d601c1da1e884b1da', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426037', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('28dbef832f1891b4a5803be05d412407', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426030', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('2950132f2e27c7f25b4efc6aeeef9ae9', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426080', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('2b087df09bd44120af539c93a89701b3', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426050', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('2c73f2bfbe3818f113e8edbcbeec55d7', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426026', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('2d3280656b24e861af277ad812e335c4', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426020', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('30173cc7d400d75a3db8428f246d945a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426072', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('355eff71dae0ba083a43c69b0333a292', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426025', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('3b5f6377bf3a481d6813684167a69918', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426038', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('3b8745837284e34c0011d4357b2d6bac', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426036', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('3e8fee0bf231e3b5ed8f3e55739cb337', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426091', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('3f56fbdd21eae880ca186466034c7f4a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426011', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('3fa65d5102119b62d47c57ba83f94237', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426083', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('4231aa4804601d1d38db7d28aba135b8', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426065', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('4971fd2c6e0cb2a9fdc67c25695f9400', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426086', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('4a45148700e21977474ee6a35235780e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426007', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('4eb719b2fc2a022d8ce02374061248f5', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426077', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('54a4d2ccac3ba9d683b12e0d0d056457', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426040', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('586770ed84d64dac4890e97a2929b1ea', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426018', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('59e7c805d0bae1bc5bb8c24a32e4dec7', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426001', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('5b57e76a53168f591ea1176c937e1a2b', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426009', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('5b7fe36d2ab71391078511a91088524a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426074', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('63cfa02b26b1ff7bd07813931770cea9', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426054', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('653481d8d31e3faa9c318e0a819a117e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426045', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('677deeb3ea407226ddeda571048aa96e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426053', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('6c27fd05f57d447f2e7062d7cfae29e7', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426092', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('6fb6b4e1a7bdbb3ee7d204a0c467e911', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426035', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('702bb41342e1b9bf03f0972752c6e359', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426069', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('7037ea362f9ebb34671f069b555d9edc', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426033', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('74f6fe8b5780dc4e087e73acac771705', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426022', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('78503ce4a23152a60e5f47ceb92c2b6c', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426061', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('7cc2f7ef3a9758ea067080afeb3c345e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426055', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('7eb0efdbe7f3d7a120223dd4ab4a72ba', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426005', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('8265e58b3e37d620f8f2245a4be60d3d', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426043', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('84852dcf62d33f16f6927dea1511ee40', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426068', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('84883dc733ea1c49b54e575fa27b2b0a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426042', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('88329cbb730b461e7b53763050f0af2f', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426095', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('89b89ea80bb9a6565d7cfd8643777c17', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426024', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('8afaf1e52055b69e4ea2df12df3467fb', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426099', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('8bdd871509d22c1ff1097646ead4cf81', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426021', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('8ccfec76e2776a99fadf316528e36367', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426063', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('920baeed3b679b89734c02445896fe7e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426002', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('94f7f873a4911d914f218bfee411a7e3', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426062', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('97fd506633f3672d62dc97fa7a1d52f6', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426096', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('986cb928b8cff46df2f4169d034c4e37', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426015', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('994e9496417d6f86ddf19663311d701f', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426016', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('9b859710f078619c33cb4384dcaf83a0', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426067', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('9c4bba1184b9b8a8df9a917ec939aba9', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426051', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('a6f65799fb245ad3f997188ff03f0ef4', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426003', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('ab0243a9474efd83c70b6944b401d204', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426032', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('ab9bbf80adae48aa47233013e5656e56', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426010', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('aec2b3f52656ec9cbdc97777ecd974bb', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426017', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('aedb7c0ab90300353a01665e1c90328b', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426075', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('b0af7f879c8be016e1ab25a3c921544e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426014', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('b167408008fcf5613bee0272a7ff2f10', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426013', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('b196bb2c9ac96a9c33b4500ee4770c7e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426076', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('b240ebe7dbd8726ac5f538e5fc5945e6', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426027', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('b6e468b313ac044d8624b46024a2e7fd', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426070', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('b8a2458abb6255248ffc0eeba79983b9', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426073', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('bb7b952ce5d97d739e61199224f36472', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426089', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('bb7cbccc5bee76d6ae162ebd21d58dbb', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426085', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('bc621d7434029d1305408ca3598f413f', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426084', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('c470b1a09e636967b59c8a99f74de6de', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426100', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('cf9f818a94e611e4e377262500bea37c', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426049', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('d0f119dfc0f61dcb6e2f8c5ce696359f', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426041', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('d3f15423df0cc0a5abbc3a41ab8c2201', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426056', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('d4d62e4793c8cd800e96421c5e7897db', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426044', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('d5c137b51b9e7b7b4b45d76b248417d1', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426023', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('d99773b77c32539e1ad88982533777c4', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426081', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('dc346172fa25a33ad1d5e67861e0d918', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426048', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('dc867510fc8bac587c1e1f7f58cfa6aa', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426039', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('dd3005e640e44afefbf164339046b13e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426019', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('e0bdcfe40752eda766b42fff1d9fc5b7', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426094', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('e4d0db5d0bc1ed7a4f702656b1adc093', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426079', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('e884c612747b79bc415438b53b44c7b9', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426058', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('f3adf13c26603b9e34d3104ef9193d74', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426046', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('f60f8b4d4e5e66d2a6619818d1814bfe', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426028', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('f8e7f49069ca02e83ecbc49b633e926d', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426098', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('f97a8fd9eca34b9e1be5b36925cc4c56', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426004', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('fa0f12a083dc05ffe2ef5d15b430041e', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426066', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('fa4e10fa21e65ff2c3a8a1c78802fa37', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426059', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('fb130220d83eb8771409c53f6fd03ad6', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426064', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('fb4a49708ee51baec3a2212ba134a61a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426082', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');
INSERT INTO `fdp_base_objcode_detail` VALUES ('fb9fdf712e2a2a0a35d43e5768c6787a', 'bbfade4ceeef599e7540c61b06ee0c70', '灭火器', 'MHQ20210426097', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', null, null, '', '', '', '', '1', '0', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', null, '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');

-- ----------------------------
-- Table structure for fdp_dev_fun
-- ----------------------------
DROP TABLE IF EXISTS `fdp_dev_fun`;
CREATE TABLE `fdp_dev_fun` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `parent_code` varchar(100) DEFAULT NULL COMMENT '上级编码',
  `config` text COMMENT '功能配置',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='功能模块';

-- ----------------------------
-- Records of fdp_dev_fun
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_file
-- ----------------------------
DROP TABLE IF EXISTS `fdp_file`;
CREATE TABLE `fdp_file` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `bus_type` varchar(32) DEFAULT NULL COMMENT '类型',
  `bus_id` varchar(64) DEFAULT NULL COMMENT '业务id',
  `path` varchar(128) DEFAULT NULL COMMENT '文件路径',
  `original_name` varchar(128) DEFAULT NULL COMMENT '原始文件名字',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `state` varchar(32) DEFAULT NULL COMMENT '状态0正常 -１删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `bus_id` (`bus_id`,`bus_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of fdp_file
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_flow_group
-- ----------------------------
DROP TABLE IF EXISTS `fdp_flow_group`;
CREATE TABLE `fdp_flow_group` (
  `id` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT '' COMMENT '名称',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `create_user` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人名称',
  `create_dept` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门编码',
  `create_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT '0' COMMENT '修改时间',
  `modify_user` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改用户编码',
  `modify_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改用户名称',
  `modify_dept` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='流程分组表';

-- ----------------------------
-- Records of fdp_flow_group
-- ----------------------------
INSERT INTO `fdp_flow_group` VALUES ('2b807570f421704044764813ccaa3673', '设备报修', '0', '2', '', '0', '1604397926482', '1', '超级管理员', '1', '南斗信息', '1604397926482', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for fdp_flow_procdef
-- ----------------------------
DROP TABLE IF EXISTS `fdp_flow_procdef`;
CREATE TABLE `fdp_flow_procdef` (
  `id` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT '' COMMENT '名称',
  `code` varchar(255) DEFAULT '' COMMENT '编号',
  `group_id` varchar(64) DEFAULT '' COMMENT '所属分组ID',
  `version` int(11) DEFAULT '0' COMMENT '版本',
  `config` text,
  `deploy_time` bigint(255) DEFAULT NULL COMMENT '部署时间',
  `icon` varchar(255) DEFAULT '' COMMENT '图标',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `create_user` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人名称',
  `create_dept` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门编码',
  `create_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT '0' COMMENT '修改时间',
  `modify_user` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改用户编码',
  `modify_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改用户名称',
  `modify_dept` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='流程定义表';

-- ----------------------------
-- Records of fdp_flow_procdef
-- ----------------------------
INSERT INTO `fdp_flow_procdef` VALUES ('ad35420530f01c87f32e849529609418', '设备报修', 'EQUIPMENT_REPAIR_FLOW', '2b807570f421704044764813ccaa3673', '1', '{\"basicSetting\":{\"flowName\":\"设备报修\",\"flowImg\":0,\"flowGroup\":\"2b807570f421704044764813ccaa3673\",\"initiator\":[{\"nodeId\":\"ec0eda08e40af088273283f3b1a429de\",\"id\":\"ec0eda08e40af088273283f3b1a429de\",\"createTime\":1598510997079,\"createUserName\":\"管理员\",\"createDeptName\":\"中储发展股份有限公司咸阳物流中心\",\"modifyTime\":1608105767508,\"modifyUserName\":\"超级管理员\",\"modifyDeptName\":\"中储咸阳物流中心\",\"name\":\"马瑞雪\",\"simpleName\":\"\",\"code\":\"maruixue\",\"sex\":0,\"type\":null,\"dept\":\"02ebd22ba609ce441f3f5c6c8b1b1f0b\",\"employeeType\":\"1\",\"employeePosition\":\"28f0f34fa2af89489ca112f123d06f66\",\"landline\":\"\",\"phone\":\"18791077059\",\"email\":\"\",\"birthday\":\"\",\"idcard\":\"\",\"nation\":\"01\",\"nativeplace\":\"\",\"address\":\"\",\"degree\":\"81\",\"married\":1,\"sort\":80,\"remark\":\"\",\"state\":0,\"entryTime\":null,\"leaveTime\":null,\"contact\":\"\",\"contactPhone\":\"\",\"fdpCore\":0,\"userId\":\"ec0eda08e40af088273283f3b1a429de\",\"userName\":\"马瑞雪\",\"label\":\"马瑞雪\"}],\"formType\":0,\"flowForm\":\"\",\"code\":\"EQUIPMENT_REPAIR_FLOW\"},\"processData\":{\"type\":\"start\",\"content\":\"所有人\",\"properties\":{\"title\":\"发起人\",\"initiator\":[],\"formOperates\":[{\"formId\":0,\"formOperate\":2},{\"formId\":1,\"formOperate\":2},{\"formId\":3,\"formOperate\":2}]},\"nodeId\":\"Gb2\",\"childNode\":{\"type\":\"approver\",\"content\":\"发起人自选\",\"properties\":{\"title\":\"使用部门意见\",\"menbers\":[],\"assigneeType\":\"optional\",\"formOperates\":[{\"formId\":0,\"formOperate\":1},{\"formId\":1,\"formOperate\":1},{\"formId\":3,\"formOperate\":1}],\"counterSign\":false,\"optionalMultiUser\":false,\"optionalRange\":\"ALL\"},\"nodeId\":\"Kb2\",\"prevId\":\"Gb2\",\"childNode\":{\"type\":\"approver\",\"content\":\"李占军\",\"properties\":{\"title\":\"设备管理部门意见\",\"menbers\":[{\"nodeId\":\"15a33ed8bf9c7363dfe3baf96ae95075\",\"id\":\"15a33ed8bf9c7363dfe3baf96ae95075\",\"createTime\":1598495699420,\"createUserName\":\"管理员\",\"createDeptName\":\"中储发展股份有限公司咸阳物流中心\",\"modifyTime\":1603864417872,\"modifyUserName\":\"超级管理员\",\"modifyDeptName\":\"中储咸阳物流中心\",\"name\":\"李占军\",\"simpleName\":\"\",\"code\":\"1012020017\",\"sex\":1,\"type\":null,\"dept\":\"8fb391d2c2abf13cf0f1322c483081f0\",\"employeeType\":\"1\",\"employeePosition\":\"28f0f34fa2af89489ca112f123d06f66\",\"landline\":\"\",\"phone\":\"13892098033\",\"email\":\"\",\"birthday\":\"\",\"idcard\":\"\",\"nation\":\"01\",\"nativeplace\":\"\",\"address\":\"\",\"degree\":\"81\",\"married\":1,\"sort\":17,\"remark\":\"\",\"state\":0,\"entryTime\":null,\"leaveTime\":null,\"contact\":\"\",\"contactPhone\":\"\",\"fdpCore\":0,\"userId\":\"15a33ed8bf9c7363dfe3baf96ae95075\",\"userName\":\"李占军\",\"label\":\"李占军\"}],\"assigneeType\":\"user\",\"formOperates\":[{\"formId\":0,\"formOperate\":1},{\"formId\":1,\"formOperate\":1},{\"formId\":3,\"formOperate\":1}],\"counterSign\":true,\"optionalMultiUser\":false,\"optionalRange\":\"ALL\"},\"nodeId\":\"Lb2\",\"prevId\":\"Kb2\",\"childNode\":{\"type\":\"approver\",\"content\":\"李占军\",\"properties\":{\"title\":\"保养/修理/改造记录\",\"menbers\":[{\"nodeId\":\"15a33ed8bf9c7363dfe3baf96ae95075\",\"id\":\"15a33ed8bf9c7363dfe3baf96ae95075\",\"createTime\":1598495699420,\"createUserName\":\"管理员\",\"createDeptName\":\"中储发展股份有限公司咸阳物流中心\",\"modifyTime\":1603864417872,\"modifyUserName\":\"超级管理员\",\"modifyDeptName\":\"中储咸阳物流中心\",\"name\":\"李占军\",\"simpleName\":\"\",\"code\":\"1012020017\",\"sex\":1,\"type\":null,\"dept\":\"8fb391d2c2abf13cf0f1322c483081f0\",\"employeeType\":\"1\",\"employeePosition\":\"28f0f34fa2af89489ca112f123d06f66\",\"landline\":\"\",\"phone\":\"13892098033\",\"email\":\"\",\"birthday\":\"\",\"idcard\":\"\",\"nation\":\"01\",\"nativeplace\":\"\",\"address\":\"\",\"degree\":\"81\",\"married\":1,\"sort\":17,\"remark\":\"\",\"state\":0,\"entryTime\":null,\"leaveTime\":null,\"contact\":\"\",\"contactPhone\":\"\",\"fdpCore\":0,\"userId\":\"15a33ed8bf9c7363dfe3baf96ae95075\",\"userName\":\"李占军\",\"label\":\"李占军\"}],\"assigneeType\":\"user\",\"formOperates\":[{\"formId\":0,\"formOperate\":1},{\"formId\":1,\"formOperate\":1},{\"formId\":3,\"formOperate\":1}],\"counterSign\":true,\"optionalMultiUser\":false,\"optionalRange\":\"ALL\"},\"nodeId\":\"Mb2\",\"prevId\":\"Lb2\",\"childNode\":{\"type\":\"approver\",\"content\":\"发起人自选\",\"properties\":{\"title\":\"使用部门验收意见\",\"menbers\":[],\"assigneeType\":\"optional\",\"formOperates\":[{\"formId\":0,\"formOperate\":1},{\"formId\":1,\"formOperate\":1},{\"formId\":3,\"formOperate\":1}],\"counterSign\":false,\"optionalMultiUser\":false,\"optionalRange\":\"ALL\"},\"nodeId\":\"Nb2\",\"prevId\":\"Mb2\"}}}}},\"formData\":{\"formRef\":\"elForm\",\"formModel\":\"formData\",\"size\":\"small\",\"labelPosition\":\"right\",\"labelWidth\":100,\"formRules\":\"rules\",\"gutter\":15,\"disabled\":false,\"span\":24,\"formBtns\":true,\"fields\":[{\"layout\":\"colFormItem\",\"tagIcon\":\"input\",\"label\":\"设备名称\",\"vModel\":\"field0\",\"formId\":0,\"tag\":\"el-input\",\"placeholder\":\"请输入设备名称设备名称设备名称\",\"defaultValue\":\"\",\"span\":24,\"style\":{\"width\":\"100%\"},\"clearable\":true,\"prepend\":\"\",\"append\":\"\",\"maxlength\":11,\"show-word-limit\":true,\"readonly\":false,\"disabled\":false,\"required\":true,\"changeTag\":true},{\"cmpType\":\"common\",\"label\":\"设备编号\",\"tag\":\"el-input\",\"tagIcon\":\"input\",\"placeholder\":\"请输入设备编号\",\"span\":24,\"labelWidth\":0,\"style\":{\"width\":\"100%\"},\"clearable\":true,\"prepend\":\"\",\"append\":\"\",\"prefix-icon\":\"\",\"suffix-icon\":\"\",\"maxlength\":null,\"show-word-limit\":false,\"readonly\":false,\"disabled\":false,\"required\":true,\"regList\":[],\"changeTag\":true,\"proCondition\":false,\"asSummary\":false,\"formId\":1,\"renderKey\":1604398033391,\"layout\":\"colFormItem\",\"vModel\":\"field1\"},{\"cmpType\":\"common\",\"label\":\"内容\",\"tag\":\"el-input\",\"tagIcon\":\"textarea\",\"type\":\"textarea\",\"placeholder\":\"设备缺陷、故障现象、改造内容、一保、二保等\",\"span\":24,\"labelWidth\":0,\"autosize\":{\"minRows\":4,\"maxRows\":4},\"style\":{\"width\":\"100%\"},\"maxlength\":null,\"show-word-limit\":false,\"readonly\":false,\"disabled\":false,\"required\":true,\"regList\":[],\"changeTag\":true,\"proCondition\":false,\"asSummary\":false,\"formId\":3,\"renderKey\":1604398059896,\"layout\":\"colFormItem\",\"vModel\":\"field3\"}]},\"advancedSetting\":{\"autoRepeat\":false,\"myAuditAutoPass\":false,\"remarkTip\":\"\",\"remarkRequired\":false,\"notVisibleForSponsor\":false}}', null, '0', '0', '0', null, '0', '1604398298878', '1', '超级管理员', '1', '南斗信息', '1608105784752', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for fdp_flow_procinst
-- ----------------------------
DROP TABLE IF EXISTS `fdp_flow_procinst`;
CREATE TABLE `fdp_flow_procinst` (
  `id` varchar(64) NOT NULL,
  `procdef_id` varchar(64) DEFAULT NULL,
  `procdef_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT '' COMMENT '标题',
  `node_id` varchar(255) DEFAULT '' COMMENT '流程节点ID',
  `task_id` varchar(64) DEFAULT '' COMMENT '任务ID',
  `start_time` bigint(255) DEFAULT NULL COMMENT '开始时间',
  `end_time` bigint(255) DEFAULT NULL COMMENT '结束时间',
  `duration` bigint(20) DEFAULT NULL COMMENT '持续时间',
  `flow_val` text COMMENT '流程变量',
  `node_list` text COMMENT '节点列表',
  `step` int(11) DEFAULT '0' COMMENT '正在执行第几个节点',
  `finished` tinyint(1) DEFAULT '0' COMMENT '是否结束',
  `create_time` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `create_user` varchar(64) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人名称',
  `create_dept` varchar(64) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门编码',
  `create_dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建部门名称',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `group_id` varchar(64) DEFAULT '' COMMENT '流程分组ID',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `procdef_config` text,
  `node_name` varchar(1000) DEFAULT '' COMMENT '流程节点',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of fdp_flow_procinst
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_flow_task
-- ----------------------------
DROP TABLE IF EXISTS `fdp_flow_task`;
CREATE TABLE `fdp_flow_task` (
  `id` varchar(64) NOT NULL,
  `node_type` varchar(255) DEFAULT NULL,
  `node_id` varchar(255) DEFAULT NULL,
  `node_name` varchar(255) DEFAULT '' COMMENT '节点名称',
  `node_code` varchar(255) DEFAULT '' COMMENT '节点编码',
  `procinst_id` varchar(64) DEFAULT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `create_time` varchar(255) DEFAULT NULL,
  `claim_time` varchar(255) DEFAULT NULL,
  `member_count` tinyint(4) DEFAULT '1',
  `un_complete_num` tinyint(4) DEFAULT '1',
  `agree_num` tinyint(4) DEFAULT NULL,
  `counter_sign` tinyint(1) DEFAULT '1' COMMENT '是否会签',
  `finished` tinyint(1) DEFAULT '0',
  `state` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_id` (`id`) USING BTREE,
  KEY `task_proc_inst_id_procinst_id_foreign` (`procinst_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of fdp_flow_task
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_flow_undo
-- ----------------------------
DROP TABLE IF EXISTS `fdp_flow_undo`;
CREATE TABLE `fdp_flow_undo` (
  `id` varchar(64) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `dept_id` varchar(64) DEFAULT NULL,
  `dept_name` varchar(255) DEFAULT NULL,
  `procinst_id` varchar(64) DEFAULT NULL,
  `task_id` varchar(64) DEFAULT NULL,
  `result` tinyint(1) DEFAULT '0' COMMENT '处理结果',
  `finished` tinyint(1) DEFAULT '0',
  `create_time` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `assertion_time` bigint(20) DEFAULT '0' COMMENT '断言时间',
  `suggest` varchar(300) DEFAULT '' COMMENT '意见',
  `assignor_undo_id` varchar(64) DEFAULT '' COMMENT '委托执行人ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of fdp_flow_undo
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_org_account
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_account`;
CREATE TABLE `fdp_org_account` (
  `id` varchar(64) NOT NULL,
  `account` varchar(100) DEFAULT '' COMMENT '账号',
  `password` varchar(64) DEFAULT '' COMMENT '密码',
  `salt` varchar(100) DEFAULT '' COMMENT 'token加密盐',
  `user_id` varchar(64) DEFAULT '' COMMENT '关联用户ID',
  `locked` tinyint(1) DEFAULT NULL COMMENT '锁定状态',
  `login_time` bigint(20) DEFAULT NULL COMMENT '登录时间',
  `login_number` bigint(20) DEFAULT NULL COMMENT '登录数量',
  `online` tinyint(1) DEFAULT NULL COMMENT '在线状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of fdp_org_account
-- ----------------------------
INSERT INTO `fdp_org_account` VALUES ('1', 'superAdmin', 'bd56f88c0c2dd0da7f5b5cc7251abd9ef635a086b21ebb62edbe480a61897efc', '666666', '1', '0', null, null, null, '1611216074456', '1', '超级管理员', '1', '南斗信息', '1611216074456', '1', '超级管理员', '1', '南斗信息', '1');

-- ----------------------------
-- Table structure for fdp_org_dept
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_dept`;
CREATE TABLE `fdp_org_dept` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `simple_name` varchar(50) DEFAULT NULL COMMENT '简称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `parent_id` varchar(64) DEFAULT '' COMMENT '上级ID',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(100) DEFAULT '' COMMENT '邮箱',
  `leader` varchar(100) DEFAULT NULL COMMENT '负责人编码',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='组织机构';

-- ----------------------------
-- Records of fdp_org_dept
-- ----------------------------
INSERT INTO `fdp_org_dept` VALUES ('1', '陕西南斗信息科技有限公司', '南斗信息', 'NDXX', '1', '', '西安市软件新城', '15191969253', '593148107@qq.com', '1', '0', '0', '', '0', '1596696322438', '1', '南斗信息', '1', '陕西南斗信息科技有限公司', '1619324883389', '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '1');

-- ----------------------------
-- Table structure for fdp_org_position
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_position`;
CREATE TABLE `fdp_org_position` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='职务';

-- ----------------------------
-- Records of fdp_org_position
-- ----------------------------
INSERT INTO `fdp_org_position` VALUES ('1233506a3f3c7935806ce4a721c81f0a', '经理助理', 'JLZL', '0', '13', '', '0', '1598437730630', '1', '超级管理员', '1', '南斗信息', '1607304843169', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('16f8354a55295ca7d981d7c89e135fa1', '总经理助理兼主任', 'ZJLZLJZR', '0', '18', '', '0', '1598437960180', '1', '超级管理员', '1', '南斗信息', '1607304876744', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('17306893d3221bc5c1de7f5d2e0f4b5c', '副经理', 'FJL', '0', '10', '', '0', '1598437628371', '1', '超级管理员', '1', '南斗信息', '1607304822511', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('28f0f34fa2af89489ca112f123d06f66', '市场管理员', 'SCGLY', '0', '1', '', '0', '1599713566657', '1', '超级管理员', '1', '南斗信息', '1599713566657', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('2ac6422bf403c2cf0a8aaa4919a26190', '书记', 'SJ', '0', '7', '', '0', '1598437548264', '1', '超级管理员', '1', '南斗信息', '1607304800324', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('2c823cd25feff112136ac35c59811d9d', '待选择', 'DXZ', '0', '1', '', '0', '1597911959537', '1', '超级管理员', '1', '南斗信息', '1607304760872', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('2f52b4b61fd09f57bc6c8761d37e6932', '文秘', 'WM', '0', '17', '', '0', '1598437941538', '1', '超级管理员', '1', '南斗信息', '1607304867226', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('3d96d1a092503d32994efc82b5d9766a', '信息员', 'XXY', '0', '20', '', '0', '1598437997732', '1', '超级管理员', '1', '南斗信息', '1607304891260', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('6e93cd208f3f119ef865df042f9e977f', '统计员', 'TJY', '0', '19', '', '0', '1598437979883', '1', '超级管理员', '1', '南斗信息', '1607304884125', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('7a742e8d08a37f33cdb0dc285c298395', '保卫干事', 'BWGS', '0', '22', '', '0', '1598438073606', '1', '超级管理员', '1', '南斗信息', '1607304903822', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('80738fd751180e8f42d67192b96d68d3', '系统管理员', 'SYS_ADMIN', '0', '0', '', '0', '1597823771148', '1', '超级管理员', '1', '南斗信息', '1597823771148', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('85d303eb7d140eff093fad8470aafd3a', '营销员', 'YXY', '0', '12', '', '0', '1598437663639', '1', '超级管理员', '1', '南斗信息', '1607304833749', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('8ae6f7ba03b2e1a8bd7cfa41c7c406f3', '从', '', '0', '0', '', '-1', '1604448526398', '1', '超级管理员', '1', '南斗信息', '1604448531925', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('8d0f6f293f98b63affa6e01cdeb1544a', '巡查员', 'XCY', '0', '2', '', '0', '1597911995344', '1', '超级管理员', '1', '南斗信息', '1607304767655', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('902183bc141ac46e997f38bd3edb91c4', '总经理', 'ZJL', '0', '6', '', '0', '1598437523265', '1', '超级管理员', '1', '南斗信息', '1607304794233', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('9eb463723a15b33aa2c83a1b60f4db67', '员工', 'YG', '0', '14', '', '0', '1598437756558', '1', '超级管理员', '1', '南斗信息', '1607304848710', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('9fe43466504a3b0089a3d8cad7f3d133', '部门经理', 'BMJL', '0', '3', '', '0', '1598435607284', '1', '超级管理员', '1', '南斗信息', '1607304774566', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('a13271eeba2ff7a843763e551a3406ff', '副主任', 'FZR', '0', '21', '', '0', '1598438045198', '1', '超级管理员', '1', '南斗信息', '1607304896974', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('a475ce99009ee6d75f6cf5b4792169e2', '收费员', 'SFY', '0', '33', '', '0', '1607304970217', '1', '超级管理员', '1', '南斗信息', '1607304970217', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('bc410d254f63d807e8b22100e84d5338', '副总经理', 'FZJL', '0', '8', '', '0', '1598437567520', '1', '超级管理员', '1', '南斗信息', '1607304806904', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('bfeb0b96993f891fd9f8cd8c5b1bc6ef', '测试部门', '', '0', '0', '', '-1', '1604448477655', '1', '超级管理员', '1', '南斗信息', '1604448483556', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('d63adad767a79d7e8fbcb69eeb3766d2', '采购员', 'CGY', '0', '15', '', '0', '1598437779013', '1', '超级管理员', '1', '南斗信息', '1607304855153', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('d944b075f235b5c334405c10825dd6d9', '部门经理助理', 'BMJLZL', '0', '4', '', '0', '1598437476143', '1', '超级管理员', '1', '南斗信息', '1607304782876', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('e54be46d808b9ba351fcd7437d3c7024', '安全员', 'AQY', '0', '5', '', '0', '1598437493400', '1', '超级管理员', '1', '南斗信息', '1607304788949', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('ef2b78c6d9bdf0b699a1211120ef34c8', '部门副经理', 'BMFJL', '0', '23', '', '0', '1598496061531', '1', '超级管理员', '1', '南斗信息', '1607304911405', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('efc98a55f1fc0480ca2115bc3650c470', '司机', 'SJ', '0', '11', '', '0', '1598437646008', '1', '超级管理员', '1', '南斗信息', '1607304827536', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('f3cb0d4fc488da4e4127bbc42e33d4ae', '市场营销', 'SCYX', '0', '16', '', '0', '1598437866683', '1', '超级管理员', '1', '南斗信息', '1607304862064', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_org_position` VALUES ('fe220cbd96ad9dec01be3eaf58735796', '经理', 'JL', '0', '9', '', '0', '1598437607075', '1', '超级管理员', '1', '南斗信息', '1607304813550', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for fdp_org_role
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_role`;
CREATE TABLE `fdp_org_role` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `data_scope` tinyint(255) DEFAULT NULL COMMENT '数据权限',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色';

-- ----------------------------
-- Records of fdp_org_role
-- ----------------------------
INSERT INTO `fdp_org_role` VALUES ('80738fd751180e8f42d67192b96d68d3', '系统管理员', 'SYS_ADMIN', null, '0', '0', '', '0', '1597823771148', '1', '超级管理员', '1', '南斗信息', '1597823771148', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for fdp_org_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_role_data_scope`;
CREATE TABLE `fdp_org_role_data_scope` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `role_id` varchar(64) DEFAULT '' COMMENT '角色编码',
  `data_scope_id` varchar(100) DEFAULT '' COMMENT '数据权限ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色菜单';

-- ----------------------------
-- Records of fdp_org_role_data_scope
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_org_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_role_resource`;
CREATE TABLE `fdp_org_role_resource` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `role_id` varchar(64) DEFAULT '' COMMENT '角色编码',
  `resource_code` varchar(100) DEFAULT '' COMMENT '权限编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色菜单';

-- ----------------------------
-- Records of fdp_org_role_resource
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_org_user
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_user`;
CREATE TABLE `fdp_org_user` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT '工号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `simple_name` varchar(50) DEFAULT '' COMMENT '简称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `employee_type` varchar(10) DEFAULT '' COMMENT '员工类型',
  `employee_position` varchar(200) DEFAULT '' COMMENT '用户职位',
  `dept` varchar(100) DEFAULT NULL COMMENT '部门编码',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `landline` varchar(20) DEFAULT NULL COMMENT '固定电话',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `birthday` varchar(100) DEFAULT '' COMMENT '出生年月日',
  `idcard` varchar(20) DEFAULT NULL COMMENT '身份证号码',
  `nation` varchar(100) DEFAULT NULL COMMENT '民族字典编码',
  `nativeplace` varchar(500) DEFAULT NULL COMMENT '籍贯',
  `degree` varchar(100) DEFAULT NULL COMMENT '文化程度字典编码',
  `married` tinyint(1) DEFAULT NULL COMMENT '婚姻状态',
  `contact` varchar(50) DEFAULT NULL COMMENT '紧急联系人',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '紧急联系人电话',
  `entry_time` bigint(20) DEFAULT NULL COMMENT '入职时间',
  `leave_time` bigint(20) DEFAULT NULL COMMENT '离职时间',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户';

-- ----------------------------
-- Records of fdp_org_user
-- ----------------------------
INSERT INTO `fdp_org_user` VALUES ('1', '超级管理员', '超级管理员', 'superAdmin', '1', '1', '', null, '1', '无', '无', '无', '无', '', null, null, null, null, null, null, null, null, null, '0', '0', '超级管理员', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1');

-- ----------------------------
-- Table structure for fdp_org_user_mang_area
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_user_mang_area`;
CREATE TABLE `fdp_org_user_mang_area` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `user_id` varchar(64) DEFAULT '' COMMENT '用户ID',
  `area_id` varchar(100) DEFAULT '' COMMENT '区域ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色菜单';

-- ----------------------------
-- Records of fdp_org_user_mang_area
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_org_user_mang_dept
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_user_mang_dept`;
CREATE TABLE `fdp_org_user_mang_dept` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `user_id` varchar(64) DEFAULT '' COMMENT '用户ID',
  `dept_id` varchar(100) DEFAULT '' COMMENT '部门ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色菜单';

-- ----------------------------
-- Records of fdp_org_user_mang_dept
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_org_user_role
-- ----------------------------
DROP TABLE IF EXISTS `fdp_org_user_role`;
CREATE TABLE `fdp_org_user_role` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `role_id` varchar(64) DEFAULT '' COMMENT '角色编码',
  `user_id` varchar(100) DEFAULT '' COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户角色';

-- ----------------------------
-- Records of fdp_org_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_sys_common_fun
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_common_fun`;
CREATE TABLE `fdp_sys_common_fun` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `bus_id` varchar(64) NOT NULL DEFAULT '' COMMENT '业务ID',
  `user_id` varchar(64) DEFAULT NULL COMMENT '用户ID',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `times` int(11) DEFAULT '0' COMMENT '使用次数',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型';

-- ----------------------------
-- Records of fdp_sys_common_fun
-- ----------------------------
INSERT INTO `fdp_sys_common_fun` VALUES ('100766338e03ef993bcc6959f9331338', 'DEPT_MANG', '1', '0', '6', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('4bc9fd1f16a73c4ccb71b68abaff9c9b', 'WORKFLOW_PROCDEF', '1', '0', '3', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('73eecaa1bcc9eb19b3e6916fc2086998', 'NOTICE_VIEW', '1', '0', '2', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('74bfcc3678ee49d0a3d0e0b1fe7a0fe2', 'USER_MANG', '1', '0', '3', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('74cb1aac132ba45c5d349d96e54aa557', 'API_DOC', '1', '0', '0', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('89003750c901147c3eb20d95f910d5af', 'MY_RECEIVED', '1', '0', '0', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('948017fc0d7d32ee6fe982473c692f9f', 'OBJCODE_CREATE_MANG', '1', '0', '2', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('a02fa09135c613efc225e6bc188ca4a4', 'ROLE_MANG', '1', '0', '1', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('a16c94f821175be7992687cb4603f77e', 'MY_SEND', '1', '0', '2', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('ad1af4eac768d6eaff1588e134251006', 'ALREADY_APPROVE', '1', '0', '1', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('c95c27850a199325fd838aec7f8bb23c', 'NOTICE_MANG', '1', '0', '2', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('e7c408eb5799311cce250084c75f951e', 'APPROVE_FORM', '1', '0', '9', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('ea7e1371efaa066f989d0e2b55082029', 'DATA_DICT', '1', '0', '2', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('f5b69cf53fb1e7c053e54e7e3b0c1c06', 'WORKFLOW_PROCMANG', '1', '0', '2', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('f6544c5d1906372dd7818b8091f4f373', 'ad35420530f01c87f32e849529609418', '1', '1', '7', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('fac051c0ab5ac90a31c072c9ac9e4161', 'POSITION_MANG', '1', '0', '1', '0', '0', null, '0', '0');
INSERT INTO `fdp_sys_common_fun` VALUES ('fe53b833eaaf7bee50017e64eb710862', 'RESOURCE_MANG', '1', '0', '2', '0', '0', null, '0', '0');

-- ----------------------------
-- Table structure for fdp_sys_common_fun_copy
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_common_fun_copy`;
CREATE TABLE `fdp_sys_common_fun_copy` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `bus_id` varchar(64) NOT NULL DEFAULT '' COMMENT '业务ID',
  `user_id` varchar(64) DEFAULT NULL COMMENT '用户ID',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `times` int(11) DEFAULT '0' COMMENT '使用次数',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型';

-- ----------------------------
-- Records of fdp_sys_common_fun_copy
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_dict`;
CREATE TABLE `fdp_sys_dict` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `parent_code` varchar(100) DEFAULT '' COMMENT '上级编码',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型';

-- ----------------------------
-- Records of fdp_sys_dict
-- ----------------------------
INSERT INTO `fdp_sys_dict` VALUES ('1', '数据状态', 'STATE', '1', 'FDP_CORE', '0', '1', '数据状态', '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597764688830', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('2', '字典类型', 'DICT_TYPE', '1', 'FDP_CORE', '0', '2', '', '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597764693832', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('28d6429f2d3c585ac11abf99c26ac8ed', '公告接收人类型', 'NOTICE_RECEIVER_TYPE', '1', 'FDP_CORE', '0', '13', '', '0', '1610327463188', '1', '超级管理员', '1', '南斗信息', '1610327463188', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('3', '平台核心', 'FDP_CORE', '1', '', '0', '0', null, '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('3321596a1350214e44213449573c2b45', '流程状态', 'FLOW_STATE', '1', 'FDP_CORE', '0', '11', '', '0', '1602983321815', '1', '超级管理员', '1', '南斗信息', '1602983321815', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('356dc1af52be33880d20db277ada5d16', '平台信息', 'PLATFORM_INFO', '1', 'FDP_CORE', '0', '0', '', '0', '1616809135966', '1', '超级管理员', '1', '南斗信息', '1616809135966', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('3e2536bdce7818fd007d89f1be197e9f', '资源类型', 'RESOURCE_TYPE', '1', 'FDP_CORE', '0', '3', '', '0', '1597764682002', '1', '超级管理员', '1', '南斗信息', '1597764890292', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('43b50f9cce280bc1f49591df81c11d75', '流程实例办结状态', 'FLOW_PROCINST_FINISHED_STATE', '1', 'FLOW_STATE', '0', '2', '', '0', '1602983581024', '1', '超级管理员', '1', '南斗信息', '1602983678327', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('5101699df8212963410229aeb8e55abd', '数据权限', 'DATA_SCOPE', '1', 'FDP_CORE', '0', '12', '', '0', '1603280807302', '1', '超级管理员', '1', '南斗信息', '1603280807302', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('5414b511d13e07e46e145fc3e6b55e76', '生成状态', 'OBJ_CODE_CREATE_STATE', '1', 'OBJ_CODE_STATE', '0', '1', '', '0', '1616490567486', '1', '超级管理员', '1', '南斗信息', '1616500673895', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('8524987991c2c403217f9ca57a0f449b', '公告状态', 'NOTICE_STATE', '1', 'FDP_CORE', '0', '12', '', '0', '1610327285514', '1', '超级管理员', '1', '南斗信息', '1610327285514', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('874c2b7d2c33bfc99b55b08995fa3570', '意见状态', 'SUGGEST_STATE', '1', 'FDP_CORE', '0', '15', '', '0', '1619323540711', '1', '超级管理员', '1', '南斗信息', '1619323540711', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('89b3ca01eeeb0d96420016426e5f712f', '民族', 'NATION', '1', 'FDP_CORE', '0', '6', '', '0', '1597887594420', '1', '超级管理员', '1', '南斗信息', '1597887594420', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('a57dcece23e81e067b12e29c019922b4', '性别', 'SEX', '1', 'FDP_CORE', '0', '5', '', '0', '1597884808628', '1', '超级管理员', '1', '南斗信息', '1597884808628', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('a8c0b8f43b623aac0f024725081e6282', '设备状态', 'EQUIPMENT_STATE', '1', 'FDP_CORE', '0', '16', '', '0', '1619323601316', '1', '超级管理员', '1', '南斗信息', '1619323601316', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('afaee680cdb8a8638b03285af93a0232', '类型', 'OBJ_CODE_TYPE', '1', 'OBJ_CODE_STATE', '0', '3', '', '0', '1616669872975', '1', '超级管理员', '1', '南斗信息', '1616669872975', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('aff4f78ca89d7e83ae476f7eccd4648a', '流程待办办结状态', 'FLOW_UNDO_FINISHED_STATE', '1', 'FLOW_STATE', '0', '6', '', '0', '1602983814622', '1', '超级管理员', '1', '南斗信息', '1602983814622', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('b7d44cc9781c7730f33b09fb784ef301', '婚姻状态', 'MARRIE_STATE', '1', 'FDP_CORE', '0', '8', '', '0', '1597887659873', '1', '超级管理员', '1', '南斗信息', '1597887659873', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('c005a7cd1ad15cb1e674e56662bb7690', '流程任务状态', 'FLOW_TASK_STATE', '1', 'FLOW_STATE', '0', '3', '', '0', '1602983462979', '1', '超级管理员', '1', '南斗信息', '1602983587801', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('cb4a98c424ca63dfca497176a53132e5', '流程待办状态', 'FLOW_UNDO_STATE', '1', 'FLOW_STATE', '0', '5', '', '0', '1602983723599', '1', '超级管理员', '1', '南斗信息', '1602983723599', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('cb4e1a114d7f961aab348997bd829833', '明细状态', 'OBJ_CODE_DETAIL_STATE', '1', 'OBJ_CODE_STATE', '0', '2', '', '0', '1616499305443', '1', '超级管理员', '1', '南斗信息', '1616499305443', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('cf993dc8739fcf9c913bd7cf7d7b91b7', '配置', 'OBJ_CODE_CONFIG', '1', 'OBJ_CODE_STATE', '0', '3', '', '0', '1616500059129', '1', '超级管理员', '1', '南斗信息', '1616500059129', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('cfb6e163013b2539fe0ade4cec2f5034', '流程实例状态', 'FLOW_PROCINST_STATE', '1', 'FLOW_STATE', '0', '0', '', '0', '1602983357604', '1', '超级管理员', '1', '南斗信息', '1602983357604', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('d9405b95a2d1235cefbbc80995b6f37e', '组织类型', 'ORG_TYPE', '1', 'FDP_CORE', '0', '4', '', '0', '1597846390599', '1', '超级管理员', '1', '南斗信息', '1597846390599', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('e159957932121b18297992a37e1ad3a6', '流程任务办结状态', 'FLOW_TASK_FINISHED_STATE', '1', 'FLOW_STATE', '0', '4', '', '0', '1602983666183', '1', '超级管理员', '1', '南斗信息', '1602983666183', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('e81ccdd5d656c0aefc265ad8339bfbd7', '文化程度', 'DEGREE', '1', 'FDP_CORE', '0', '7', '', '0', '1597887616065', '1', '超级管理员', '1', '南斗信息', '1597887616065', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_dict` VALUES ('f113c2c30ba54ef25de08f5e39d23ebd', '员工类型', 'EMPLOYEE_TYPE', '1', 'FDP_CORE', '0', '0', '', '0', '1597910202757', '1', '超级管理员', '1', '南斗信息', '1597910202757', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('f8a01cb29520d8ccdf24f357278a0829', '公告置顶状态', 'NOTICE_TOP_STATE', '1', 'FDP_CORE', '0', '14', '', '0', '1610328734895', '1', '超级管理员', '1', '南斗信息', '1610328734895', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('fd8397bec2365c2e7bf275959ccbce48', '一物一码状态', 'OBJ_CODE_STATE', '1', 'FDP_CORE', '0', '16', '', '0', '1616499217594', '1', '超级管理员', '1', '南斗信息', '1616499217594', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict` VALUES ('ffb57ac98f13866b1bdf2d47ac5174cb', '是否', 'YES_NO', '1', 'FDP_CORE', '0', '0', '', '0', '1599998297954', '1', '超级管理员', '1', '南斗信息', '1599998297954', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for fdp_sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_dict_item`;
CREATE TABLE `fdp_sys_dict_item` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `dict_code` varchar(100) DEFAULT '' COMMENT '字典分类编码',
  `name` varchar(3000) DEFAULT '' COMMENT '名称',
  `value` varchar(100) DEFAULT '' COMMENT '编码',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典项';

-- ----------------------------
-- Records of fdp_sys_dict_item
-- ----------------------------
INSERT INTO `fdp_sys_dict_item` VALUES ('002cec6bde9d44df1dcde0c58d7e9d3a', 'DEGREE', '研究生肄业', '09', '0', '6', '', '0', '1597891431235', '1', '超级管理员', '1', '南斗信息', '1597891431235', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('014bf1e3f2afc12c89a77e9013130bad', 'NATION', '高山族', '23', '0', '23', '', '0', '1597888923019', '1', '超级管理员', '1', '南斗信息', '1597890706669', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('02abfb16c51301598acbc9d36e6d5208', 'SUGGEST_STATE', '已处理', '1', '0', '1', '', '0', '1600268926668', '1', '超级管理员', '1', '南斗信息', '1600268926668', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('0417b90c05899d210ea8fdef75320e63', 'DEGREE', '专科', '20', '0', '11', '', '0', '1597891530106', '1', '超级管理员', '1', '南斗信息', '1597891530106', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('05bf9e787b5961b184a2e15f413d734c', 'NOTICE_RECEIVER_TYPE', '全部', '0', '0', '0', '', '0', '1610327475419', '1', '超级管理员', '1', '南斗信息', '1610327475419', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('05db9c3ad6e797d53df3fc5a710fc961', 'DEGREE', '大学肄业', '19', '0', '10', '', '0', '1597891515169', '1', '超级管理员', '1', '南斗信息', '1597891515169', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('0799616078f2da1ce4f9a6bfa07b06fc', 'NATION', '保安族', '47', '0', '47', '', '0', '1597889570277', '1', '超级管理员', '1', '南斗信息', '1597890550531', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('0e68319c99ffe4da3f1c6b3f62ca7b19', 'OBJ_CODE_CONFIG', 'SALT', 'A610784178EB5B60694DB888B99B9316', '0', '5', 'nd20210327', '0', '1616810175910', '1', '超级管理员', '1', '南斗信息', '1616810175910', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('0e9d99b7f6dcc98f8935291da92b2429', 'RESOURCE_TYPE', '菜单', '2', '0', '2', '', '0', '1597764735154', '1', '超级管理员', '1', '南斗信息', '1609927117510', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('0ec5e7da74402144d73661d3e7dffd37', 'FLOW_UNDO_STATE', '同意', '2', '0', '2', '', '0', '1602983764083', '1', '超级管理员', '1', '南斗信息', '1602983764083', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('0f3665988fc740a301cbf2b50a7ac8a0', 'FLOW_TASK_STATE', '通过', '2', '0', '2', '', '0', '1602983519051', '1', '超级管理员', '1', '南斗信息', '1602983533813', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1', 'STATE', '启用', '0', '0', '0', '', '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597763999325', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('12871f87df831f06240888bd760e8adf', 'EQUIPMENT_STATE', '启用', '1', '0', '1', '', '0', '1600243754824', '1', '超级管理员', '1', '南斗信息', '1619323380380', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1294d1f802c691665d7c30148eaa4058', 'FLOW_TASK_STATE', '驳回', '-1', '0', '4', '', '0', '1602983544447', '1', '超级管理员', '1', '南斗信息', '1602983544447', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('149f6563bedeebd79cbb6ac1b5656d11', 'NATION', '独龙族', '51', '0', '51', '', '0', '1597889663686', '1', '超级管理员', '1', '南斗信息', '1597890523568', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1835d71b88dc1cb491ffb626d89d2a46', 'DEGREE', '大学毕业', '11', '0', '8', '', '0', '1597891470997', '1', '超级管理员', '1', '南斗信息', '1597891470997', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1885edd485309e68b3ec7ade5600ef20', 'OBJ_CODE_CREATE_STATE', '正在生成', '1', '0', '1', '', '0', '1616490582545', '1', '超级管理员', '1', '南斗信息', '1616490582545', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1b0c2fcf7452e02c12ac4e5e49d9f7b8', 'NATION', '侗族', '12', '0', '12', '', '0', '1597888453528', '1', '超级管理员', '1', '南斗信息', '1597890793365', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1ee3d7876440c2309fa731fec22c30a5', 'CONTRACT_BOOK_PAY_STATE', '无账单', '0', '0', '0', '', '0', '1605346616164', '1', '超级管理员', '1', '南斗信息', '1605612487863', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1eeaa241385d8a1f7e59af9bb713168c', 'NATION', '仡佬族', '37', '0', '37', '', '0', '1597889283089', '1', '超级管理员', '1', '南斗信息', '1597890624828', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1efb66ec0ae10fed8438835da29945f0', 'DEGREE', '待选择', '81', '0', '0', '', '0', '1598491993014', '1', '超级管理员', '1', '南斗信息', '1598492016895', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('1fa4176174e5f63bd1f9131f4e1734b4', 'OBJ_CODE_CREATE_STATE', '已生成', '2', '0', '2', '', '0', '1616490600426', '1', '超级管理员', '1', '南斗信息', '1616490600426', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('2', 'STATE', '停用', '1', '0', '0', '', '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597673951074', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('22605903e5fba29be5c0b473820bface', 'NOTICE_STATE', '已录入', '0', '0', '0', '', '0', '1610327298970', '1', '超级管理员', '1', '南斗信息', '1610327298970', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('232a00cc42ae46120f944767d9ed29b4', 'NATION', '佤族', '21', '0', '21', '', '0', '1597888823519', '1', '超级管理员', '1', '南斗信息', '1597890729113', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('25a7f18508afadf5021f3ace0c86aac4', 'SUGGEST_STATE', '已登记', '0', '0', '0', '', '0', '1600268912407', '1', '超级管理员', '1', '南斗信息', '1600268912407', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('26bfa53352d8bb95f8b3238aa1dc892a', 'OBJ_CODE_CONFIG', 'DESC_BACK_COLOR', '#009688', '0', '3', '', '0', '1616741568892', '1', '超级管理员', '1', '南斗信息', '1616741568892', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('26ee833e73f42c6c462658a356a1d488', 'NATION', '纳西族', '27', '0', '27', '', '0', '1597889010846', '1', '超级管理员', '1', '南斗信息', '1597890682590', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('28191bbc633f3e118bbaa30e2e29eede', 'NATION', '鄂伦春族', '52', '0', '52', '', '0', '1597889694368', '1', '超级管理员', '1', '南斗信息', '1597890516002', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('289206ee16a4e5a1020c2f2ec5df65ea', 'NATION', '黎族', '19', '0', '19', '', '0', '1597888772787', '1', '超级管理员', '1', '南斗信息', '1597890737648', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('298d0b6cb0c2e184b1b646b20c2c3b14', 'DEGREE', '中专中技', '30', '0', '16', '', '0', '1597891663770', '1', '超级管理员', '1', '南斗信息', '1597891663770', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('2ff13e5c4a5a4d853b27c0b59e4e4880', 'NATION', '普米族', '40', '0', '40', '', '0', '1597889374519', '1', '超级管理员', '1', '南斗信息', '1597890606843', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('3', 'DICT_TYPE', '列表', '1', '0', '0', '', '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597673951074', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('303dc34ea89c9123851f2f7e37d64c79', 'DEGREE', '技工学校肄业', '49', '0', '23', '', '0', '1597891857085', '1', '超级管理员', '1', '南斗信息', '1597891857085', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('317814de27b7931db0f69fc7d360de0b', 'NATION', '怒族', '42', '0', '42', '', '0', '1597889433949', '1', '超级管理员', '1', '南斗信息', '1597890593196', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('33915fa3c249a003d65db96acfd8c171', 'OBJ_CODE_DETAIL_STATE', '已使用', '2', '0', '2', '', '0', '1616499373881', '1', '超级管理员', '1', '南斗信息', '1616500685898', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('35b9fe8350bb0ff0b30bf3db409648f4', 'NATION', '仫佬族', '32', '0', '32', '', '0', '1597889159664', '1', '超级管理员', '1', '南斗信息', '1597890655021', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('367b05b5c0b0357e5f417d57d1f76db8', 'NATION', '布朗族', '34', '0', '34', '', '0', '1597889195789', '1', '超级管理员', '1', '南斗信息', '1597890644003', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('3c2f821daefe62dc7cba3a355310e251', 'DATA_SCOPE', '管理部门', '100', '0', '3', '', '0', '1604406742678', '1', '超级管理员', '1', '南斗信息', '1604406757751', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('4', 'DICT_TYPE', '树形', '2', '0', '0', '', '0', '1597673951074', '1', '超级管理员', '1', '南斗信息', '1597673951074', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('41efe831c529d61510b5af880436e9b1', 'MARRIE_STATE', '已婚', '3', '0', '3', '', '0', '1597887688573', '1', '超级管理员', '1', '南斗信息', '1598491730897', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('42551d2b4bbdcf056cd3a0ab8037c4b5', 'NATION', '东乡族', '26', '0', '26', '', '0', '1597888988161', '1', '超级管理员', '1', '南斗信息', '1597890688244', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('43c80a9c3da834a577d298ecd111d118', 'YES_NO', '是', '1', '0', '0', '', '0', '1599998311292', '1', '超级管理员', '1', '南斗信息', '1599998337745', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('43fae179ebfb60d08ad6be7ad4c6eac8', 'FLOW_TASK_FINISHED_STATE', '结束', '1', '0', '1', '', '0', '1602983698645', '1', '超级管理员', '1', '南斗信息', '1602983698645', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('47d6499f8220d2dcca3641109c4e0a5f', 'NATION', '乌孜别克族', '43', '0', '43', '', '0', '1597889464605', '1', '超级管理员', '1', '南斗信息', '1597890585034', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('49e49acb4724bfebc49d3a5618922ee7', 'DEGREE', '大学本科', '10', '0', '7', '', '0', '1597891450540', '1', '超级管理员', '1', '南斗信息', '1597891450540', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('4ae5a87263783ac94bf8a1ec3ea8066e', 'DATA_SCOPE', '仅本人数据权限', '1', '0', '1', '', '0', '1603280821179', '1', '超级管理员', '1', '南斗信息', '1603280821179', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('5282ebf3e237cdbc495a6a715bd95b6d', 'RESOURCE_TYPE', '动作', '3', '0', '3', '', '0', '1597764743838', '1', '超级管理员', '1', '南斗信息', '1609927112321', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('550ffc6ca52226de08971c42c3ba38a3', 'DEGREE', '硕士', '04', '0', '5', '', '0', '1597891403073', '1', '超级管理员', '1', '南斗信息', '1597891403073', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('579a06bc5e1e28d9f9fe20a4334c6d46', 'FLOW_TASK_FINISHED_STATE', '处理中', '0', '0', '0', '', '0', '1602983690130', '1', '超级管理员', '1', '南斗信息', '1602983690130', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('59364ee96b241ddff120448f6e8fdeba', 'SEX', '男', '1', '0', '1', '', '0', '1597884841541', '1', '超级管理员', '1', '南斗信息', '1597884857480', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('594552e2b521b3dcba0933b322de57fc', 'EMPLOYEE_TYPE', '全职', '1', '0', '0', '', '0', '1597910226020', '1', '超级管理员', '1', '南斗信息', '1597910226020', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('595c8d5d307d819159c42ef1ee8126f8', 'NATION', '土家族', '15', '0', '15', '', '0', '1597888521722', '1', '超级管理员', '1', '南斗信息', '1597890774377', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('59fdf41dad970e63f948e160d987a915', 'NATION', '朝鲜族', '10', '0', '10', '', '0', '1597888380099', '1', '超级管理员', '1', '南斗信息', '1597890097948', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('5a679affa18f34af070481cd8ecd68ed', 'NATION', '外国血统', '98', '0', '98', '', '0', '1597891041542', '1', '超级管理员', '1', '南斗信息', '1597891079478', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('5a8afba9328de1375d06589d2d632c41', 'FLOW_UNDO_FINISHED_STATE', '已处理', '1', '0', '1', '', '0', '1602983833284', '1', '超级管理员', '1', '南斗信息', '1602983833284', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('5b320c3d8fbd1d80f1b5ab58232b9533', 'NATION', '塔塔尔族', '50', '0', '50', '', '0', '1597889643533', '1', '超级管理员', '1', '南斗信息', '1597890531312', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('5d44a81d779634e29255924e9b7b3344', 'OBJ_CODE_CONFIG', 'DEFAULT_SIZE', '300', '0', '2', '默认宽度', '0', '1616501416878', '1', '超级管理员', '1', '南斗信息', '1616739515098', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('5f5c91237bbd56b7e5ffdbb1b5048f92', 'DEGREE', '文盲或半文盲', '80', '0', '40', '', '0', '1597892279561', '1', '超级管理员', '1', '南斗信息', '1597892279561', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('6096cada7a95dd8a2f02a9037f6efe97', 'NATION', '汉族', '01', '0', '1', '', '0', '1597888042334', '1', '超级管理员', '1', '南斗信息', '1597890408706', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('6333a10c20541c4df09861931455ea89', 'EMPLOYEE_TYPE', '实习', '3', '0', '2', '', '0', '1597910262036', '1', '超级管理员', '1', '南斗信息', '1597910262036', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('64785f504992800209fc11811a62a6ce', 'DEGREE', '相当大学毕业', '18', '0', '9', '', '0', '1597891496033', '1', '超级管理员', '1', '南斗信息', '1597891496033', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('651fe0b10f3cf5e7d08b307ec0f9128d', 'DATA_SCOPE', '管理区域', '1100', '0', '5', '', '0', '1604406903449', '1', '超级管理员', '1', '南斗信息', '1604406903449', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('673e337a9e319203f8bfadb118faac12', 'CONTRACT_BOOK_PAY_STATE', '已付款', '2', '0', '2', '', '0', '1605612504321', '1', '超级管理员', '1', '南斗信息', '1605612504321', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('676034d05a3253c1cc38108d0bef5317', 'EMPLOYEE_TYPE', '劳务外包', '6', '0', '5', '', '0', '1597910306465', '1', '超级管理员', '1', '南斗信息', '1597910306465', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('692d5b877a1eae62c95533d542c45589', 'NATION', '白族', '14', '0', '14', '', '0', '1597888498330', '1', '超级管理员', '1', '南斗信息', '1597890780592', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('6ce9625e9cd503af8d51a82bd643c914', 'DEGREE', '小学', '70', '0', '36', '', '0', '1597892194619', '1', '超级管理员', '1', '南斗信息', '1597892194619', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('6ed77909aaf2f99e441ec614a034fe3a', 'DEGREE', '相当高中毕业', '58', '0', '28', '', '0', '1597891994421', '1', '超级管理员', '1', '南斗信息', '1597891994421', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('70e50dcbf1ac8f7989d6f20946f03465', 'DEGREE', '小学肄业', '79', '0', '39', '', '0', '1597892256350', '1', '超级管理员', '1', '南斗信息', '1597892256350', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('72feb966bbdc7e0a9408683511d8365b', 'NATION', '塔吉克族', '41', '0', '41', '', '0', '1597889395412', '1', '超级管理员', '1', '南斗信息', '1597890600483', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('73709012da382ccd5f9cb2df227347c9', 'NATION', '满族', '11', '0', '11', '', '0', '1597888420998', '1', '超级管理员', '1', '南斗信息', '1597890799363', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('7386cfdc1aabf6412193eb7025db8aff', 'DEGREE', '小学毕业', '71', '0', '37', '', '0', '1597892212445', '1', '超级管理员', '1', '南斗信息', '1597892212445', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('73a1624fe3b2918dfde811c5655efad6', 'DEGREE', '中专中技肄业', '39', '0', '20', '', '0', '1597891770080', '1', '超级管理员', '1', '南斗信息', '1597891770080', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('74688d426ce9870d1b1ff000748c5073', 'NOTICE_TOP_STATE', '正常', '0', '0', '0', '', '0', '1610328746466', '1', '超级管理员', '1', '南斗信息', '1610328746466', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('75849f3e8d9ab03158e8eb10a025ab36', 'DEGREE', '相当中专中技', '38', '0', '19', '', '0', '1597891738879', '1', '超级管理员', '1', '南斗信息', '1597891738879', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('77853b7b695c334aba41a42ec023cc99', 'DATA_SCOPE', '本部门', '10', '0', '2', '', '0', '1603293314870', '1', '超级管理员', '1', '南斗信息', '1603293333246', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('77893efa37f9b1c0a33eaf37190af4fc', 'DEGREE', '初中毕业', '61', '0', '31', '', '0', '1597892050882', '1', '超级管理员', '1', '南斗信息', '1597892050882', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('7b073192b59765917d7620f56be9ee31', 'MARRIE_STATE', '未婚', '2', '0', '2', '', '0', '1597887700654', '1', '超级管理员', '1', '南斗信息', '1598491724231', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('7b6bac3a92f5f2a29e49cc8fce4b0268', 'OBJ_CODE_TYPE', '设备', '1', '0', '0', '/view/cmstimp/wechat/manger/equipment/scan_code_enter', '0', '1616669951674', '1', '超级管理员', '1', '南斗信息', '1617353796157', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('7f873fcfec2e6b74e448109ae4451130', 'DEGREE', '大专肄业', '29', '0', '15', '', '0', '1597891631971', '1', '超级管理员', '1', '南斗信息', '1597891631971', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('80fbfbcad0038a8690cf65eb1a711f45', 'EQUIPMENT_STATE', '异常', '3', '0', '3', '', '0', '1600243770487', '1', '超级管理员', '1', '南斗信息', '1619323380380', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8203efc12177bf091ef2ebf532d296b8', 'NATION', '景颇族', '28', '0', '28', '', '0', '1597889029130', '1', '超级管理员', '1', '南斗信息', '1597890676406', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('82424bf87a117746d79621048eb7c0b3', 'DEGREE', '职业初中毕业', '62', '0', '32', '', '0', '1597892082352', '1', '超级管理员', '1', '南斗信息', '1597892082352', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('82e5dae92f8359735be38fa35806a312', 'CONTRACT_BOOK_PAY_STATE', '待付款', '1', '0', '1', '', '0', '1605346637561', '1', '超级管理员', '1', '南斗信息', '1605612495769', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('839638b58fa496c231cd3db1d91ef8e6', 'OBJ_CODE_CONFIG', 'MAX_TOTAL', '200', '0', '1', '一次最多生成1000各二维码', '0', '1616500100863', '1', '超级管理员', '1', '南斗信息', '1616726864923', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('839e30740d8adcb6ff0cf932ea378253', 'DEGREE', '专科毕业', '22', '0', '13', '', '0', '1597891569859', '1', '超级管理员', '1', '南斗信息', '1597891569859', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('84adcb27ae7d6beea51a6fb257b58d30', 'NOTICE_RECEIVER_TYPE', '指定部门', '1', '0', '1', '', '0', '1610327492698', '1', '超级管理员', '1', '南斗信息', '1610327492698', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('84c6e663b63ddead3876d1d289f53835', 'ORG_TYPE', '部门', '2', '0', '0', '', '0', '1597847226818', '1', '超级管理员', '1', '南斗信息', '1597847226818', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('85332c65cd56f1bfc1117dad1af15dc4', 'FLOW_PROCINST_STATE', '撤回', '3', '0', '3', '', '0', '1602983418163', '1', '超级管理员', '1', '南斗信息', '1602983418163', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('88cd8f6809430fd506746f7c82b640e1', 'EMPLOYEE_TYPE', '退休返聘', '5', '0', '4', '', '0', '1597910294231', '1', '超级管理员', '1', '南斗信息', '1597910294231', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('89c57885471d90ba7b6b64fc2e8c8f0f', 'DEGREE', '大学专科', '21', '0', '12', '', '0', '1597891553270', '1', '超级管理员', '1', '南斗信息', '1597891553270', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8aa86a822d52fa0f95dcc6ed20b3b721', 'DEGREE', '博士后', '02', '0', '3', '', '0', '1597891339259', '1', '超级管理员', '1', '南斗信息', '1597891339259', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8b7d05bf580319f97b4a3f703aa4aaac', 'NATION', '哈萨克族', '17', '0', '17', '', '0', '1597888731386', '1', '超级管理员', '1', '南斗信息', '1597890749767', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8c4302fecfaf81f582e72f9e731ed332', 'EQUIPMENT_STATE', '停用', '2', '0', '2', '', '0', '1600243763274', '1', '超级管理员', '1', '南斗信息', '1619323380380', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8e4e3adbeee326ee9cd70024ebafb662', 'FLOW_UNDO_STATE', '拒绝', '3', '0', '3', '', '0', '1602983773643', '1', '超级管理员', '1', '南斗信息', '1602983773643', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8eb7f4d99bd0eb53a1a86f0723e04508', 'SEX', '女', '0', '0', '2', '', '0', '1597884853341', '1', '超级管理员', '1', '南斗信息', '1597884853341', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8f46eb135c297965aa46f6fbc9a3918a', 'NOTICE_STATE', '已发布', '1', '0', '1', '', '0', '1610327307081', '1', '超级管理员', '1', '南斗信息', '1610327307081', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('8fd435f45df656eb3da756abf5b4ed79', 'DEGREE', '高中肄业', '59', '0', '29', '', '0', '1597892011871', '1', '超级管理员', '1', '南斗信息', '1597892011871', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('90154ce2048c05efc4759b64ff3350e5', 'NATION', '锡伯族', '38', '0', '38', '', '0', '1597889301514', '1', '超级管理员', '1', '南斗信息', '1597890619510', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('908f566f659d63dfd41001fd953b8efa', 'NATION', '穿青人', '61', '0', '61', '', '0', '1597889897254', '1', '超级管理员', '1', '南斗信息', '1597890479967', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9350c607330ed99b99eb6a241e0e1626', 'NATION', '羌族', '33', '0', '33', '', '0', '1597889178255', '1', '超级管理员', '1', '南斗信息', '1597890649115', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('93cf1c66d6ae03acfa827fa0914b3910', 'NATION', '撒拉族', '35', '0', '35', '', '0', '1597889217647', '1', '超级管理员', '1', '南斗信息', '1597890636411', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('98d9d2f1f0fadaf980e7ba48ff50d12a', 'NATION', '哈尼族', '16', '0', '16', '', '0', '1597888710329', '1', '超级管理员', '1', '南斗信息', '1597890754618', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9b5f322a05e12033401c78785d414f2b', 'DEGREE', '研究生毕业', '01', '0', '2', '', '0', '1597891320065', '1', '超级管理员', '1', '南斗信息', '1597891320065', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9b6972e35fd370645445bd028cfad790', 'DEGREE', '博士', '03', '0', '4', '', '0', '1597891387194', '1', '超级管理员', '1', '南斗信息', '1597891387194', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9ba3370b9d0748bb7b8b0ed512abce51', 'NATION', '回族', '03', '0', '3', '', '0', '1597888189582', '1', '超级管理员', '1', '南斗信息', '1597890435567', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9cf548fb7468415abd2a5bc8a956de17', 'EQUIPMENT_STATE', '已过期', '4', '0', '4', '', '0', '1618037379814', '1', '超级管理员', '1', '南斗信息', '1619323380380', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9d29e75f64e934f2f934112a6904f226', 'NATION', '鄂温克族', '45', '0', '45', '', '0', '1597889509620', '1', '超级管理员', '1', '南斗信息', '1597890565146', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9df823b7a8d85303861cfc0d88bae5c1', 'NATION', '彝族', '07', '0', '7', '', '0', '1597890855098', '1', '超级管理员', '1', '南斗信息', '1597890855098', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9e1ac10760383993eaac271ed138db55', 'DEGREE', '高中', '50', '0', '24', '', '0', '1597891876468', '1', '超级管理员', '1', '南斗信息', '1597891876468', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('9fc50dd0e987c01bb3f28b7bb3540bcf', 'DEGREE', '相当初中毕业', '68', '0', '34', '', '0', '1597892143986', '1', '超级管理员', '1', '南斗信息', '1597892143986', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a0dba696bd9815f33074e290516013ea', 'ORG_TYPE', '公司', '1', '0', '0', '', '0', '1597846482983', '1', '超级管理员', '1', '南斗信息', '1597846482983', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a42ecffa6ecb1cc15faaefbf5c7d42fc', 'EQUIPMENT_STATE', '登记', '0', '0', '0', '', '0', '1600243747129', '1', '超级管理员', '1', '南斗信息', '1616990146710', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a59b9fa3b1b56f03dbbd7fec9893636d', 'NATION', '布依族', '09', '0', '9', '', '0', '1597890888756', '1', '超级管理员', '1', '南斗信息', '1597890888756', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a6156941756084cb7d3385df3621ee6d', 'NATION', '阿昌族', '39', '0', '39', '', '0', '1597889326318', '1', '超级管理员', '1', '南斗信息', '1597890612763', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a7a72ebaa25911ff85286da0ac5fc720', 'FLOW_PROCINST_FINISHED_STATE', '结束', '1', '0', '1', '', '0', '1602983633291', '1', '超级管理员', '1', '南斗信息', '1602983633291', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a8daffc0046671e3463e25f77a7b09e8', 'NATION', '裕固族', '48', '0', '48', '', '0', '1597889589011', '1', '超级管理员', '1', '南斗信息', '1597890543956', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('a97e7339a145d3f11ba77ae00f6ea15f', 'DEGREE', '职业高中毕业', '52', '0', '26', '', '0', '1597891926405', '1', '超级管理员', '1', '南斗信息', '1597891926405', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b0702ea6a80af01e07b4691cb51aa1ac', 'NATION', '毛难族', '36', '0', '36', '', '0', '1597889255760', '1', '超级管理员', '1', '南斗信息', '1597890630408', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b0711d0b3e165236289634ef21f3fa31', 'DEGREE', '中专毕业', '31', '0', '17', '', '0', '1597891678281', '1', '超级管理员', '1', '南斗信息', '1597891678281', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b17917ced0a8abcf56e2ca5ba4e72aa9', 'NATION', '京族', '49', '0', '49', '', '0', '1597889608673', '1', '超级管理员', '1', '南斗信息', '1597890537811', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b1e9e0192359fb1ef63410c8811f7846', 'NATION', '赫哲族', '53', '0', '53', '', '0', '1597889730888', '1', '超级管理员', '1', '南斗信息', '1597890508999', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b202d8b44b3cd6d46acdfe7db26fc728', 'NOTICE_RECEIVER_TYPE', '指定人员', '2', '0', '2', '', '0', '1610327507551', '1', '超级管理员', '1', '南斗信息', '1610327507551', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b2d89db1432e5f263fbc87b7d449bc7e', 'NATION', '崩龙族', '46', '0', '46', '', '0', '1597889539748', '1', '超级管理员', '1', '南斗信息', '1597890557753', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b3817880dc72d377958828fe8fb0d82e', 'OBJ_CODE_CONFIG', 'DEFAULT_PATH', '/view/public/objcode/index', '0', '4', '扫码页面', '0', '1616809833623', '1', '超级管理员', '1', '南斗信息', '1616809833623', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b395d842263dd1d9a25ad12f8deaead1', 'RESOURCE_TYPE', '模块', '1', '0', '1', '', '0', '1597764727383', '1', '超级管理员', '1', '南斗信息', '1597764727383', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b53814056f31d20bfe1910b64275c0e9', 'FLOW_TASK_STATE', '审批中', '1', '0', '1', '', '0', '1602983498548', '1', '超级管理员', '1', '南斗信息', '1602983498548', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b63f9cababa6bdd13583e9749fec5ba1', 'NATION', '维吾尔族', '05', '0', '5', '', '0', '1597888263930', '1', '超级管理员', '1', '南斗信息', '1597890453438', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b697ddc1aa875a73c29bd29aac3b2d48', 'NATION', '柯尔克孜族', '29', '0', '29', '', '0', '1597889050620', '1', '超级管理员', '1', '南斗信息', '1597890671309', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b8830cd19e88777d2611f93257529a09', 'NATION', '门巴族', '54', '0', '54', '', '0', '1597889749696', '1', '超级管理员', '1', '南斗信息', '1597890500628', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b883e88cdd870842ae1aab6144bcab85', 'DEGREE', '中技毕业', '32', '0', '18', '', '0', '1597891696020', '1', '超级管理员', '1', '南斗信息', '1597891696020', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b8dc39681c488aed976dcef9b3ded68d', 'FLOW_UNDO_STATE', '等待处理', '0', '0', '0', '', '0', '1602983741669', '1', '超级管理员', '1', '南斗信息', '1602983741669', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('b93b6b5c01340bf7f14bc8981bf23c82', 'NATION', '土族', '30', '0', '30', '', '0', '1597889066562', '1', '超级管理员', '1', '南斗信息', '1597890666109', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('ba14ec909ed684667457ba56d0dae5cc', 'NATION', '藏族', '04', '0', '4', '', '0', '1597888239091', '1', '超级管理员', '1', '南斗信息', '1597890442617', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('ba87f586006b241a0850148dd26f9b56', 'FLOW_PROCINST_FINISHED_STATE', '审批中', '0', '0', '0', '', '0', '1602983624201', '1', '超级管理员', '1', '南斗信息', '1602983624201', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('badb480ea69a19e5983357de7df5b1f7', 'DEGREE', '初中肄业', '69', '0', '35', '', '0', '1597892172136', '1', '超级管理员', '1', '南斗信息', '1597892172136', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('c07a2d1bbf5e65605d859acbcbb717e9', 'FLOW_TASK_STATE', '已抄送', '3', '0', '3', '', '0', '1602983525143', '1', '超级管理员', '1', '南斗信息', '1602983525143', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('c2080836bac01a7cb667043db752c5d9', 'FLOW_PROCINST_STATE', '通过', '2', '0', '2', '', '0', '1602983405688', '1', '超级管理员', '1', '南斗信息', '1602983405688', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('c27f1948ff20ebe73f3bc9a9376cd723', 'DEGREE', '农业初中毕业', '63', '0', '33', '', '0', '1597892115442', '1', '超级管理员', '1', '南斗信息', '1597892115442', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('c2f0277c3ed14d12e5f7fd638a36e483', 'NATION', ' 达斡尔族', '31', '0', '31', '', '0', '1597889138707', '1', '超级管理员', '1', '南斗信息', '1597890660803', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('c37e9cbd400e9be0634c3d58a1b8d2f1', 'OBJ_CODE_CREATE_STATE', '生成失败', '-1', '0', '3', '', '0', '1616490612082', '1', '超级管理员', '1', '南斗信息', '1616490612082', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('c59c0d828d9e55be8b2adbab526b8ef2', 'DEGREE', '研究生', '00', '0', '1', '', '0', '1597891241687', '1', '超级管理员', '1', '南斗信息', '1597891263783', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('cb301f4f335760875bc21ff5db2ff056', 'NATION', '傈傈族', '20', '0', '20', '', '0', '1597888801636', '1', '超级管理员', '1', '南斗信息', '1597890717627', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('cbfa142a882bf9be6c8d302b5613990c', 'NOTICE_TOP_STATE', '置顶', '1', '0', '1', '', '0', '1610328753557', '1', '超级管理员', '1', '南斗信息', '1610328753557', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('ce0d0c71aec0ed3719bd282c6b485d2c', 'OBJ_CODE_DETAIL_STATE', '待使用', '1', '0', '1', '', '0', '1616499358828', '1', '超级管理员', '1', '南斗信息', '1616500681650', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('cf4cbf7bd76280a9d4e4c5558de19e1b', 'NATION', '其他', '97', '0', '97', '', '0', '1597891014030', '1', '超级管理员', '1', '南斗信息', '1597891070942', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('cf9b577b1c2bf24510a2a0e1c4b2a995', 'FLOW_UNDO_STATE', '已读', '1', '0', '1', '', '0', '1602983754903', '1', '超级管理员', '1', '南斗信息', '1602983754903', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('cfe5c54697468496b5ece24d6a13af55', 'YES_NO', '否', '0', '0', '1', '', '0', '1599998326631', '1', '超级管理员', '1', '南斗信息', '1599998326631', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d166f4a234053381a22615a2a670049d', 'NATION', '瑶族', '13', '0', '13', '', '0', '1597888475226', '1', '超级管理员', '1', '南斗信息', '1597890786953', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d19fcbc0ad4956dffa8ddab80ff4cb38', 'NATION', '畲族', '22', '0', '22', '', '0', '1597888895788', '1', '超级管理员', '1', '南斗信息', '1597890711508', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d2054297506084fd8d6a0d919e755ff9', 'FLOW_PROCINST_STATE', '审批中', '1', '0', '0', '', '0', '1602983392099', '1', '超级管理员', '1', '南斗信息', '1602983392099', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d2fa549409eed9f7e45e417f87991ae9', 'FLOW_UNDO_FINISHED_STATE', '处理中', '0', '0', '0', '', '0', '1602983826180', '1', '超级管理员', '1', '南斗信息', '1602983826180', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d4da797a4dffb03b7240a3d857552236', 'DEGREE', '相当小学毕业', '78', '0', '38', '', '0', '1597892237895', '1', '超级管理员', '1', '南斗信息', '1597892237895', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d8d550227cdab9508ccd64358bce787e', 'DEGREE', '农业高中毕业', '53', '0', '27', '', '0', '1597891948246', '1', '超级管理员', '1', '南斗信息', '1597891948246', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('d921e3690590f728ec3176b0262d8713', 'NATION', '拉祜族', '24', '0', '24', '', '0', '1597888945652', '1', '超级管理员', '1', '南斗信息', '1597890701116', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('da85ce60656f02c80053f3bd6306af3f', 'DATA_SCOPE', '本区域', '1000', '0', '4', '', '0', '1604406852757', '1', '超级管理员', '1', '南斗信息', '1604406852757', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('db573b1fd117d0bd832f83d8d4f4955f', 'NATION', '基诺族', '56', '0', '56', '', '0', '1597889801475', '1', '超级管理员', '1', '南斗信息', '1597890486513', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('dbd9da1d276e4c21b4b8bd7231610df5', 'DEGREE', '高中毕业', '51', '0', '25', '', '0', '1597891901443', '1', '超级管理员', '1', '南斗信息', '1597891901443', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('dfadff9642f581462f42d43f3ed07c1a', 'DEGREE', '初中', '60', '0', '30', '', '0', '1597892029671', '1', '超级管理员', '1', '南斗信息', '1597892029671', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('e115691429c56402efd9a219a2db9ccb', 'FLOW_UNDO_STATE', '委托', '4', '0', '4', '', '0', '1602983782170', '1', '超级管理员', '1', '南斗信息', '1602983782170', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('e1655c0855bbb0ac10036dbbd518325c', 'DEGREE', '技工学校毕业', '41', '0', '22', '', '0', '1597891833610', '1', '超级管理员', '1', '南斗信息', '1597891833610', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('e8c99163c01ed9284b23c94b4a3ec545', 'NATION', '珞巴族', '55', '0', '55', '', '0', '1597889769369', '1', '超级管理员', '1', '南斗信息', '1597890491561', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('e962c3e40bf0d72d41378c4a64419425', 'EMPLOYEE_TYPE', '劳务派遣', '4', '0', '3', '', '0', '1597910279884', '1', '超级管理员', '1', '南斗信息', '1597910279884', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('ec9a7457d35231e3deb317e8e24e31e0', 'MARRIE_STATE', '待选择', '1', '0', '1', '', '0', '1598491744370', '1', '超级管理员', '1', '南斗信息', '1598491744370', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('eca2c18328867819b5353113afe81225', 'NATION', '水族', '25', '0', '25', '', '0', '1597888960871', '1', '超级管理员', '1', '南斗信息', '1597890693453', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('efb3b1d3b4a79533552f649898fd3ad4', 'NATION', '蒙古族', '02', '0', '2', '', '0', '1597888117342', '1', '超级管理员', '1', '南斗信息', '1597890415557', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('f4f3f2f2c9ea298a40fe859b4d19fbe6', 'EMPLOYEE_TYPE', '兼职', '2', '0', '1', '', '0', '1597910237533', '1', '超级管理员', '1', '南斗信息', '1597910237533', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('f4fdf50e91e33818aa3e8450b538d700', 'FLOW_PROCINST_STATE', '驳回', '-1', '0', '4', '', '0', '1602983429571', '1', '超级管理员', '1', '南斗信息', '1602983429571', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('f6f9e1e9fdb6e41b3be487e7cb2d1a91', 'NATION', '傣族', '18', '0', '18', '', '0', '1597888747713', '1', '超级管理员', '1', '南斗信息', '1597890743955', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('fb67a1f767da2ee8143adf47d3398719', 'NATION', '俄罗斯族', '44', '0', '44', '', '0', '1597889489573', '1', '超级管理员', '1', '南斗信息', '1597890577375', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('fbfd79fc53866b1e82d5f3dd59c95dd5', 'DEGREE', '技校', '40', '0', '21', '', '0', '1597891794130', '1', '超级管理员', '1', '南斗信息', '1597891794130', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('fc0784e2e9e84e87cb63ed0873ddaa57', 'NATION', '苗族', '06', '0', '6', '', '0', '1597890834201', '1', '超级管理员', '1', '南斗信息', '1597890834201', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('fc797cb4ca725d041b5ddea72e9176a2', 'DEGREE', '相当专科毕业', '28', '0', '14', '', '0', '1597891602987', '1', '超级管理员', '1', '南斗信息', '1597891602987', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('fd4f0974be0f25e0a7e127e4d671d20d', 'NATION', '壮族', '08', '0', '8', '', '0', '1597890873428', '1', '超级管理员', '1', '南斗信息', '1597890873428', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_dict_item` VALUES ('ff4829a8d35734d85293a72154228037', 'DATA_SCOPE', '全部数据权限', '10000', '0', '999', '', '0', '1603280831377', '1', '超级管理员', '1', '南斗信息', '1604406806350', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for fdp_sys_msg
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_msg`;
CREATE TABLE `fdp_sys_msg` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `bus_id` varchar(64) NOT NULL DEFAULT '' COMMENT '业务ID',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `content` varchar(3000) DEFAULT '' COMMENT '内容',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `receive_time` bigint(20) DEFAULT NULL COMMENT '接收时间',
  `receive_user` varchar(100) DEFAULT '' COMMENT '接收用户ID',
  `receive_user_name` varchar(50) DEFAULT NULL COMMENT '接收用户名',
  `receive_dept` varchar(100) DEFAULT NULL COMMENT '接收用户部门编码',
  `receive_dept_name` varchar(50) DEFAULT NULL COMMENT '接收用户部门名称',
  `read_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型';

-- ----------------------------
-- Records of fdp_sys_msg
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_notice`;
CREATE TABLE `fdp_sys_notice` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `recivers` text COMMENT '接收者',
  `top_state` tinyint(1) DEFAULT '0' COMMENT '置顶状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  `release_time` bigint(20) DEFAULT NULL COMMENT '发布时间',
  `release_user` varchar(100) DEFAULT '' COMMENT '发布人编码',
  `release_user_name` varchar(50) DEFAULT NULL COMMENT '发布人名称',
  `release_dept` varchar(100) DEFAULT NULL COMMENT '发布部门编码',
  `release_dept_name` varchar(50) DEFAULT NULL COMMENT '发布部门名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型';

-- ----------------------------
-- Records of fdp_sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_sys_notice_receiver
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_notice_receiver`;
CREATE TABLE `fdp_sys_notice_receiver` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `notice_id` varchar(64) NOT NULL DEFAULT '' COMMENT '公告ID',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `receiver_time` bigint(20) DEFAULT NULL COMMENT '接收时间',
  `receiver_user` varchar(100) DEFAULT '' COMMENT '接收人编码',
  `receiver_user_name` varchar(50) DEFAULT NULL COMMENT '接收人名称',
  `receiver_dept` varchar(100) DEFAULT NULL COMMENT '接收部门编码',
  `receiver_dept_name` varchar(50) DEFAULT NULL COMMENT '接收部门名称',
  `read_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='字典类型';

-- ----------------------------
-- Records of fdp_sys_notice_receiver
-- ----------------------------

-- ----------------------------
-- Table structure for fdp_sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_resource`;
CREATE TABLE `fdp_sys_resource` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `parent_code` varchar(100) DEFAULT NULL COMMENT '上级编码',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `online_design_flag` tinyint(1) DEFAULT NULL COMMENT '是否在线设计标志',
  `path` varchar(100) DEFAULT NULL COMMENT '资源位置',
  `perms` varchar(3000) DEFAULT '' COMMENT '权限标识',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限';

-- ----------------------------
-- Records of fdp_sys_resource
-- ----------------------------
INSERT INTO `fdp_sys_resource` VALUES ('0c337490b8f06a44fd88b4684f5ac378', '删除', 'MERCHANTS_MANAGE_DEL', '3', 'MERCHANTS_MANG', 'layui-icon-delete', null, '', 'merchants:del', '0', '2', '', '0', '1604409632447', '1', '超级管理员', '1', '南斗信息', '1604412538486', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('1', '开发工作台', 'DEV_WORKBENCH', '1', '', 'layui-icon-code-circle', '0', '', '', '0', '15', '开发工作台', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1608345279827', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('117a8c72d9f7643970f25c7b755dec5d', '删除', 'STORE_MANAGE_DEL', '3', 'STORE_MANG', 'layui-icon-delete', null, '', 'store:del', '0', '2', '', '0', '1604406222969', '1', '超级管理员', '1', '南斗信息', '1604406222969', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('19c1f9fb4487f75e4c9d9aa0cefae1ec', '重置密码', 'USER_MANAGE_ACCOUNT_RESET', '3', 'USER_MANG', 'layui-icon-password', null, '', 'user:account:reset:pwd', '0', '4', '', '0', '1604390871999', '1', '超级管理员', '1', '南斗信息', '1604390871999', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('2', '资源管理', 'RESOURCE_MANG', '2', 'SYS_MANG', 'layui-icon-set', '0', '/view/sys/resource/list', '', '0', '2', '资源管理', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('24b84a39aa48d4a2bd601e44746545e0', '生码管理', 'OBJCODE_CREATE_MANG', '2', 'OBJ_CODE', 'layui-icon-file-b', null, '/view/base/objcode/list', '', '0', '1', '', '0', '1619322598327', '1', '超级管理员', '1', '南斗信息', '1619322598327', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('254c86b0338dbc1c52349aad93848d3e', '导出', 'OBJ_CODE_CREATE_EXPORT', '3', 'OBJCODE_CREATE_MANG', 'layui-icon-file-b', null, '', 'objcode:create:export', '0', '3', '', '0', '1619322692959', '1', '超级管理员', '1', '南斗信息', '1619322692959', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('2901c6b2232ace5138bf29b59b43bedf', '新增', 'STORE_MANAGE_ADD', '3', 'STORE_MANG', 'layui-icon-addition', null, '', 'store:add', '0', '1', '', '0', '1604406183448', '1', '超级管理员', '1', '南斗信息', '1604406183448', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('2cff349c6682f1588970537ae89fc19b', '删除', 'USER_MANAGE_DEL', '3', 'USER_MANG', 'layui-icon-delete', null, '', 'user:del', '0', '2', '', '0', '1604390540676', '1', '超级管理员', '1', '南斗信息', '1604390540676', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('2e6cbfdc286c39c2064ead5b6446fcaa', '分配权限', 'USER_MANAGE_AUTHORIZATION', '3', 'USER_MANG', 'layui-icon-auz', null, '', 'user:authorization', '0', '5', '', '0', '1604391142747', '1', '超级管理员', '1', '南斗信息', '1604391142747', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('3', '组织架构', 'ORG_FRAMEWORK', '1', '', 'layui-icon-app', '0', '', '', '0', '7', '组织架构', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1619321857056', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('32bdb4db09fd4574503e541282cfe295', '子部门分配', 'COUPON_CHILD_DEPT_DISTRIBUTION', '3', 'COUPON_LIST', 'layui-icon-file-b', null, '', 'coupon:distributeToDept', '0', '1', '', '0', '1607947434096', '1', '超级管理员', '1', '南斗信息', '1607947595672', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('32cd90ea55ab3b0b6872bac036b33f16', '查看', 'MATERIAL_VIEW', '2', 'MATERIAL_MANG', 'layui-icon-file-b', null, '', '', '0', '4', '', '0', '1608371000055', '1', '超级管理员', '1', '南斗信息', '1608371000055', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('38052b098adfb1bdb86cb18e8b42dfcc', '新增', 'MERCHANTS_MANAGE_ADD', '3', 'MERCHANTS_MANG', 'layui-icon-addition', null, '', 'merchants:add', '0', '1', '', '0', '1604408464087', '1', '超级管理员', '1', '南斗信息', '1604408464087', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('3929ed93b716065f8dd5dbfe59c38c53', '流程管理', 'WORKFLOW_PROCMANG', '2', 'APPROVE_MANG', 'layui-icon-file-b', null, '/view/workflow/procmang/index', '', '0', '2', '', '0', '1598711186300', '1', '超级管理员', '1', '南斗信息', '1603028512938', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('3dbd86cd9bc39638d14b484b70fcae82', '已发送', 'MY_SEND', '2', 'BUS_APPROVE', 'layui-icon-file-b', null, '/view/workflow/mySend/index', '', '0', '2', '', '0', '1599745768477', '1', '超级管理员', '1', '南斗信息', '1602982901623', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('3f12d11803568f3826a8a559a5392da1', '删除', 'WORKFLOW_PROCMANAGE_DEL', '3', 'WORKFLOW_PROCMANG', 'layui-icon-delete', null, '', 'workflow:procmanage:del', '0', '1', '', '0', '1604417786860', '1', '超级管理员', '1', '南斗信息', '1604417786860', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('4', '系统管理', 'SYS_MANG', '1', '', 'layui-icon-set', '0', '', '', '0', '9', '系统管', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1619321880247', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('4031c17dd7d11b83d7b33b3991507d0a', '修改', 'CHARGE_ITEM_MODIFY', '3', 'CHARGE_ITEM', 'layui-icon-refresh', null, '', 'charage:item:modify', '0', '2', '', '0', '1604418564174', '1', '超级管理员', '1', '南斗信息', '1604418564174', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('413d227a22cae03f3db77efe26190ba6', '删除', 'MATERIAL_DEL', '2', 'MATERIAL_MANG', 'layui-icon-file-b', null, '', 'material:del', '0', '2', '', '0', '1608370943631', '1', '超级管理员', '1', '南斗信息', '1608370943631', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('48ad72697f1d4bf3a294815452b0f21f', '已处理', 'ALREADY_APPROVE', '2', 'BUS_APPROVE', 'layui-icon-file-b', null, '/view/workflow/alreadyApprove/index', '', '0', '4', '', '0', '1602983080030', '1', '超级管理员', '1', '南斗信息', '1603030032096', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('498c04b9ea6fa5060a96132e85ffece9', '审批表', 'APPROVE_FORM', '2', 'BUS_APPROVE', 'layui-icon-form', null, '/view/workflow/approveform/index', '', '0', '1', '', '0', '1598879384236', '1', '超级管理员', '1', '南斗信息', '1602986575465', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('4c4d73462d6faae93d752ed6b5d574b0', '首页', 'HOME_PAGE', '2', '', 'layui-icon-file-b', null, '/view/welcome', '', '0', '-1', '', '0', '1604406378998', '1', '超级管理员', '1', '南斗信息', '1604406390856', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('5', '接口文档', 'API_DOC', '2', 'DEV_WORKBENCH', 'layui-icon-list', '0', '/doc.html', '', '0', '0', '接口文档', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('50619217bb057dbf9e55fd16db57d7dd', '职务管理', 'POSITION_MANG', '2', 'ORG_FRAMEWORK', 'layui-icon-file-b', null, '/view/org/position/list', '', '0', '4', '', '0', '1601372707729', '1', '超级管理员', '1', '南斗信息', '1601372770213', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('5cb3b9ec9c3f104c061e5388f56d33bf', '修改', 'POSITION_MANAGE_MODIFY', '3', 'POSITION_MANG', 'layui-icon-refresh', null, '', 'position:manage:modify', '0', '2', '', '0', '1604421636742', '1', '超级管理员', '1', '南斗信息', '1604421636742', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('5d371a58e181ef8a70cb05284e8088e2', '删除', 'CHARGE_ITEM_DEL', '3', 'CHARGE_ITEM', 'layui-icon-delete', null, '', 'charage:item:del', '0', '3', '', '0', '1604418590591', '1', '超级管理员', '1', '南斗信息', '1604418590591', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('5ed20ae45a29a5c301cfa060b7f94ef1', '删除', 'ROLE_MANAGE_DEL', '3', 'ROLE_MANG', 'layui-icon-delete', null, '', 'role:manage:del', '0', '3', '', '0', '1604422034419', '1', '超级管理员', '1', '南斗信息', '1604422034419', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('6', '数据字典', 'DATA_DICT', '2', 'SYS_MANG', 'layui-icon-read', '0', '/view/sys/dict/list', '', '0', '1', '数据字典', '0', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1597148077959', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('608927914b6a21b701751d92a866d1ae', '发票维护', 'INVOICE_EDIT', '3', 'CONTRACT_PAY', 'layui-icon-file-b', null, '', 'invoice:edit', '0', '1', '', '0', '1611388875472', '1', '超级管理员', '1', '南斗信息', '1611388875472', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('613c7266d8052f164f1eaafee9330d66', '删除', 'POSITION_MANAGE_DEL', '3', 'POSITION_MANG', 'layui-icon-delete', null, '', 'position:manage:del', '0', '3', '', '0', '1604421675560', '1', '超级管理员', '1', '南斗信息', '1604421675560', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('636eeb0f35f2557fd648c257f29c1e4e', '增加', 'MAERITAL_ADD', '3', 'MATERIAL_MANG', 'layui-icon-file-b', null, '', 'material:add', '0', '1', '', '0', '1608370913716', '1', '超级管理员', '1', '南斗信息', '1608370913716', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('6c529c58f711f718bd8315f586ff0a82', '处理', 'SUGGEST_HANDLE', '3', 'SUGGEST_LIST', 'layui-icon-edit', null, '', 'suggest:handle', '0', '1', '', '0', '1604420965503', '1', '超级管理员', '1', '南斗信息', '1604420965503', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('707b1b6050379a1a0322797b98b81098', '删除', 'AREA_MANAGE_DEL', '3', 'AREA_MANG', 'layui-icon-delete', null, '', 'area:mange:del', '0', '3', '', '0', '1604416365940', '1', '超级管理员', '1', '南斗信息', '1604416365940', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('720200e619123686e9f5966703916c53', '添加', 'COUPON-ADD', '3', 'CREATE_COUPON', 'layui-icon-file-b', null, '/view/cmstimp/coupon/coupon/edit', 'coupon:add', '0', '1', '', '0', '1607852994078', '1', '超级管理员', '1', '南斗信息', '1607852994078', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('72b4569d7299c78aef7e7c2d067a41f0', '修改', 'STORE_MANAGE_MODIFY', '3', 'STORE_MANG', 'layui-icon-refresh', null, '', 'store:modify', '0', '3', '', '0', '1604406315528', '1', '超级管理员', '1', '南斗信息', '1604406315528', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('733833fc8cca1f311658d00da83714cf', '查看', 'COUPON_DISTRIBUTE_VIEW', '3', 'COUPON_LIST', 'layui-icon-file-b', null, '', 'coupon:distributeView', '0', '0', '', '0', '1607947693534', '1', '超级管理员', '1', '南斗信息', '1607947693534', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('75fd201e4e76d8c48476369e13e0d4c4', '公告查阅', 'NOTICE_VIEW', '2', 'NOTICE', 'layui-icon-notice', null, '/view/sys/notice/view_list', '', '0', '2', '', '0', '1603804848385', '1', '超级管理员', '1', '南斗信息', '1603804848385', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('76ef58e8a3e4f36f5a5c3a9c7014d5de', '删除', 'OBJ_CODE_CREATE_DEL', '3', 'OBJCODE_CREATE_MANG', 'layui-icon-file-b', null, '', 'objcode:create:del', '0', '2', '', '0', '1619322643303', '1', '超级管理员', '1', '南斗信息', '1619322643303', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('7c2b163eb202ae7b1a021c061ebedf59', '修改', 'USER_MANAGE_MODIFY', '3', 'USER_MANG', 'layui-icon-refresh', null, '', 'user:modify', '0', '3', '', '0', '1604390658835', '1', '超级管理员', '1', '南斗信息', '1604390788834', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('7c67436b6c4520ba16bacbb496613053', '修改', 'CONTRACT_BOOK_MODIFY', '2', 'CONTRACT_LIST', 'layui-icon-refresh', null, '', 'contractBook:modify', '0', '2', '', '0', '1605423136614', '1', '超级管理员', '1', '南斗信息', '1605423136614', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('7c76227403b9822c97fd72ec89a2f77f', '组织管理', 'DEPT_MANG', '2', 'ORG_FRAMEWORK', 'layui-icon-tree', null, '/view/org/dept/list', '', '0', '1', '', '0', '1597846292882', '1', '超级管理员', '1', '南斗信息', '1597882114103', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('81019401c6684147b88a1fc80b3d4952', '删除', 'NOTICE_MANAGE_DEL', '3', 'NOTICE_MANG', 'layui-icon-delete', null, '', 'notice:manage:del', '0', '3', '', '0', '1604420494464', '1', '超级管理员', '1', '南斗信息', '1604420494464', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('826a9a07efa8a19c345bd5bd0cfc8f86', '新增', 'CHARGE_ITEM_ADD', '3', 'CHARGE_ITEM', 'layui-icon-addition', null, '', 'charage:item:add', '0', '1', '', '0', '1604418522379', '1', '超级管理员', '1', '南斗信息', '1604418522379', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('8b77dacbb6a94e18bb8352dcc59061f2', '删除', 'COUPON-DEL', '3', 'CREATE_COUPON', 'layui-icon-file-b', null, '', 'coupon:del', '0', '2', '', '0', '1607853025667', '1', '超级管理员', '1', '南斗信息', '1607853025667', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('8cd058b5df62c1267a3415aac2c4ed44', '我收到的', 'MY_RECEIVED', '2', 'BUS_APPROVE', 'layui-icon-file-b', null, '/view/workflow/myRead/index', '', '0', '4', '', '0', '1599755107521', '1', '超级管理员', '1', '南斗信息', '1600182441185', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('8f5151edad0805aa9271a6680a5aeeb6', '待处理', 'WAIT_APPROVE', '2', 'BUS_APPROVE', 'layui-icon-edit', null, '/view/workflow/waitApprove/index', '', '0', '3', '', '0', '1599755002168', '1', '超级管理员', '1', '南斗信息', '1603029258551', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('91c057389626a22218e8613ff1980180', 'OA审批', 'BUS_APPROVE', '1', '', 'layui-icon-edit', null, '', '', '0', '1', '', '0', '1598878818747', '1', '超级管理员', '1', '南斗信息', '1619321625273', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('97217767a38ace2efaa8956eb42231f9', '修改', 'WORKFLOW_PROCDEF_MODIFY', '3', 'WORKFLOW_PROCDEF', 'layui-icon-refresh', null, '', 'workflow:procdef:modify', '0', '2', '', '0', '1604417521942', '1', '超级管理员', '1', '南斗信息', '1604417558420', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('98454a0e3594f3116ddafb7ced12dcb8', '修改', 'REMODEL_ACCEPTANCE_FORM_MODIFY', '3', 'REMODEL_ACCEPTANCE_FORM', 'layui-icon-refresh', null, '', 'remodel:acceptance:form:modify', '0', '2', '', '0', '1604415240426', '1', '超级管理员', '1', '南斗信息', '1604415240426', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('9c97854f01acdca5e6b01e33d0df4a8c', '编辑', 'MATERIAL_EDIT', '2', 'MATERIAL_MANG', 'layui-icon-file-b', null, '', 'material:modify', '0', '3', '', '0', '1608370970964', '1', '超级管理员', '1', '南斗信息', '1608371105865', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('a00ce22235160349207b53b0ec6e73a6', '删除', 'WORKFLOW_PROCDEF_DEL', '3', 'WORKFLOW_PROCDEF', 'layui-icon-delete', null, '', 'workflow:procdef:del', '0', '3', '', '0', '1604417549799', '1', '超级管理员', '1', '南斗信息', '1604417549799', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('a1b66d2850d2a510ef28faf8f70e5742', '删除', 'CONTRACT_BOOK_DEL', '2', 'CONTRACT_LIST', 'layui-icon-delete', null, '', 'contractBook:del', '0', '3', '', '0', '1605423187623', '1', '超级管理员', '1', '南斗信息', '1605423187623', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('a7b547a828462a69d8360356324d674d', '访客领取', 'COUPON_GUEST_DISTRIBUTION', '2', 'COUPON_LIST', 'layui-icon-file-b', null, '', 'coupon:distributeToGuest', '0', '4', '', '0', '1607947576153', '1', '超级管理员', '1', '南斗信息', '1607947620794', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('a9421c95ec43964e708568e5c88ba874', '一物一码', 'OBJ_CODE', '1', '', 'layui-icon-app', null, '', '', '0', '3', '', '0', '1619322560439', '1', '超级管理员', '1', '南斗信息', '1619322560439', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('ad50f73a577517fbe077e32859a26efa', '工作流程', 'APPROVE_MANG', '1', '', 'layui-icon-component', null, '', '', '0', '8', '', '0', '1598624624928', '1', '超级管理员', '1', '南斗信息', '1619321870623', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('af7706c86c07633f0b7ea9592b925abb', '修改', 'MERCHANTS_MANAGE_MODIFY', '3', 'MERCHANTS_MANG', 'layui-icon-refresh', null, '', 'merchants:modify', '0', '3', '', '0', '1604409689545', '1', '超级管理员', '1', '南斗信息', '1604409689545', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('b3e5db6ca37148c3986d3055f9a6c4a9', '重置密码', 'MERCHANTS_MANAGE_ACCOUNT_RESET', '3', 'MERCHANTS_MANG', 'layui-icon-password', null, '', 'merchants:account:reset:pwd', '0', '4', '', '0', '1604409778204', '1', '超级管理员', '1', '南斗信息', '1604409778204', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('ba29df83b3a78a3c21e220fcf115e73c', '新增', 'USER_MANAGE_ADD', '3', 'USER_MANG', 'layui-icon-addition', null, '', 'user:add', '0', '1', '', '0', '1604390487082', '1', '超级管理员', '1', '南斗信息', '1604390487082', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('bc9235d728e0f5c6c7f385d51d1152af', '分配', 'COUPON-DISTRIBUTE', '3', 'CREATE_COUPON', 'layui-icon-file-b', null, '', 'coupon:distribute', '0', '4', '', '0', '1607853153676', '1', '超级管理员', '1', '南斗信息', '1607858128697', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('bf6a83185d02ce119fe2f140c25e07f5', '分配权限', 'ROLE_MANAGE_AUTHORIZATION', '3', 'ROLE_MANG', 'layui-icon-auz', null, '', 'role:manage:authorization', '0', '4', '', '0', '1604422238357', '1', '超级管理员', '1', '南斗信息', '1604448004562', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('bfdf56955c430ec5b9fe75832025c9f9', '通知公告', 'NOTICE', '1', '', 'layui-icon-notice', null, '', '', '0', '0', '', '0', '1603801648384', '1', '超级管理员', '1', '南斗信息', '1608103671965', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('c007504bedb82c5e24a9b570ac79a673', '新增', 'OBJ_CODE_CREATE_ADD', '3', 'OBJCODE_CREATE_MANG', 'layui-icon-file-b', null, '', 'objcode:create:add', '0', '1', '', '0', '1619322621767', '1', '超级管理员', '1', '南斗信息', '1619322621767', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('c7be06644d83074f360ab775e4e37b2a', '公告管理', 'NOTICE_MANG', '2', 'NOTICE', 'layui-icon-notice', null, '/view/sys/notice/list', '', '0', '1', '', '0', '1603801699668', '1', '超级管理员', '1', '南斗信息', '1610770348125', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('c9d12f83849b08e09d08c88f48d8f8da', '用户管理', 'USER_MANG', '2', 'ORG_FRAMEWORK', 'layui-icon-user', null, '/view/org/user/index', '', '0', '2', '', '0', '1597882108480', '1', '超级管理员', '1', '南斗信息', '1611968191246', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('cb40dc53d7c3de668de14f52db9b0e2d', '修改', 'AREA_MANAGE_MODIFY', '3', 'AREA_MANG', 'layui-icon-refresh', null, '', 'area:mange:modify', '0', '2', '', '0', '1604416331640', '1', '超级管理员', '1', '南斗信息', '1604416371965', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('d08522d74ba640c5fb72c7d449febeb5', '修改', 'ROLE_MANAGE_MODIFY', '3', 'ROLE_MANG', 'layui-icon-refresh', null, '', 'role:manage:modify', '0', '2', '', '0', '1604422003362', '1', '超级管理员', '1', '南斗信息', '1604422003362', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('d08def964475109cf59cde2cb4f5fe81', '编辑', 'COUPON-EDIT', '3', 'CREATE_COUPON', 'layui-icon-file-b', null, '', 'coupon:modify', '0', '3', '', '0', '1607853064896', '1', '超级管理员', '1', '南斗信息', '1607853064896', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('d73c4de00986b7376507e08c43cc3e21', '新增', 'AREA_MANAGE_ADD', '3', 'AREA_MANG', 'layui-icon-addition', null, '', 'area:mange:add', '0', '1', '', '0', '1604416157428', '1', '超级管理员', '1', '南斗信息', '1604416157428', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('da2032561c7f73407cb7cc0f214d0cbd', '新增', 'NOTICE_MANAGE_ADD', '3', 'NOTICE_MANG', 'layui-icon-addition', null, '', 'notice:manage:add', '0', '1', '', '0', '1604420419101', '1', '超级管理员', '1', '南斗信息', '1604420419101', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('dcd0b9fc3bb4fbeb9a61bf9669af1bd8', '新增', 'WORKFLOW_PROCDEF_ADD', '3', 'WORKFLOW_PROCDEF', 'layui-icon-addition', null, '', 'workflow:procdef:add', '0', '1', '', '0', '1604417480160', '1', '超级管理员', '1', '南斗信息', '1604417480160', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('e125c5ca109c1bae19182cf7c12f0d7b', '新增', 'POSITION_MANAGE_ADD', '3', 'POSITION_MANG', 'layui-icon-addition', null, '', 'position:manage:add', '0', '1', '', '0', '1604421588687', '1', '超级管理员', '1', '南斗信息', '1604421588687', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('e5969213cfe05a7b1b066f837c1baafc', '删除', 'REMODEL_ACCEPTANCE_FORM_DEL', '3', 'REMODEL_ACCEPTANCE_FORM', 'layui-icon-delete', null, '', 'remodel:acceptance:form:del', '0', '3', '', '0', '1604415293017', '1', '超级管理员', '1', '南斗信息', '1604415293017', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('e7c1cba8087b6df0072dab8817799ffc', '新增', 'ROLE_MANAGE_ADD', '3', 'ROLE_MANG', 'layui-icon-addition', null, '', 'role:manage:add', '0', '1', '', '0', '1604421964869', '1', '超级管理员', '1', '南斗信息', '1604421964869', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('ebd773ecc25bde425804caa5e6cebfd3', '商户领取', 'COUPON_MERCHANTS_DISTRIBUTION', '3', 'COUPON_LIST', 'layui-icon-file-b', null, '', 'coupon:distributeToMerchants', '0', '3', '', '0', '1607947490019', '1', '超级管理员', '1', '南斗信息', '1607947612405', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('ec4e3a02f1955760b6c0079ab6c39ac7', '修改', 'NOTICE_MANAGE_MODIFY', '3', 'NOTICE_MANG', 'layui-icon-refresh', null, '', 'notice:manage:modify', '0', '2', '', '0', '1604420459233', '1', '超级管理员', '1', '南斗信息', '1604420459233', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('ee6b74a5627f2824e95268222e8c6822', '编辑企业', 'MERCHANTS_MANAGE_COMPANY_EDIT', '3', 'MERCHANTS_MANG', 'layui-icon-file-b', null, '', 'merchantsCompany:edit', '0', '5', '', '0', '1606818197303', '1', '超级管理员', '1', '南斗信息', '1606818197303', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('f08df93dc7655abf20a0138817ff9e1a', '流程设计', 'WORKFLOW_PROCDEF', '2', 'APPROVE_MANG', 'layui-icon-set-sm', null, '/view/workflow/procdef/list', '', '0', '1', '', '0', '1598711049329', '1', '超级管理员', '1', '南斗信息', '1600915495737', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('f2ceca50798854a41adb90019c8892be', '新增', 'CONTRACT_BOOK_ADD', '2', 'CONTRACT_LIST', 'layui-icon-addition', null, '', 'contractBook:add', '0', '1', '', '0', '1605423002205', '1', '超级管理员', '1', '南斗信息', '1605423002205', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('f49c23eed122dc842c17002e5e7af1c2', '管理员分配', 'COUPON_CHILD_MANG_DISTRIBUTION', '3', 'COUPON_LIST', 'layui-icon-file-b', null, '', 'coupon:distributeToMang', '0', '2', '', '0', '1607947462469', '1', '超级管理员', '1', '南斗信息', '1607947604382', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('f76e88520f4b50bacd5702f6deeed069', '角色管理', 'ROLE_MANG', '2', 'ORG_FRAMEWORK', 'layui-icon-templeate-1', null, '/view/org/role/list', '', '0', '3', '', '0', '1597822853669', '1', '超级管理员', '1', '南斗信息', '1597890413089', '1', '超级管理员', '1', '南斗信息', '1');
INSERT INTO `fdp_sys_resource` VALUES ('f96046bca276d66b64d9b6a2b94af801', '新增', 'REMODEL_ACCEPTANCE_FORM_ADD', '3', 'REMODEL_ACCEPTANCE_FORM', 'layui-icon-add', null, '', 'remodel:acceptance:form:add', '0', '1', '', '0', '1604415165693', '1', '超级管理员', '1', '南斗信息', '1604415165693', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource` VALUES ('120bc99981c2169ad5fcba77d9db4e4b', '智慧供暖系统', 'CASE_HEATHING', '2', 'CASE', 'layui-icon-file-b', null, 'http://ims.nandou-china.com/', '', '0', '2', '', '0', '1648306020783', '1', '超级管理员', '1', 'nandou有限公司', '1648306020783', '1', '超级管理员', '1', 'nandou有限公司', '0');
INSERT INTO `fdp_sys_resource` VALUES ('9d7654ce1879fccf2afd5471ea6804d5', '智慧市场管理系统', 'CASE_IMS', '2', 'CASE', 'layui-icon-file-b', null, 'http://ims.nandou-china.com/', '', '0', '1', '', '0', '1648305940323', '1', '超级管理员', '1', 'nandou有限公司', '1648305940323', '1', '超级管理员', '1', 'nandou有限公司', '0');
INSERT INTO `fdp_sys_resource` VALUES ('e1072ec97b1a5293099c9b53871c437b', '经典案例', 'CASE', '1', '', 'layui-icon-login-wechat', null, '', '', '0', '20', '', '0', '1648305864440', '1', '超级管理员', '1', 'nandou有限公司', '1648306052203', '1', '超级管理员', '1', '陕西南斗信息有限公司', '0');
INSERT INTO `fdp_sys_resource` VALUES ('f2f853ece17598cb8979abc81eb4047f', '地图搜索采集导出工具', 'MAP_COLLECT_EXPORT', '2', 'CASE', 'layui-icon-file-b', null, 'http://www.nandou-china.com/mapPoiExport.html', '', '0', '3', '', '0', '1648430215966', '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '1648430293143', '1', '超级管理员', '1', '陕西南斗信息科技有限公司', '0');

-- ----------------------------
-- Table structure for fdp_sys_resource_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `fdp_sys_resource_data_scope`;
CREATE TABLE `fdp_sys_resource_data_scope` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `code` varchar(100) NOT NULL DEFAULT '' COMMENT '编码',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型',
  `auth_sql` varchar(3000) DEFAULT '' COMMENT '权限sql',
  `resource_code` varchar(64) DEFAULT NULL COMMENT '资源编码',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(100) DEFAULT '' COMMENT '创建人编码',
  `create_user_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_dept` varchar(100) DEFAULT NULL COMMENT '创建部门编码',
  `create_dept_name` varchar(50) DEFAULT NULL COMMENT '创建部门名称',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `modify_user` varchar(100) DEFAULT NULL COMMENT '修改用户编码',
  `modify_user_name` varchar(50) DEFAULT NULL COMMENT '修改用户名称',
  `modify_dept` varchar(100) DEFAULT NULL COMMENT '修改部门编码',
  `modify_dept_name` varchar(50) DEFAULT NULL COMMENT '修改部门名称',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限';

-- ----------------------------
-- Records of fdp_sys_resource_data_scope
-- ----------------------------
INSERT INTO `fdp_sys_resource_data_scope` VALUES ('1086966063073b2c19ad0343871c3c32', '本部门', '10', '0', '', 'USER_MANG', '1604399020577', '1', '超级管理员', '1', '南斗信息', '1604399020577', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource_data_scope` VALUES ('2ce8b83aedd738fb648a48439f2f6190', '仅本人数据权限', '1', '0', '', 'USER_MANG', '1604399020568', '1', '超级管理员', '1', '南斗信息', '1604399020568', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource_data_scope` VALUES ('2f5f047053f00b6e2fb6eface57d6b0d', '本部门', '10', '0', '', 'DEV_WORKBENCH', '1608103423663', '1', '超级管理员', '1', '南斗信息', '1608103423663', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource_data_scope` VALUES ('a0c10100d1821e9329a3ab212130bd7d', '全部数据权限', '1000', '0', '', 'USER_MANG', '1604399020583', '1', '超级管理员', '1', '南斗信息', '1604399020583', '1', '超级管理员', '1', '南斗信息', '0');
INSERT INTO `fdp_sys_resource_data_scope` VALUES ('b7ee9362c500a8d9be2d88269c83cf06', '本区域', '1000', '0', '', 'DEV_WORKBENCH', '1608103423666', '1', '超级管理员', '1', '南斗信息', '1608103423666', '1', '超级管理员', '1', '南斗信息', '0');

-- ----------------------------
-- Table structure for temp
-- ----------------------------
DROP TABLE IF EXISTS `temp`;
CREATE TABLE `temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `louhao` varchar(255) DEFAULT NULL,
  `danyuan` varchar(255) DEFAULT NULL,
  `fanghao` varchar(255) DEFAULT NULL,
  `mianji` varchar(255) DEFAULT NULL,
  `xingming` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of temp
-- ----------------------------

DROP TABLE IF EXISTS `fdp_sys_visit`;
CREATE TABLE `fdp_sys_visit` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `module` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `view_time` bigint(64) NOT NULL COMMENT '查看时间',
  `source` varchar(100) DEFAULT NULL COMMENT '来源',
  `ip` varchar(100) DEFAULT '' COMMENT 'IP',
  `terminal_info` longtext COMMENT '终端信息',
  `fdp_core` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否核心',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='访问次数';

-- ----------------------------
-- View structure for fdp_sys_notice_receiver_v
-- ----------------------------
DROP VIEW IF EXISTS `fdp_sys_notice_receiver_v`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fdp_sys_notice_receiver_v` AS select `receiver`.`id` AS `id`,`receiver`.`notice_id` AS `notice_id`,`receiver`.`receiver_time` AS `receiver_time`,`receiver`.`receiver_user` AS `receiver_user`,`receiver`.`receiver_user_name` AS `receiver_user_name`,`receiver`.`state` AS `receiver_state`,`receiver`.`read_time` AS `read_time`,`notice`.`title` AS `title`,`notice`.`content` AS `content`,`notice`.`release_time` AS `release_time`,`notice`.`create_dept_name` AS `create_dept_name` from (`fdp_sys_notice_receiver` `receiver` left join `fdp_sys_notice` `notice` on((`notice`.`id` = `receiver`.`notice_id`))) where ((`notice`.`state` = 1) and (`notice`.`del` = 0)) ;
