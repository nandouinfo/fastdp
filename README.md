# 南斗·快速开发平台

## 平台简介

回顾多年开发历程，做了太多的重复工作。无数次的登录、注册、字典、组织、用户、权限等等业务功能的开发皆没有满意之作每每临时信手平凑，烦不胜烦。直到自己壮胆开办<a href="http://www.nandou-china.com" target="_blank">南斗信息</a>后在项目中日益感触到统一开发平台的重要性，以此痛定思痛借鉴各开发平
台取其精华剔除糟粕，整理出了此开发平台。以支撑后续各个项目快速搭建底层基石、统一规范、专注业务、快速交付。。平台可用于所有的Web应用程序，如网站管理后台，网站会员中心，CMS，CRM，OA。所有前端后台代码封装过后十分精简易上手，出错概率低。同时支持移动客户端访问。系统会陆续更新一些实用功能。

南斗·快速开发平台 是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用。

* 感谢 [layui] <a href="https://www.layui.com" target="_blank">www.layui.com</a> 后台主题 UI 框架。
* 阿里云折扣场：[<a href="https://www.aliyun.com/minisite/goods?userCode=oiyi33zx" target="_blank">点我进入</a>]，腾讯云秒杀场：[<a href="https://cloud.tencent.com/act/cps/redirect?redirect=1014&cps_key=d69a83bff196a661dd6393c80cdc1ea2&from=console" target="_blank">点我进入</a>]
* 阿里云优惠券：[<a href="https://www.aliyun.com/minisite/goods?userCode=oiyi33zx" target="_blank">点我领取</a>]，腾讯云优惠券：[<a href="https://cloud.tencent.com/act/cps/redirect?redirect=1040&cps_key=d69a83bff196a661dd6393c80cdc1ea2&from=console" target="_blank">点我领取</a>]


## 平台愿景

解决软件研发行业的技术壁垒，让普通研发工程师、产品经理、需求顾问、乃至客户自己可简单快速的搭建符合业务需求的软件应用，为信息化建设做出星点贡献


## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  组织管理：配置系统组织机构，树结构展现支持数据权限。
3.  职位管理：配置系统用户所属担任职务。
4.  资源管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
5.  OA审批  ：提交审批表、查看待办、已办、处理中的审批流程。
5.  工作流程：设计流程审批表、流程审批执行流程等（完全参考钉钉自行开发）。
5.  一物一码：创建唯一二维码，可自定义样式实现一物一码业务（具体业务须自行研发）。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  通知公告：系统通知公告信息发布维护。
8.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
9.  登录日志：系统登录日志记录查询包含登录异常。
10. 在线用户：当前系统中活跃用户状态监控。
11. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
12. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
13. 系统接口：根据业务代码自动生成相关的api接口文档。
14. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
15. 缓存监控：对系统的缓存查询，删除、清空等操作。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

* 18中UI风格任由选择

## 在线体验

演示地址：<a href="http://www.nandou-china.com" target="_blank">www.nandou-china.com</a>

- superAdmin/781215  


## 演示图

<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ee29d886db41605532c794b5fa742d8601a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4bfe970f2ac9f7e2edc50ed558a463331a8.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-7a1071e9debeb7531dd8f9fdaf94df05eb8.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-1901200408963dc86e2fc36616329d90579.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-cd819b1e89c0080530ecbe70bbabe7758d0.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-665a0eb440e2ee972cd2c4baff36955bc40.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-20e9af424f8929d69372b3184ef49a5b184.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-2a4bc7252bbf40e40155aecb5d84d3a10a1.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-baafba7858bf8f98715a43b0d428249b77d.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-0843f37fcf9f5999ab090848fd36b691cda.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8a2fae9532737081b60d9e5ead386423289.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-6a2bcdb5b7bbaa86392578e5ac11956e9e5.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-f5164703bb014990b7690f276d7312a9750.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-64abaa5203c0ae173d9e76c124a02cdb676.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-672fe031ba9d15abdb3ab8def34e9f27652.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ec7a6dd8bfabacc590476855af0b01fdb90.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-343620da793c3080e8bb110691f89b0b80a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-fb620703a1c48217c898c22b5b9b1ce78f1.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-23861f597c4ff6fe6020e9cef89d1041ec2.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-2415cb3dba72b513659b108ddb9e1124cd1.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-dc4d20012bdecb126f0ee9d880006a140e7.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-afa340e161dd40aac5629067255191b125c.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-243d088cad635ee2bb7bf60415646a967a4.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-878715df2a8073f3ad59cf2f4f59028893c.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d71f2f85d1d468ba954301ec8f730cc0573.png"/></td>
    </tr>
</table>

## 打包
    如果编译失败parent.relativePath问题则修改fastdp中 pom.xml  进行依次编译二次，后续不在需要  
        1、第一次编译
         <modules>
         </modules>
        2、第二次编译
         <modules>
            <module>fastdp-core</module>
            <module>fastdp-starter</module>
         </modules>
## 附件服务
    当前以直接java的方式返回文件，如果遇到视频的时候出现不能正常查看，可以使用nginx 做静态文件服务器解决，进行重定向到nginx

## 参考资料

   流程后端
        https://github.com/go-workflow/go-workflow

   流程前端
        https://github.com/go-workflow/workflow-ui

## 更新日志

    2021-11-11 主要修复流程bug

        1、表单项非必填时，不显示流程
        2、表单项包含省市区组件时保存失败(数据库字段长度不够)
              fdp_flow_procdef   -> config  ->long text
              fdp_flow_procinst   -> procdef_config  ->long text
        3、流程设计流程设计部分配置无效(金额、日期、时间)
        4、首页点击通知消息报错


## 开发平台交流群

QQ群： [加入QQ群] 1026165545
