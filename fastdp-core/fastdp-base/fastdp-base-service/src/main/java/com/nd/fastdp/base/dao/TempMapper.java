package com.nd.fastdp.base.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.base.model.entity.Temp;
import com.nd.fastdp.db.mybatis.wrapper.QueryWrapperX;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TempMapper extends BaseMapper<Temp> {

    default List<Temp> list(String type){

        return selectList(new QueryWrapperX<Temp>().eq("type", type));
    }
}
