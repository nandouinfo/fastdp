package com.nd.fastdp.base.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_base_material")
@Data
@Accessors(chain = true)
public class Material extends BaseEntity {

    private String name;

    private String remark;

    private Integer state;
    private Integer del;

    private Integer type;

    private Integer fdpCore;

}
