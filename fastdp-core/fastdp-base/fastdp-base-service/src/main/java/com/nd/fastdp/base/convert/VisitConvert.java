package com.nd.fastdp.base.convert;

import com.nd.fastdp.base.model.dto.visit.VisitAddParam;
import com.nd.fastdp.base.model.entity.Visit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VisitConvert {

    VisitConvert INSTANCE = Mappers.getMapper(VisitConvert.class);

    Visit from(VisitAddParam param);
}
