package com.nd.fastdp.base.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_visit")
@Data
@Accessors(chain = true)
public class Visit extends FdpEntity {

    private String module;

    private Long viewTime;

    private String source;

    private String ip;

    private String terminalInfo;

}
