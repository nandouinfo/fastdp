package com.nd.fastdp.base.service;

import com.nd.fastdp.base.convert.VisitConvert;
import com.nd.fastdp.base.dao.VisitMapper;
import com.nd.fastdp.base.model.dto.visit.VisitAddParam;
import com.nd.fastdp.base.model.entity.Visit;
import com.nd.fastdp.base.model.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisitServiceImpl implements VisitService {

    @Autowired
    private VisitMapper visitMapper;
    
    @Override
    public void add(VisitAddParam param) {

        Visit visit = VisitConvert.INSTANCE.from(param);

        visitMapper.insert(visit);
    }

}
