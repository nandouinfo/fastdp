package com.nd.fastdp.base.service;

import com.nd.fastdp.base.convert.TempConvert;
import com.nd.fastdp.base.dao.TempMapper;
import com.nd.fastdp.base.model.bo.temp.TempBO;
import com.nd.fastdp.base.model.dto.temp.TempAddParam;
import com.nd.fastdp.base.model.entity.Temp;
import com.nd.fastdp.base.model.service.TempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempServiceImpl implements TempService {

    @Autowired
    private TempMapper tempMapper;
    
    @Override
    public void add(TempAddParam param) {

        Temp temp = TempConvert.INSTANCE.from(param);

        tempMapper.insert(temp);
    }

    @Override
    public List<TempBO> list(String type) {

        List<Temp> list = tempMapper.list(type);

        return TempConvert.INSTANCE.to(list);
    }

}
