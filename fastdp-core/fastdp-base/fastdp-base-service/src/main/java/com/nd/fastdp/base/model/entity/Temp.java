package com.nd.fastdp.base.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("temp")
@Data
@Accessors(chain = true)
public class Temp extends FdpEntity {

    private String type;

    private String json;
}
