package com.nd.fastdp.base.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.base.model.entity.Visit;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitMapper extends BaseMapper<Visit> {

}
