package com.nd.fastdp.base.convert;

import com.nd.fastdp.base.model.bo.temp.TempBO;
import com.nd.fastdp.base.model.dto.temp.TempAddParam;
import com.nd.fastdp.base.model.entity.Temp;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TempConvert {

    TempConvert INSTANCE = Mappers.getMapper(TempConvert.class);

    Temp from(TempAddParam param);

    List<TempBO> to(List<Temp> temps);
}
