package com.nd.fastdp.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.base.convert.MaterialConvert;
import com.nd.fastdp.base.dao.MaterialMapper;
import com.nd.fastdp.base.model.bo.material.MaterialBO;
import com.nd.fastdp.base.model.dto.material.MaterialAddParam;
import com.nd.fastdp.base.model.dto.material.MaterialListParam;
import com.nd.fastdp.base.model.dto.material.MaterialModifyParam;
import com.nd.fastdp.base.model.dto.material.MaterialQueryParam;
import com.nd.fastdp.base.model.entity.Material;
import com.nd.fastdp.base.model.service.MaterialService;
import com.nd.fastdp.file.service.FileService;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialServiceImpl implements MaterialService {

    @Autowired
    private MaterialMapper materialMapper;

    @Autowired
    private FileService fileService;

    @Override
    public MaterialBO add(MaterialAddParam param) {

        Material material = MaterialConvert.INSTANCE.from(param);

        materialMapper.insert(material);

        fileService.updateBusId(param.getFile(), material.getId());

        return MaterialConvert.INSTANCE.to(material);
    }

    @Override
    public void del(DelParam param) {
        materialMapper.del(param);
    }

    @Override
    public MaterialBO modify(MaterialModifyParam param) {

        Material material = MaterialConvert.INSTANCE.from(param);

        fileService.updateBusId(param.getFile(), material.getId());

        materialMapper.updateById(material);

        return get(material.getId());
    }

    @Override
    public void modifyState(StateParam param) {
        materialMapper.updateState(param);
    }

    @Override
    public MaterialBO get(String id) {

        Material material = materialMapper.selectById(id);

        return MaterialConvert.INSTANCE.to(material);
    }

    @Override
    public List<MaterialBO> list(MaterialListParam param) {

        List<Material> materials =  materialMapper.list(param);

        return MaterialConvert.INSTANCE.to(materials);
    }

    @Override
    public Page<MaterialBO> page(MaterialQueryParam param) {

        IPage<Material> page =  materialMapper.page(param.getPage(), param);

        return MaterialConvert.INSTANCE.to(page);
    }
}
