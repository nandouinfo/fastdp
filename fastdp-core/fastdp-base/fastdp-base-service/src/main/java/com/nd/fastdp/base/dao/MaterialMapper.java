package com.nd.fastdp.base.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.base.model.dto.material.MaterialListParam;
import com.nd.fastdp.base.model.dto.material.MaterialQueryParam;
import com.nd.fastdp.base.model.entity.Material;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.bo.LoginUserDataScope;
import com.nd.fastdp.framework.pojo.bo.LoginUserMangDept;
import com.nd.fastdp.framework.pojo.constant.DataScopeEnum;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface MaterialMapper extends BaseMapper<Material> {

    String MODULE_CODE = "MATERIAL_MANG";

    default List<Material> list(MaterialListParam param){

        return selectList(queryWrapper(param));
    }

    default IPage<Material> page(IPage<Material> page, MaterialQueryParam param){

        return selectPage(page, queryWrapper(param));
    }

    default QueryWrapper<Material> queryWrapper(MaterialListParam param){

        QueryWrapper queryWrapper = new QueryWrapper<Material>().eq("del", DelStatusEnum.NORMAL.getValue())
                                            .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                            .eq(null != param.getState(), "state", param.getState())
                                            .eq(null != param.getType(), "type", param.getType())
                                            .orderByDesc("create_time");

        fullQuerytDataScope(param.getLoginUser(), queryWrapper);

        return queryWrapper;
    }

    default void del(DelParam param) {

        Material material = null;
        for (String id : param.getIds()) {

            material = new Material();

            material.setId(id);
            material.init(param);
            material.setDel(DelStatusEnum.DEL.getValue());

            updateById(material);
        }
    }

    default void updateState(StateParam param) {

        Material material = null;
        for (String id : param.getIds()) {

            material = new Material();

            material.setId(id);
            material.init(param);
            material.setState(param.getState());

            updateById(material);
        }
    }

    default void fullQuerytDataScope(LoginUser loginUser, QueryWrapper queryWrapper) {

        if (null != loginUser) {
            Integer dataScope = null;
            HashMap<String, LoginUserDataScope> dataScopeMap = loginUser.getDataScopeMap();
            LoginUserDataScope loginUserDataScope = dataScopeMap.get(MODULE_CODE);
            if (null == loginUserDataScope) {
                return;
            }
            String dataScopeStr = loginUserDataScope.getCode();
            if (dataScopeStr != null) {
                try {
                    dataScope = Integer.parseInt(dataScopeStr);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }

                int dataScopeBase = dataScope.intValue();

                if (DataScopeEnum.ALL.getValue().intValue() == dataScopeBase) {

                }else if (DataScopeEnum.SELF_DEPT.getValue().intValue() == dataScopeBase) {

                    queryWrapper.eq("create_dept", loginUser.getDept());

                } else if (DataScopeEnum.SELF.getValue().intValue() == dataScopeBase) {

                    queryWrapper.in("create_user", loginUser.getUserId());
                }
            }
        }
    }
}
