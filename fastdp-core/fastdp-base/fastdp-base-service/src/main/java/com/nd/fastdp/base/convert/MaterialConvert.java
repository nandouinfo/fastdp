package com.nd.fastdp.base.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.base.model.bo.material.MaterialBO;
import com.nd.fastdp.base.model.dto.material.MaterialAddParam;
import com.nd.fastdp.base.model.dto.material.MaterialModifyParam;
import com.nd.fastdp.base.model.entity.Material;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MaterialConvert {

    MaterialConvert INSTANCE = Mappers.getMapper(MaterialConvert.class);

    Material from(MaterialAddParam param);

    Material from(MaterialModifyParam param);

    MaterialBO to(Material material);

    List<MaterialBO> to(List<Material> materials);

    Page<MaterialBO> to(IPage<Material> materials);
}
