package com.nd.fastdp.base.model.dto.temp;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("临时信息添加参数")
@Data
@Accessors(chain = true)
public class TempAddParam extends AddParam implements Serializable {

    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value = "类型", required = true, example = "1")
    private Integer type;

    private String json;
}
