package com.nd.fastdp.base.model.bo.material;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class MaterialBO extends BaseBO implements Serializable {

    private String name;
    private String remark;
    private Integer state;

    private Integer type;

    private Integer fdpCore;
}
