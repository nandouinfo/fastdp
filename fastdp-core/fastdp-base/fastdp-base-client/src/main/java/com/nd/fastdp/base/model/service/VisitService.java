package com.nd.fastdp.base.model.service;

import com.nd.fastdp.base.model.dto.visit.VisitAddParam;

public interface VisitService {

    void add(VisitAddParam param);

}
