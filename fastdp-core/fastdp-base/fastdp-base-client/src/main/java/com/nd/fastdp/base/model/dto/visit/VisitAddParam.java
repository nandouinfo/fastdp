package com.nd.fastdp.base.model.dto.visit;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("访问信息添加参数")
@Data
@Accessors(chain = true)
public class VisitAddParam extends AddParam implements Serializable {

    private String module;

    @ApiModelProperty(hidden = true)
    private Long viewTime;

    private String source;

    @ApiModelProperty(hidden = true)
    private String ip;

    @ApiModelProperty(hidden = true)
    private String terminalInfo;
}
