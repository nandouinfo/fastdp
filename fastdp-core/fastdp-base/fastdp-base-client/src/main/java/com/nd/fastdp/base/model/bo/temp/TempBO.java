package com.nd.fastdp.base.model.bo.temp;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class TempBO extends BaseBO implements Serializable {

    private String type;

    private String json;
}
