package com.nd.fastdp.base.model.dto.material;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import com.nd.fastdp.framework.validator.InEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("素材修改参数")
@Data
@Accessors(chain = true)
public class MaterialModifyParam extends ModifyParam implements Serializable {

    private String bigAreaId;

    @ApiModelProperty(value = "ID", required = true, example = "C4CA4238A0B923820DCC509A6F75849B")
    @NotEmpty(message = "ID不能为空")
    private String id;

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value = "类型", required = true, example = "1")
    private Integer type;

    @ApiModelProperty(value = "备注", required = true, example = "测试备注")
    private String remark;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", required = true, example = "1")
    private Integer state;

    private String file;
}
