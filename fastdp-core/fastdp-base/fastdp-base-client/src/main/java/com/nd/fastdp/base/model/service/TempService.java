package com.nd.fastdp.base.model.service;

import com.nd.fastdp.base.model.bo.temp.TempBO;
import com.nd.fastdp.base.model.dto.temp.TempAddParam;

import java.util.List;

public interface TempService {

    void add(TempAddParam param);

    List<TempBO> list(String type);
}
