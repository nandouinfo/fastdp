package com.nd.fastdp.base.model.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.base.model.bo.material.MaterialBO;
import com.nd.fastdp.base.model.dto.material.MaterialAddParam;
import com.nd.fastdp.base.model.dto.material.MaterialListParam;
import com.nd.fastdp.base.model.dto.material.MaterialModifyParam;
import com.nd.fastdp.base.model.dto.material.MaterialQueryParam;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;

import java.util.List;

public interface MaterialService {

    MaterialBO add(MaterialAddParam param);

    void del(DelParam param);

    MaterialBO modify(MaterialModifyParam param);

    void modifyState(StateParam param);

    MaterialBO get(String id);

    List<MaterialBO> list(MaterialListParam param);

    Page<MaterialBO> page(MaterialQueryParam param);
}
