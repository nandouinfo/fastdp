package com.nd.fastdp.base.model.bo.visit;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class VisitBO extends BaseBO implements Serializable {

    private String module;

    private Long viewTime;

    private String source;

    private String ip;

    private String terminalInfo;
}
