package com.nd.fastdp.objcode.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nd.fastdp.db.mybatis.mapper.FdpBaseMapper;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.objcode.model.dto.ObjCodeDetailListParam;
import com.nd.fastdp.objcode.model.dto.ObjCodeListParam;
import com.nd.fastdp.objcode.model.entity.ObjCode;
import com.nd.fastdp.objcode.model.entity.ObjCodeDetail;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObjCodeDetailMapper extends FdpBaseMapper<ObjCodeDetail> {

    String MODULE_CODE = "OBJCODE_CREATE_MANG";

    default void delByObjCodeId(String objCodeId){

        delete(new QueryWrapper<ObjCodeDetail>().eq("obj_code_id" ,"objCodeId"));
    }

    default List<ObjCodeDetail> findByObjCodeId(String objCodeId){

        return selectList(new QueryWrapper<ObjCodeDetail>().eq("del", DelStatusEnum.NORMAL.getValue()).eq("obj_code_id" ,objCodeId));
    }

    default List<ObjCodeDetail> list(ObjCodeDetailListParam param){

        return selectList(queryWrapper(param));
    }

    default QueryWrapper<ObjCodeDetail> queryWrapper(ObjCodeDetailListParam param){

        QueryWrapper queryWrapper = new QueryWrapper<ObjCodeDetail>().eq("del", DelStatusEnum.NORMAL.getValue())
                .like(StringUtils.isNotEmpty(param.getNo()), "no", param.getNo())
                .eq(null != param.getState(), "state", param.getState())
                .eq("obj_code_id", param.getObjCodeId())
                .orderByAsc("no");

        filterDataAuthority(queryWrapper, MODULE_CODE, param.getLoginUser());

        return queryWrapper;
    }

    Integer count(@Param("param") ObjCodeDetailListParam param);

}
