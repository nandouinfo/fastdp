package com.nd.fastdp.objcode.convert;

import com.nd.fastdp.objcode.model.bo.ObjCodeDetailBO;
import com.nd.fastdp.objcode.model.entity.ObjCodeDetail;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ObjCodeDetailConvert {

    ObjCodeDetailConvert INSTANCE = Mappers.getMapper(ObjCodeDetailConvert.class);

    List<ObjCodeDetailBO> to(List<ObjCodeDetail> details);

    ObjCodeDetailBO to(ObjCodeDetail detail);
}
