package com.nd.fastdp.objcode.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.file.service.FileService;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.properties.FdpProperties;
import com.nd.fastdp.objcode.config.ObjCodeConfig;
import com.nd.fastdp.objcode.constant.ObjCodeItemStateEnum;
import com.nd.fastdp.objcode.constant.ObjCodeStateEnum;
import com.nd.fastdp.objcode.convert.ObjCodeConvert;
import com.nd.fastdp.objcode.convert.ObjCodeDetailConvert;
import com.nd.fastdp.objcode.dao.ObjCodeDetailMapper;
import com.nd.fastdp.objcode.dao.ObjCodeMapper;
import com.nd.fastdp.objcode.model.bo.ObjCodeBO;
import com.nd.fastdp.objcode.model.bo.ObjCodeDetailBO;
import com.nd.fastdp.objcode.model.dto.*;
import com.nd.fastdp.objcode.model.entity.ObjCode;
import com.nd.fastdp.objcode.model.entity.ObjCodeDetail;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ObjCodeServiceImpl implements ObjCodeService {

    @Autowired
    private ObjCodeMapper objCodeMapper;

    @Autowired
    private ObjCodeDetailMapper objCodeDetailMapper;

    @Autowired
    private ObjCodeConfig objCodeConfig;

    @Autowired
    private FileService fileService;

    @Autowired
    private FdpProperties fdpProperties;

    @Override
    @Transactional
    public ObjCodeBO add(ObjCodeAddParam param) {

        if(param.getTotal() > objCodeConfig.getMaxTotal()){
            throw new BusinessException("每次最多生成" + objCodeConfig.getMaxTotal() + "个二维码!");
        }

        if(param.getTotal() <= 0){
            throw new BusinessException("每次最少生成1个二维码!");
        }

        ObjCode objCode = ObjCodeConvert.INSTANCE.from(param);
        objCode.setState(ObjCodeStateEnum.CREATED.getValue());
        objCode.setSalt(objCodeConfig.getSalt());

        objCodeMapper.insert(objCode);

        String path = objCodeConfig.getTypePath(objCode.getType());
        for (long i = objCode.getStartVal(); i < objCode.getStartVal() + objCode.getTotal(); i++){

            ObjCodeDetail item = new ObjCodeDetail();

            item.setName(objCode.getName());
            item.setNo(objCode.getPrefix() + i);
            item.setObjCodeId(objCode.getId());

            item.setCreateDept(objCode.getCreateDept());
            item.setCreateDeptName(objCode.getCreateDeptName());
            item.setCreateUser(objCode.getCreateUser());
            item.setCreateUserName(objCode.getCreateUserName());

            item.setModifyDept(objCode.getModifyDept());
            item.setModifyDeptName(objCode.getModifyDeptName());
            item.setModifyUser(objCode.getModifyUser());
            item.setModifyUserName(objCode.getModifyUserName());

            item.setDel(DelStatusEnum.NORMAL.getValue());
            item.setState(ObjCodeItemStateEnum.WAIT_USE.getValue());
            item.setPath(path);

            objCodeDetailMapper.insert(item);
        }

        return ObjCodeConvert.INSTANCE.to(objCode);
    }

    /*@Override
    @Transactional
    public void createQrCode(String id) throws IOException {

        ObjCode objCode = objCodeMapper.selectById(id);

        if(objCode == null){
            throw new BusinessException("业务数据不存在");
        }

        List<File> files = new ArrayList<>();
        String url = objCodeConfig.getTypePath(objCode.getType());

        for (long i = objCode.getStartVal(); i < objCode.getStartVal() + objCode.getTotal(); i++){

            ObjCodeDetail item = new ObjCodeDetail();

            item.setName(objCode.getName());
            item.setNo(objCode.getPrefix() + i);
            item.setObjCodeId(objCode.getId());

            item.setCreateDept(objCode.getCreateDept());
            item.setCreateDeptName(objCode.getCreateDeptName());
            item.setCreateUser(objCode.getCreateUser());
            item.setCreateUserName(objCode.getCreateUserName());

            item.setModifyDept(objCode.getModifyDept());
            item.setModifyDeptName(objCode.getModifyDeptName());
            item.setModifyUser(objCode.getModifyUser());
            item.setModifyUserName(objCode.getModifyUserName());

            item.setDel(DelStatusEnum.NORMAL.getValue());
            item.setState(ObjCodeItemStateEnum.WAIT_USE.getValue());

            objCodeDetailMapper.insert(item);

            if(url.indexOf("?") > 0){
                url += "&objcodeItem=" + item.getId();
            }else{
                url += "?objcodeItem=" + item.getId();
            }

            File file = RqcodeUtil.create(objCode.getPrefix() + i, objCode.getWidth().intValue(), objCode.getHeight().intValue(), url);
            files.add(file);
        }

    }*/

    @Override
    public void del(DelParam param) {
        objCodeMapper.del(param);
    }

    @Override
    public void modifyState(StateParam param) {
        objCodeMapper.updateState(param);
    }

    @Override
    public void used(ObjCodeUsedParam param) {

        if(StringUtils.isEmpty(param.getId())){
            throw new BusinessException("无效二维码");
        }

        ObjCodeDetail target = objCodeDetailMapper.selectById(param.getId());

        if(target == null){
            throw new BusinessException("无效二维码");
        }

        ObjCodeDetail used = new ObjCodeDetail();
        used.setId(param.getId());
        used.setState(ObjCodeItemStateEnum.USED.getValue());
        used.setUsedTime(System.currentTimeMillis());

        if(param.getLoginUser() != null){
            used.setUsedUser(param.getLoginUser().getUserId());
            used.setUsedUserName(param.getLoginUser().getName());
            used.setUsedDept(param.getLoginUser().getDept());
            used.setUsedDeptName(param.getLoginUser().getDeptName());
        }

        ObjCode objCode = objCodeMapper.selectById(target.getObjCodeId());

        ObjCode targetObjCode = new ObjCode();
        targetObjCode.setId(objCode.getId());

        if(objCode.getUsedCount() == null){
            targetObjCode.setUsedCount(1);
        }else{
            targetObjCode.setUsedCount(objCode.getUsedCount() + 1);
        }

        objCodeMapper.updateById(targetObjCode);

        objCodeDetailMapper.updateById(used);
    }

    @Override
    public ObjCodeBO get(String id) {

        ObjCode objCode = objCodeMapper.selectById(id);

        return ObjCodeConvert.INSTANCE.to(objCode);
    }

    @Override
    public ObjCodeDetailBO getDetail(String id) {

        ObjCodeDetail codeDetail = objCodeDetailMapper.selectById(id);

        return ObjCodeDetailConvert.INSTANCE.to(codeDetail);
    }

    @Override
    public List<ObjCodeBO> list(ObjCodeListParam param) {

        List<ObjCode> objCodes = objCodeMapper.list(param);

        return ObjCodeConvert.INSTANCE.to(objCodes);
    }

    @Override
    public List<ObjCodeDetailBO> list(ObjCodeDetailListParam param) {

        List<ObjCodeDetail> list = objCodeDetailMapper.list(param);

        return ObjCodeDetailConvert.INSTANCE.to(list);
    }

    @Override
    public List<ObjCodeDetailBO> findById(String objCodeId) {

        List<ObjCodeDetail> list = objCodeDetailMapper.findByObjCodeId(objCodeId);

        return ObjCodeDetailConvert.INSTANCE.to(list);
    }

    @Override
    public Page<ObjCodeBO> page(ObjCodeQueryParam param) {

        IPage<ObjCode> objCodes = objCodeMapper.page(param);

        return ObjCodeConvert.INSTANCE.to(objCodes);
    }

    @Override
    public Integer count(ObjCodeListParam param) {
        return objCodeMapper.count(param);
    }

    @Override
    public Integer codeCount(ObjCodeListParam param) {
        return objCodeMapper.codeCount(param);
    }

    @Override
    public Integer codeUsedCount(ObjCodeListParam param) {
        return objCodeMapper.codeUsedCount(param);
    }

    @Override
    public Integer count(ObjCodeDetailListParam param) {
        return objCodeDetailMapper.count(param);
    }
}
