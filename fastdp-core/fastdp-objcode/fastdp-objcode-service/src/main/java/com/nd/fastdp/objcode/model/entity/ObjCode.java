package com.nd.fastdp.objcode.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_base_objcode")
@Data
@Accessors(chain = true)
public class ObjCode extends BaseEntity {

    private String name;
    private String type;
    private String prefix;//前缀
    private Long startVal;//起始值
    private Integer total;//总数

    private Integer hasDesc;
    private String descBackColor;

    private Integer usedCount;//已使用数量

    private String remark;

    private Integer state;
    private Integer del;

    private String salt;

    private Integer fdpCore;

}
