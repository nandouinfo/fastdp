package com.nd.fastdp.objcode.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.objcode.model.bo.ObjCodeBO;
import com.nd.fastdp.objcode.model.dto.ObjCodeAddParam;
import com.nd.fastdp.objcode.model.entity.ObjCode;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ObjCodeConvert {

    ObjCodeConvert INSTANCE = Mappers.getMapper(ObjCodeConvert.class);

    ObjCode from(ObjCodeAddParam param);

    ObjCodeBO to(ObjCode ObjCode);

    List<ObjCodeBO> to(List<ObjCode> ObjCodes);

    Page<ObjCodeBO> to(IPage<ObjCode> ObjCodes);
}
