package com.nd.fastdp.objcode.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_base_objcode_detail")
@Data
@Accessors(chain = true)
public class ObjCodeDetail extends BaseEntity {

    private String objCodeId;

    private String name;
    private String no;//编号

    private String path;//地址

    private Long usedTime;
    private String usedUser;
    private String usedUserName;
    private String usedDept;
    private String usedDeptName;

    private String remark;

    private Integer state;
    private Integer del;

    private Integer fdpCore;

}
