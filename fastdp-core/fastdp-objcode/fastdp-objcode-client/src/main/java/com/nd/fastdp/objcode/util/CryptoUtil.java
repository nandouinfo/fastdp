package com.nd.fastdp.objcode.util;

import cn.hutool.crypto.SecureUtil;

public class CryptoUtil {

    /**
     * 校验是否有效
     * @param content
     * @param salt
     * @param timeStamp
     * @param ciphertext
     * @return
     */
    public static boolean check(String content, String salt, Long timeStamp, String ciphertext){

        return SecureUtil.md5(content + "_" + salt + "_" + timeStamp).equals(ciphertext);
    }

    /**
     * 生成加密字符串
     * @param content
     * @param timeStamp
     * @param salt
     * @return
     */
    public static String encrypt(String content, Long timeStamp, String salt){

        return SecureUtil.md5(content + "_" + salt + "_" + timeStamp);
    }
}
