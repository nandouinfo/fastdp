package com.nd.fastdp.objcode.model.bo;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ObjCodeDetailBO extends BaseBO implements Serializable {

    private String objCodeId;

    private String name;
    private String no;//编号

    private String path;//地址

    private Long usedTime;
    private String usedUser;
    private String usedUserName;
    private String usedDept;
    private String usedDeptName;

    private String remark;

    private Integer state;
    private Integer del;

    private Integer fdpCore;

}
