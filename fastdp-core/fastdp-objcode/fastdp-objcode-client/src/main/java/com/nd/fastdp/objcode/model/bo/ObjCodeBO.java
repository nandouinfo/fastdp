package com.nd.fastdp.objcode.model.bo;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ObjCodeBO extends BaseBO implements Serializable {

    private String name;
    private String type;

    private String prefix;//前缀
    private Long startVal;//起始值
    private Integer total;//总数

    private Integer hasDesc;
    private String descBackColor;

    private Integer usedCount;//已使用数量

    private String remark;

    private Integer state;

    private String salt;

    private Integer fdpCore;

}
