package com.nd.fastdp.objcode.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
@AllArgsConstructor
public enum ObjCodeItemStateEnum implements BaseEnum {

    WAIT_USE(1, "待使用"),
    USED(2, "已使用");

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    public static String getDesc(Integer value) {
        if (null == value) {
            return "";
        }
        ObjCodeItemStateEnum[] base = values();
        for (int i = 0; i < base.length; i++) {
            if (base[i].getValue().intValue() == value.intValue()) {
                return base[i].getDesc();
            }
        }
        return "";
    }

    public static String getDesc(String value) {

        if (StringUtils.isBlank(value)) {
            return "";
        }

        return getDesc(Integer.valueOf(value));
    }

    public static Integer getValue(String desc) {

        if (StringUtils.isBlank(desc)) {
            return null;
        }
        ObjCodeItemStateEnum[] base = values();
        for (int i = 0; i < base.length; i++) {
            if (base[i].getDesc().equals(desc)) {
                return base[i].getValue();
            }
        }

        return null;
    }
}




