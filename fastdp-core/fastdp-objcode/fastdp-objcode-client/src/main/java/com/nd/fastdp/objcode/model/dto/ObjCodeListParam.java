package com.nd.fastdp.objcode.model.dto;

import com.nd.fastdp.framework.pojo.dto.ListParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("一物一码列表参数")
@Data
@Accessors(chain = true)
public class ObjCodeListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "名称", example = "测试")
    private String name;

    private Integer state;
}
