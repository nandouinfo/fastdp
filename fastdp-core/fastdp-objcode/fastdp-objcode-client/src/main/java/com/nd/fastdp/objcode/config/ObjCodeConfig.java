package com.nd.fastdp.objcode.config;

import com.nd.fastdp.sys.model.bo.dict.DictItemBO;
import com.nd.fastdp.sys.service.DictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ObjCodeConfig {

    @Autowired
    DictItemService dictItemService;

    private static final String DICT_CODE = "OBJ_CODE_CONFIG";

    public Integer getMaxTotal(){

        Integer result = 1000;

        List<DictItemBO> configs = dictItemService.listAllEnable(DICT_CODE);
        for (DictItemBO config : configs){
            if("MAX_TOTAL".equals(config.getName())){
                result = Integer.parseInt(config.getValue().trim());
                break;
            }
        }

        return result;
    }

    public String getDefaultBackColor(){

        String result = "#1E9FFF";

        List<DictItemBO> configs = dictItemService.listAllEnable(DICT_CODE);
        for (DictItemBO config : configs){
            if("DESC_BACK_COLOR".equals(config.getName())){
                result = config.getValue().trim();
                break;
            }
        }

        return result;
    }

    public String getDefaultPath(){

        String result = "";

        List<DictItemBO> configs = dictItemService.listAllEnable(DICT_CODE);
        for (DictItemBO config : configs){
            if("DEFAULT_PATH".equals(config.getName())){
                result = config.getValue().trim();
                break;
            }
        }

        return result;
    }

    public String getSalt(){

        String result = "";

        List<DictItemBO> configs = dictItemService.listAllEnable(DICT_CODE);
        for (DictItemBO config : configs){
            if("SALT".equals(config.getName())){
                result = config.getValue().trim();
                break;
            }
        }

        return result;
    }

    public String getTypePath(String type){

        String result = "";

        List<DictItemBO> configs = dictItemService.listAllEnable("OBJ_CODE_TYPE");
        for (DictItemBO config : configs){
            if(type.equals(config.getValue())){
                result = config.getRemark();
                break;
            }
        }

        return result;
    }

}
