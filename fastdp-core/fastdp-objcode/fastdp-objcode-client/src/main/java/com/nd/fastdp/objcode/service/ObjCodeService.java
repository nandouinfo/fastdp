package com.nd.fastdp.objcode.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.objcode.model.bo.ObjCodeBO;
import com.nd.fastdp.objcode.model.bo.ObjCodeDetailBO;
import com.nd.fastdp.objcode.model.dto.*;

import java.util.List;

public interface ObjCodeService {

    ObjCodeBO add(ObjCodeAddParam param);

    void del(DelParam param);

    void modifyState(StateParam param);

    ObjCodeBO get(String id);

    ObjCodeDetailBO getDetail(String id);

    void used(ObjCodeUsedParam param);

    List<ObjCodeBO> list(ObjCodeListParam param);

    List<ObjCodeDetailBO> list(ObjCodeDetailListParam param);

    List<ObjCodeDetailBO> findById(String objCodeId);

    Page<ObjCodeBO> page(ObjCodeQueryParam param);

    Integer count(ObjCodeListParam param);

    Integer codeCount(ObjCodeListParam param);

    Integer codeUsedCount(ObjCodeListParam param);

    Integer count(ObjCodeDetailListParam param);
}
