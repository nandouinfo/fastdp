package com.nd.fastdp.objcode.model.dto;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("一物一码使用参数")
@Data
@Accessors(chain = true)
public class ObjCodeUsedParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "二维码Id", required = true, example = "1")
    @NotEmpty(message = "二维码Id不能为空")
    private String id;
}
