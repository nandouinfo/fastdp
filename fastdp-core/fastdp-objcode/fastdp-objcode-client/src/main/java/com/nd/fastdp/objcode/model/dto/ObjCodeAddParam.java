package com.nd.fastdp.objcode.model.dto;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("一物一码生成参数")
@Data
@Accessors(chain = true)
public class ObjCodeAddParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "类型", required = true, example = "1")
    @NotEmpty(message = "类型不能为空")
    private String type;

    @ApiModelProperty(value = "前缀", required = true, example = "ZCXY")
    @NotEmpty(message = "前缀不能为空")
    private String prefix;

    @ApiModelProperty(value = "起始值", required = true, example = "20200228001")
    @NotEmpty(message = "起始值不能为空")
    private Long startVal;

    @ApiModelProperty(value = "总数", required = true, example = "10")
    @NotEmpty(message = "总数不能为空")
    private Integer total;

    @ApiModelProperty(value = "是否存在描述信息", example = "1")
    private Integer hasDesc;

    @ApiModelProperty(value = "描述信息背景颜色", example = "#FFFFFF")
    private String descBackColor;

    @ApiModelProperty(value = "备注", required = true, example = "测试备注")
    private String remark;

}
