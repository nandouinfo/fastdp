package com.nd.fastdp.objcode.model.dto;

import com.nd.fastdp.framework.pojo.dto.ListParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("一物一码详情列表参数")
@Data
@Accessors(chain = true)
public class ObjCodeDetailListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "objCodeId", example = "1")
    private String objCodeId;

    @ApiModelProperty(value = "编号", example = "1")
    private String no;

    @ApiModelProperty(value = "状态", example = "1")
    private Integer state;
}
