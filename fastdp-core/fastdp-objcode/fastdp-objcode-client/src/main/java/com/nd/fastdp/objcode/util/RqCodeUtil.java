package com.nd.fastdp.objcode.util;

import cn.hutool.core.img.Img;
import cn.hutool.core.img.ImgUtil;
import com.nd.fastdp.framework.pojo.constant.YestNoEnum;
import com.nd.fastdp.objcode.model.bo.ObjCodeBO;
import com.nd.fastdp.objcode.model.bo.ObjCodeDetailBO;
import com.nd.fastdp.utils.RqcodeUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class RqCodeUtil {

    public static File simpleRqCode(String fileName, String content) throws IOException {

        File file = null;
        Integer width = 300;
        Integer height = 300;

        file = RqcodeUtil.create(fileName, width, height, content);

        return file;
    }

    public static File simpleRqCode(String content) throws IOException {

        File file = null;
        Integer width = 300;
        Integer height = 300;

        file = RqcodeUtil.create(UUID.randomUUID().toString(), width, height, content);

        return file;
    }

    public static File hasDescRqCode(String title, String no, String content, String descBackColor) throws IOException {

        String fileName = title + "_" + no;

        File file = null;
        Integer width = 300;
        Integer height = 300;

        file = RqcodeUtil.create(fileName, width, height, 5,6, content);

        file = addDescText(file, title, no, descBackColor);


        return file;
    }

    private static File addDescText(File file, String name, String no, String descBackColor) throws IOException {

        Integer width = 300;
        Integer height = 420;

        File img = File.createTempFile(name + "_" + no + "_" + UUID.randomUUID().toString(), "." + ImgUtil.IMAGE_TYPE_PNG);
        img.deleteOnExit();

        BufferedImage bufferedImage = ImageIO.read(file);

        BufferedImage addDescTextPanelImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics garphics = addDescTextPanelImage.createGraphics();
        garphics.drawImage(bufferedImage, 0, 0, width, width, null);

        garphics.setColor(Color.decode(descBackColor));

        garphics.fillRect(0, 300, 300, 150);

        ImageIO.write(addDescTextPanelImage, ImgUtil.IMAGE_TYPE_PNG, img);

        Img.from(img).pressText(name, Color.WHITE, //文字
                new Font("黑体", Font.BOLD, 28), //字体
                0, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                150, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                1f )//透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
                .pressText("NO." + no, Color.WHITE, //文字
                        new Font("黑体", Font.BOLD, 18), //字体
                        0, //x坐标修正值。 默认在中间，偏移量相对于中间偏移
                        180, //y坐标修正值。 默认在中间，偏移量相对于中间偏移
                        1f)//透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
                .round(0.2).write(img);

        if(file.exists()){
            file.delete();
        }

        return img;
    }
}
