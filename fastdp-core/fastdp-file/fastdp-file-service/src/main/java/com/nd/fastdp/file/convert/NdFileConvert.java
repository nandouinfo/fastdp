package com.nd.fastdp.file.convert;

import com.nd.fastdp.file.model.bo.FileBO;
import com.nd.fastdp.file.model.entity.NdFile;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface NdFileConvert {

    NdFileConvert INSTANCE = Mappers.getMapper(NdFileConvert.class);

    FileBO to(NdFile ndfile);

    List<FileBO> to(List<NdFile> files);

}
