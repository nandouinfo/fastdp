package com.nd.fastdp.file.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @Auther: 刘畅
 * @Date: 2019-03-08 21:01
 * @Description: 文件实体
 */
@Data
@TableName("fdp_file")
@Accessors(chain = true)
public class NdFile {

    public static final String DEFAULT_TYPE = "1";

    @TableId(type = IdType.UUID)
    private String id;
    private String busId;
    private String busType;
    private String path;
    private String originalName;
    private Integer state;

    private Long createTime;

}
