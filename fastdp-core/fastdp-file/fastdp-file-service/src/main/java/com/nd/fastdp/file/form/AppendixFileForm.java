package com.nd.fastdp.file.form;

import lombok.Data;

import java.util.List;

/**
 * @Auther: 刘畅
 * @Date: 2019-04-07 11:24
 * @Description:
 */
@Data
public class AppendixFileForm {

    List<String> ids;
    String busType;
    String busId;

}
