package com.nd.fastdp.file.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.file.model.entity.NdFile;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileMapper extends BaseMapper<NdFile> {

    void updateState(StateParam param);

    void unBindBusId(String id);

    List<NdFile> findByTemp(Long handleTime);

    List<NdFile> findByTypeAndBusinessId(@Param("type") String type, @Param("busId") String busId);

    void delByTypeAndBusId(@Param("type") String type, @Param("busId") String busId);
}
