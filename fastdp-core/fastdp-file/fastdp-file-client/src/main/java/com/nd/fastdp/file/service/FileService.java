package com.nd.fastdp.file.service;

import com.nd.fastdp.file.model.bo.FileBO;
import com.nd.fastdp.file.model.dto.FileAddParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;

import java.io.File;
import java.util.List;

/**
 * @Auther: 刘畅
 */
public interface FileService {

    FileBO add(FileAddParam param);

    void updateBusId(List<String> ids, String busId, String type);

    void updateBusId(List<String> ids, String busId);

    void updateBusId(String id, String busId, String type);

    void updateBusId(String id, String busId);

    void updateState(StateParam param);

    void delByTypeAndBusId(String type, String busId);

    List<FileBO> findByTypeAndBusId(String type, String busId);

    List<FileBO> findById(List<String> ids);

    FileBO get(String id);

    File file(String id);

    byte[] fileStream(String id);

    void clearTemp();
}
