package com.nd.fastdp.file.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCodeEnum implements BaseEnum {

    // ========== 文件 模块 ==========
    UPLOAD_ISNULL_ERROR("1007001001", "请选择上传文件"),
    UPLOAD_INVALID_ERROR("1007001002","文件已失效请重新上传"),
    SOURCE_NOT_EXIST_ERROR("1007001003","资源不存在");

    private String value;
    private String desc;

}
