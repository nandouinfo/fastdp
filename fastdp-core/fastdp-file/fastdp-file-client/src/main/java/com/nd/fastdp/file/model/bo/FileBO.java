package com.nd.fastdp.file.model.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * @Auther: 刘畅
 */
@Data
@Accessors(chain = true)
public class FileBO implements Serializable {

    private String id;

    private String busId;

    private String busType;

    private String path;

    private String originalName;
}
