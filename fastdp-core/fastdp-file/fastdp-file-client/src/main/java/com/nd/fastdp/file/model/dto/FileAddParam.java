package com.nd.fastdp.file.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("文件上传业务 DTO")
public class FileAddParam implements Serializable {

    @NotEmpty(message = "文件不能为空")
    @ApiModelProperty(value = "文件", required = true)
    private byte[] file;

    @NotEmpty(message = "文件原始名称不能为空")
    @ApiModelProperty(value = "文件原始名称", required = true, example = "测试文件")
    private String originalFilename;
}
