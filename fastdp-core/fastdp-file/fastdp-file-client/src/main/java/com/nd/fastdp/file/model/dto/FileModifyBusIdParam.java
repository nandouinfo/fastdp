package com.nd.fastdp.file.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("文件绑定业务 DTO")
public class FileModifyBusIdParam implements Serializable {

    @NotEmpty(message = "文件ID不能为空")
    @ApiModelProperty(value = "文件ID", required = true, example = "1")
    private List<String> ids;

    @NotEmpty(message = "文件类型不能为空")
    @ApiModelProperty(value = "文件类型", required = true, example = "BANNER")
    private String busType;

    @NotEmpty(message = "业务ID不能为空")
    @ApiModelProperty(value = "业务ID", required = true, example = "1")
    private String busId;
}
