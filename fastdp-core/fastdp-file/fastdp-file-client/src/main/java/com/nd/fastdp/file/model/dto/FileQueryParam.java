package com.nd.fastdp.file.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("文件查询业务 DTO")
public class FileQueryParam implements Serializable {

    @NotEmpty(message = "业务ID不能为空")
    @ApiModelProperty(value = "业务ID", required = true)
    private String busId;

    @NotEmpty(message = "业务类型不能为空")
    @ApiModelProperty(value = "业务类型", required = true, example = "BANNER")
    private String busType;
}
