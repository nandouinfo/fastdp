package com.nd.fastdp.file.convert;

import com.nd.fastdp.file.model.bo.FileBO;
import com.nd.fastdp.file.vo.FileVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FileConvert {

    FileConvert INSTANCE = Mappers.getMapper(FileConvert.class);

    FileVO to(FileBO bo);

    List<FileVO> to(List<FileBO> bo);
}
