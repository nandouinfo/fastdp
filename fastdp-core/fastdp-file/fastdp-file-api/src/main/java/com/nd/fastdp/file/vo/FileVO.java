package com.nd.fastdp.file.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Auther: 刘畅
 */
@Data
@Accessors(chain = true)
public class FileVO implements Serializable {

    private String id;

    private String originalName;
}
