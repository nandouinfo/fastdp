package com.nd.fastdp.org.convert;

import com.nd.fastdp.org.model.bo.role.RoleDataScopeBO;
import com.nd.fastdp.org.model.entity.RoleDataScope;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoleDataScopeConvert {

    RoleDataScopeConvert INSTANCE = Mappers.getMapper(RoleDataScopeConvert.class);

    List<RoleDataScopeBO> to(List<RoleDataScope> roleDataScopes);
}
