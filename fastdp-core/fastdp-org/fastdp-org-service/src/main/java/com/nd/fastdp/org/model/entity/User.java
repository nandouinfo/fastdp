package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_user")
@Data
@Accessors(chain = true)
public class User extends BaseEntity {

    private String name;
    private String simpleName; //简称
    private String code; //编码

    private Integer sex;

    private String type;
    private String dept;

    private String employeeType;

    private String employeePosition;

    private String landline;//固定电话
    private String phone;//电话
    private String email;//邮箱

    private String birthday;//出生年月日
    private String idcard;//身份证号码

    private String nation;//民族字典编码
    private String nativeplace;//籍贯
    private String address;//地址

    private String degree;//文化程度字典编码
    private Integer married;//婚姻状态

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    private Long entryTime;//入职时间
    private Long leaveTime;//离职时间

    private String contact;//紧急联系人
    private String contactPhone;//紧急联系人电话

    private Integer fdpCore;

}
