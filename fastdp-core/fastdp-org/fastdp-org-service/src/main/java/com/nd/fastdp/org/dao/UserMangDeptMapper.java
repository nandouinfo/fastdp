package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.org.model.entity.Dept;
import com.nd.fastdp.org.model.entity.UserMangDept;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMangDeptMapper extends BaseMapper<UserMangDept> {

    default void del(String userId) {

        delete(new QueryWrapper<UserMangDept>().eq("user_id", userId));
    }

    List<Dept> findByUserId(String userId);

    default List<UserMangDept> findByDept(String deptId){

        return selectList(new QueryWrapper<UserMangDept>().eq("dept_id", deptId));
    }
}
