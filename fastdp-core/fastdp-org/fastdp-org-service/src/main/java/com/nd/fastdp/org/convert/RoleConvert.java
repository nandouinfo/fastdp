package com.nd.fastdp.org.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.org.model.bo.role.RoleBO;
import com.nd.fastdp.org.model.dto.role.RoleAddParam;
import com.nd.fastdp.org.model.dto.role.RoleModifyParam;
import com.nd.fastdp.org.model.entity.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoleConvert {

    RoleConvert INSTANCE = Mappers.getMapper(RoleConvert.class);

    Role from(RoleAddParam param);

    Role from(RoleModifyParam param);

    RoleBO to(Role role);

    List<RoleBO> to(List<Role> roles);

    Page<RoleBO> to(IPage<Role> roles);
}
