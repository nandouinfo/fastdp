package com.nd.fastdp.org.convert;

import com.nd.fastdp.org.model.bo.user.UserRoleBO;
import com.nd.fastdp.org.model.entity.UserRole;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserRoleConvert {

    UserRoleConvert INSTANCE = Mappers.getMapper(UserRoleConvert.class);

    List<UserRoleBO> to(List<UserRole> userRoles);
}
