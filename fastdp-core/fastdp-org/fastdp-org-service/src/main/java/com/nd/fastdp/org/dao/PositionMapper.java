package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.dto.position.PositionListParam;
import com.nd.fastdp.org.model.dto.position.PositionQueryParam;
import com.nd.fastdp.org.model.entity.Position;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionMapper extends BaseMapper<Position> {

    default void del(DelParam param) {

        Position position = null;
        for (String id : param.getIds()) {

            position = new Position();

            position.setId(id);
            position.init(param);
            position.setDel(DelStatusEnum.DEL.getValue());

            updateById(position);
        }
    }

    default void updateState(StateParam param) {

        Position position = null;
        for (String id : param.getIds()) {

            position = new Position();

            position.setId(id);
            position.init(param);
            position.setState(param.getState());

            updateById(position);
        }
    }

    default Position getByName(String name){

        return selectOne(new QueryWrapper<Position>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                 .eq("name", name));
    }

    default boolean isExist(String name){

        return getByName(name) != null;
    }

    default boolean isExist(String id, String name){

        Position resource = selectOne(new QueryWrapper<Position>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                           .ne("id", id)
                                                           .eq("name", name));

        return resource != null;
    }

    List<Position> findByUserId(@Param("userId") String userId);

    default List<Position> listAllEnable(){

        return selectList(new QueryWrapper<Position>().eq("del", DelStatusEnum.NORMAL_VAL).eq("state", StatusEnum.ENABLE_VAL).orderByAsc("sort"));
    }

    default List<Position> listAll(){

        return selectList(new QueryWrapper<Position>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }

    default List<Position> list(PositionListParam param){

        return selectList(new QueryWrapper<Position>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                    .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                    .eq(null != param.getState(), "state", param.getState())
                                                    .orderByAsc("sort"));
    }

    default IPage<Position> page(IPage<Position> page, PositionQueryParam param){

        return selectPage(page, new QueryWrapper<Position>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                        .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                        .eq(null != param.getState(), "state", param.getState())
                                                        .orderByAsc("sort"));
    }
}
