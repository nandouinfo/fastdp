package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_user_mang_dept")
@Data
@Accessors(chain = true)
public class UserMangDept extends FdpEntity {

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 部门ID
     */
    private String deptId;

}
