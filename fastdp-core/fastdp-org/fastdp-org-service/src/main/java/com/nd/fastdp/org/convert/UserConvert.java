package com.nd.fastdp.org.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.org.model.bo.user.UserBO;
import com.nd.fastdp.org.model.dto.user.UserAddParam;
import com.nd.fastdp.org.model.dto.user.UserModifyParam;
import com.nd.fastdp.org.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserConvert {

    UserConvert INSTANCE = Mappers.getMapper(UserConvert.class);

    User from(UserAddParam param);

    User from(UserModifyParam param);

    UserBO to(User user);

    List<UserBO> to(List<User> users);

    Page<UserBO> to(IPage<User> users);
}
