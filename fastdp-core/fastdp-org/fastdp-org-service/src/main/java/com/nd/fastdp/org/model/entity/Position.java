package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_position")
@Data
@Accessors(chain = true)
public class Position extends BaseEntity {

    private String name;
    private String code;

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;


    private Integer fdpCore;


}
