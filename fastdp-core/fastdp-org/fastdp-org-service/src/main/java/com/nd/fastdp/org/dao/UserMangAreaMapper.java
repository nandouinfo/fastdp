package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.org.model.entity.UserMangArea;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMangAreaMapper extends BaseMapper<UserMangArea> {

    default void del(String userId) {

        delete(new QueryWrapper<UserMangArea>().eq("user_id", userId));
    }

    default List<UserMangArea> findByAreaId(String areaId){

        return selectList(new QueryWrapper<UserMangArea>().eq("area_id", areaId));
    }
}
