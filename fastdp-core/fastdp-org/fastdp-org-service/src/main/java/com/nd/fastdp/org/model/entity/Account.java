package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_account")
@Data
@Accessors(chain = true)
public class Account extends BaseEntity {

    private String account;
    private String password;//密码

    private String Salt;//密码加密盐

    private Integer online;//在线状态
    private Integer locked;//锁定状态
    private Long loginTime;//登录时间
    private Integer loginNumber;//登录次数

    private String userId;//关联用户ID

    private Integer fdpCore;

}
