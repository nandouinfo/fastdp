package com.nd.fastdp.org.service;

import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.org.constant.LockedEnum;
import com.nd.fastdp.org.constant.OrgRespCodeNum;
import com.nd.fastdp.org.convert.AccountConvert;
import com.nd.fastdp.org.dao.AccountMapper;
import com.nd.fastdp.org.model.bo.account.AccountBO;
import com.nd.fastdp.org.model.dto.account.AccountAddParam;
import com.nd.fastdp.org.model.dto.account.AccountUpdatePwdParam;
import com.nd.fastdp.org.model.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public AccountBO getByAccount(String account) {

        return AccountConvert.INSTANCE.to(accountMapper.getByAccount(account));
    }

    @Override
    public void add(AccountAddParam param) {

        Account account =AccountConvert.INSTANCE.from(param);
        account.setLocked(LockedEnum.NORMAL.getValue());

        accountMapper.insert(account);
    }

    @Override
    public void modify(AccountUpdatePwdParam param) {

        Account target = accountMapper.getByAccount(param.getAccount());

        if(target == null) {
            throw new BusinessException(OrgRespCodeNum.ACCOUNT_NOT_EXIST);
        }

        Account account =AccountConvert.INSTANCE.from(param);

        account.setLocked(LockedEnum.NORMAL.getValue());
        account.setId(target.getId());

        accountMapper.updateById(account);
    }
}
