package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.dto.dept.DeptListParam;
import com.nd.fastdp.org.model.dto.dept.DeptQueryParam;
import com.nd.fastdp.org.model.entity.Dept;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeptMapper extends BaseMapper<Dept> {

    @SuppressWarnings("deprecation")
    default Dept getByCode(@Param("code") String code){

        return selectOne(new QueryWrapper<Dept>().eq("code", code).eq("del", DelStatusEnum.NORMAL.getValue()));
    }

    default boolean isExist(String code){

        Dept dept = selectOne(new QueryWrapper<Dept>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .eq("code", code));

        return dept != null;
    }

    default boolean isExist(String id, String code){

        Dept dept = selectOne(new QueryWrapper<Dept>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .ne("id", id)
                                                      .eq("code", code));

        return dept != null;
    }

    default List<Dept> listAllEnable(){

        return selectList(new QueryWrapper<Dept>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                  .eq("state", StatusEnum.ENABLE.getValue())
                                                  .orderByAsc("sort"));
    }

    default List<Dept> listAll(){

        return selectList(new QueryWrapper<Dept>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }

    default List<Dept> list(DeptListParam param){

        return selectList(new QueryWrapper<Dept>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                   .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                   .like(StringUtils.isNotEmpty(param.getCode()), "code", param.getCode())
                                                   .eq(null != param.getType(), "type", param.getType())
                                                   .eq(StringUtils.isNotEmpty(param.getParentId()), "parent_Id", param.getParentId())
                                                   .eq(null != param.getState(), "state", param.getState())
                                                   .orderByAsc("sort"));
    }

    default IPage<Dept> page(IPage<Dept> page, DeptQueryParam param){

        return selectPage(page, new QueryWrapper<Dept>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                        .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                        .like(StringUtils.isNotEmpty(param.getCode()), "code", param.getCode())
                                                        .eq(null != param.getType(), "type", param.getType())
                                                        .eq("parent_Id", param.getParentId())
                                                        .eq(null != param.getState(), "state", param.getState())
                                                        .orderByAsc("sort"));
    }

    default void del(DelParam param) {

        Dept dept = null;
        for (String id : param.getIds()) {

            dept = new Dept();

            dept.setId(id);
            dept.init(param);
            dept.setDel(DelStatusEnum.DEL.getValue());

            updateById(dept);
        }
    }

    default void updateState(StateParam param) {

        Dept dept = null;
        for (String id : param.getIds()) {

            dept = new Dept();

            dept.setId(id);
            dept.init(param);
            dept.setState(param.getState());

            updateById(dept);
        }
    }

}
