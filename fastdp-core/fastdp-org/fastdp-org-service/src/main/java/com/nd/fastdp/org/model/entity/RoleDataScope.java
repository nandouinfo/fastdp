package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_role_data_scope")
@Data
@Accessors(chain = true)
public class RoleDataScope extends FdpEntity {

    /**
     * 角色编码
     */
    private String roleId;

    /**
     * 数据权限ID
     */
    private String dataScopeId;

}
