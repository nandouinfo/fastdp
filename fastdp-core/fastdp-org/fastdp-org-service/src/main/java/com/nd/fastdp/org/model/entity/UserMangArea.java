package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_user_mang_area")
@Data
@Accessors(chain = true)
public class UserMangArea extends FdpEntity {

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 区域ID
     */
    private String areaId;

}
