package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_role_resource")
@Data
@Accessors(chain = true)
public class RoleResource extends FdpEntity {

    /**
     * 角色编码
     */
    private String roleId;

    /**
     * 权限编码
     */
    private String resourceCode;

}
