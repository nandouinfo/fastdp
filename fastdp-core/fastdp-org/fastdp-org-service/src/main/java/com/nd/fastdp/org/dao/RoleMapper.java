package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.dto.role.RoleListParam;
import com.nd.fastdp.org.model.dto.role.RoleQueryParam;
import com.nd.fastdp.org.model.entity.Role;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper extends BaseMapper<Role> {

    default void del(DelParam param) {

        Role role = null;
        for (String id : param.getIds()) {

            role = new Role();

            role.setId(id);
            role.init(param);
            role.setDel(DelStatusEnum.DEL.getValue());

            updateById(role);
        }
    }

    default void updateState(StateParam param) {

        Role role = null;
        for (String id : param.getIds()) {

            role = new Role();

            role.setId(id);
            role.init(param);
            role.setState(param.getState());

            updateById(role);
        }
    }

    default Role getByName(String name){

        return selectOne(new QueryWrapper<Role>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                 .eq("name", name));
    }

    default boolean isExist(String name){

        return getByName(name) != null;
    }

    default boolean isExist(String id, String name){

        Role resource = selectOne(new QueryWrapper<Role>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                           .ne("id", id)
                                                           .eq("name", name));

        return resource != null;
    }

    List<Role> findByUserId(@Param("userId") String userId);

    default List<Role> listAllEnable(){

        return selectList(new QueryWrapper<Role>().eq("del", DelStatusEnum.NORMAL_VAL).eq("state", StatusEnum.ENABLE_VAL).orderByAsc("sort"));
    }

    default List<Role> listAll(){

        return selectList(new QueryWrapper<Role>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }

    default List<Role> list(RoleListParam param){

        return selectList(new QueryWrapper<Role>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                    .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                    .eq(null != param.getState(), "state", param.getState())
                                                    .orderByAsc("sort"));
    }

    default IPage<Role> page(IPage<Role> page, RoleQueryParam param){

        return selectPage(page, new QueryWrapper<Role>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                        .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                        .eq(null != param.getState(), "state", param.getState())
                                                        .orderByAsc("sort"));
    }
}
