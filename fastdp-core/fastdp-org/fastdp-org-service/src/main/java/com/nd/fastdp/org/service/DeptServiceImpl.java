package com.nd.fastdp.org.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.constant.VerifyRespCodeEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.convert.DeptConvert;
import com.nd.fastdp.org.dao.DeptMapper;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import com.nd.fastdp.org.model.dto.dept.DeptAddParam;
import com.nd.fastdp.org.model.dto.dept.DeptListParam;
import com.nd.fastdp.org.model.dto.dept.DeptModifyParam;
import com.nd.fastdp.org.model.dto.dept.DeptQueryParam;
import com.nd.fastdp.org.model.entity.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptMapper deptMapper;

    @Override
    public DeptBO add(DeptAddParam param) {

        Dept dept = DeptConvert.INSTANCE.from(param);

        if(deptMapper.isExist(dept.getCode())){
            throw new BusinessException(VerifyRespCodeEnum.CODE_EXIST);
        }

        deptMapper.insert(dept);

        return DeptConvert.INSTANCE.to(dept);
    }

    @Override
    public void del(DelParam param) {
        deptMapper.del(param);
    }

    @Override
    public DeptBO modify(DeptModifyParam param) {

        Dept dept = DeptConvert.INSTANCE.from(param);

        if(deptMapper.isExist(dept.getId(), dept.getCode())){
            throw new BusinessException(VerifyRespCodeEnum.CODE_EXIST);
        }

        deptMapper.updateById(dept);

        return get(dept.getId());
    }

    @Override
    public void modifyState(StateParam param) {
        deptMapper.updateState(param);
    }

    @Override
    public DeptBO get(String id) {

        Dept dict = deptMapper.selectById(id);

        return DeptConvert.INSTANCE.to(dict);
    }

    @Override
    public DeptBO getByCode(String code) {

        Dept dict = deptMapper.getByCode(code);

        return DeptConvert.INSTANCE.to(dict);
    }

    @Override
    public List<DeptBO> listAllEnable() {

        List<Dept> depts =  deptMapper.listAllEnable();

        return DeptConvert.INSTANCE.to(depts);
    }

    @Override
    public List<DeptBO> listAll() {

        List<Dept> depts =  deptMapper.listAll();

        return DeptConvert.INSTANCE.to(depts);
    }

    @Override
    public List<DeptBO> list(DeptListParam param) {

        List<Dept> depts =  deptMapper.list(param);

        return DeptConvert.INSTANCE.to(depts);
    }

    @Override
    public Page<DeptBO> page(DeptQueryParam param) {

        IPage<Dept> page =  deptMapper.page(param.getPage(), param);

        return DeptConvert.INSTANCE.to(page);
    }
}
