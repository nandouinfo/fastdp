package com.nd.fastdp.org.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.constant.VerifyRespCodeEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.convert.PositionConvert;
import com.nd.fastdp.org.dao.PositionMapper;
import com.nd.fastdp.org.model.bo.position.PositionBO;
import com.nd.fastdp.org.model.dto.position.PositionAddParam;
import com.nd.fastdp.org.model.dto.position.PositionListParam;
import com.nd.fastdp.org.model.dto.position.PositionModifyParam;
import com.nd.fastdp.org.model.dto.position.PositionQueryParam;
import com.nd.fastdp.org.model.entity.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionServiceImpl implements PositionService {

    @Autowired
    private PositionMapper positionMapper;

    @Override
    public PositionBO add(PositionAddParam param) {

        Position position = PositionConvert.INSTANCE.from(param);

        if(positionMapper.isExist(position.getName())){
            throw new BusinessException(VerifyRespCodeEnum.NAME_EXIST);
        }

        positionMapper.insert(position);

        return PositionConvert.INSTANCE.to(position);
    }

    @Override
    public void del(DelParam param) {
        positionMapper.del(param);
    }

    @Override
    public PositionBO modify(PositionModifyParam param) {

        Position position = PositionConvert.INSTANCE.from(param);

        if(positionMapper.isExist(position.getId(), position.getName())){
            throw new BusinessException(VerifyRespCodeEnum.NAME_EXIST);
        }

        positionMapper.updateById(position);

        return get(position.getId());
    }

    @Override
    public void modifyState(StateParam param) {
        positionMapper.updateState(param);
    }

    @Override
    public PositionBO get(String id) {

        Position dict = positionMapper.selectById(id);

        return PositionConvert.INSTANCE.to(dict);
    }

    @Override
    public List<PositionBO> findByUserId(String userId) {

        List<Position> positions =  positionMapper.findByUserId(userId);

        return PositionConvert.INSTANCE.to(positions);
    }

    @Override
    public List<PositionBO> listAllEnable() {

        List<Position> positions =  positionMapper.listAllEnable();

        return PositionConvert.INSTANCE.to(positions);
    }

    @Override
    public List<PositionBO> listAll() {

        List<Position> positions =  positionMapper.listAll();

        return PositionConvert.INSTANCE.to(positions);
    }

    @Override
    public List<PositionBO> list(PositionListParam param) {

        List<Position> positions =  positionMapper.list(param);

        return PositionConvert.INSTANCE.to(positions);
    }

    @Override
    public Page<PositionBO> page(PositionQueryParam param) {

        IPage<Position> page =  positionMapper.page(param.getPage(), param);

        return PositionConvert.INSTANCE.to(page);
    }
}
