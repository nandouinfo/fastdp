package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.org.model.entity.UserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

    default void del(String userId) {

        delete(new QueryWrapper<UserRole>().eq("user_id", userId));
    }

    default void del(String userId, String roleId) {

        delete(new QueryWrapper<UserRole>().eq("user_id", userId).eq("role_id", roleId));
    }

    default List<UserRole> findRoleByUserId(String userId){

        return selectList(new QueryWrapper<UserRole>().eq("user_id", userId));
    }

    default List<UserRole> findUserByRoleId(String roleId){

        return selectList(new QueryWrapper<UserRole>().eq("role_id", roleId));
    }
}
