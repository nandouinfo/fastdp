package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.org.model.entity.Account;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountMapper extends BaseMapper<Account> {

    @SuppressWarnings("deprecation")
    default Account getByAccount(@Param("account") String account){

        return selectOne(new QueryWrapper<Account>().eq("account", account));
    }
}
