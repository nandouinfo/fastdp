package com.nd.fastdp.org.convert;

import com.nd.fastdp.org.model.bo.role.RoleResourceBO;
import com.nd.fastdp.org.model.entity.RoleResource;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoleResourceConvert {

    RoleResourceConvert INSTANCE = Mappers.getMapper(RoleResourceConvert.class);

    List<RoleResourceBO> to(List<RoleResource> roleResources);
}
