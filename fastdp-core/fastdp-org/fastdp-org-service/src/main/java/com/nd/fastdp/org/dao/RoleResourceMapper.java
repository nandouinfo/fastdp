package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.org.model.entity.RoleResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleResourceMapper extends BaseMapper<RoleResource> {

    default void del(String roleId) {

        delete(new QueryWrapper<RoleResource>().eq("role_id", roleId));
    }

    default List<RoleResource> findRoleResourcesByRoleId(String roleId){

        return selectList(new QueryWrapper<RoleResource>().eq("role_id", roleId));
    }
}
