package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_user_role")
@Data
@Accessors(chain = true)
public class UserRole extends FdpEntity {

    /**
     * 用户编码
     */
    private String userId;
    /**
     * 角色编码
     */
    private String roleId;

}
