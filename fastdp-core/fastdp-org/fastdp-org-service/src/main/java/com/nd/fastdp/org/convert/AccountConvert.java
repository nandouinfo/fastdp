package com.nd.fastdp.org.convert;

import com.nd.fastdp.org.model.bo.account.AccountBO;
import com.nd.fastdp.org.model.dto.account.AccountAddParam;
import com.nd.fastdp.org.model.dto.account.AccountUpdatePwdParam;
import com.nd.fastdp.org.model.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountConvert {

    AccountConvert INSTANCE = Mappers.getMapper(AccountConvert.class);

    AccountBO to(Account account);

    Account from(AccountAddParam account);

    Account from(AccountUpdatePwdParam account);
}
