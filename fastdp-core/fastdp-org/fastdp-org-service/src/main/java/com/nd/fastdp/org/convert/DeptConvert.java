package com.nd.fastdp.org.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import com.nd.fastdp.org.model.dto.dept.DeptAddParam;
import com.nd.fastdp.org.model.dto.dept.DeptModifyParam;
import com.nd.fastdp.org.model.entity.Dept;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DeptConvert {

    DeptConvert INSTANCE = Mappers.getMapper(DeptConvert.class);

    Dept from(DeptAddParam param);

    Dept from(DeptModifyParam param);

    DeptBO to(Dept org);

    List<DeptBO> to(List<Dept> depts);

    Page<DeptBO> to(IPage<Dept> depts);
}
