package com.nd.fastdp.org.convert;

import com.nd.fastdp.org.model.bo.user.UserMangAreaBO;
import com.nd.fastdp.org.model.entity.UserMangArea;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMangAreaConvert {

    UserMangAreaConvert INSTANCE = Mappers.getMapper(UserMangAreaConvert.class);

    List<UserMangAreaBO> to(List<UserMangArea> userMangAreas);
}
