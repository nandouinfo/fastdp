package com.nd.fastdp.org.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.org.model.bo.position.PositionBO;
import com.nd.fastdp.org.model.dto.position.PositionAddParam;
import com.nd.fastdp.org.model.dto.position.PositionModifyParam;
import com.nd.fastdp.org.model.entity.Position;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PositionConvert {

    PositionConvert INSTANCE = Mappers.getMapper(PositionConvert.class);

    Position from(PositionAddParam param);

    Position from(PositionModifyParam param);

    PositionBO to(Position position);

    List<PositionBO> to(List<Position> positions);

    Page<PositionBO> to(IPage<Position> positions);
}
