package com.nd.fastdp.org.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.org.model.entity.RoleDataScope;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleDataScopeMapper extends BaseMapper<RoleDataScope> {

    default void del(String roleId) {

        delete(new QueryWrapper<RoleDataScope>().eq("role_id", roleId));
    }

    default List<RoleDataScope> findByRoleId(String roleId){

        return selectList(new QueryWrapper<RoleDataScope>().eq("role_id", roleId));
    }
}
