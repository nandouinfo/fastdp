package com.nd.fastdp.org.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_org_dept")
@Data
@Accessors(chain = true)
public class Dept extends BaseEntity {

    private String name;
    private String simpleName; //简称
    private String code;
    private String parentId;

    private String type; //类型

    private String address;//地址
    private String phone;//电话
    private String email;//邮箱
    private String leader;//负责人

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    private Integer fdpCore;
}
