package com.nd.fastdp.org.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.bo.role.RoleBO;
import com.nd.fastdp.org.model.bo.role.RoleResourceBO;
import com.nd.fastdp.org.model.dto.role.*;

import java.util.List;

/**
 * 角色服务
 */
public interface RoleService {

    List<RoleBO> findByUserId(String userId);

    RoleBO add(RoleAddParam param);

    void del(DelParam param);

    RoleBO modify(RoleModifyParam param);

    void modifyState(StateParam param);

    RoleBO get(String id);

    List<RoleBO> listAllEnable();

    List<RoleBO> listAll();

    List<RoleBO> list(RoleListParam param);

    Page<RoleBO> page(RoleQueryParam param);

    void add(RoleResourceAddParam param);

    List<RoleResourceBO> findRoleResourcesByRoleId(String roleId);
}
