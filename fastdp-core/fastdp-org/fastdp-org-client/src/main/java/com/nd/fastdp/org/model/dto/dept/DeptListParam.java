package com.nd.fastdp.org.model.dto.dept;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ListParam;
import com.nd.fastdp.framework.validator.InEnum;
import com.nd.fastdp.org.constant.DeptTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("部门列表参数")
@Data
@Accessors(chain = true)
public class DeptListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "名称", example = "测试")
    private String name;

    @ApiModelProperty(value = "简称", example = "测试")
    private String simpleName;

    @ApiModelProperty(value = "编码", example = "TEST")
    @NotEmpty(message = "编码不能为空")
    private String code;

    @ApiModelProperty(value = "父级ID", example = "PARENT")
    private String parentId;

    @NotEmpty(message = "类型不能为空")
    @InEnum(value = DeptTypeEnum.class, message = "类型必须是 {value}")
    @ApiModelProperty(value = "类型。1 代表【公司】；2 代表【部门】；", example = "1")
    private String type;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", example = "1")
    private Integer state;
}
