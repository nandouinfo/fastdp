package com.nd.fastdp.org.model.dto.user;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.AddParam;
import com.nd.fastdp.framework.validator.InEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("用户添加参数")
@Data
@Accessors(chain = true)
public class UserAddParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "简称", required = true, example = "测试")
    @NotEmpty(message = "简称不能为空")
    private String simpleName;

    @ApiModelProperty(value = "编码", required = true, example = "TEST")
    @NotEmpty(message = "编码不能为空")
    private String code;

    @ApiModelProperty(value = "组织编码", required = true, example = "CESHIZHUZHI")
    private String dept;

    @ApiModelProperty(value = "员工类型", example = "1")
    private String employeeType;

    @ApiModelProperty(value = "员工职务", example = "1")
    private String employeePosition;

    @NotEmpty(message = "性别不能为空")
    @ApiModelProperty(value = "类型。1 代表【男】；2 代表【女】；", required = true, example = "1")
    private Integer sex;

    @ApiModelProperty(value = "固定电话", example = "029-00000000")
    private String landline;

    @ApiModelProperty(value = "电话", example = "15111111111")
    private String phone;

    @ApiModelProperty(value = "邮箱", example = "test@test.com")
    private String email;

    @ApiModelProperty(value = "出生日期", example = "1991-11-11")
    private String birthday;

    @ApiModelProperty(value = "身份证号", example = "111111111111111111")
    private String idcard;

    @ApiModelProperty(value = "民族字典值", example = "1")
    private String nation;

    @ApiModelProperty(value = "籍贯", example = "陕西")
    private String nativeplace;

    @ApiModelProperty(value = "文化程度字典", example = "1")
    private String degree;

    @ApiModelProperty(value = "婚姻状态字典", example = "1")
    private Integer married;

    @ApiModelProperty(value = "地址", example = "测试地址")
    private String address;

    @ApiModelProperty(value = "紧急联系人", example = "张三")
    private String contact;

    @ApiModelProperty(value = "紧急联系人电话", example = "15111111111")
    private String contactPhone;

    @ApiModelProperty(value = "入职时间", example = "1597883188668")
    private Long entryTime;

    @ApiModelProperty(value = "离职时间", example = "1597883188668")
    private Long leaveTime;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序", required = true, example = "1")
    private Integer sort;

    @ApiModelProperty(value = "备注", required = true, example = "测试备注")
    private String remark;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", required = true, example = "1")
    private Integer state;
}
