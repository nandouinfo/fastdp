package com.nd.fastdp.org.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum LockedEnum implements BaseEnum {

    LOCKED(-1, "停用"),
    NORMAL(0, "正常");

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(LockedEnum::getValue).toArray();

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    @Deprecated
    public static boolean isValid(Integer status) {
        if (status == null) {
            return false;
        }
        return NORMAL.value.equals(status);
    }

    public static final Integer NORMAL_VAL = LockedEnum.NORMAL.getValue();

    public static final Integer LOCKED_VAL = LockedEnum.LOCKED.getValue();
}
