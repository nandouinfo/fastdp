package com.nd.fastdp.org.model.dto.user;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ListParam;
import com.nd.fastdp.framework.validator.InEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("用户列表参数")
@Data
@Accessors(chain = true)
public class UserListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "名称", example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "简称", example = "测试")
    @NotEmpty(message = "简称不能为空")
    private String simpleName;

    @ApiModelProperty(value = "编码", example = "TEST")
    @NotEmpty(message = "编码不能为空")
    private String code;

    @ApiModelProperty(value = "组织编码", example = "PARENT")
    private String dept;

    @ApiModelProperty(value = "职位", example = "1111")
    private String position;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", example = "1")
    private Integer state;
}
