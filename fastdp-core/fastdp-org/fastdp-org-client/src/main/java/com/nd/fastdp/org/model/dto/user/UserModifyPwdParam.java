package com.nd.fastdp.org.model.dto.user;

import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("用户修改密码")
@Data
@Accessors(chain = true)
public class UserModifyPwdParam extends ModifyParam implements Serializable {

    @ApiModelProperty(value = "ID", required = true, example = "C4CA4238A0B923820DCC509A6F75849B")
    private String id;
    @ApiModelProperty(value = "旧密码", required = true, example = "123456")
    @NotEmpty(message = "旧密码不能为空")
    private String pwd;
    @ApiModelProperty(value = "新密码", required = true, example = "123456")
    @NotEmpty(message = "密码不能为空")
    private String newPwd;
    @ApiModelProperty(value = "确认密码", required = true, example = "123456")
    @NotEmpty(message = "确认密码不能为空")
    private String confirmPwd;
}

