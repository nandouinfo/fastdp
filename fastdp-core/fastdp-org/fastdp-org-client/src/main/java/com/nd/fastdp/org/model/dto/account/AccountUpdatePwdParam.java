package com.nd.fastdp.org.model.dto.account;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("账号修改密码参数")
@Data
@Accessors(chain = true)
public class AccountUpdatePwdParam extends AddParam implements Serializable {

    private String account;

    private String oldPassword;

    private String password;//密码

    private String salt;//密码加密盐

    private String userId;
}
