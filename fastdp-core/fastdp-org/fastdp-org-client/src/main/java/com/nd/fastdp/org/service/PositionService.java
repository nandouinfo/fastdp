package com.nd.fastdp.org.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.bo.position.PositionBO;
import com.nd.fastdp.org.model.dto.position.PositionAddParam;
import com.nd.fastdp.org.model.dto.position.PositionListParam;
import com.nd.fastdp.org.model.dto.position.PositionModifyParam;
import com.nd.fastdp.org.model.dto.position.PositionQueryParam;

import java.util.List;

/**
 * 职务服务
 */
public interface PositionService {

    List<PositionBO> findByUserId(String userId);

    PositionBO add(PositionAddParam param);

    void del(DelParam param);

    PositionBO modify(PositionModifyParam param);

    void modifyState(StateParam param);

    PositionBO get(String id);

    List<PositionBO> listAllEnable();

    List<PositionBO> listAll();

    List<PositionBO> list(PositionListParam param);

    Page<PositionBO> page(PositionQueryParam param);
}
