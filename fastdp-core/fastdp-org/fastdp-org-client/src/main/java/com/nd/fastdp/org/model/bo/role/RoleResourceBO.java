package com.nd.fastdp.org.model.bo.role;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class RoleResourceBO extends BaseBO implements Serializable {

    private String roleId;
    private String resourceCode;
}
