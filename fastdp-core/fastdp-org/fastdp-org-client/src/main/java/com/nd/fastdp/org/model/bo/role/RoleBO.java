package com.nd.fastdp.org.model.bo.role;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class RoleBO extends BaseBO implements Serializable {

    private String id;
    private String name;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;

    List<RoleDataScopeBO> dataScopes;

    List<RoleResourceBO> roleResourceS;
}
