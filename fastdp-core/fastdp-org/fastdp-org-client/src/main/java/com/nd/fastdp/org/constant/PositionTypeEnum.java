package com.nd.fastdp.org.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PositionTypeEnum implements BaseEnum {

    MarketManager("SCGLY", "市场管理员"),
    TollCollector("SFY", "收费员");

    /**
     * 状态值
     */
    private String value;
    /**
     * 状态名
     */
    private String desc;

}
