package com.nd.fastdp.org.model.dto.user;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@ApiModel("用户角色删除参数")
@Data
@Accessors(chain = true)
public class UserRoleDelParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "角色ID", required = true, example = "1")
    private String roleId;

    @ApiModelProperty(value = "用户Id", required = true, example = "TEST_USER")
    private String userId;
}
