package com.nd.fastdp.org.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import com.nd.fastdp.org.model.dto.dept.DeptAddParam;
import com.nd.fastdp.org.model.dto.dept.DeptListParam;
import com.nd.fastdp.org.model.dto.dept.DeptModifyParam;
import com.nd.fastdp.org.model.dto.dept.DeptQueryParam;

import java.util.List;

/**
 * 部门服务
 */
public interface DeptService {

    DeptBO add(DeptAddParam param);

    void del(DelParam param);

    DeptBO modify(DeptModifyParam param);

    void modifyState(StateParam param);

    DeptBO get(String id);

    DeptBO getByCode(String code);

    List<DeptBO> listAllEnable();

    List<DeptBO> listAll();

    List<DeptBO> list(DeptListParam param);

    Page<DeptBO> page(DeptQueryParam param);
}
