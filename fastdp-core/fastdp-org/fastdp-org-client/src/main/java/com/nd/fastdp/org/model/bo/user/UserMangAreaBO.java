package com.nd.fastdp.org.model.bo.user;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserMangAreaBO extends BaseBO implements Serializable {

    private String userId;
    private String areaId;

}
