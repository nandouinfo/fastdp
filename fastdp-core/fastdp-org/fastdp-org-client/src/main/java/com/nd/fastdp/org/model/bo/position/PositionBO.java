package com.nd.fastdp.org.model.bo.position;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class PositionBO extends BaseBO implements Serializable {

    private String id;
    private String name;
    private String code;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;
}
