package com.nd.fastdp.org.model.bo.account;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountBO extends BaseBO implements Serializable {

    private String account;
    private String password;//密码

    private String salt;//密码加密盐

    private Integer online;//在线状态
    private Integer locked;//锁定状态
    private Long loginTime;//登录时间
    private Integer loginNumber;//登录次数

    private String userId;

    private Integer fdpCore;
}
