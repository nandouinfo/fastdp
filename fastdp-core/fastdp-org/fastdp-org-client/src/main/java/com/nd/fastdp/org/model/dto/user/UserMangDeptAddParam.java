package com.nd.fastdp.org.model.dto.user;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@ApiModel("用户管理部门添加参数")
@Data
@Accessors(chain = true)
public class UserMangDeptAddParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "部门ID列表", required = true, example = "['1', '2']")
    private List<String> deptIds;

    @ApiModelProperty(value = "用户Id", required = true, example = "TEST_USER")
    private String userId;
}
