package com.nd.fastdp.org.model.dto.user;

import com.nd.fastdp.framework.pojo.dto.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("用户查询参数")
@Data
@Accessors(chain = true)
public class UserQueryParam extends UserListParam implements Serializable {

    @ApiModelProperty(value = "分页", example = "current：1, size:10")
    private PageParam page;
}
