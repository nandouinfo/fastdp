package com.nd.fastdp.org.model.dto.role;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@ApiModel("角色资源添加参数")
@Data
@Accessors(chain = true)
public class RoleResourceAddParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "资源编码", required = true, example = "['TEST1_MANG', 'TEST2_MANG']")
    private List<String> resourceCodes;

    @ApiModelProperty(value = "角色编码", required = true, example = "TEST_ROLE")
    private String roleId;

    private List<String> dataScopeIds;
}
