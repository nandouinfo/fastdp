package com.nd.fastdp.org.model.bo.role;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class RoleDataScopeBO extends BaseBO implements Serializable {

    /**
     * 角色编码
     */
    private String roleId;

    /**
     * 数据权限ID
     */
    private String dataScopeId;
}
