package com.nd.fastdp.org.service;

import com.nd.fastdp.org.model.bo.account.AccountBO;
import com.nd.fastdp.org.model.dto.account.AccountAddParam;
import com.nd.fastdp.org.model.dto.account.AccountUpdatePwdParam;

/**
 * 账号服务
 */
public interface AccountService {

    AccountBO getByAccount(String account);

    void add(AccountAddParam param);

    void modify(AccountUpdatePwdParam param);
}
