package com.nd.fastdp.org.model.dto.dept;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import com.nd.fastdp.framework.validator.InEnum;
import com.nd.fastdp.org.constant.DeptTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("部门修改参数")
@Data
@Accessors(chain = true)
public class DeptModifyParam extends ModifyParam implements Serializable {

    @ApiModelProperty(value = "ID", required = true, example = "C4CA4238A0B923820DCC509A6F75849B")
    @NotEmpty(message = "ID不能为空")
    private String id;

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "简称", required = true, example = "测试")
    @NotEmpty(message = "简称不能为空")
    private String simpleName;

    @ApiModelProperty(value = "编码", required = true, example = "TEST")
    @NotEmpty(message = "编码不能为空")
    private String code;

    @ApiModelProperty(value = "父级编码", example = "PARENT")
    private String parentId;

    @NotEmpty(message = "类型不能为空")
    @InEnum(value = DeptTypeEnum.class, message = "类型必须是 {value}")
    @ApiModelProperty(value = "类型。1 代表【公司】；2 代表【部门】；", required = true, example = "1")
    private String type;

    @ApiModelProperty(value = "地址", example = "测试地址")
    private String address;

    @ApiModelProperty(value = "电话", example = "029-00000000")
    private String phone;

    @ApiModelProperty(value = "邮箱", example = "test@test.com")
    private String email;

    @ApiModelProperty(value = "负责人", example = "1")
    private String leader;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序", required = true, example = "1")
    private Integer sort;

    @ApiModelProperty(value = "备注", required = true, example = "测试备注")
    private String remark;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", required = true, example = "1")
    private Integer state;
}
