package com.nd.fastdp.org.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import com.nd.fastdp.org.model.bo.user.UserBO;
import com.nd.fastdp.org.model.bo.user.UserRoleBO;
import com.nd.fastdp.org.model.dto.user.*;

import java.util.List;

/**
 * 用户服务
 */
public interface UserService {

    UserBO add(UserAddParam param);

    void del(DelParam param);

    UserBO modify(UserModifyParam param);

    void modifyState(StateParam param);

    UserBO get(String id);

    UserBO getByCode(String code);

    List<UserBO> listAllEnable();

    List<UserBO> listAll();

    List<UserBO> findByRoleId(String roleId);

    List<UserBO> list(UserListParam param);

    Page<UserBO> page(UserQueryParam param);

    //----------------所属角色-----------------------------
    void add(UserRoleAddParam param);

    void del(String userId, String roleId);

    List<UserRoleBO> findRoleByUserId(String userId);

    List<UserBO> listByRoleId(String roleId);
    //----------------所属角色-----------------------------

    //----------------管理区域-----------------------------
    void add(UserMangAreaAddParam param);


    List<UserBO> findUserByMangArea(String areaId);

    List<UserBO> findManager(String areaId, String positionCode);
    //----------------管理区域-----------------------------

    //----------------管理部门-----------------------------
    void add(UserMangDeptAddParam param);

    List<DeptBO> findMangDeptByUserId(String userId);
    //----------------管理部门-----------------------------




}
