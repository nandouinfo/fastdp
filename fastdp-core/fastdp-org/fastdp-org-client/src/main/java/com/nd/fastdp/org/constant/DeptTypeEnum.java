package com.nd.fastdp.org.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum DeptTypeEnum implements IntArrayValuable, BaseEnum {

    COMPANY(1, "公司"),
    DEPT(2, "部门");

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(DeptTypeEnum::getValue).toArray();

    @Override
    public int[] array() {
        return ARRAYS;
    }
}
