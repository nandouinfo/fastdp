package com.nd.fastdp.org.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  OrgRespCodeNum implements BaseEnum {

    ACCOUNT_NOT_EXIST("2-0003-0001-0001", "账号不存在");

    private String value;
    private String desc;


}
