/*
 * Copyright 2019-2029 geekidea(https://github.com/geekidea)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nd.fastdp.utils;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 *  用户客户端信息工具类
 * </p>
 **/
public class TerminalInfoUtil {

    private static final Pattern DEVICE_INFO_PATTERN = Pattern.compile(";\\s?(\\S*?\\s?\\S*?)\\s?Build/(\\S*?)[;)]");
    private static final Pattern DEVICE_INFO_PATTERN_1 = Pattern.compile(";\\s?(\\S*?\\s?\\S*?)\\s?\\)");

    private static final String USER_AGENT = "User-Agent";

    /**
     * 获取用户客户端信息
     * @param request
     * @return
     */
    public static TerminalInfo get(HttpServletRequest request){

        String userAgent = request.getHeader(USER_AGENT);

        return get(userAgent);
    }

    /**
     * 获取用户客户端信息
     * @param userAgentString
     * @return
     */
    public static TerminalInfo get(String userAgentString){

        TerminalInfo terminalInfo = new TerminalInfo();

        UserAgent userAgent = UserAgentUtil.parse(userAgentString);

        // 浏览器名称
        terminalInfo.setBrowserName(userAgent.getBrowser().getName());
        // 浏览器版本
        terminalInfo.setBrowserversion(userAgent.getVersion());
        // 浏览器引擎名称
        terminalInfo.setEngineName(userAgent.getEngine().getName());
        // 浏览器引擎版本
        terminalInfo.setEngineVersion(userAgent.getEngineVersion());
        // 用户操作系统名称
        terminalInfo.setOsName(userAgent.getOs().getName());
        // 用户操作平台名称
        terminalInfo.setPlatformName(userAgent.getPlatform().getName());
        // 是否是手机
        terminalInfo.setMobile(userAgent.isMobile());

        // 获取移动设备名称和机型
        TerminalDeviceInfo deviceInfo = getDeviceInfo(userAgentString);
        // 设置移动设备名称和机型
        terminalInfo.setDeviceName(deviceInfo.getName());
        terminalInfo.setDeviceModel(deviceInfo.getModel());

        // ip
        terminalInfo.setIp(IpUtil.getRequestIp());

        return terminalInfo;
    }

    /**
     * 获取移动端用户设备的名称和机型
     * @param userAgentString
     * @return
     */
    public static TerminalDeviceInfo getDeviceInfo(String userAgentString){
        TerminalDeviceInfo deviceInfo = new TerminalDeviceInfo();
        try {

            Matcher matcher = DEVICE_INFO_PATTERN.matcher(userAgentString);
            String model = null;
            String name = null;

            if (matcher.find()) {
                model = matcher.group(1);
                name = matcher.group(2);
            }

            if (model == null && name == null){
                matcher = DEVICE_INFO_PATTERN_1.matcher(userAgentString);
                if (matcher.find()) {
                    model = matcher.group(1);
                }
            }
           
            deviceInfo.setName(name);
            deviceInfo.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceInfo;
    }
}
