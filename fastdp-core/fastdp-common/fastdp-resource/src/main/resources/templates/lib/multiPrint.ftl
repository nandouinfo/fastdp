<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <title>打印</title>
        <#include "*/lib/head.ftl" />
        <style>
            .el-tabs__nav{
                background-color: white;
                border: 0px!important;
            }

            .el-tabs--right .el-tabs__header.is-right{
                margin-left: 0px!important;
            }

            .el-tabs--card>.el-tabs__header .el-tabs__item{
                border-top-right-radius: 10px;
                border-bottom-right-radius: 10px;
                border: 1px solid #E4E7ED!important;
                writing-mode: vertical-rl!important;
                min-height: 65px!important;
                padding: 5px 0px!important;
                font-size: 14px!important;
                text-align: center!important;
            }

            .el-tabs__content{
                height: 100%;
            }

            .el-tab-pane{
                height: 100%;
                background-color: rgb(82, 86, 89);
            }
        </style>
    </head>
    <body style="overflow-y: hidden">
        <div id="multi-print">
            <template>
                <el-tabs tab-position="right" type="card">
                    <template v-for="(file, i) in files">
                        <el-tab-pane :label="file.name" :lazy="true"><iframe :src="file.url" style="width: 100%; height: 100%;" frameborder="0"></iframe></el-tab-pane>
                    </template>
                </el-tabs>
            </template>
        </div>

        <link rel="stylesheet" href="${context.contextPath}/static/lib/element-ui/2.15.0/index.css" media="all"/>
        <script src="${context.contextPath}/static/lib/element-ui/2.15.0/index.js"></script>

        <script>
            layui.use(['FdpUtil', 'form', 'jquery', 'element', 'http', 'util'], function () {

                var files = FdpCommand.execute({
                    module: "PUBLIC-PRINT",
                    cmdName: "multiPrintParam",
                    options: null
                })

                for(var i = 0; i < files.length; i++){
                    files[i].url = files[i].url + "?token=" + SessionUtils.getToken();
                }


                var Main = {
                    data() {
                        return {
                            files: files
                        };
                    }
                };

                var Ctor = Vue.extend(Main)
                new Ctor().$mount('#multi-print')
            });
        </script>
    </body>
</html>