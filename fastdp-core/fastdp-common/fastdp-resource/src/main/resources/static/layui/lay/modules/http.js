;
layui.define(['jquery', 'jform', 'layer', 'upload'], function (e) {

    "use strict";

    var $ = layui.jquery,
        layer = layui.layer,
        upload = layui.upload;

    var http = {};

    /**
     * 结果统一处理
     * @param loadinglayerIndex
     * @param result
     * @param back
     * @private
     */
    var _resultHandler = function (loadinglayerIndex, result, back, options) {

        if (result.code == GLOBAL.returnCode.OK) {

            if (back) {
                back(result.data);
            }

        } else if (result.code == GLOBAL.returnCode.UNAUTHORIZED) {//未登录

            if (options.failHandle) {

                options.failHandle(result.msg);

            } else {

                layer.msg(result.msg, {offset: '100px'});

                setTimeout(function () {
                    SessionUtils.loginOut();
                    top.window.location.href = GLOBAL.path.basePath + "/";
                }, 100);
            }

        } else if (result.code == GLOBAL.returnCode.NOT_PERMISSION) {//无权限

            layer.msg(result.msg, {offset: '100px'});

            setTimeout(function () {
                window.location.href = GLOBAL.path.basePath + "/view/error/403";
            }, 100);

        } else if (result.code == GLOBAL.returnCode.NOT_FOUND) {//资源不存在

            layer.msg(result.msg, {offset: '100px'});

            setTimeout(function () {
                window.location.href = GLOBAL.path.basePath + "/view/error/404";
            }, 100);

        } else {

            if (options.failHandle) {
                options.failHandle(result.msg);
            } else {
                layer.msg(result.msg, {offset: '100px'});
            }
        }

        if (loadinglayerIndex) {
            layer.close(loadinglayerIndex);
        }
    }

    http.post = function (params) {

        var index = null;

        if (!params.hideLoading) {

            index = layer.load(3);
        }

        params.error = params.error ? params.error : function (e) {

            if (index) {
                layer.close(index);
            }

            console.error(e);

            layer.msg("系统维护中请稍后重试！", {offset: '100px'});
            /*setTimeout(function(){
                top.location.href = "/login.html";
            },1500);*/
        }

        params.type = params.type ? params.type : "post";
        params.dataType = params.dataType ? params.dataType : "json";
        //params.async = params.async ? params.async : true;
        params.contentType = params.contentType ? params.contentType : "application/json";
        params.data = JSON.stringify(params.data);

        params.success = function (result) {
            _resultHandler(index, result, params.backFun, params);
        };

        fillHeaderToken(params)

        $.ajax(params);
    };

    http.get = function (params) {

        var index = null;

        if (!params.hideLoading) {
            index = layer.load(3);
        }

        params.error = params.error ? params.error : function (e) {
            if (index) {
                layer.close(index);
            }
            console.error(e);
            layer.msg("系统维护中请稍后重试！", {offset: '100px'});
        }

        params.success = function (result) {

            _resultHandler(index, result, params.backFun, params);
        };

        fillHeaderToken(params)

        $.ajax(params);
    }

    http.getTpl = function (params) {

        var tpl = "";

        var index = layer.load(3);

        params.async = false;
        params.error = params.error ? params.error : function (e) {
            if (index) {
                layer.close(index);
            }
            console.error(e);
            layer.msg("系统维护中请稍后重试！", {offset: '100px'});
        }

        params.success = function (result) {

            if (index) {
                layer.close(index);
            }

            tpl = result;
        };
        fillHeaderToken(params);
        $.ajax(params);

        return tpl;
    }

    http.getScript = function (url, backFun) {

        $.getScript(url, function(response, status){
            if(backFun){
                backFun(response, status);
            }
        });
    }

    http.postForm = function (params) {

        var index = layer.load(3);

        params.error = params.error ? params.error : function (e) {
            layer.close(index);
            layer.msg("系统维护中请稍后重试！", {offset: '100px'});
            console.error(e);
        }

        fillHeaderToken(params)

        $("#" + params.formId).ajaxSubmit({
            url: params.url,
            type: "POST",
            success: function (result) {

                _resultHandler(index, result, params.backFun, params);
            }
        });
    };

    http.upload = function (params) {
        var ACCESS_TOKEN = LocalStorageUtils.get(GLOBAL.cacheKeys.ACCESS_TOKEN);

        if (ACCESS_TOKEN) {

            params.headers = {
                token: ACCESS_TOKEN
            }
        }
        return upload.render(params);
    };

    function fillHeaderToken(params) {

        var ACCESS_TOKEN = LocalStorageUtils.get(GLOBAL.cacheKeys.ACCESS_TOKEN);

        if (ACCESS_TOKEN) {

            params.headers = {
                token: ACCESS_TOKEN
            }
        }
    }

    e("http", http)
});
