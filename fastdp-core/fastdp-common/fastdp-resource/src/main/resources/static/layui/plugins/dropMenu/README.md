#  LayUI.dropMenu下拉菜单组件

#### 介绍
LayUI的下拉菜单组件

#### 软件架构
基于LayUI模块化组件

#### 安装教程
```
layui.config({
    base: '/js/', //拓展模块的根目录
})
.extend({
    dropMenu: '/dropMenu/dropMenu'
});
```

#### 使用说明
```
<input type='button' class='layui-btn rowmoreBtn layui-icon layui-icon-more' value="更多"/>

layui.dropMenu.render({
    type:0, //默认0，单击和鼠标经过时都生效，1点击时，2鼠标经过时（非必填）
	elem:".rowmoreBtn", //$(".rowmoreBtn")
	width:"120px", //菜单宽度（非必填）
    css:{"background":"red"}, //菜单样式（非必填）
    location:"c", //下拉菜单依靠位置，c居中、l左、r右（非必填）
    align: "c", //菜单内容文本对齐方式，c居中、l左、r右（非必填）
    //菜单数据，isShow是否显示，可配合权限来显示
	data:[
		{title:'选项1', icon:'layui-icon-set', event:'more1', isShow:true},
		{title:'选项2', icon:'layui-icon-notice', event:'more2'},
		{title:'选项3', icon:'layui-icon-search', event:'more3'},
		{title:'选项1', icon:'layui-icon-friends', event:'more1'},
		{title:'选项2', icon:'layui-icon-console', event:'more2'},
		{title:'选项3', icon:'layui-icon-app', event:'more3'},
		{title:'选项1', icon:'layui-icon-home', event:'more1'},
		{title:'选项2', event:'more2'},
		{title:'选项3', icon:'layui-icon-face-smile', event:'more3', isShow:false}
	],
    //触发的事件（非必填）
	event:{
		more1: function(){
		  alert('触发了事件1');
		},
		more2: function(){
		  alert('触发了事件2');
		},
		more3: function(){
		  alert('触发了事件3');
		}
	},
    done:function(dropM){ } //菜单渲染完成后触发事件，dropM下拉菜单对象
});

//事件可以单独用util.event来处理
  layui.util.event('lay-event', {
    more1: function(){
      alert('触发了事件1');
    },
    more2: function(){
      alert('触发了事件2');
    },
    more3: function(){
      alert('触发了事件3');
    }
  });

//事件可以使用table.on('tool')工具条事件来处理
   layui.table.on(`tool(mytable)`, function(obj){ 
        var layEvent= obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var row = obj.data; //获得当前行数据
        var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
        if(layEvent === 'more1'){
            alert('触发了事件1');
        } 
        else if(layEvent === 'more2'){
            alert('触发了事件2');
        }
   });

//跟layui.table结合，用lay-events标签动态显示菜单内容
 <!-- 行操作栏 -->
 <script type="text/html" id="rowtool">
    <div class='layui-inline'>
      <input type='button' class='layui-btn' value="编辑" lay-event="edit"/>
    </div>
    {{#  if(d.is_canDel){ }}
        <input type='button' class='layui-btn layui-btn-xs layui-btn-primary rowmoreBtn' value='更多' lay-events="viewinfo,delete"/>
    {{#  } else { }}
        <input type='button' class='layui-btn layui-btn-xs layui-btn-primary rowmoreBtn' value='更多' lay-events="neworder,recycle"/>
    {{#  } }}
 </script>

layui.table.render({
    elem: '#demo',
    url: '/demo/table/user/',
    page: true,
    cols: [[        
        { field: 'button', title: '操作', align:'center', width: "110", fixed: true, toolbar: '#rowtool' },
        { field: 'id', title: 'ID', align: 'center', width: 120},
        { field: 'name', title: '名称', align: 'center', width: 180 },
    ]],
    done(){
        layui.dropMenu.render({
             elem:".rowmoreBtn",
             data:[
		     {title:'查看', icon:'layui-icon-set', event:'viewinfo'},
		     {title:'删除', icon:'layui-icon-delete', event:'delete'},
                     {title:'新建订单', icon:'layui-icon-add-circle', event:'neworder'},
                     {title:'放入回收站', icon:'layui-icon-cart', event:'recycle'},
             ]
        });
    }
});
```

#### 效果图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/103102_46c20218_727020.png "Snipaste_2020-09-22_15-57-42.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/103246_e9dc9aa3_727020.png "Snipaste_2020-09-22_16-17-10.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/105024_6d5ac978_727020.png "Snipaste_2020-09-22_16-22-10.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/1205/105041_afc83eb6_727020.png "Snipaste_2020-11-18_10-58-56.png")

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
