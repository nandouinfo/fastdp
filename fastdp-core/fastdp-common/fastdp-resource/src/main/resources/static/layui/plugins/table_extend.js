/**
 * layui 表格操作扩展
 *
 * 表格增删改查 功能扩展
 */
layui.define(['jquery', 'http', 'element', 'FdpUtil'], function (exports) {

    var layer = layui.layer,
        $ = layui.jquery,
        element = layui.element,
        http = layui.http;

    var table_extend = {

        bind: function (table) {//绑定表格操作事件

            //监听工具栏事件
            $(document).off("click", ".tool-btn button").on("click", ".tool-btn button", function () {

                if(!$(this).attr("data-options")){
                    return;
                }

                var options = Utils.parseOptions($(this).attr("data-options"));

                var url = options.url;
                var params = options.params;
                var title = options.title;
                var callBack = options.callBack;
                var event = $(this).attr("lay-event");
                var target = $(this).attr("target");

                if (event == "add") {

                    if ("_blank" == target) {
                        window.open(GLOBAL.path.basePath + url, "blank");
                        return;
                    }

                    Win.open({
                        url: GLOBAL.path.basePath + url,
                        title: title,
                        type: 1,
                        width: options.width
                    });

                } else if (event == "del") {

                    var checkStatus = table.checkStatus('lay_table');

                    if (checkStatus.data.length <= 0) {
                        layer.msg("请选择要删除的行！");
                        return;
                    }

                    layer.confirm('删除后不可恢复，请确认!', function (index) {

                        var ids = [];

                        var data = [];

                        for (var i = 0; i < checkStatus.data.length; i++) {
                            ids.push(checkStatus.data[i].id);
                            data.push(checkStatus.data[i]);
                        }

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: ids,
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    callBack.options = {
                                        data: data
                                    };

                                    FdpCommand.execute(callBack);

                                } else {

                                    table.reload('lay_table', null);
                                }

                                layer.close(index);
                            }
                        });
                    });
                } else if (event == "export") {

                    Export.execute({
                        busType: options.busType,
                        params: FdpCommand.execute(callBack),
                        reqType: options.reqType
                    })

                } else if (event == "import") {//导入

                    if(!url){
                        Win.open({
                            url: GLOBAL.path.basePath + "/view/lib/import?options=" + encodeURIComponent(JSON.stringify(options)),
                            title: title,
                            type: 1
                        });
                    }else{
                        Win.open({
                            url: GLOBAL.path.basePath + url + (url.indexOf("?") >= 0 ? "&" : "?") + "options=" + encodeURIComponent(JSON.stringify(options)),
                            title: title,
                            type: 1
                        });
                    }

                }

            });

            //监听行工具事件
            table.on('tool(table)', function (obj) {

                var options = Utils.parseOptions($(this).attr("data-options"));

                var url = options.url;
                var params = options.params;
                var title = options.title;
                var callBack = options.callBack;
                var winOptions = options.win ? options.win : {};
                var target = $(this).attr("target");
                var msg = options.msg;
                var isAppendToken = ("undefined" === typeof options.isAppendToken) ? true : options.isAppendToken;

                winOptions.title = ("undefined" === typeof winOptions.title) ? title : winOptions.title;
                var enablePrint = ("undefined" === typeof options.enablePrint) ? false : options.enablePrint;


                var data = obj.data;

                if (obj.event === 'del') {

                    layer.confirm( (title ? title : '删除后不可恢复，请确认!'), function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: [data.id],
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };
                                    FdpCommand.execute(callBack);

                                } else {

                                    obj.del();
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'quick-del') {

                    layer.confirm('删除，请确认!', function (index) {

                        if (callBack) {

                            callBack.options = obj;
                            FdpCommand.execute(callBack);

                        } else {

                            obj.del();
                        }

                        layer.close(index);
                    });

                } else if (obj.event === 'recall') {

                    layer.confirm('撤销后不可恢复，请确认!', function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: null,
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };
                                    FdpCommand.execute(callBack);

                                } else {

                                    obj.del();
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'updateState') {

                    layer.confirm('修改状态为: <span style="color:red;">' + title + '</span>， 请确认!', function (index) {

                        params.ids = [data.id];

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: params,
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    callBack.options = obj;
                                    FdpCommand.execute(callBack);

                                } else {

                                    obj.update({'stateName': title});
                                    obj.update({'state': params.state});
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'resetPwd') {

                    layer.confirm('重置密码，请确认!', function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            async: false,
                            backFun: function (result) {

                                if (msg) {
                                    layer.msg(msg);
                                } else {
                                    layer.msg('处理成功,重置密码为 ' + result);
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'edit') {
                    var separation = "?";
                    if (url.indexOf("?") >= 0) {
                        separation = "&";
                    }

                    if ("_blank" == target) {
                        let tempUrl = GLOBAL.path.basePath + url + separation + "id=" + data.id;
                        let tempSeparation = "?";
                        if (tempUrl.indexOf("?") >= 0) {
                            tempSeparation = "&";
                        }

                        if(isAppendToken){
                            tempUrl += tempSeparation + "token=" + SessionUtils.getToken();
                        }

                        window.open(tempUrl, "blank");
                        return;
                    }

                    Win.open({
                        url: GLOBAL.path.basePath + url + separation + "id=" + data.id,
                        title: title ? title : "编辑",
                        type: 1,
                        width: options.width,
                        enablePrint: enablePrint
                    })

                } else if (obj.event === 'view') {

                    var separation = "?";
                    if (url.indexOf("?") >= 0) {
                        separation = "&";
                    }

                    var hasId = url.indexOf("?id=") >= 0;

                    if ("_blank" == target) {
                        let tempUrl = GLOBAL.path.basePath + url + (!hasId ? separation + "id=" + data.id : "");
                        let tempSeparation = "?";
                        if (tempUrl.indexOf("?") >= 0) {
                            tempSeparation = "&";
                        }
                        tempUrl += tempSeparation + "token=" + SessionUtils.getToken();
                        window.open(tempUrl, "blank");
                        return;
                    }

                    winOptions.url = GLOBAL.path.basePath + url + (!hasId ? separation + "id=" + data.id : "");
                    winOptions.title = winOptions.title || '查看';
                    winOptions.type = winOptions.type || 1;
                    winOptions.content = winOptions.url;
                    winOptions.width = winOptions.width || options.width;
                    winOptions.enablePrint = enablePrint;

                    Win.open(winOptions)

                } else if (obj.event === 'workflowform_view') {

                    var winOptions = {};

                    let tempSeparation = "?";
                    if (url.indexOf("?") >= 0) {
                        tempSeparation = "&";
                    }
                    //url += tempSeparation + "token=" + SessionUtils.getToken();

                    winOptions.url = GLOBAL.path.basePath + url;
                    winOptions.title = winOptions.title || '详情';
                    winOptions.type = 2;
                    winOptions.content = winOptions.url;
                    winOptions.isAppendToken = false;
                    if(options.width != 'undefined'){
                        winOptions.width = winOptions.width || options.width;
                    }

                    winOptions.enablePrint = enablePrint;

                    var undo = null;
                    http.get({
                        url: GLOBAL.path.adminApi + '/workflow/undo/getLoginUserByProcinstId/' + (data.procinstId ? data.procinstId : data.id),
                        async: false,
                        backFun: function (result) {

                            undo = result;
                        }
                    });

                    if (undo != null) {

                        winOptions.url = winOptions.url.split("?")[0] + "?undoId=" + undo.id + "#/view"
                        winOptions.content = winOptions.url;
                    }

                    Win.open(winOptions);

                    return;


                } else if (obj.event === 'download') {

                    let tempSeparation = "?";
                    if (url.indexOf("?") >= 0) {
                        tempSeparation = "&";
                    }
                    url += tempSeparation + "token=" + SessionUtils.getToken();

                    var separation = "?";
                    if (url.indexOf("?") >= 0) {
                        separation = "&";
                    }

                    window.open(GLOBAL.path.adminApi + url + separation + "id=" + data.id, obj.event);

                } else if (obj.event === 'createBill') {

                    layer.confirm('是否生成账单，请确认!', function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: [data.id],
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    layer.msg("账单已生成");

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };
                                    FdpCommand.execute(callBack);

                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'reject') {

                    //'驳回后不可修改，请确认!'
                    layer.prompt({
                        formType: 2,
                        value: '',
                        title: '意见',
                        area: ['300px', '240px'] //自定义文本域宽高
                    }, function (suggest, index, elem) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: {
                                id: data.id,
                                suggest: suggest
                            },
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    layer.msg("已驳回");

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };
                                    FdpCommand.execute(callBack);
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'startRemodel') {

                    http.post({
                        url: GLOBAL.path.adminApi + url,
                        async: false,
                        backFun: function (result) {

                            Utils.print(GLOBAL.path.adminApi + "/cmstimp/remodel/form/print/" + data.id)

                            if (callBack) {

                                callBack.options = {
                                    rowObj: obj,
                                    data: data
                                };
                                FdpCommand.execute(callBack);
                            }

                            layer.close(index);
                        }
                    });

                } else if (obj.event === 'printRemodelForm') {

                    Utils.print(GLOBAL.path.adminApi + url)

                }  else if (obj.event === 'print') {

                    Utils.print(GLOBAL.path.adminApi + url)

                } else if (obj.event === 'invalid') {

                    layer.confirm('是否作废，请确认!', function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: [data.id],
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    layer.msg("已作废");

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };

                                    FdpCommand.execute(callBack);
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'effective') {

                    layer.confirm('是否生效，请确认!', function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: [data.id],
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    layer.msg("已生效");

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };

                                    FdpCommand.execute(callBack);
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'dueSoonNotice') {

                    layer.confirm('是否发送到期提醒通知，请确认!', function (index) {

                        http.post({
                            url: GLOBAL.path.adminApi + url,
                            data: [data.id],
                            async: false,
                            backFun: function (result) {

                                if (callBack) {

                                    layer.msg("已发送");

                                    callBack.options = {
                                        rowObj: obj,
                                        data: data
                                    };

                                    FdpCommand.execute(callBack);
                                }

                                layer.close(index);
                            }
                        });
                    });

                } else if (obj.event === 'qrcode') {

                    Appendix.qrcode({
                        url: url,
                        width: options.width,
                        height: options.height
                    });

                }else{

                    callBack.options = {
                        rowObj: obj
                    };

                    FdpCommand.execute(callBack);
                }

                return false;
            });
        }

    };

    exports('table_extend', table_extend);
});
