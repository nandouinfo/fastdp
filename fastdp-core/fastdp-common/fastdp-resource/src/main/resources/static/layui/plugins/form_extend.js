/**
 *  layui form 表单组件扩展
 **/
layui.define(['FdpUtil', 'laytpl', 'jquery', 'http'], function(exports) {

    var layer = layui.layer,
        laytpl = layui.laytpl,
        element = layui.element,
        $ = layui.jquery,
        http = layui.http;

    var form_extend = {

        select: {

            render: function(){

                $(document).find('select').each(function(){

                    var el = $(this),
                        options = $.trim(el.attr('lay-dict')),
                        placeholder = $.trim(el.attr('placeholder')),
                        value = el.attr("value");

                    if(options == null || options.length <= 0 || el.find("option").length > 0){
                        return true;//continue
                    }

                    var data = [];

                    if(placeholder && placeholder.length > 0){
                        data.push({
                            value: "",
                            name: placeholder
                        })
                    }

                    if(options.indexOf("{") < 0){

                        data = data.concat(top.Dict.findItemByDictCode(options));

                        if(value != null && value.length > 0 && el.length > 0){
                            for(var i = 0; i < data.length; i++){
                                if(data[i].value == value){
                                    data[i].checked = true;
                                    break;
                                }
                            }
                        }

                    }else{

                        options = eval("(" + options + ")");
                        options.valField = options.valField || 'value';
                        options.textField = options.textField || 'name';
                        options.value = options.value || value;
                        options.reqType = options.reqType || 'post';

                        if(options.textField.indexOf("+") > 0){
                            options.textField = options.textField.split("+");
                        }

                        var req = options.reqType == 'post' ? http.post : http.get;

                        req({
                            url: options.url,
                            async: false,
                            data: options.param,
                            backFun: function(result){

                                if(options.placeholder){
                                    data.push({
                                        value: "",
                                        name: options.placeholder
                                    })
                                }

                                if(result){

                                    for(var i = 0; i < result.length; i++){

                                        if(options.inArr){

                                            if($.inArray(result[i][options.valField], options.inArr) >= 0){

                                                data.push({
                                                    value: result[i][options.valField],
                                                    name: (options.textField instanceof Array) ? (result[i][options.textField[0]] + '(' +  result[i][options.textField[1]] + ')') : result[i][options.textField],
                                                    checked: result[i][options.valField] == options.value ? true : false
                                                })
                                            }
                                        }else{
                                            data.push({
                                                value: result[i][options.valField],
                                                name: (options.textField instanceof Array) ? (result[i][options.textField[0]] + '(' +  result[i][options.textField[1]] + ')') : result[i][options.textField],
                                                checked: result[i][options.valField] == options.value ? true : false
                                            })
                                        }
                                    }
                                }
                            }});
                    }

                    laytpl(['{{#  layui.each(d, function(index, t){ }}'
                        ,'<option value="{{ t.value }}" {{ t.checked ? "selected" : "" }}>{{ t.name }}</option>'
                        ,'{{#  }); }}'
                    ].join('')).render(data, function(html){

                        el.html(html);
                    });
                })
            }
        },

        radio: {

            render: function(){

                $(document).find("input[type=radio]").each(function(){

                    var el = $(this),
                        options = $.trim(el.attr('lay-dict')),
                        name = $(this).attr("name");

                    if(options == null || options.length <= 0 || el.find("option").length > 0){
                        return true;//continue
                    }

                    var data = top.Dict.findItemByDictCode(options);

                    laytpl(['{{#  layui.each(d, function(index, t){ }}'
                        ,'<input type="radio" name="' + name + '" value="{{t.value}}" title="{{t.name}}">'
                        ,'{{#  }); }}'
                    ].join('')).render(data, function(html){

                        el.replaceWith(html);
                    });
                })
            }
        },

        checkbox: {

            render: function(){

                $(document).find("input[type=checkbox]").each(function(){

                    var el = $(this),
                        options = $.trim(el.attr('lay-dict')),
                        name = $(this).attr("name");

                    if(options == null || options.length <= 0 || el.find("option").length > 0){
                        return true;//continue
                    }

                    var data = top.Dict.findItemByDictCode(options);

                    laytpl(['{{#  layui.each(d, function(index, t){ }}'
                        ,'<input type="checkbox" name="' + name + '" title="{{t.name}}" value="{{t.value}}">'
                        ,'{{#  }); }}'
                    ].join('')).render(data, function(html){

                        el.replaceWith(html);
                    });
                })
            }
        }
    }

    exports('form_extend', form_extend);
});
