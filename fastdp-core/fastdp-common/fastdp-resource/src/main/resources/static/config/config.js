var GLOBAL = {
    path: {
        adminApi: basePath,//后台接口地址
        wechatApi: basePath,//后台接口地址
        fileApi: basePath,//文件接口地址
        basePath: basePath //頁面基本路径
    },
    returnCode: {
        OK: "200",//未登录或无访问权限
        FAILED: "-1",//未登录或无访问权限
        UNAUTHORIZED: "401",//未登录或无访问权限
        NOT_PERMISSION: "403", //没有权限
        PARAM_ERROR: "400", //参数校验失败
        NOT_FOUND: "404", //资源不存在
        HTTP_BAD_METHOD: "405", // "错误的请求方法"
        INTERNAL_SERVER_ERROR: "500" //系统异常
    },
    cacheKeys: {
        ACCESS_TOKEN: 'token',
        REFRESH_TOKEN: 'refreshToken',
        LOGIN_USER: 'loginUser'
    },
    cacheExpireTime: {
        ORG: 1000 * 60 * 3,
        UER:  1000 * 60 * 3,
        POSITION:  1000 * 60 * 3
    },
    page: {
        size: 10
    },
    constant: {

        Dict: {

            STATE: {
                ENABLE: {
                    label: '启用',
                    value: 0
                },
                STOP: {
                    label: '停用',
                    value: 1
                }
            },

            YES_NO: {
                NO: {
                    label: '否',
                    value: 0
                },
                YES: {
                    label: '是',
                    value: 1
                }
            },

            RESOURCE_TYPE: {
                MODULE: {
                    label: '模块',
                    value: 1
                },
                MENU: {
                    label: '菜单',
                    value: 2
                },
                ACTION: {
                    label: '动作',
                    value: 3
                }
            },

            ORG_TYPE: {
                COMPANY: {
                    label: '公司',
                    value: 1
                },
                DEPT: {
                    label: '部门',
                    value: 2
                }
            },

            DICT_TYPE: {
                LIST: {
                    label: '列表',
                    value: 1
                },
                TREE: {
                    label: '树形',
                    value: 2
                }
            },

            DATA_SCOPE_TYPE: {
                SYS: {
                    label: '系统',
                    value: 0
                },
                CUSTOMER: {
                    label: '自定义',
                    value: 1
                }
            },

            COMMON_FUN_TYPE: {
                RESOURCE: {
                    label: '资源',
                    value: 0
                },
                OA_FORM: {
                    label: 'OA表单',
                    value: 1
                }
            },

            MATERIAL_TYPE: {
                IMG: {
                    label: '图片',
                    value: 1
                },
                VIDEO: {
                    label: '视频',
                    value: 2
                }
            },

            COUPON_USE_STATE: {

                UN_USE: {
                    label: '待使用',
                    value: 1
                },
                ALREAD_USE: {
                    label: '已使用',
                    value: 2
                }
            },

            NOTICE_STATE: {
                BOOKED: {
                    label: '录入',
                    value: 0
                },
                RELEASE: {
                    label: '发布',
                    value: 1
                }
            },

            NOTICE_TOP_STATE: {
                NORMAL: {
                    label: '正常',
                    value: 0
                },
                TOP: {
                    label: '置顶',
                    value: 1
                }
            },

            NOTICE_RECEIVER_TYPE: {
                ALL: {
                    label: '全部',
                    value: 0
                },
                DEPT: {
                    label: '指定部门',
                    value: 1
                },
                EMPLOYEE: {
                    label: '指定用户',
                    value: 2
                }
            },

            NOTICE_READ_STATE: {
                UN_READ: {
                    label: '未读',
                    value: 0
                },
                READ: {
                    label: '已读',
                    value: 1
                }
            },

            FLOW_FORM_TYPE: {
                ONLINE: {
                    label: '在线设计',
                    value: 0
                },
                CUSTOM: {
                    label: '定制表单',
                    value: 1
                }
            },

            OBJ_CODE_CREATE_STATE: {
                CREATEING: {
                    label: '正在生成',
                    value: 1
                },
                CREATED: {
                    label: '已生成',
                    value: 2
                },
                ERROR: {
                    label: '生成失败',
                    value: -1
                }
            },

            OBJ_CODE_DETAIL_STATE: {
                WAIT_USE: {
                    label: '待使用',
                    value: 1
                },
                USED: {
                    label: '已使用',
                    value: 2
                }
            }
        },

        BUS_PARENT_ROOT: '0',

        FDPCORE: 1
    }
}
