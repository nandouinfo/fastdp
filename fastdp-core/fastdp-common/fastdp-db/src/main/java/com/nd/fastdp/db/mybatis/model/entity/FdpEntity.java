package com.nd.fastdp.db.mybatis.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class FdpEntity {

    @TableId(type = IdType.UUID)
    private String id;
}
