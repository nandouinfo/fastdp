package com.nd.fastdp.db.mybatis.mapper;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.bo.LoginUserDataScope;
import com.nd.fastdp.framework.pojo.bo.LoginUserMangArea;
import com.nd.fastdp.framework.pojo.bo.LoginUserMangDept;
import com.nd.fastdp.framework.pojo.constant.DataScopeEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public interface FdpBaseMapper<T> extends BaseMapper<T> {

    /**
     * 过滤数据权限
     * @param queryWrapper
     * @param moduleCode 模块资源编码
     * @param loginUser
     */
    default void filterDataAuthority(QueryWrapper queryWrapper, String moduleCode, LoginUser loginUser) {

        if (null != loginUser) {

            Integer dataScope = null;

            HashMap<String, LoginUserDataScope> dataScopeMap = loginUser.getDataScopeMap();

            LoginUserDataScope loginUserDataScope = dataScopeMap.get(moduleCode);

            if (null == loginUserDataScope) {
                return;
            }

            String dataScopeStr = loginUserDataScope.getCode();

            if (dataScopeStr != null) {

                try {

                    dataScope = Integer.parseInt(dataScopeStr);

                } catch (Exception e) {

                    e.printStackTrace();

                    throw e;
                }

                int dataScopeBase = dataScope.intValue();

                if (DataScopeEnum.ALL.getValue().intValue() == dataScopeBase) {// 全部

                } else if (DataScopeEnum.SELF_MANGE_AREA.getValue().intValue() == dataScopeBase) {// 管理的区域

                    List<LoginUserMangArea> areas = loginUser.getAreas();
                    List<String> areaIds = areas.stream().map(v -> v.getId()).collect(Collectors.toList());

                    if (areaIds.isEmpty()) {

                        queryWrapper.eq("1", "2");

                    } else {

                        queryWrapper.and(new Consumer<QueryWrapper>() {
                            @Override
                            public void accept(QueryWrapper contractBookQueryWrapper) {
                                contractBookQueryWrapper.in("big_area_id", areaIds);
                                contractBookQueryWrapper.or();
                                contractBookQueryWrapper.in("area_id", areaIds);
                            }
                        });
                    }

                } else if (DataScopeEnum.SELF_AREA.getValue().intValue() == dataScope) {



                } else if (DataScopeEnum.SELF_MANGE_DEPT.getValue().intValue() == dataScopeBase) {// 管理的部门

                    List<LoginUserMangDept> managerDept = loginUser.getDepts();
                    List<String> deptCodeList = managerDept.stream().map(v -> v.getId()).collect(Collectors.toList());

                    if (deptCodeList.isEmpty()) {

                        queryWrapper.eq("1", "2");

                    } else {

                        queryWrapper.in("create_dept", deptCodeList);
                    }

                } else if (DataScopeEnum.SELF_DEPT.getValue().intValue() == dataScopeBase) {//所属部门

                    queryWrapper.in("dept", loginUser.getDept());

                } else if (DataScopeEnum.SELF.getValue().intValue() == dataScopeBase) {//创建者是自己

                    queryWrapper.in("create_user", loginUser.getUserId());
                }
            }
        }
    }

}
