package com.nd.fastdp.db.mybatis.model.entity;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import lombok.Data;

@Data
public class BaseEntity extends FdpEntity{

    private Long createTime;
    private String createUser;
    private String createUserName;
    private String createDept;
    private String createDeptName;

    private Long modifyTime;
    private String modifyUser;
    private String modifyUserName;
    private String modifyDept;
    private String modifyDeptName;

    public void init(AddParam param){

        this.createTime = param.getCreateTime();
        this.createUser = param.getCreateUser();
        this.createUserName = param.getCreateUserName();
        this.createDept = param.getCreateDept();
        this.createDeptName = param.getCreateDeptName();

        this.modifyTime = param.getModifyTime();
        this.modifyUser = param.getModifyUser();
        this.modifyUserName = param.getModifyUserName();
        this.modifyDept = param.getModifyDept();
        this.modifyDeptName = param.getModifyDeptName();
    }

    public void init(ModifyParam param){

        this.modifyTime = param.getModifyTime();
        this.modifyUser = param.getModifyUser();
        this.modifyUserName = param.getModifyUserName();
        this.modifyDept = param.getModifyDept();
        this.modifyDeptName = param.getModifyDeptName();
    }

}
