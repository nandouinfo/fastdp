package com.nd.fastdp.security.pojo.convert;

import com.nd.fastdp.framework.pojo.bo.LoginUserMangDept;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LoginUserMangDeptConvert {

    LoginUserMangDeptConvert INSTANCE = Mappers.getMapper(LoginUserMangDeptConvert.class);

    List<LoginUserMangDept> from(List<DeptBO> depts);
}
