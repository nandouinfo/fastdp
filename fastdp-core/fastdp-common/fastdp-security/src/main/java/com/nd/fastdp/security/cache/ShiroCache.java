package com.nd.fastdp.security.cache;

import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.security.jwt.JwtToken;

/**
 * 登录信息缓存操作服务
 **/
public interface ShiroCache {

    /**
     * 缓存登录信息
     *
     * @param jwtToken
     * @param loginUser
     */
    void put(JwtToken jwtToken, LoginUser loginUser);

    /**
     * 通过账号，从缓存中获取登录用户
     *
     * @param account
     * @return
     */
    LoginUser get(String account);

    /**
     * 删除对应用户的缓存
     *
     * @param token
     * @param account
     */
    void del(String token, String account);

    /**
     * 刷新登录信息
     *
     * @param oldToken
     * @param account
     * @param newToken
     */
    void refresh(String oldToken, String account, JwtToken newToken);

    /**
     * 通过用户名称获取盐值
     *
     * @param account
     * @return
     */
    String getSalt(String account);

    /**
     * 判断token在缓存中是否存在
     *
     * @param token
     * @return
     */
    boolean exists(String token);

    /**
     * 在一个用户多个账号情况下删除用户所有登录缓存
     */
    void delAll(String account);
}
