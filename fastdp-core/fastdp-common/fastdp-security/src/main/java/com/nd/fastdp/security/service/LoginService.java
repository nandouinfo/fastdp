package com.nd.fastdp.security.service;

import com.nd.fastdp.security.pojo.param.LoginParam;

import javax.servlet.http.HttpServletRequest;

public interface LoginService {

    /**
     * 登录
     *
     * @param loginParam
     * @return
     * @throws Exception
     */
    String login(LoginParam loginParam) throws Exception;


    /**
     * 退出
     * @param request
     * @throws Exception
     */
    void logout(HttpServletRequest request) throws Exception;

    /**
     * 检查验证码是否正确
     *
     * @param verifyToken
     * @param code
     * @throws Exception
     */
    void checkVerifyCode(String verifyToken, String code) throws Exception;
}
