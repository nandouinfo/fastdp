package com.nd.fastdp.security.pojo.convert;

import com.nd.fastdp.framework.pojo.bo.LoginUserRole;
import com.nd.fastdp.org.model.bo.role.RoleBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LoginUserRoleConvert {

    LoginUserRoleConvert INSTANCE = Mappers.getMapper(LoginUserRoleConvert.class);

    List<LoginUserRole> from(List<RoleBO> roles);
}
