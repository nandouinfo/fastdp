package com.nd.fastdp.security.pojo.convert;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LoginUserMangAreaConvert {

    LoginUserMangAreaConvert INSTANCE = Mappers.getMapper(LoginUserMangAreaConvert.class);

}
