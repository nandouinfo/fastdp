package com.nd.fastdp.security.cache.impl;

import com.nd.fastdp.cache.redis.RedisClient;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.security.cache.ShiroCache;
import com.nd.fastdp.security.jwt.JwtToken;
import com.nd.fastdp.security.pojo.constant.LoginUserCacheKeyConstant;
import com.nd.fastdp.security.properties.JwtProperties;
import com.nd.fastdp.utils.HttpServletRequestUtil;
import com.nd.fastdp.utils.TerminalInfo;
import com.nd.fastdp.utils.TerminalInfoUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.util.List;
import java.util.Set;

@Service
public class ShiroCacheImpl implements ShiroCache {

    @Autowired
    private JwtProperties jwtProperties;

    @Autowired
    private RedisClient redisClient;

    @Override
    public void put(JwtToken jwtToken, LoginUser loginUser) {

        if (jwtToken == null) {
            throw new BusinessException("token不能为空");
        }
        if (loginUser == null) {
            throw new BusinessException("loginUser不能为空");
        }
        // token
        String token = jwtToken.getToken();
        // 盐值
        String salt = jwtToken.getSalt();

        // 登录账号
        String account = loginUser.getAccount();

        // token md5值
        String tokenMd5 = DigestUtils.md5Hex(token);

        // 用户客户端信息
        TerminalInfo clientInfo = TerminalInfoUtil.get(HttpServletRequestUtil.getRequest());

        // Redis缓存登录用户信息
        loginUser.setSalt(salt);
        loginUser.setTerminalInfo(clientInfo);

        // 缓存过期时间与JwtToken过期时间一致
        Duration expireDuration = Duration.ofSeconds(jwtToken.getExpireSecond());

        // 判断是否启用单个用户登录，如果是，这每个用户只有一个有效token
        /*boolean singleLogin = jwtProperties.isSingleLogin();
        if (singleLogin) {
            delAll(account);
        }*/

        // 1. tokenMd5:jwtTokenRedisVo
        String loginTokenRedisKey = String.format(LoginUserCacheKeyConstant.LOGIN_TOKEN, tokenMd5);

        redisClient.set(loginTokenRedisKey, jwtToken, expireDuration.getSeconds());

        // 2. username:loginSysUserRedisVo
        redisClient.set(String.format(LoginUserCacheKeyConstant.LOGIN_USER, account), loginUser, expireDuration.getSeconds());
        // 3. salt hash,方便获取盐值鉴权
        redisClient.set(String.format(LoginUserCacheKeyConstant.LOGIN_SALT, account), salt, expireDuration.getSeconds());
        // 4. login user token
        redisClient.set(String.format(LoginUserCacheKeyConstant.LOGIN_USER_TOKEN, account, tokenMd5), loginTokenRedisKey, expireDuration.getSeconds());

    }

    @Override
    public LoginUser get(String account) {

        if (StringUtils.isBlank(account)) {
            throw new BusinessException("account");
        }

        LoginUser loginUser = (LoginUser)redisClient.get(String.format(LoginUserCacheKeyConstant.LOGIN_USER, account));

        return loginUser;
    }

    @Override
    public void del(String token, String account) {

        if (token == null) {
            throw new BusinessException("token不能为空");
        }

        if (account == null) {
            throw new BusinessException("account不能为空");
        }

        String tokenMd5 = DigestUtils.md5Hex(token);
        // 1. delete tokenMd5
        redisClient.del(String.format(LoginUserCacheKeyConstant.LOGIN_TOKEN, tokenMd5));
        // 2. delete username
        redisClient.del(String.format(LoginUserCacheKeyConstant.LOGIN_USER, account));
        // 3. delete salt
        redisClient.del(String.format(LoginUserCacheKeyConstant.LOGIN_SALT, account));
        // 4. delete user token
        redisClient.del(String.format(LoginUserCacheKeyConstant.LOGIN_USER_TOKEN, account, tokenMd5));
    }

    @Override
    public void refresh(String oldToken, String account, JwtToken newToken) {

        // 获取缓存的登录用户信息
        LoginUser loginUser = get(account);
        // 删除之前的token信息
        del(oldToken, account);
        // 缓存登录信息
        put(newToken, loginUser);
    }

    @Override
    public String getSalt(String account) {

        if (StringUtils.isBlank(account)) {
            throw new BusinessException("account不能为空");
        }
        String salt = (String) redisClient.get(String.format(LoginUserCacheKeyConstant.LOGIN_SALT, account));
        return salt;
    }

    @Override
    public boolean exists(String token) {

        if (token == null) {
            throw new BusinessException("token不能为空");
        }
        String tokenMd5 = DigestUtils.md5Hex(token);
        Object object = redisClient.get(String.format(LoginUserCacheKeyConstant.LOGIN_TOKEN, tokenMd5));
        return object != null;
    }

    @Override
    public void delAll(String account) {

        Set<String> userTokenMd5Set = redisClient.keys(String.format(LoginUserCacheKeyConstant.LOGIN_USER_ALL_TOKEN, account));
        if (CollectionUtils.isEmpty(userTokenMd5Set)) {
            return;
        }

        // 1. 删除登录用户的所有token信息
        List<Object> tokenMd5List = redisClient.mGet(userTokenMd5Set);

        redisClient.del(tokenMd5List);

        // 2. 删除登录用户的所有user:token信息
        redisClient.del(userTokenMd5Set);

        // 3. 删除登录用户信息
        redisClient.del(String.format(LoginUserCacheKeyConstant.LOGIN_USER, account));

        // 4. 删除登录用户盐值信息
        redisClient.del(String.format(LoginUserCacheKeyConstant.LOGIN_SALT, account));
    }
}
