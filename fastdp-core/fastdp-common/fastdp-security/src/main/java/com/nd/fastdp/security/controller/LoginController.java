package com.nd.fastdp.security.controller;

import com.nd.fastdp.cache.redis.RedisClient;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.log.annotation.OperationLogIgnore;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.security.pojo.param.LoginParam;
import com.nd.fastdp.security.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Api(tags={"认证授权接口"})
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private RedisClient redisClient;

    @PostMapping("/login")
    @OperationLogIgnore
    @ApiOperation(value = "登录")
    @ApiImplicitParam(name = "token", required = false, dataType = "String", paramType="header")
    public Result login(@Validated @RequestBody LoginParam loginParam, @Validated HttpServletResponse response) throws Exception {

        String token = loginService.login(loginParam);

        return Result.OK(token);
    }


    @GetMapping("/profile")
    @ApiOperation(value = "登录用户信息")
    public Result getSysUser(@ApiIgnore @CurrentUser LoginUser loginUser) throws Exception {

        return Result.OK(loginUser);
    }

    @PostMapping("/logout")
    @ApiOperation(value = "退出")
    @OperationLogIgnore
    @ApiImplicitParam(name = "token", required = false, dataType = "String", paramType="header")
    public Result logout(@Validated HttpServletRequest request) throws Exception {

        loginService.logout(request);

        return Result.OK();
    }

}
