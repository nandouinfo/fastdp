package com.nd.fastdp.security.util;

import cn.hutool.crypto.digest.DigestUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.springframework.util.DigestUtils;

public class PasswordUtil {

    public static String encodePassword(String password) {
        return DigestUtils.md5DigestAsHex(password.getBytes());
    }

    /**
     * 密码加盐，再加密
     *
     * @param pwd
     * @param salt
     * @return
     */
    public static String encrypt(String pwd, String salt) {
        if (StringUtils.isBlank(pwd)) {
            throw new AuthenticationException("密码不能为空");
        }
        if (StringUtils.isBlank(salt)) {
            throw new AuthenticationException("盐值不能为空");
        }

        return DigestUtil.sha256Hex(pwd + salt);
    }


    public static void main(String[] args) {
        System.out.printf(encrypt("123456", "123456"));
    }
}
