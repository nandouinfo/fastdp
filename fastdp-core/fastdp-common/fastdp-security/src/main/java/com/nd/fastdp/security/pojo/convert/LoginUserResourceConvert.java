package com.nd.fastdp.security.pojo.convert;

import com.nd.fastdp.framework.pojo.bo.LoginUserResource;
import com.nd.fastdp.sys.model.bo.resource.ResourceBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LoginUserResourceConvert {

    LoginUserResourceConvert INSTANCE = Mappers.getMapper(LoginUserResourceConvert.class);

    List<LoginUserResource> from(List<ResourceBO> permissions);
}
