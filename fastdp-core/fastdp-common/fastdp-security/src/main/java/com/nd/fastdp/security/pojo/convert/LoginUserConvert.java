package com.nd.fastdp.security.pojo.convert;

import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.org.model.bo.user.UserBO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LoginUserConvert {

    LoginUserConvert INSTANCE = Mappers.getMapper(LoginUserConvert.class);

    @Mappings({@Mapping(source = "code", target = "account")})
    LoginUser from(UserBO userBO);
}
