package com.nd.fastdp.security.exception;

import com.auth0.jwt.exceptions.JWTVerificationException;

public class TokenNotEmptyException extends JWTVerificationException {

    private static final long serialVersionUID = -7076928975713577708L;

    public TokenNotEmptyException(String message) {
        super(message);
    }
}
