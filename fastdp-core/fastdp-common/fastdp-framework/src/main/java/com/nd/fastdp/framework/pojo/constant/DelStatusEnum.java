package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DelStatusEnum implements BaseEnum{

    NORMAL(0, "正常"),
    DEL(-1, "删除");

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    public static final Integer NORMAL_VAL = DelStatusEnum.NORMAL.value;

    public static final Integer DEL_VAL = DelStatusEnum.DEL.value;
}
