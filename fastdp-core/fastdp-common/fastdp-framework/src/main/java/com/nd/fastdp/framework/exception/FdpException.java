package com.nd.fastdp.framework.exception;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义异常
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FdpException extends RuntimeException{

    private static final long serialVersionUID = -2470461654663264392L;

    private String errorCode;
    private String message;

    public FdpException() {
        super();
    }

    public FdpException(String message) {
        super(message);
        this.message = message;
    }

    public FdpException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }

    public FdpException(BaseEnum respCode) {
        super(respCode.getDesc());
        this.errorCode = (String)respCode.getValue();
        this.message = respCode.getDesc();
    }

    public FdpException(String message, Throwable cause) {
        super(message, cause);
    }

    public FdpException(Throwable cause) {
        super(cause);
    }

}
