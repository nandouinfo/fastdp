package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CoreStatusEnum implements BaseEnum{

    YES(1, "核心数据");

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;
}
