package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 数据验证错误码枚举类
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum VerifyRespCodeEnum implements BaseEnum {

    CODE_EXIST("1-0001-0001-0001", "编码或值已存在"),
    NAME_EXIST("1-0001-0001-0002", "名称已存在"),
    FILE_IS_EMPTY("1-0001-0001-0003", "请选择导入文件");;

    private String value;
    private String desc;

}
