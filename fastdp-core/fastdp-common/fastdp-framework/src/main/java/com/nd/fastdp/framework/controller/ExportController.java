package com.nd.fastdp.framework.controller;

import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.ExportParam;
import com.nd.fastdp.framework.service.ExportHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Author: lxc
 * @Date: 2020/09/26 17:15:20
 * @Version: 1.0
 * @Description:
 */
@RequestMapping("/public")
@RestController
public class ExportController {

    @Autowired(required=false)
    private Map<String, ExportHandler> exportHandlerMap;

    @PostMapping("/export")
    public void export(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody ExportParam param, HttpServletRequest request, HttpServletResponse response) {

        String busType = param.getBusType();

        ExportHandler service = exportHandlerMap.get("exportService_" + busType);

        service.execute(loginUser,param.getParams(), request, response);
    }

    @GetMapping("/export_temp")
    public void export_temp(HttpServletRequest request, HttpServletResponse response) {

        ExportHandler service = exportHandlerMap.get("exportService_temp");

        service.execute(null, null, request, response);
    }

}
