package com.nd.fastdp.framework.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * 列表数据接口参数
 */
@Getter
@AllArgsConstructor
public abstract class ListParam extends Param implements Serializable {

    @Override
    public void init() {

    }
}
