package com.nd.fastdp.framework.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 状态通用数据传输对象
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class StateParam extends ModifyParam  implements Serializable {

    @ApiModelProperty(value = "记录ID列表", required = true, example = "['1','2']")
    private List<String> ids;

    @ApiModelProperty(value = "状态", required = true, example = "1")
    private Integer state;

    public void setId(String id){

        List<String> ids = new ArrayList<>();

        ids.add(id);

        this.ids = ids;
    }

}
