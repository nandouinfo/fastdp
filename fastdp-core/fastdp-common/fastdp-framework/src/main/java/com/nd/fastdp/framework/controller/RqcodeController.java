package com.nd.fastdp.framework.controller;

import com.nd.fastdp.framework.properties.FdpProperties;
import com.nd.fastdp.utils.RqcodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: LC
 * @Date: 2021/01/09 09:38:20
 * @Version: 1.0
 * @Description: 生成二维码
 */
@RequestMapping("/public")
@RestController
public class RqcodeController {

    @Autowired
    private FdpProperties fdpProperties;

    /**
     * 生成二维码
     * @param url
     * @param name
     * @param response
     * @throws IOException
     */
    @GetMapping("/recode")
    public void export(String url, String name, HttpServletResponse response) throws IOException {

        if(url.indexOf("http") != 0){
            url = fdpProperties.getDomainName() + url;
        }

        RqcodeUtil.create(name, url, response);
    }

}
