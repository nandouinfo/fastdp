package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DataScopeEnum implements BaseEnum {

    SELF(1, "自己", "self"),
    SELF_DEPT(10, "本部门", "selfDept"),
    SELF_MANGE_DEPT(100, "管理部门", "selfManagerDept"),
    SELF_AREA(1000, "本区域", "selfArea"),
    SELF_MANGE_AREA(1100, "管理区域", "selfManagerArea"),

    ALL(10000, "全部", "all");

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    private String code;

    public static final int SELF_VAL = 1;
    public static final int SELF_DEPT_VAL = 2;
    public static final int SELF_MANGE_DEPT_VAL = 3;
    public static final int SELF_MANGE_AREA_VAL = 4;
    public static final int ALL_VAL = 5;
}