package com.nd.fastdp.framework.pojo.bo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FlowBaseBO extends BaseBO{

    private String procinstId;
}
