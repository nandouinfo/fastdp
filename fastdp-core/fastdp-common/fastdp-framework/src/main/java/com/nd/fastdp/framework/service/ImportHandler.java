package com.nd.fastdp.framework.service;

import com.nd.fastdp.framework.pojo.bo.LoginUser;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.Map;

/**
 * @Author: lxc
 * @Date: 2020/09/26 17:20:46
 * @Version: 1.0
 * @Description: 导入
 */
public abstract class ImportHandler {

    public abstract void execute(LoginUser loginUser, InputStream inputStream, HttpServletRequest request);
}
