package com.nd.fastdp.framework.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 修改数据接口参数
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public abstract class ModifyParam extends Param implements Serializable {

    private String id;

    @ApiModelProperty(value = "修改时间", hidden = true, example = "1597392934286")
    private Long modifyTime;
    @ApiModelProperty(value = "修改用户编码", hidden = true, example = "ZHNANGSAN")
    private String modifyUser;
    @ApiModelProperty(value = "修改用户名称", hidden = true, example = "张三")
    private String modifyUserName;
    @ApiModelProperty(value = "修改部门编码", hidden = true, example = "CESHIBUMEN")
    private String modifyDept;
    @ApiModelProperty(value = "修改部门名称", hidden = true, example = "测试部门")
    private String modifyDeptName;

    @Override
    public void init(){

        this.modifyTime = new Date().getTime();

        if(getLoginUser() != null){
            this.modifyUser = getLoginUser().getUserId();
            this.modifyUserName = getLoginUser().getName();
            this.modifyDept = getLoginUser().getDept();
            this.modifyDeptName = getLoginUser().getDeptName();
        }

    }
}
