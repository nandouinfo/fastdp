package com.nd.fastdp.framework.pojo.vo;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.pojo.constant.RespCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 接口统一响应格式
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {

    private String code;
    private String msg;
    private Object data;

    public static Result OK(){

        return new Result(RespCodeEnum.OK.getValue(), RespCodeEnum.OK.getDesc(), null);
    }

    public static Result OK(Object data){

        Result result = OK();
        result.setData(data);

        return result;
    }

    public static Result OK(Object data, String msg){

        return new Result(RespCodeEnum.OK.getValue(), msg, data);
    }

    /**
     * 逻辑检查校验
     * @param msg
     * @return
     */
    public static Result FAILD(String code, String msg){

        return new Result(code, msg, null);
    }

    public static Result FAILD(BaseEnum baseEnum){

        return new Result((String)baseEnum.getValue(), baseEnum.getDesc(), null);
    }

}
