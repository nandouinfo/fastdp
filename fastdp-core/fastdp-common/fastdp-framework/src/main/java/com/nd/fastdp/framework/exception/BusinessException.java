package com.nd.fastdp.framework.exception;


import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.pojo.constant.RespCodeEnum;

/**
 * 业务异常
 */
public class BusinessException extends FdpException {

    private static final long serialVersionUID = -2303357122330162359L;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String errorCode, String message) {
        super(errorCode, message);
    }

    public BusinessException(BaseEnum respCode) {
        super(respCode);
    }
}
