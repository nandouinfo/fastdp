package com.nd.fastdp.framework.swagger;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "fastdp.swagger")
public class SwaggerProperties {

    /**
     * 是否启用Swagger
     */
    private boolean enable = false;

    /**
     * 扫描的基本包
     */
    @Value("${fastdp.swagger.base.package}")
    private String basePackage;

    /**
     * 联系人邮箱
     */
    @Value("${fastdp.swagger.contact.email}")
    private String contactEmail;

    /**
     * 联系人名称
     */
    @Value("${fastdp.swagger.contact.name}")
    private String contactName;

    /**
     * 联系人网址
     */
    @Value("${fastdp.swagger.contact.url}")
    private String contactUrl;

    /**
     * 描述
     */
    private String description;

    /**
     * 标题
     */
    private String title;

    /**
     * 网址
     */
    private String url;

    /**
     * 版本
     */
    private String version;

    /**
     * 自定义参数配置
     */
    @NestedConfigurationProperty
    private List<GroupConfig> groupConfig;

    /**
     * 自定义参数配置
     */
    @NestedConfigurationProperty
    private List<ParameterConfig> parameterConfig;

    /**
     * 自定义参数配置
     */
    @Data
    public static class ParameterConfig {

        /**
         * 名称
         */
        private String name;

        /**
         * 描述
         */
        private String description;

        /**
         * 参数类型
         * header, cookie, body, query
         */
        private String type = "head";

        /**
         * 数据类型
         */
        private String dataType = "String";

        /**
         * 是否必填
         */
        private boolean required;

        /**
         * 默认值
         */
        private String defaultValue;

    }

    /**
     * 自定义参数配置
     */
    @Data
    public static class GroupConfig {

        /**
         * 分组名称
         */
        private String name;

        /**
         * 匹配的地址
         */
        private String path;
    }
}
