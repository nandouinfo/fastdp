package com.nd.fastdp.framework.pojo.dto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author: lxc
 * @Date: 2020/09/26 17:17:16
 * @Version: 1.0
 * @Description: 导入导出参数
 */
@Data
@Accessors(chain = true)
public class ExportParam implements Serializable {
    private String busType;
    private Map<String, Object> params;
}