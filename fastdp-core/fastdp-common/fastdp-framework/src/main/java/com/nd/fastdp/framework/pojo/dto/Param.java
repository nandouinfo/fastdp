package com.nd.fastdp.framework.pojo.dto;

import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.bo.LoginUserDataScope;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据接口参数
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Param implements Serializable {

    @ApiModelProperty(value = "登录用户", hidden = true)
    private LoginUser loginUser;

    public void setLoginUser(LoginUser loginUser) {
        this.loginUser = loginUser;
        init();
    }

    public LoginUser getLoginUser() {
        return this.loginUser;
    }

    public abstract void init();
}
