package com.nd.fastdp.framework.pojo.constant;

import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum StatusEnum implements IntArrayValuable, BaseEnum {

    TEMP(-1, "临时"),
    ENABLE(0, "启用"),
    STOP(1, "停用"),
    SEAL_UP(2, "封存");

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(StatusEnum::getValue).toArray();

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    @Deprecated
    public static boolean isValid(Integer status) {
        if (status == null) {
            return false;
        }
        return ENABLE.value.equals(status)
                || STOP.value.equals(status);
    }

    @Override
    public int[] array() {
        return ARRAYS;
    }

    public static final Integer ENABLE_VAL = StatusEnum.ENABLE.getValue();

    public static final Integer STOP_VAL = StatusEnum.STOP.getValue();
}
