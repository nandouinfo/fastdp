package com.nd.fastdp.framework.pojo.constant;

import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum YestNoEnum implements IntArrayValuable, BaseEnum {

    NO(0, "否"),
    YES(1, "是");

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(YestNoEnum::getValue).toArray();

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    @Override
    public int[] array() {
        return ARRAYS;
    }

    public static final Integer NO_VAL = YestNoEnum.NO.getValue();

    public static final Integer YES_VAL = YestNoEnum.YES.getValue();
}
