package com.nd.fastdp.framework.swagger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 修改 Swagger UI 默认样式
 * 具体配置在 FilterConfig
 */
public class SwaggerStyleFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        String path = ((HttpServletRequest)servletRequest).getRequestURI();

        if(path.indexOf("swaggerbootstrapui.css") >= 0){
            servletRequest.getRequestDispatcher("/static/css/swaggerbootstrapui.css").forward(servletRequest, servletResponse);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
