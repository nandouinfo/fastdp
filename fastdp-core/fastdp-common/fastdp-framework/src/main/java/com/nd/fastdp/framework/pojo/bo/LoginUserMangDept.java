package com.nd.fastdp.framework.pojo.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class LoginUserMangDept implements Serializable {

    private String id;
    private String name;
    private String simpleName; //简称
    private String code;
    private String parentId;

    private String type; //类型

    private String address;//地址
    private String phone;//电话
    private String email;//邮箱
    private String leader;//负责人

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;

}
