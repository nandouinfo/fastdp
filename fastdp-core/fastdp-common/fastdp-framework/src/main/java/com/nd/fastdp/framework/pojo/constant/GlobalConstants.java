package com.nd.fastdp.framework.pojo.constant;

/**
 * Mall 全局枚举
 */
public interface GlobalConstants {

    // 全局请求路径枚举类，用于定义不同用户类型的根请求路径
    /**
     * 根路径 - 用户
     */
    String ROOT_PATH_USER = ""; ///users
    /**
     * 根路径 - 管理员
     */
    String ROOT_PATH_ADMIN = ""; ///admins

    // 用户类型
    /**
     * 用户类型 - 用户
     */
    @Deprecated
    Integer USER_TYPE_USER = 1;
    /**
     * 用户类型 - 管理员
     */
    @Deprecated
    Integer USER_TYPE_ADMIN = 2;

    /**
     * 管理员用户ID
     */
    String ADMIN_USER_ID = "1";

    // HTTP Request Attr
    /**
     * HTTP Request Attr - 用户编号
     */
    String REQUEST_ATTR_USER_ID_KEY = "user_id";
    /**
     * HTTP Request Attr - 用户类型
     */
    String REQUEST_ATTR_USER_TYPE_KEY = "user_type";
    /**
     * HTTP Request Attr - Controller 执行返回
     */
    String REQUEST_ATTR_COMMON_RESULT = "common_result";

}
