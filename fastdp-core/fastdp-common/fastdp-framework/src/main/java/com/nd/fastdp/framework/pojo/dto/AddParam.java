package com.nd.fastdp.framework.pojo.dto;

import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 添加数据接口参数
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public abstract class AddParam extends Param implements Serializable {

    @ApiModelProperty(value = "创建时间", hidden = true, example = "1597392934286")
    private Long createTime;
    @ApiModelProperty(value = "创建用户编码", hidden = true, example = "ZHNANGSAN")
    private String createUser;
    @ApiModelProperty(value = "创建用户名称", hidden = true, example = "张三")
    private String createUserName;
    @ApiModelProperty(value = "创建部门编码", hidden = true, example = "CESHIBUMEN")
    private String createDept;
    @ApiModelProperty(value = "创建部门名称", hidden = true, example = "测试部门")
    private String createDeptName;

    @ApiModelProperty(value = "修改时间", hidden = true, example = "1597392934286")
    private Long modifyTime;
    @ApiModelProperty(value = "修改用户编码", hidden = true, example = "ZHNANGSAN")
    private String modifyUser;
    @ApiModelProperty(value = "修改用户名称", hidden = true, example = "张三")
    private String modifyUserName;
    @ApiModelProperty(value = "修改组织编码", hidden = true, example = "CESHIBUMEN")
    private String modifyDept;
    @ApiModelProperty(value = "修改组织名称", hidden = true, example = "测试部门")
    private String modifyDeptName;

    @ApiModelProperty(value = "删除状态", hidden = true, example = "0")
    private Integer del = DelStatusEnum.NORMAL.getValue();

    public void init(){

        this.createTime = new Date().getTime();
        this.modifyTime = new Date().getTime();

        if(getLoginUser() != null){
            this.createUser = getLoginUser().getUserId();
            this.createUserName = getLoginUser().getName();
            this.createDept = getLoginUser().getDept();
            this.createDeptName = getLoginUser().getDeptName();


            this.modifyUser = getLoginUser().getUserId();
            this.modifyUserName = getLoginUser().getName();
            this.modifyDept = getLoginUser().getDept();
            this.modifyDeptName = getLoginUser().getDeptName();
        }
    }
}
