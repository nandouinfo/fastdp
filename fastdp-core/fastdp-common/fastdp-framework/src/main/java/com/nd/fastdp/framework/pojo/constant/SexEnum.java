package com.nd.fastdp.framework.pojo.constant;

import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum SexEnum implements IntArrayValuable, BaseEnum {

    MAN(1, "男"),
    WOMAN(0, "女");

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(SexEnum::getValue).toArray();

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;

    @Deprecated
    public static boolean isValid(Integer status) {
        if (status == null) {
            return false;
        }
        return MAN.value.equals(status)
                || WOMAN.value.equals(status);
    }

    @Override
    public int[] array() {
        return ARRAYS;
    }
}
