package com.nd.fastdp.framework.pojo.bo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BaseBO {

    private String id;

    private Long createTime;
    private String createUser;
    private String createUserName;
    private String createDept;
    private String createDeptName;

    private Long modifyTime;
    private String modifyUser;
    private String modifyUserName;
    private String modifyDept;
    private String modifyDeptName;
}
