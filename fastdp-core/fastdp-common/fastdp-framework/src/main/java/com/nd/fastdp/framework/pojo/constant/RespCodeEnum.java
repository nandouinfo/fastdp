package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 错误码枚举类
 */
@Getter
@AllArgsConstructor
public enum RespCodeEnum implements BaseEnum {

    FAIL("-1", "失败"),
    OK("200", "成功"),
    UNAUTHORIZED("401", "认证失败"),
    NOT_PERMISSION("403", "没有权限"),

    PARAM_ERROR("400", "参数校验失败"),
    NOT_FOUND("404", "资源不存在"),
    HTTP_BAD_METHOD("405", "错误的请求方法"),
    INTERNAL_SERVER_ERROR("500", "系统异常"),
    NOT_IMPLEMENTED("501", "没有实现");

    private String value;
    private String desc;

}
