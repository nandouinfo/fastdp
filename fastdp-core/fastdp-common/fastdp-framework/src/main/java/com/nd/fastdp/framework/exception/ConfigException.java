package com.nd.fastdp.framework.exception;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.pojo.constant.RespCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *配置异常
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ConfigException extends FdpException {

    private static final long serialVersionUID = -2303357122330162359L;

    public ConfigException(String message) {
        super(message);
    }

    public ConfigException(String errorCode, String message) {
        super(errorCode, message);
    }

    public ConfigException(BaseEnum respCode) {
        super(respCode);
    }

}
