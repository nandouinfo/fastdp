package com.nd.fastdp.framework.service;

import com.nd.fastdp.framework.pojo.bo.LoginUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Author: lxc
 * @Date: 2020/09/26 17:20:46
 * @Version: 1.0
 * @Description: 导出
 */
public abstract class ExportHandler {

    public abstract void execute(LoginUser loginUser, Map<String, Object> param, HttpServletRequest request, HttpServletResponse response);
}
