package com.nd.fastdp.framework.config;

import com.nd.fastdp.framework.swagger.SwaggerStyleFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean registrationBean(){

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SwaggerStyleFilter());
        registration.addUrlPatterns("/webjars/*");
        registration.setName("SwaggerStyleFilter");
        registration.setOrder(1);
        return registration;
    }
}
