package com.nd.fastdp.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastdpFrameworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastdpFrameworkApplication.class, args);
    }


}
