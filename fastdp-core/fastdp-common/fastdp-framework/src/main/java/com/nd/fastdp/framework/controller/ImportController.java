package com.nd.fastdp.framework.controller;

import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.constant.VerifyRespCodeEnum;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.framework.service.ImportHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * @Author: lxc
 * @Date: 2020/09/26 17:15:20
 * @Version: 1.0
 * @Description:
 */
@RequestMapping("/public")
@RestController
public class ImportController {

    @Autowired(required=false)
    private Map<String, ImportHandler> importHandlerMap;

    @PostMapping("/import")
    public Result Import(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException {

        if(file == null || file.isEmpty()){
            throw new BusinessException(VerifyRespCodeEnum.FILE_IS_EMPTY);
        }

        String busType = request.getParameter("busType");

        ImportHandler service = importHandlerMap.get("importService_" + busType);

        service.execute(loginUser, file.getInputStream(), request);

        return Result.OK();
    }

}
