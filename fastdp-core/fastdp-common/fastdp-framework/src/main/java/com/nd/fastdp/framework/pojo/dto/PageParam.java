package com.nd.fastdp.framework.pojo.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("分页参数")
@Data
@Accessors(chain = true)
public class PageParam extends Page implements Serializable {
}
