package com.nd.fastdp.framework.validator;

/**
 * 可生成 Int 数组的接口
 */
public interface IntArrayValuable {

    /**
     * @return int 数组
     */
    int[] array();

}
