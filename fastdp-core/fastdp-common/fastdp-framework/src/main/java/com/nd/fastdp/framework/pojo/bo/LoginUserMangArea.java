package com.nd.fastdp.framework.pojo.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class LoginUserMangArea implements Serializable {

    private String id;

    private String name;
    private String type;
    private String parentId;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;
}
