package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserTypeEnum implements BaseEnum {

    SUPER_ADMIN(1, "超级管理员"),
    NORMAL(2, "普通用户");

    /**
     * 状态值
     */
    private Integer value;
    /**
     * 状态名
     */
    private String desc;
}
