package com.nd.fastdp.framework.exception;


import com.nd.fastdp.framework.pojo.constant.BaseEnum;

/**
 * DAO异常
 */
public class DaoException extends FdpException {

    private static final long serialVersionUID = -2303357122330162359L;

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String errorCode, String message) {
        super(errorCode, message);
    }

    public DaoException(BaseEnum respCode) {
        super(respCode);
    }
}
