package com.nd.fastdp.framework.pojo.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class LoginUserResource implements Serializable {

    private String name;
    private String code;

    @ApiModelProperty("权限类型，1：模块，2：菜单， 3： 动作")
    private Integer type;

    private String parentCode;
    private String icon;

    private String path;
    private String perms;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer onlineDesignFlag;

    private Integer fdpCore;
}
