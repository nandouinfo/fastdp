package com.nd.fastdp.framework.pojo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DataScopeTypeEnum implements BaseEnum {

    SYS(0, "系统"),
    CUSTOME(1, "自定义");

    private Integer value;
    private String desc;
}
