package com.nd.fastdp.framework.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 查询数据接口参数
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public abstract class QueryParam extends Param implements Serializable {

    @ApiModelProperty(value = "分页", example = "current：1, size:10")
    private PageParam page;

    @Override
    public void init() {

    }
}
