package com.nd.fastdp.framework.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExceptionParam {

    private String code;
    private String msg;

}
