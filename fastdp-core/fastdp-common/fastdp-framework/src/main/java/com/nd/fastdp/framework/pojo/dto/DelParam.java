package com.nd.fastdp.framework.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 删除通用数据传输对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DelParam extends ModifyParam implements Serializable {

    public DelParam(String id){
        this.ids = new ArrayList<String>();
        this.ids.add(id);
    }

    @ApiModelProperty(value = "记录ID列表", required = true, example = "['1','2']")
    private List<String> ids;


}
