package com.nd.fastdp.framework.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HanyuPinyinParam implements Serializable {

    private String text;
}
