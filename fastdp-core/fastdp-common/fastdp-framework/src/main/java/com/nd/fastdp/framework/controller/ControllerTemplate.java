package com.nd.fastdp.framework.controller;

import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.constant.RespCodeEnum;
import com.nd.fastdp.framework.pojo.dto.*;
import com.nd.fastdp.framework.pojo.vo.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * api接口模板
 */
public abstract class ControllerTemplate {

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情",hidden = true)
    public Result get(@PathVariable("id") String id){

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }

    @PostMapping("/add")
    @ApiOperation(value = "添加",hidden = true)
    @ApiResponses(value={@ApiResponse(code=999, message="删除接口扩展的异常")})
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody AddParam param) {

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改",hidden = true)
    @ApiResponses(value={@ApiResponse(code=999, message="修改接口扩展的异常")})
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody ModifyParam param) {

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "修改状态",hidden = true)
    @ApiResponses(value={@ApiResponse(code=888, message="修改状态接口扩展的异常")})
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除",hidden = true)
    @ApiResponses(value={@ApiResponse(code=999, message="删除接口扩展的异常")})
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody List<String> ids){

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表",hidden = true)
    @ApiResponses(value={@ApiResponse(code=999, message="查询接口扩展的异常")})
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ListParam param){

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询",hidden = true)
    @ApiResponses(value={@ApiResponse(code=999, message="分页接口扩展的异常")})
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody QueryParam param){

        return Result.FAILD(RespCodeEnum.NOT_IMPLEMENTED);
    }
}
