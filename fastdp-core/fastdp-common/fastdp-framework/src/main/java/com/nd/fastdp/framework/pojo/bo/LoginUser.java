package com.nd.fastdp.framework.pojo.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nd.fastdp.framework.pojo.constant.UserTypeEnum;
import com.nd.fastdp.utils.TerminalInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginUser implements Serializable {

    private static final long serialVersionUID = -1758338570596088158L;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("关联的用户Id")
    private String userId;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("简称")
    private String simpleName;

    @ApiModelProperty("性别，0：女，1：男，默认1")
    private Integer sex;

    @ApiModelProperty("状态，1：超级管理员，2：普通用户")
    private Integer type;

    @ApiModelProperty(value = "员工类型", example = "1")
    private String employeeType;

    @ApiModelProperty(value = "员工职务", example = "1")
    private String employeePosition;

    @ApiModelProperty("状态，1：正常，0：停用")
    private Integer state;

    @ApiModelProperty("在线状态，0：不在线，1：在线")
    private Integer online;//在线状态

    @ApiModelProperty("登录时间")
    private Long loginTime;//登录时间

    @ApiModelProperty("登录次数")
    private Integer loginNumber;//登录次数

    @ApiModelProperty("部门编码")
    private String dept;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("部门简称")
    private String deptSimpleName;

    @ApiModelProperty("角色列表")
    private List<LoginUserRole> roles;

    @ApiModelProperty("权限列表")
    private List<LoginUserResource> resources;

    @ApiModelProperty("管理区域列表")
    private List<LoginUserMangArea> areas;

    @ApiModelProperty("管理部门列表")
    private List<LoginUserMangDept> depts;

    @ApiModelProperty(value = "包装后的盐值",hidden = true)
    private String salt;

    @ApiModelProperty(value = "登录客户端信息",hidden = true)
    private TerminalInfo terminalInfo;

    //模块数据权限表
    private HashMap<String, LoginUserDataScope> dataScopeMap;

    public boolean isSuperAdmin() {
        if (null == type) {
            return false;
        }
        return UserTypeEnum.SUPER_ADMIN.getValue().intValue() == type.intValue() ? true : false;
    }

    /*public boolean hasOperationAreaPermission(String areaId){

        Boolean result = false;

        if(isSuperAdmin()) {
            result = true;
            return result;
        }

        if(areas == null || areas.size() <= 0) {

            result = false;

            return result;
        }


        for (LoginUserMangArea area : areas){
            if(area.getId().equals(areaId)){

                result = true;

                break;
            }
        }

        return result;
    }*/
}
