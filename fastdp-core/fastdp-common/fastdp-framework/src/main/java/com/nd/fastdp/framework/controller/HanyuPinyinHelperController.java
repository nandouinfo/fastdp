package com.nd.fastdp.framework.controller;

import com.nd.fastdp.framework.pojo.dto.HanyuPinyinParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.utils.HanyuPinyinHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class HanyuPinyinHelperController {

    @PostMapping("/pinyin")
    @ApiOperation(value = "中文转拼音")
    public Result pinyin(@RequestBody HanyuPinyinParam param) {

        return Result.OK(HanyuPinyinHelper.getPinyinString(param.getText()));
    }

    @PostMapping("/getFirstLetters")
    @ApiOperation(value = "中文转拼音首字母")
    public Result getFirstLetters(@RequestBody HanyuPinyinParam param) {

        String firstLetters = HanyuPinyinHelper.getFirstLettersUp(param.getText());

        return Result.OK(firstLetters);
    }

}
