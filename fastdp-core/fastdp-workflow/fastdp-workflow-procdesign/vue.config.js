const path = require( 'path' )

function resolve ( dir ) {
  return path.join( __dirname, dir )
}

module.exports = {
  runtimeCompiler: true,
  publicPath: '/static/workflow/procdesign', // /approvalflow/
  outputDir: '../fastdp-workflow-ui/src/main/resources/static/workflow/procdesign',
  devServer: {
    port: 8088,
    proxy: {
      '/*':{
        target: "http://127.0.0.1:8081",
        changeOrigin: true,
        sercure: false,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  filenameHashing: false,
  productionSourceMap: true,
  configureWebpack: config => {
    //调试JS
    config.devtool = "source-map";
  },
  css: {
    loaderOptions: {
      // 给 stylus-loader 传递选项
      stylus: {
        import: '~@/assets/style/global.styl'
      }
    }
  },
  chainWebpack ( config ) {
    // set svg-sprite-loader
    config.module
      .rule( 'svg' )
      .exclude.add( resolve( 'src/components/DynamicForm/icons' ) )
      .end()
    config.module
      .rule( 'icons' )
      .test( /\.svg$/ )
      .include.add( resolve( 'src/components/DynamicForm/icons' ) )
      .end()
      .use( 'svg-sprite-loader' )
      .loader( 'svg-sprite-loader' )
      .options( {
        symbolId: 'icon-[name]'
      } )
      .end()
  }
}
