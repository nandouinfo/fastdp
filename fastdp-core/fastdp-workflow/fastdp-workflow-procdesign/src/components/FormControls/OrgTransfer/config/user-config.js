/* eslint-disable no-useless-escape */
/* eslint-disable no-prototype-builtins */
import { GET_DEPT_ROOT, GET_DEPT_TREE, GET_USER_BY_DEPT, GET_PAGE_EMPLOYEE} from '@/api'

const toHump = name => name.replace( /\_(\w)/g, function ( all, letter ) {
  return letter.toUpperCase()
} )

// 获取组织结构根节点
// 需要自行设置nodeID  重要！！！
async function getRootDept () {
  let rootDepts = []
  try {
    rootDepts = ( await GET_DEPT_ROOT() ).data

    for(var i = 0; i < rootDepts.length; i++){
      rootDepts[i].deptId = rootDepts[i].id;
      rootDepts[i].label = rootDepts[i].name;
      rootDepts[i].nodeId = rootDepts[i].id;
    }

  } catch ( err ) { }
  return rootDepts[0]
}

// 需要自行设置nodeID  重要！！！
async function getDepChildNode ( orgId ) {
  const promises = [GET_DEPT_TREE( { parentId: orgId } )]
  let res = []
  // loadUser && promises.push( GET_USER_BY_DEPT( { deptId: orgId } ) )
  try {
    res = (await Promise.all( promises ))

    for(var i = 0; i < res.length; i++){

      for(var j = 0; j < res[i].data.length; j++){
        res[i].data[j].deptId = res[i].data[j].id;
        res[i].data[j].label = res[i].data[j].name;
        res[i].data[j].nodeId = res[i].data[j].id;
      }
    }

  } catch ( error ) {/* this.$message.error('获取子节点数据出错')*/ }
  const nodes = res.reduce( ( p, c ) => {
    return [...p, ...c.data]
  }, [] )
  return nodes
}
// 需要返回一个promise
async function loadDepOrUser ( node, loadDep = true ) {
  let nodeData = []
  if ( !node || node.level === 0 ) { // 根目录
    const rootDepts = await getRootDept()  // 获取根节点
    nodeData = [rootDepts]
  }else {

    if(node.data.deptId) {

      nodeData = await getDepChildNode( node.data.deptId )  // 获取部门节点

      var userDatas = ( await GET_PAGE_EMPLOYEE({dept: node.data.deptId}) ).data  // 获取部门下人员

      for(var i = 0; i < userDatas.length; i++){
        userDatas[i].userId = userDatas[i].id;
        userDatas[i].userName = userDatas[i].name;
        userDatas[i].label = userDatas[i].name;
        userDatas[i].nodeId = userDatas[i].id;
      }

      if(nodeData && nodeData.length > 0) {
        nodeData = nodeData.concat(userDatas);
      }else {
        nodeData = userDatas;
      }
    }

  }/* else if ( node.level === 1 ) {
    nodeData = await getDepChildNode( node.data.deptId )  // 获取部门节点
  } else if ( !loadDep && node.level === 2 ) {
    nodeData = ( await GET_PAGE_EMPLOYEE({dept: node.data.deptId}) ).data  // 获取部门下人员
    for(var i = 0; i < nodeData.length; i++){
      nodeData[i].userId = nodeData[i].id;
      nodeData[i].userName = nodeData[i].name;
      nodeData[i].label = nodeData[i].name;
      nodeData[i].nodeId = nodeData[i].id;
    }
  }*/
  return nodeData
}

//用户选择
export const USER_CONFIG = {
  tabName: '指定人员',  // 选项卡名称
  type: 'user', //选项卡键值 传入的selected要和键值保持一致 eg: {dep: [], role: []}
  children: 'children', // 子节点标志
  // 生成每个节点的id 保证唯一
  nodeId: function ( data ) {
    return data.id
  },
  // 生成节点的名称 可选值 string | function
  label: function ( data, node ) {
    return data.name
  },
  // 判断是否为叶子节点 可选值 string | function
  isLeaf: function ( data, node ) {
    return data.hasOwnProperty( 'userId' ) // 含有userId为人员  且为叶子节点
  },
  // 搜索后的结果如果需要展示一些提示文字 例如搜索人员 提示人员所属部门  可以使用该属性
  // function
  searchResTip: function ( data ) {
    return '用户';
  },
  // 判断该节点是否可选 例如同时选择部门和部门下的人
  disabled: function ( data, node ) {
    return !data.hasOwnProperty( 'userId' );
  },
  // 动态请求后台拿到节点数据 返回一个promise
  onload(node){
    return loadDepOrUser( node, false );
  },
  // 搜索节点方法
  onsearch: async function ( searchString, resolve, reject ) {
    // const param = { page: 1, limit: 200, searchName: searchString }

    var result = [];

    var depts = ( await GET_DEPT_TREE({name: searchString}) ).data;
    for(var i = 0; i < depts.length; i++){
      depts[i].deptId = depts[i].id;

      result.push(depts[i]);
    }

    var users = ( await GET_PAGE_EMPLOYEE({name: searchString}) ).data;
    for(var i = 0; i < users.length; i++){
      users[i].userId = users[i].id;

      result.push(users[i])
    }

    resolve( result )
  }
}

