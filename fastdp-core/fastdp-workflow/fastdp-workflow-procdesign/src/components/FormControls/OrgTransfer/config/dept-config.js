/* eslint-disable no-useless-escape */
/* eslint-disable no-prototype-builtins */
import { GET_DEPT_ROOT, GET_DEPT_TREE, GET_USER_BY_DEPT} from '@/api'

const toHump = name => name.replace( /\_(\w)/g, function ( all, letter ) {
  return letter.toUpperCase()
} )
// 需要自行设置nodeID  重要！！！
async function getDepChildNode ( orgId ) {
  const promises = [GET_DEPT_TREE( { parentId: orgId } )]
  let res = []
  // loadUser && promises.push( GET_USER_BY_DEPT( { deptId: orgId } ) )
  try {
    res = (await Promise.all( promises ))

    for(var i = 0; i < res.length; i++){

      for(var j = 0; j < res[i].data.length; j++){
        res[i].data[j].deptId = res[i].data[j].id;
        res[i].data[j].label = res[i].data[j].name;
        res[i].data[j].nodeId = res[i].data[j].id;
      }
    }

  } catch ( error ) {/* this.$message.error('获取子节点数据出错')*/ }
  const nodes = res.reduce( ( p, c ) => {
    return [...p, ...c.data]
  }, [] )
  return nodes
}
// 需要返回一个promise
async function loadDepOrUser ( node, loadDep = true ) {
  let nodeData = []
  if ( !node || node.level === 0 ) { // 根目录
    const rootDepts = await getRootDept()  // 获取根节点
    nodeData = [rootDepts]
  } else if ( node.level === 1 ) {
    nodeData = await getDepChildNode( node.data.deptId )  // 获取部门节点
  }
  return nodeData
}
// 获取组织结构根节点
// 需要自行设置nodeID  重要！！！
async function getRootDept () {
  let rootDepts = []
  try {
    rootDepts = ( await GET_DEPT_ROOT() ).data

    for(var i = 0; i < rootDepts.length; i++){
      rootDepts[i].deptId = rootDepts[i].id;
      rootDepts[i].label = rootDepts[i].name;
      rootDepts[i].nodeId = rootDepts[i].id;
    }

  } catch ( err ) { }
  return rootDepts[0]
}



/**
 * 部门选择配置
 */
export const DEP_CONFIG  = {
  tabName: '部门',  // 选项卡名称
  type: 'dept', //选项卡键值 传入的selected要和键值保持一致 eg: {dep: [], role: []}
  children: 'children', // 子节点标志
  // 生成每个节点的id 保证唯一
  nodeId: function ( data ) {
    return data.deptId
  },
  // 生成节点的名称 可选值 string | function
  label: function ( data, node ) {
    return data.name;
  },
  // 判断是否为叶子节点 可选值 string | function
  isLeaf: function ( data, node ) {
    return true //
  },
  // 搜索后的结果如果需要展示一些提示文字 例如搜索人员 提示人员所属部门  可以使用该属性
  // function
  searchResTip: function ( data ) {
    return ""
  },
  // 判断该节点是否可选
  disabled: function ( data, node ) {
    return false
  },
  // 动态请求后台拿到节点数据 返回一个promise
  onload(node){
    return loadDepOrUser( node );
  },
  // 搜索节点方法
  onsearch: async function ( searchString, resolve, reject ) {
    // const param = { page: 1, limit: 200, searchName: searchString }

    var list = (await GET_DEPT_TREE({name: searchString})).data;
    for(var i = 0; i < list.length; i++){
      list[i].deptId = list[i].id;
    }

    resolve( list )
  }
}
