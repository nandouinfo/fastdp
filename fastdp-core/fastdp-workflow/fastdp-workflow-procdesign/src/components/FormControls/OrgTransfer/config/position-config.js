/* eslint-disable no-useless-escape */
/* eslint-disable no-prototype-builtins */
import { POSITION_LIST } from '@/api'


export const POSITION_CONFIG = {
  tabName: '职务',  // 选项卡名称
  type: 'position', //选项卡键值 传入的selected要和键值保持一致 eg: {dep: [], role: []}
  children: 'children', // 子节点标志

  // 生成每个节点的id 保证唯一
  nodeId: function ( data ) {
    return data.positionId
  },

  // 生成节点的名称 可选值 string | function
  label: function ( data, node ) {
    return data.name;
  },

  // 判断是否为叶子节点 可选值 string | function
  isLeaf: function ( data, node ) {
    return true // 角色无子节点
  },

  // 搜索后的结果如果需要展示一些提示文字 例如搜索人员 提示人员所属部门  可以使用该属性
  // function
  searchResTip: function ( data ) {
    return ""
  },

  // 判断该节点是否可选
  disabled: function ( data, node ) {
    return false
  },

  // 动态请求后台拿到节点数据 返回一个promise
  async onload(node) {

    var nodeData = (await POSITION_LIST(null)).data  // 获取部门下人员
    for(var i = 0; i < nodeData.length; i++){
      nodeData[i].positionId = nodeData[i].id;
    }

    return nodeData;
  },

  // 搜索节点方法
  onsearch: async function ( searchString, resolve, reject ) {
    // const param = { page: 1, limit: 200, searchName: searchString }

    var list = (await POSITION_LIST({name: searchString})).data;
    for(var i = 0; i < list.length; i++){
      list[i].positionId = list[i].id;
    }

    resolve( list )
  }
}
