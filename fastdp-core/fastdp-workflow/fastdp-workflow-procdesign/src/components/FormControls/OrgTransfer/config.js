import { DEP_USER_CONFIG } from './config/dept-user-config.js'
import { DEP_CONFIG } from './config/dept-config.js'
import { USER_CONFIG } from './config/user-config.js'
import { ROLE_CONFIG } from './config/role-config.js'
import { POSITION_CONFIG } from './config/position-config.js'

export const CONFIG_LIST = [DEP_CONFIG, ROLE_CONFIG, USER_CONFIG, DEP_USER_CONFIG, POSITION_CONFIG]
