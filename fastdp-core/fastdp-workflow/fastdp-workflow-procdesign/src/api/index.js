import {get, post} from '@/http'

//var serviceName = "http://127.0.0.1:8080/fastdp"

var serviceName = ""

// 获取组织机构根节点
export const GET_DEPT_ROOT = () => post( serviceName + '/org/dept/list', {parentId: ''} )

// 获取组织机构子节点
export const GET_DEPT_TREE = data => post( serviceName + '/org/dept/list', data )

// 根据部门id获取分页人员信息
export const GET_PAGE_EMPLOYEE = data => post( serviceName + '/org/user/list', data )

// 获取组织机构下人员信息
export const GET_USER_BY_DEPT = data => get( serviceName + 'userData.json', data )

// 获取流程分组列表
export const GROUP_LIST = data => post( serviceName + '/workflow/group/list', data )

//角色列表
export const ROLE_LIST = (data) => post( serviceName + '/org/role/list', data )

//职务列表
export const POSITION_LIST = (data) => post( serviceName + '/org/position/list', data )

// 流程定义
export const PROC_DEF = {
    get: function (id) {
        return get(serviceName + '/workflow/procdef/get/' + id)
    },
    save: function (params) {
        return post(serviceName + '/workflow/procdef/' + (params.id ? 'modify' : 'add'), params)
    },
    flow_line: function (params) {
        return post(serviceName + '/workflow/procdef/flowLine', params)
    }
}

// 流程实例
export const PROC_INST = {
    get: function (id) {
        return get(serviceName + '/workflow/procinst/get/' + id)
    },
    start: function (param) {
        return post(serviceName + '/workflow/procinst/start', param)
    },
    recall: function (id) {
        return post(serviceName + '/workflow/procinst/recall?id=' + id, null)
    },
}

// 流程任务
export const PROC_TASK = {
    get: function (id) {
        return get(serviceName + '/workflow/task/get/' + id)
    },
    listByProcinst: function (procinstId) {
        return get(serviceName + '/workflow/task/listByProcinst/' + procinstId)
    }
}

// 流程待办
export const PROC_UNDO = {
    get: function (id) {
        return get(serviceName + '/workflow/undo/get/' + id)
    },
    agree: function (param) {
        return post(serviceName + '/workflow/undo/agree', param)
    },
    refuse: function (param) {
        return post(serviceName + '/workflow/undo/refuse', param)
    },
    assignor: function (param) {
        return post(serviceName + '/workflow/undo/assignor', param)
    },
    readed: function (param) {
        return post(serviceName + '/workflow/undo/readed', param)
    },
}

export const UPLOAD_FILE = {
    path : serviceName + "/file/noBindUpload",
    onSuccess: function (response, file, fileList){
        console.info('response:' + response);
    }
}


