<!DOCTYPE html>
<html>
<head>
    <title>审批管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

</head>
<body>
    <div class="content">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md2">
                <div class="layui-card" lay-height="100px">
                    <div class="layui-card-header">
                        流程分组
                        <a href="javascript:;" title="添加分组" id="add-group-btn"><i class="layui-icon layui-icon-addition"></i></a>
                    </div>
                    <div class="layui-card-body">
                        <input type="text" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search">
                        <div id="procGroupTree" class="eleTree ele3" lay-filter="procGroupTree" style="margin-top: 20px;"></div>
                    </div>
                </div>
            </div>
            <div class="layui-col-md10">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <form class="layui-form" action="" onsubmit="return false">
                            <input type="hidden" name="groupId" value="" id="groupId">
                            <div class="layui-form-item">

                                <div class="layui-inline">
                                    <input type="text" name="name" placeholder="流程名称" autocomplete="off" class="layui-input">
                                </div>

                                <div class="layui-inline">
                                    <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>

                                <div class="layui-inline tool-btn right">
                                    <@shiro.hasPermission name="workflow:procdef:add">
                                    <button style="display: none;" class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add"  data-options="{url: '/static/workflow/procdesign/index.html?groupId=0', title: '添加'}" target="_blank"><i class="layui-icon">&#xe654;</i></button>
                                    </@shiro.hasPermission>
                                    <@shiro.hasPermission name="workflow:procdef:del">
                                    <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del"  data-options="{url: '/workflow/procdef/del', title: '删除'}"><i class="layui-icon">&#xe640;</i></button>
                                    </@shiro.hasPermission>
                                </div>
                            </div>
                        </form>

                        <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                        <script type="text/html" id="operation_btn">
                            <@shiro.hasPermission name="workflow:procdef:modify">
                            <a class="text-blue" lay-event="edit" data-options="{url: '/static/workflow/procdesign/index.html', title: '编辑', isAppendToken: false}" target="_blank"> 编辑 </a> |
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="workflow:procdef:del">
                            <a class="text-danger" lay-event="del"  data-options="{url: '/workflow/procdef/del', title: '删除'}"> 删除 </a>
                            </@shiro.hasPermission>
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        layui.use(['laytpl', 'table', 'http', 'eleTree', 'jquery', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            //模块名称
            var MODULE = "WORKFLOW-PROCDEF";

            var procGroupTree = null;
            http.post({
                url: GLOBAL.path.adminApi + '/workflow/group/list',
                async: true,
                backFun: function(result){

                    if(result != null){
                        for(var i = 0; i < result.length; i++){

                            if(result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                                result[i].label = result[i].name;
                            }else{
                                result[i].label = '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                            }

                            result[i].value = result[i].id;
                        }
                    }else{
                        result = [];
                    }

                    procGroupTree = eleTree.render({
                        elem: '#procGroupTree',
                        data: result,
                        highlightCurrent: true,
                        showLine: true,
                        showCheckbox: false,
                        defaultExpandedKeys: [''],
                        contextmenuList: [{eventName: "update", text: "修改"},
                                          {eventName: "del", text: "删除"}],
                        accordion: true,
                        expandOnClickNode: true,
                        request: {
                            name: 'label',
                            key: 'value'
                        },
                        searchNodeMethod: function(value,data) {
                            if (!value) return true;
                            return data.label.indexOf(value) !== -1;
                        }
                    });

                    // 节点点击事件
                    eleTree.on("nodeClick(procGroupTree)",function(d) {

                        $("input[name='groupId']").val(d.data.currentData.id);

                        var options = Utils.parseOptions($(".addBtn").attr("data-options"));
                        var dataUrl = options.url.split("?")[0];
                        options.url = dataUrl + "?groupId=" + d.data.currentData.id;
                        $(".addBtn").attr("data-options", JSON.stringify(options));

                        $("#search_btn").click();

                        $(".addBtn").show();
                    })

                    eleTree.on("nodeUpdate(procGroupTree)",function(d) {
                        Win.open({
                            url: "${context.contextPath}/view/workflow/procdef/editGroup?id=" + d.data.id,
                            type: "1",
                            title: "编辑分组"
                        })
                    })

                    eleTree.on("nodeDel(procGroupTree)", function(d) {

                        top.layer.confirm('删除后不可恢复，请确认!', function (index) {

                            var nodes = DataConvert.treeToListConvert(d.data, {})//遍历删除所有子节点

                            var ids = [];

                            for (var i = 0; i < nodes.length; i++) {
                                ids.push(nodes[i].id);
                            }

                            http.post({
                                url: GLOBAL.path.adminApi + '/workflow/group/del',
                                data: ids,
                                async: false,
                                backFun: function (result) {

                                    procGroupTree.remove(d.data.value);//直接删除父节点

                                    layer.close(index);
                                    top.layui.layer.msg("操作成功！");
                                }
                            });
                        });

                    })

                    $(".eleTree-search").on("change",function() {
                        procGroupTree.search($(this).val());
                    })
                }});

            $("#add-group-btn").click(function () {

                Win.open({
                    url: "${context.contextPath}/view/workflow/procdef/editGroup",
                    type: "1",
                    title: "添加分组"
                })
            });

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/workflow/procdef/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            groupId: $("input[name='groupId']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'name', title: '名称', width:150},
                    {field:'deployTime', title: '发布时间', width:180, templet: function (d) {
                        return util.toDateString(d.deployTime);
                    }},
                    {field:'sort', title: '排序', width:80},
                    {field:'createTime', title: '创建时间', width:180, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:150},
                    {field:'stateName', title: '状态', width:80, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.STATE.ENABLE.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }
                    }},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', minWidth:125}
                ]],
                id: 'lay_table',
                page: true
            });

            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });

            /**
             * 更新字典项列表
             */
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshProcdefTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

            /**
             * 添加节点
             */
            FdpCommand.register({
                module: MODULE,
                cmdName: "addProcGroupNode",
                callBack: function (options) {

                    options.label = null;

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        options.label = options.name;
                    }else{
                        options.label = '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    options.value = options.id;

                    procGroupTree.append("", options)
                }
            })

            FdpCommand.register({
                module: MODULE,
                cmdName: "updateProcGroupNode",
                callBack: function (options) {

                    options.label = null;

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        options.label = options.name;
                    }else{
                        options.label = '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    options.value = options.id;

                    procGroupTree.updateKeySelf(options.id, options)
                }
            })

        });
    </script>
</body>
</html>


