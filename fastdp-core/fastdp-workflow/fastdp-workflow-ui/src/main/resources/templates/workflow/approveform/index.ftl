<!DOCTYPE html>
<html>
<head>
    <title>审批表单</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

</head>
<body>
    <div id="container" id="workflow_form" class="layui-fluid">

        <template v-if="groups">

            <div v-if="hasAuthCount > 0">

                <div class="layui-row layui-col-space15'" v-for="(group, i) in groups" v-if="formMap[group.id].length > 0">
                    <div class="layui-card" style="margin-bottom: 20px;">
                        <div class="layui-card-header">{{group.name}}({{formMap[group.id].length}})</div>
                        <div class="layui-card-body">
                            <ul class="layui-row layuiadmin-card-team">

                                <li class="layui-col-md2" v-for="(form, j) in formMap[group.id]">
                                    <a href="javascript:;" @click="openForm(form.id)">
                                        <span class="layui-team-img" style="height: 40px; width: 40px; background-color: white;">
                                            <img :src="'${context.contextPath}/static/workflow/procdesign/img/approverIcon/A (' + (form.icon ==0 ? 1 : form.icon) + ').png'" style="height: 40px; width: 40px;"></img>
                                        </span>
                                        <span>{{form.name}}</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div v-else>
                无可用申请表单
            </div>

        </template>

    </div>

    <script>
        layui.use(['laytpl', 'table', 'http', 'eleTree', 'jquery', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            //模块名称
            var MODULE = "WORKFLOW-APPROVEFORM";

            new Vue({

                el: "#container",

                data(){
                    return {
                        groups: [],
                        formMap: {},
                        hasAuthCount: 0
                    }
                },

                created(){

                    var that = this;

                    that.loadGroup();
                },

                methods: {

                    /**
                     * 加载流程分组
                     */
                    loadGroup(){

                        var that = this;

                        http.post({
                            url: GLOBAL.path.adminApi + '/workflow/group/list',
                            async: false,
                            data: {
                                state: GLOBAL.constant.Dict.STATE.ENABLE.value
                            },
                            backFun: function(result){

                                for(var i = 0; i < result.length; i++){
                                    that.loadForm(result[i].id);
                                }

                                that.groups = result;
                            }});
                    },

                    /**
                     * 加载流程表单
                     */
                    loadForm(groupId){

                        var that = this;

                        http.post({
                            url: GLOBAL.path.adminApi + '/workflow/procdef/list',
                            async: false,
                            data: {
                                groupId: groupId,
                                state: GLOBAL.constant.Dict.STATE.ENABLE.value
                            },
                            backFun: function(result){

                                that.formMap[groupId] = [];

                                for(var i = 0; i < result.length; i++){

                                    if(that.hasAuth(result[i]) && GLOBAL.constant.Dict.FLOW_FORM_TYPE.ONLINE.value == JSON.parse(result[i].config).basicSetting.formType){
                                        that.formMap[groupId].push(result[i]);

                                        that.hasAuthCount = that.hasAuthCount + 1;
                                    }
                                }
                            }});
                    },

                    openForm(procdefId){

                        CommonFun.add({
                            busId: procdefId,
                            type: GLOBAL.constant.Dict.COMMON_FUN_TYPE.OA_FORM.value
                        })

                        Win.open({
                            content: '${context.contextPath}/static/workflow/procdesign/index.html?procdefId=' + procdefId + "#/form",
                            type: 2,
                            title: '填写审批单',
                            isAppendToken: false
                        })
                    },

                    hasAuth(form){

                        var that = this;

                        var result = false;

                        var loginUser = SessionUtils.getLoginUser();

                        var config = JSON.parse(form.config);

                        if(form.fdpCore == GLOBAL.constant.FDPCORE){
                            return false;
                        }

                        var initiator = config.basicSetting.initiator;

                        if(!initiator){
                            return true;
                        }


                        if(initiator.length == 0 || loginUser.superAdmin){

                            result = true;
                        }else{
                            for(var i = 0; i < initiator.length; i++){

                                if(loginUser.dept == initiator[i].deptId || loginUser.userId == initiator[i].userId){
                                    result = true;
                                    break;
                                }
                            }
                        }

                        return result;
                    },

                    formatUndoDate(date){
                        return DateUtil.format(date);
                    },
                    moreUndos(){

                    },
                    openUndo(procinst){

                    }
                }
            })
        });
    </script>
</body>
</html>


