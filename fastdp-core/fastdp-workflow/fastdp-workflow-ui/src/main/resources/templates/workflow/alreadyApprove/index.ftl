<!DOCTYPE html>
<html>
<head>
    <title>待处理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

</head>
<body>
<div class="content">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md2">
            <div class="layui-card" lay-height="100px">
                <div class="layui-card-body">
                    <input type="text" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search">
                    <div id="procGroupTree" class="eleTree ele3" lay-filter="procGroupTree" style="margin-top: 20px;"></div>
                </div>
            </div>
        </div>
        <div class="layui-col-md10">
            <div class="layui-card">
                <div class="layui-card-body">

                    <form class="layui-form" action="" onsubmit="return false">
                        <input type="hidden" name="groupId" value="" id="groupId">
                        <input type="hidden" name="procdefId" value="" id="procdefId">
                        <input type="hidden" name="state" value="${RequestParameters.state!}" id="state">

                        <div class="layui-form-item">

                            <div class="layui-inline">
                                <input type="text" name="title" placeholder="流程名称" autocomplete="off" class="layui-input">
                            </div>

                            <div class="layui-inline">
                                <input type="text" id="startTime" name="startTime" placeholder="申请时间范围开始" autocomplete="off" class="layui-input">
                            </div>

                            <div class="layui-inline">
                                <input type="text" id="endTime" name="endTime" placeholder="申请时间范围截至" autocomplete="off" class="layui-input">
                            </div>


                            <div class="layui-inline">
                                <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </div>
                    </form>

                    <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                    <script type="text/html" id="operation_btn">

                        <a class="text-blue edit-btn" lay-event="workflowform_view" data-options="{url: '/static/workflow/procdesign/index.html?id={{d.id}}#/view',width:'{{JSON.parse(d.procdefConfig).basicSetting.width}}', enablePrint: true}" target="_blank">详情</a>

                    </script>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="${context.contextPath}/static/config/flow-admin-config.js"></script>


<script>
    layui.use(['laytpl', 'table', 'http', 'eleTree', 'jquery', 'FdpUtil', 'util', 'laydate'], function(){

        var laytpl = layui.laytpl,
            table = layui.table,
            layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            eleTree = layui.eleTree,
            util = layui.util,
            laydate = layui.laydate;

        //模块名称
        var MODULE = "WORKFLOW-MYSEND";

        var NODE_TYPE = {
            PROC_DEF: 1,
            PROC_GROUP: 0
        }

        laydate.render({
            elem: '#starTime'
        });

        laydate.render({
            elem: '#endTime'
        });

        var procGroupTree = null;
        http.post({
            url: GLOBAL.path.adminApi + '/workflow/group/list',
            async: true,
            backFun: function(result){

                if(result != null){

                    result.splice(0,0, {
                        name: '全部',
                        id: '-1',
                        state: GLOBAL.constant.Dict.STATE.ENABLE.value,
                        checked: true
                    })

                    for(var i = 0; i < result.length; i++){

                        if(result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                            result[i].label = result[i].name;
                        }else{
                            result[i].label = '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                        }

                        result[i].value = result[i].id;
                    }
                }else{
                    result = [];
                }

                procGroupTree = eleTree.render({
                    elem: '#procGroupTree',
                    data: result,
                    highlightCurrent: true,
                    showLine: true,
                    showCheckbox: false,
                    defaultExpandAll: true,
                    accordion: false,
                    expandOnClickNode: false,
                    request: {
                        name: 'label',
                        key: 'value'
                    },
                    searchNodeMethod: function(value,data) {
                        if (!value) return true;
                        return data.label.indexOf(value) !== -1;
                    }
                });

                for(var i = 0; i < result.length; i++){

                    if(result[i].id == "-1"){
                        continue;
                    }

                    http.post({
                        url: GLOBAL.path.adminApi + '/workflow/procdef/list',
                        async: false,
                        data: {
                            groupId: result[i].id
                        },
                        backFun: function (procdefs) {

                            if(procdefs){

                                for(var j = 0; j < procdefs.length; j++){

                                    procdefs[j].nodeType = NODE_TYPE.PROC_DEF;
                                    procdefs[j].value = procdefs[j].id;
                                    procdefs[j].label = procdefs[j].name;
                                    procdefs[j].data = procdefs[j];

                                    procGroupTree.append(result[i].id, procdefs[j])
                                }
                            }
                        }
                    });
                }

                procGroupTree.unCheckNodes();

                // 节点点击事件
                eleTree.on("nodeClick(procGroupTree)",function(d) {

                    $("input[name='procdefId']").val("");
                    $("input[name='groupId']").val("");

                    if(d.data.currentData.id == "-1"){
                        $("input[name='procdefId']").val("");
                        $("input[name='groupId']").val("");
                        return;
                    }

                    if(NODE_TYPE.PROC_DEF == d.data.currentData.nodeType){
                        $("input[name='procdefId']").val(d.data.currentData.id);
                    }else{
                        $("input[name='groupId']").val(d.data.currentData.id);
                    }

                    $("#search_btn").click();
                })

                $(".eleTree-search").on("change",function() {
                    procGroupTree.search($(this).val());
                })
            }});

        //方法级渲染
        table.render({
            elem: '#lay_table_content',
            requestFun: function(page, size, renderFun) {

                var startTime = $("#startTime").val();
                var endTime = $("#endTime").val();

                if(startTime){
                    startTime = new Date(startTime).getTime();
                }

                if(endTime){
                    endTime = new Date(endTime).getTime();
                }

                http.post({
                    url: GLOBAL.path.adminApi + '/workflow/procinst/alreadyApprovePage',
                    async: false,
                    data: {
                        title: $("input[name='title']").val(),
                        groupId: $("input[name='groupId']").val(),
                        procdefId: $("input[name='procdefId']").val(),
                        startTime: startTime,
                        endTime: endTime,
                        state: $("input[name='state']").val(),
                        page: {
                            current: page,
                            size: size ? size : GLOBAL.page.size
                        }
                    },
                    backFun: function(result){

                        renderFun({
                            data: result.records,
                            count: result.total
                        });
                    }});
            },
            cols: [[
                {checkbox: true, fixed: true},
                {type: 'numbers'},
                {field:'title', title: '标题', width:300},
                {field:'createUserName', title: '发起人', width:150},
                {field:'startTime', title: '申请时间', width:180, templet: function (d) {
                        return util.toDateString(d.startTime);
                    }},
                {field:'nodeName', title: '当前节点', width:180},
                {field:'stateName', title: '状态', width:100, templet: function (d) {
                        return Dict.getItemName(Dict.Type.FLOW_PROCINST_STATE, d.state);
                    }},
                {fixed: 'right', title:'操作', toolbar: '#operation_btn', minWidth:125}
            ]],
            id: 'lay_table',
            page: true
        });

        $('#search_btn').on('click', function(){

            //执行重载
            table.reload('lay_table', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
    });
</script>
</body>
</html>


