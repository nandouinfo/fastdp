<!DOCTYPE html>
<html>
<head>
    <title>我的申请</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">

</head>
    <body style="background-color:white;">
        <div>
            <div class="layui-tab layui-tab-brief" style="margin-top: 0px;"  lay-filter="docDemoTabBrief">
                <ul class="layui-tab-title">
                    <li class="layui-this">待查阅</li>
                    <li>已查阅</li>
                    <li>全部</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <iframe src="${context.contextPath}/view/workflow/myRead/list?state=0&token=${RequestParameters.token!}" frameborder="0"></iframe>
                    </div>
                    <div class="layui-tab-item">
                        <iframe src="${context.contextPath}/view/workflow/myRead/list?state=1&token=${RequestParameters.token!}" frameborder="0"></iframe>
                    </div>
                    <div class="layui-tab-item">
                        <iframe src="${context.contextPath}/view/workflow/myRead/list?token=${RequestParameters.token!}" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <script>
            layui.use(['element', 'jquery'], function(){

                var $ = layui.jquery;

                $("iframe").width("100%").height($(window).height() - 100);

            })
        </script>

    </body>
</html>


