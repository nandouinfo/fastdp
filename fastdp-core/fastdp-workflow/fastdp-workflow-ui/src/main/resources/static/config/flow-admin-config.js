layui.use(['http', 'FdpUtil'], function(){

    var layer = layui.layer,
        $ = layui.jquery,
        http = layui.http;

    //追加字典类型
    Dict.appendType("FLOW_PROCINST_STATE");
    Dict.appendType("FLOW_PROCINST_FINISHED_STATE");


    Dict.appendType("FLOW_TASK_STATE");
    Dict.appendType("FLOW_TASK_FINISHED_STATE");

    Dict.appendType("FLOW_UNDO_STATE");
    Dict.appendType("FLOW_UNDO_FINISHED_STATE");


    GLOBAL.constant.Dict.FLOW_PROCINST_STATE = {
        IN_APPROVAL: {
            label: '审批中',
            value: 1
        },
        PASS: {
            label: '通过',
            value: 2
        },
        REJECT: {
            label: '驳回',
            value: -1
        },
        RECALL: {
            label: '撤回',
            value: 3
        }
    }

    GLOBAL.constant.Dict.FLOW_PROCINST_FINISHED_STATE = {
        IN_APPROVAL: {
            label: '审批中',
            value: 0
        },
        END: {
            label: '结束',
            value: 1
        }
    }

    GLOBAL.constant.Dict.FLOW_TASK_STATE = {

        IN_APPROVAL: {
            label: '审批中',
            value: 1
        },
        PASS: {
            label: '通过',
            value: 2
        },
        REJECT: {
            label: '驳回',
            value: -1
        },
        COPY: {
            label: '已抄送',
            value: 3
        }
    }

    GLOBAL.constant.Dict.FLOW_TASK_FINISHED_STATE = {
        PROCESSING: {
            label: '处理中',
            value: -1
        },
        END: {
            label: '结束',
            value: 1
        }
    }

    GLOBAL.constant.Dict.FLOW_UNDO_STATE = {

        WAIT_HANDLE: {
            label: '等待处理',
            value: 0
        },
        READED: {
            label: '已读',
            value: 1
        },
        AGREE: {
            label: '同意',
            value: 2
        },
        REFUSE: {
            label: '拒绝',
            value: 3
        },
        ASSIGNOR: {
            label: '委托',
            value: 4
        }
    }

    GLOBAL.constant.Dict.FLOW_UNDO_FINISHED_STATE = {

        PROCESSING: {
            label: '处理中',
            value: 0
        },
        PROCESSEND: {
            label: '已处理',
            value: 1
        }
    }


})
