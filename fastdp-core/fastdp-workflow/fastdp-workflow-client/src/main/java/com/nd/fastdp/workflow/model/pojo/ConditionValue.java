package com.nd.fastdp.workflow.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ConditionValue implements Serializable {

    private String type;
    private Object value;
}
