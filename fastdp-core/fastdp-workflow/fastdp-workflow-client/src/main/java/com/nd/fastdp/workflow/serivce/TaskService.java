package com.nd.fastdp.workflow.serivce;

import com.nd.fastdp.workflow.model.bo.task.TaskBO;
import com.nd.fastdp.workflow.model.dto.task.TaskAddParam;

import java.util.List;

public interface TaskService {

    Integer addReadCount(String id);

    Integer addAgreenCount(String id);

    TaskBO add(TaskAddParam param);

    List<TaskBO> list(String procinstId);

    TaskBO get(String id);

    TaskBO finished(String id, Integer state);
}
