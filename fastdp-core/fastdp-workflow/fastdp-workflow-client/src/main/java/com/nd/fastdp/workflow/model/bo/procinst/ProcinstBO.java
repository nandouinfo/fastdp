package com.nd.fastdp.workflow.model.bo.procinst;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ProcinstBO extends BaseBO implements Serializable {

    private String id;

    private String groupId;// 流程分组ID
    private String procdefId; //对应表procdef的id
    private String procdefName; //对应表procdef的name
    private String procdefConfig;//流程定义

    private String title; //标题，如："张三的请假流程"

    private String nodeId; //当前所处于节点的Id
    private String nodeName;//当前所处节点的名称
    private String candidate; //当前审批人或者审批用户组
    private String taskId; //当前任务id

    private Long startTime;//开始时间
    private Long endTime;//结束时间
    private Long duration;// 耗时

    private String nodeList;//节点列表
    private Integer step;//当前执行到第几个节点

    private String flowVal;//流程变量

    private Integer finished; //是否已结束
    private Integer state;

    private Long createTime;
    private String createUser;
    private String createUserName;
    private String createDept;
    private String createDeptName;
}
