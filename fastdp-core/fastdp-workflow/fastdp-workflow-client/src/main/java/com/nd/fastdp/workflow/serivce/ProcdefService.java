package com.nd.fastdp.workflow.serivce;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.workflow.model.bo.procdef.ProcdefBO;
import com.nd.fastdp.workflow.model.dto.procdef.*;
import com.nd.fastdp.workflow.model.pojo.Node;

import java.util.List;

public interface ProcdefService {

    ProcdefBO add(ProcdefAddParam param);

    void del(DelParam param);

    ProcdefBO modify(ProcdefModifyParam param);

    void modifyState(StateParam param);

    ProcdefBO get(String id);

    ProcdefBO getByCode(String code);

    List<ProcdefBO> listAllEnable();

    List<ProcdefBO> listAll();

    List<ProcdefBO> list(ProcdefListParam param);

    Page<ProcdefBO> page(ProcdefQueryParam param);

    List<Node> flowLine(ProcdefFlowLineParam param);
}
