package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程实例任务会签类型
 */
@Getter
@AllArgsConstructor
public enum CounterSignTypeEnum implements BaseEnum {

    YEST(true, "会签(须所有审批人同意)"),
    NO(false, "或签(一名审批人同意或拒绝即可)");

    private Boolean value;

    private String desc;

}
