package com.nd.fastdp.workflow.model.dto.procdef;

import com.nd.fastdp.framework.pojo.dto.Param;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

@ApiModel("流程线查询参数")
@Data
@Accessors(chain = true)
public class ProcdefFlowLineParam extends Param implements Serializable {

    private String procdefId;

    private Map<String, Object> var;

    @Override
    public void init() {

    }
}
