package com.nd.fastdp.workflow.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class FormOperate implements Serializable {

    private String formId;
    private String formOperate;
}
