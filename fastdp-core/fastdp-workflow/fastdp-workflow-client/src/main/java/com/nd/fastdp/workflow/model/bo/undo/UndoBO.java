package com.nd.fastdp.workflow.model.bo.undo;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UndoBO extends BaseBO implements Serializable {

    private String id;

    private String userId; //表示当前审批的用户
    private String userName; //当前审批的用户名称
    private String deptId;//部门
    private String deptName;//部门名称

    private String procinstId;//流程实例ID
    private String taskId;//任务表TaskId

    private Integer result;//处理结果

    private String assignorUndoId;//委托待办ID

    private Integer finished;

    private Long createTime;//创建时间
    private Long assertionTime;//断言时间

    private String suggest;//意见
}
