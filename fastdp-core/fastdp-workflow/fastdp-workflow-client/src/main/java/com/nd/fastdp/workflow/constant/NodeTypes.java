package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  NodeTypes implements BaseEnum {

    START("start", "开始"),
    CONDITION("condition", "条件"),
    APPROVER("approver", "审批"),
    NOTIFIER("copy", "抄送人"),
    EMPTY("empty", "空节点");

    private String value;

    private String desc;
}
