package com.nd.fastdp.workflow.model.bo.task;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import com.nd.fastdp.workflow.model.bo.undo.UndoBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class TaskBO extends BaseBO implements Serializable {

    private String id;

    private String nodeId;//表示节点，如："主管审批"结点
    private String nodeType;//节点类型
    private String nodeName;//节点名称
    private String nodeCode;//节点编码
    private Integer step;//表示任务对应的执行流位置
    private String procinstId; //流程实例ID

    private String assignee;//任务的处理人
    private Long createTime;
    private Long claimTime;//审批时间

    private Integer memberCount;//表示当前任务需要多少人审批之后才能结束，默认是 1
    private Integer unCompleteNum;//表示还有多少人没有审批，默认是1
    private Integer agreeNum;//表示通过的人数

    private Boolean counterSign;//是否会签

    private Integer state;//同意拒绝

    private Integer finished;

    private List<UndoBO> undos;
}
