package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程实例结束标识状态
 */
@Getter
@AllArgsConstructor
public enum ProcinstFinishedFlagEnum implements BaseEnum {

    IN_APPROVAL(0, "审批中"),
    END(1, "结束");

    private Integer value;

    private String desc;

}
