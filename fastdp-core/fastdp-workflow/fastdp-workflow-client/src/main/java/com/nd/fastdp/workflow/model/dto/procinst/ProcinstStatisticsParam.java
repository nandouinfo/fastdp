package com.nd.fastdp.workflow.model.dto.procinst;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("流程实例统计参数")
@Data
@Accessors(chain = true)
public class ProcinstStatisticsParam extends AddParam implements Serializable {

    private String code;
    private Integer state;
    private String groupId;
}
