package com.nd.fastdp.workflow.model.bo.group;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class GroupBO extends BaseBO implements Serializable {

    private String id;

    private String name;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;
}
