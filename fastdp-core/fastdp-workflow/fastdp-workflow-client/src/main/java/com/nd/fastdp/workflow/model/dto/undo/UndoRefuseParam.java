package com.nd.fastdp.workflow.model.dto.undo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("流程任务待办拒绝参数")
@Data
@Accessors(chain = true)
public class UndoRefuseParam {

    private String undoId;

    private String suggest;
}
