package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProcinstStateType implements BaseEnum {

    IN_APPROVAL(1, "审批中"),
    PASS(2, "通过"),
    REJECT(-1, "驳回"),
    RECALL(3, "撤回");

    private Integer value;

    private String desc;
}
