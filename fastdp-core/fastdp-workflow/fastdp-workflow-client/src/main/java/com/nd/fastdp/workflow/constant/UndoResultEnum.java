package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程待办处理结果
 */
@Getter
@AllArgsConstructor
public enum UndoResultEnum implements BaseEnum {

    WAIT_HANDLE(0, "等待处理"),
    READED(1, "已读"),
    AGREE(2, "同意"),
    REFUSE(3, "拒绝"),
    ASSIGNOR(4, "委托");

    private Integer value;

    private String desc;

}
