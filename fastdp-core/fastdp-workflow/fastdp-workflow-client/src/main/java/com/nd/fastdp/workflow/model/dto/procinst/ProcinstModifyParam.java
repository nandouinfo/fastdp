package com.nd.fastdp.workflow.model.dto.procinst;

import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("流程实例添加参数")
@Data
@Accessors(chain = true)
public class ProcinstModifyParam extends ModifyParam implements Serializable {

    @ApiModelProperty(value = "流程实例的Id", required = true, example = "1")
    @NotEmpty(message = "流程实例的Id不能为空")
    private String id;

    @ApiModelProperty(value = "节点ID", required = true, example = "")
    @NotNull(message = "节点ID不能为空")
    private String nodeId;

    @ApiModelProperty(value = "当前审批人或者审批用户组", required = true, example = "")
    @NotNull(message = "当前审批人或者审批用户组不能为空")
    private String candidate;

    @ApiModelProperty(value = "任务ID", required = true, example = "")
    @NotNull(message = "任务ID不能为空")
    private String taskId;

    @ApiModelProperty(value = "结束标识", example = "")
    private Integer finished;

    @ApiModelProperty(value = "结束时间", example = "")
    private Long endTime;

}
