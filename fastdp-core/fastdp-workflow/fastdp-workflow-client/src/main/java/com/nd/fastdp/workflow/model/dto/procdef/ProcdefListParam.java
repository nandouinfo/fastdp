package com.nd.fastdp.workflow.model.dto.procdef;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ListParam;
import com.nd.fastdp.framework.validator.InEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("流程定义列表参数")
@Data
@Accessors(chain = true)
public class ProcdefListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "名称", example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "分组", required = true, example = "测试")
    @NotEmpty(message = "分组不能为空")
    private String groupId;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", example = "1")
    private Integer state;
}
