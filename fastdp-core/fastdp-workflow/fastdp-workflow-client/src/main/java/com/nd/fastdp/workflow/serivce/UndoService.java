package com.nd.fastdp.workflow.serivce;

import com.nd.fastdp.workflow.model.bo.undo.UndoBO;
import com.nd.fastdp.workflow.model.dto.undo.*;

import java.util.List;

public interface UndoService {

    UndoBO get(String id);

    void add(UndoAddParam param);

    //委托
    void assignor(UndoAssignorParam param);

    //同意
    void agree(UndoAgreeParam param);

    //已读
    void read(UndoReadParam param);

    //拒绝
    void refuse(UndoRefuseParam param);

    List<UndoBO> listByTaskId(String taskId);

    List<UndoBO> listByProcinstId(String procinstId);

    void finished(String id, Integer state);
}
