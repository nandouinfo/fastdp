package com.nd.fastdp.workflow.model.dto.procinst;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("流程实例启动参数")
@Data
@Accessors(chain = true)
public class ProcinstStartParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "流程定义的Id", required = true, example = "1")
    @NotEmpty(message = "流程定义的Id不能为空")
    private String procdefId;

    @ApiModelProperty(value = "标题", required = true, example = "张三的请假单")
    @NotNull(message = "标题不能为空")
    private String title;

    @ApiModelProperty(value = "流程变量", example = "")
    private String flowVal;

    @ApiModelProperty(value = "流程节点", example = "")
    private String nodeList;//流程节点
}
