package com.nd.fastdp.workflow.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class NodeInfo implements Serializable {

    private String title;
    private String nodeId;
    private String type;
    private List aprover;
    private String aproverType;
    private Integer memberCount; //审批人员数量
    private Integer level;
    private String actType;
}
