package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AssigneeType implements BaseEnum {

    USER("user", "指定成员"),
    DIRECTOR("director", "主管"),
    ROLE("role", "角色"),
    POSITION("position", "岗位"),
    MYSELF("myself", "发起人自己"),
    OPTIONAL("optional", "发起人自选");

    private String value;

    private String desc;
}
