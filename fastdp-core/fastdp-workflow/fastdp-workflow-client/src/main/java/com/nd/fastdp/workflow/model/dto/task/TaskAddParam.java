package com.nd.fastdp.workflow.model.dto.task;

import com.nd.fastdp.workflow.model.pojo.Node;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("流程任务添加参数")
@Data
@Accessors(chain = true)
public class TaskAddParam {

    private String procinstId;

    private Node node;
}
