package com.nd.fastdp.workflow.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Approver implements Serializable {

    private String id;
    private String name;

    private String deptId;
    private String userId;
    private String roleId;
    private String positionId;

}
