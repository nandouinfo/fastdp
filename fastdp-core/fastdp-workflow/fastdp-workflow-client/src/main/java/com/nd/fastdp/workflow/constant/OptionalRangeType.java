package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OptionalRangeType implements BaseEnum {

    ALL("ALL", "全公司"),
    USER("USER", "指定成员"),
    ROLE("ROLE", "角色"),
    POSITION("POSITION", "职务");

    private String value;

    private String desc;
}
