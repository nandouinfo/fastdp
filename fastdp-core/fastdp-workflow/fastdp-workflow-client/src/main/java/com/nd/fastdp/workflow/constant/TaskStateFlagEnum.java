package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程任务状态标识
 */
@Getter
@AllArgsConstructor
public enum TaskStateFlagEnum implements BaseEnum {

    IN_APPROVAL(1, "审批中"),
    PASS(2, "通过"),
    REJECT(-1, "驳回"),
    COPY(3, "已抄送");

    private Integer value;

    private String desc;

}
