package com.nd.fastdp.workflow.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
public class Properties  implements Serializable {

    private String title;//节点标题
    private String code;//节点编码
    private String assigneeType;// 待审批人员类型

    private Boolean counterSign;//会签，true 需所有审批人同意, false 一名审批人同意或拒绝即可
    private Boolean optionalMultiUser;// 是否允许多选用户
    private String optionalRange;//选择范围

    private List<Map<String, Object>> menbers;//参与节点的: 人、角色、岗位、等

    private List<FormOperate> formOperates;//表单操作权限

    //以下为条件节点特有
    private List<Condition> conditions;

    private Object initiator;//发起人为此时走选择此流程
    private Integer priority; //优先级别

    private Boolean userOptional;//是否允许用户自选

    private String flowForm;//自定义表单组件


    private List roles;
    private List positions;
    private List users;

}
