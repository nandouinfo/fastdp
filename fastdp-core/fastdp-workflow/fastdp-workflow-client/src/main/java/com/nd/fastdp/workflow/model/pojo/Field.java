package com.nd.fastdp.workflow.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Field {

    private String layout;
    private String tagIcon;
    private String label;
    private String vModel;
    private Integer formId;
    private String tag;
    private String placeholder;
    private String defaultValue;
    private Integer span;
    private Boolean clearable;
    private String prepend;
    private String append;
    private Integer maxlength;
    private Boolean readonly;
    private Boolean disabled;
    private Boolean required;
    private Boolean changeTag;

    private String type;

    private List<Field> children;

}
