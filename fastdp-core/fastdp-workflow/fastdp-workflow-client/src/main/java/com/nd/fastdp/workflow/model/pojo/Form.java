package com.nd.fastdp.workflow.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Form {

    private String formRef;
    private String formModel;
    private String size;
    private String labelPosition;
    private Double labelWidth;
    private String formRules;
    private Integer gutter;
    private Boolean disabled;
    private Integer span;
    private Boolean formBtns;

    private List<Field> fiedls;


    public Field findById(Integer formId){

       return findById(formId, fiedls);
    }

    private Field findById(Integer formId, List<Field> fiedls){

        Field field = null;

        for(Field f : fiedls){

            if(f.getFormId().intValue() == formId.intValue()){

                field = f;

                break;
            }

            if(f.getChildren() != null && f.getChildren().size() > 0){

                field = findById(formId, f.getChildren());

                if(field != null){
                    break;
                }
            }

        }

        return field;
    }
}
