package com.nd.fastdp.workflow.model.bo.procdef;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ProcdefBO extends BaseBO implements Serializable {

    private String id;

    private String name;
    private String code;
    private String groupId;
    private Integer version;
    private String config;
    private String icon;

    private Long deployTime;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;
}
