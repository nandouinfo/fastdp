package com.nd.fastdp.workflow.model.dto.procinst;

import com.nd.fastdp.framework.pojo.dto.PageParam;
import com.nd.fastdp.framework.pojo.dto.Param;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("流程查询参数")
@Data
@Accessors(chain = true)
public class ProcinstQueryParam extends Param implements Serializable {

    @ApiModelProperty(value = "分页", example = "current：1, size:10")
    private PageParam page;

    @ApiModelProperty(value = "操作人")
    private String userId;//操作用户Id

    private String groupId;// 流程分组ID
    private String procdefId; //对应表procdef的id

    private Long startTime;
    private Long endTime;

    private Long completeStartTime;
    private Long completeEndTime;

    private Integer state;
    private Integer finished;

    @Override
    public void init() {
        this.userId = getLoginUser().getUserId();
    }
}
