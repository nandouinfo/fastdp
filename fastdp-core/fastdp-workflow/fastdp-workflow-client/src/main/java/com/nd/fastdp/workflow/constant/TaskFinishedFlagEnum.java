package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程任务结束标识状态
 */
@Getter
@AllArgsConstructor
public enum TaskFinishedFlagEnum implements BaseEnum {

    PROCESSING(0, "处理中"),
    END(1, "结束");

    private Integer value;

    private String desc;

}
