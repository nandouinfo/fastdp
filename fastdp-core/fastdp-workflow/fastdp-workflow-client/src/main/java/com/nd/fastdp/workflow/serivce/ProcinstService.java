package com.nd.fastdp.workflow.serivce;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.workflow.model.bo.procinst.ProcinstBO;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstModifyParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstQueryParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstStartParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstStatisticsParam;

public interface ProcinstService {

    ProcinstBO start(ProcinstStartParam param);

    void addStep(String id);

    void recall(String id);

    ProcinstBO modify(ProcinstModifyParam param);

    void modify(String id, String flowVal);

    void del(DelParam delParam);

    ProcinstBO get(String id);

    ProcinstBO finished(String id, Integer state);

    ProcinstBO updateTask(String id, String taskId);

    Page<ProcinstBO> page(ProcinstQueryParam param);

    Page<ProcinstBO> mySendPage(ProcinstQueryParam param);

    Page<ProcinstBO> waitApprovePage(ProcinstQueryParam param);

    Page<ProcinstBO> alreadyApprovePage(ProcinstQueryParam param);

    Integer statistics(ProcinstStatisticsParam param);
}
