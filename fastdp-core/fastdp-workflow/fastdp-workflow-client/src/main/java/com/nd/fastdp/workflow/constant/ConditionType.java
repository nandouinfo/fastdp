package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ConditionType implements BaseEnum {

    LT("lt", "小于"),
    LTE("lte", "小于等于"),
    GT("gt", "大于"),
    GTE("gte", "大于等于"),
    EQ("eq", "等于"),
    BET("bet", "介于两数之间");

    private String value;

    private String desc;
}
