package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FormOperateType implements BaseEnum {

    EDIT("2", "可编辑"),
    READ("1", "只读"),
    HIDE("approver", "隐藏");

    private String value;

    private String desc;
}
