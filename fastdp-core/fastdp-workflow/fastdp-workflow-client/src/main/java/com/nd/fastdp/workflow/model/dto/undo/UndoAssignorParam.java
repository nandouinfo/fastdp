package com.nd.fastdp.workflow.model.dto.undo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("流程任务待办委托执行人")
@Data
@Accessors(chain = true)
public class UndoAssignorParam {

    private String undoId;

    private String userId;//委托人ID
    private String userName; //当前审批的用户名称
    private String deptId;//部门
    private String deptName;//部门名称

    private String suggest;
}
