package com.nd.fastdp.workflow.model.dto.undo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("流程任务待办已读参数")
@Data
@Accessors(chain = true)
public class UndoReadParam {

    private String undoId;

    private String suggest;
}
