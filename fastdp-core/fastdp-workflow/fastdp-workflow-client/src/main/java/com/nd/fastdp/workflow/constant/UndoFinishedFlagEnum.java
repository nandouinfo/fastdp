package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程待办状态
 */
@Getter
@AllArgsConstructor
public enum UndoFinishedFlagEnum implements BaseEnum {

    PROCESSING(0, "处理中"),
    PROCESSEND(1, "已处理");

    private Integer value;

    private String desc;

}
