package com.nd.fastdp.workflow.serivce;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.workflow.model.bo.group.GroupBO;
import com.nd.fastdp.workflow.model.dto.group.GroupAddParam;
import com.nd.fastdp.workflow.model.dto.group.GroupListParam;
import com.nd.fastdp.workflow.model.dto.group.GroupModifyParam;
import com.nd.fastdp.workflow.model.dto.group.GroupQueryParam;

import java.util.List;

public interface GroupService {

    GroupBO add(GroupAddParam param);

    void del(DelParam param);

    GroupBO modify(GroupModifyParam param);

    void modifyState(StateParam param);

    GroupBO get(String id);

    List<GroupBO> listAllEnable();

    List<GroupBO> listAll();

    List<GroupBO> list(GroupListParam param);

    Page<GroupBO> page(GroupQueryParam param);
}
