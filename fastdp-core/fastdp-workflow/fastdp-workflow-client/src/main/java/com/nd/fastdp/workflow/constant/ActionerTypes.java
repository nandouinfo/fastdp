package com.nd.fastdp.workflow.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ActionerTypes implements BaseEnum {

    CREATER("creater", "创建人"),
    APPROVER("approver", "审批"),
    NOTIFIER("copy", "抄送人");

    private String value;

    private String desc;
}
