package com.nd.fastdp.workflow.model.dto.undo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel("流程任务待办添加参数")
@Data
@Accessors(chain = true)
public class UndoAddParam {

    private String procinstId;//流程实例ID
    private String taskId;//任务表TaskId

    private String userId; //表示当前审批的用户
    private String userName; //当前审批的用户名称

    private String deptId;//部门
    private String deptName;//部门名称
}
