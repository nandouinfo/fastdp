package com.nd.fastdp.workflow.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.workflow.model.dto.group.GroupListParam;
import com.nd.fastdp.workflow.model.dto.group.GroupQueryParam;
import com.nd.fastdp.workflow.model.entity.Group;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupMapper extends BaseMapper<Group> {

    default List<Group> listAllEnable(){

        return selectList(new QueryWrapper<Group>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                  .eq("state", StatusEnum.ENABLE.getValue())
                                                  .orderByAsc("sort"));
    }

    default List<Group> listAll(){

        return selectList(new QueryWrapper<Group>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }

    default List<Group> list(GroupListParam param){

        return selectList(new QueryWrapper<Group>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                       .eq(null != param.getState(), "state", param.getState())
                                                       .orderByAsc("sort"));
    }

    default IPage<Group> page(IPage<Group> page, GroupQueryParam param){

        return selectPage(page, new QueryWrapper<Group>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                            .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                            .eq(null != param.getState(), "state", param.getState())
                                                            .orderByAsc("sort"));
    }

    default void del(DelParam param) {

        Group group = null;
        for (String id : param.getIds()) {

            group = new Group();

            group.setId(id);
            group.init(param);
            group.setDel(DelStatusEnum.DEL.getValue());

            updateById(group);
        }
    }

    default void updateState(StateParam param) {

        Group group = null;
        for (String id : param.getIds()) {

            group = new Group();

            group.setId(id);
            group.init(param);
            group.setState(param.getState());

            updateById(group);
        }
    }
}
