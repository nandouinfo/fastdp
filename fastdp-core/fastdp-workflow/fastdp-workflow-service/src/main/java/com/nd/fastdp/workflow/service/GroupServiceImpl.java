package com.nd.fastdp.workflow.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.workflow.convert.GroupConvert;
import com.nd.fastdp.workflow.dao.GroupMapper;
import com.nd.fastdp.workflow.model.bo.group.GroupBO;
import com.nd.fastdp.workflow.model.dto.group.GroupAddParam;
import com.nd.fastdp.workflow.model.dto.group.GroupListParam;
import com.nd.fastdp.workflow.model.dto.group.GroupModifyParam;
import com.nd.fastdp.workflow.model.dto.group.GroupQueryParam;
import com.nd.fastdp.workflow.model.entity.Group;
import com.nd.fastdp.workflow.serivce.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupMapper groupMapper;

    @Override
    public GroupBO add(GroupAddParam param) {

        Group group = GroupConvert.INSTANCE.from(param);

        groupMapper.insert(group);

        return GroupConvert.INSTANCE.to(group);
    }

    @Override
    public void del(DelParam param) {
        groupMapper.del(param);
    }

    @Override
    public GroupBO modify(GroupModifyParam param) {

        Group group = GroupConvert.INSTANCE.from(param);

        groupMapper.updateById(group);

        return get(group.getId());
    }

    @Override
    public void modifyState(StateParam param) {
        groupMapper.updateState(param);
    }

    @Override
    public GroupBO get(String id) {

        Group group = groupMapper.selectById(id);

        return GroupConvert.INSTANCE.to(group);
    }

    @Override
    public List<GroupBO> listAllEnable() {

        List<Group> groups =  groupMapper.listAllEnable();

        return GroupConvert.INSTANCE.to(groups);
    }

    @Override
    public List<GroupBO> listAll() {

        List<Group> groups =  groupMapper.listAll();

        return GroupConvert.INSTANCE.to(groups);
    }

    @Override
    public List<GroupBO> list(GroupListParam param) {

        List<Group> groups =  groupMapper.list(param);

        return GroupConvert.INSTANCE.to(groups);
    }

    @Override
    public Page<GroupBO> page(GroupQueryParam param) {

        IPage<Group> page =  groupMapper.page(param.getPage(), param);

        return GroupConvert.INSTANCE.to(page);
    }
}
