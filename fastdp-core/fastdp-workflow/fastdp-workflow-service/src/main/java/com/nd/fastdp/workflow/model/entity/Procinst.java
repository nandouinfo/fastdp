package com.nd.fastdp.workflow.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import com.nd.fastdp.framework.pojo.dto.AddParam;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用于保存流程实例，当用户启动一个流程时，就会在这个表存入一个流程实例，
 */
@TableName("fdp_flow_procinst")
@Data
@Accessors(chain = true)
public class Procinst extends FdpEntity {

    private String groupId;// 流程分组ID
    private String procdefId; //对应表procdef的id
    private String procdefName; //对应表procdef的name
    private String procdefConfig;//流程定义

    private String title; //标题，如："张三的请假流程"

    private String nodeId; //当前所处于节点的Id
    private String nodeName;//当前所处节点的名称
    private String taskId; //当前任务id

    private Long startTime;//开始时间
    private Long endTime;//结束时间
    private Long duration;// 耗时

    private String flowVal;//流程变量

    private String nodeList;//节点列表

    private Integer step;//当前执行到第几个节点

    private Integer finished; //是否已结束
    private Integer state;
    private Integer del;

    private Long createTime;
    private String createUser;
    private String createUserName;
    private String createDept;
    private String createDeptName;

    public void init(AddParam param){

        this.createTime = param.getCreateTime();
        this.createUser = param.getCreateUser();
        this.createUserName = param.getCreateUserName();
        this.createDept = param.getCreateDept();
        this.createDeptName = param.getCreateDeptName();
    }

}
