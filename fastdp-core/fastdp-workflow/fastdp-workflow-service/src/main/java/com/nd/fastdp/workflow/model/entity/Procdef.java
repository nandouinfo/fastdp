package com.nd.fastdp.workflow.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 流程定义
 */
@TableName("fdp_flow_procdef")
@Data
@Accessors(chain = true)
public class Procdef extends BaseEntity {

    private String name;
    private String code;
    private String groupId;
    private Integer version;
    private String config;
    private String icon;

    private Long deployTime;

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    private Integer fdpCore;

}
