package com.nd.fastdp.workflow.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.workflow.model.dto.procdef.ProcdefListParam;
import com.nd.fastdp.workflow.model.dto.procdef.ProcdefQueryParam;
import com.nd.fastdp.workflow.model.entity.Procdef;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProcdefMapper extends BaseMapper<Procdef> {

    default Procdef getByCode(String code){

        return selectOne(new QueryWrapper<Procdef>().eq("code", code));
    }

    default List<Procdef> listAllEnable(){

        return selectList(new QueryWrapper<Procdef>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                  .eq("state", StatusEnum.ENABLE.getValue())
                                                  .orderByAsc("sort"));
    }

    default List<Procdef> listAll(){

        return selectList(new QueryWrapper<Procdef>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }


    default List<Procdef> list(ProcdefListParam param){

        return selectList(new QueryWrapper<Procdef>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                       .eq(null != param.getGroupId(), "group_id", param.getGroupId())
                                                       .eq(null != param.getState(), "state", param.getState())
                                                       .orderByAsc("sort"));
    }

    default IPage<Procdef> page(IPage<Procdef> page, ProcdefQueryParam param){

        return selectPage(page, new QueryWrapper<Procdef>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                            .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                            .eq(StringUtils.isNotEmpty(param.getGroupId()), "group_id", param.getGroupId())
                                                            .eq(null != param.getState(), "state", param.getState())
                                                            .orderByAsc("sort"));
    }

    default void del(DelParam param) {

        Procdef procdef = null;
        for (String id : param.getIds()) {

            procdef = new Procdef();

            procdef.setId(id);
            procdef.init(param);
            procdef.setDel(DelStatusEnum.DEL.getValue());

            updateById(procdef);
        }
    }

    default void updateState(StateParam param) {

        Procdef procdef = null;
        for (String id : param.getIds()) {

            procdef = new Procdef();

            procdef.setId(id);
            procdef.init(param);
            procdef.setState(param.getState());

            updateById(procdef);
        }
    }
}
