package com.nd.fastdp.workflow.service;

import com.nd.fastdp.workflow.model.bo.procinst.ProcinstBO;
import com.nd.fastdp.workflow.model.bo.task.TaskBO;
import com.nd.fastdp.workflow.model.bo.undo.UndoBO;

public abstract class WorkFlowListener {

    /**
     * 审批通过
     * @param procinst
     */
    abstract void pass(ProcinstBO procinst);

    /**
     * 审批驳回
     * @param procinst
     */
    abstract void reject(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO);

    /**
     * 同意
     * @param procinst
     */
    abstract void agree(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO);

    /**
     * 已读
     * @param procinst
     */
    abstract void read(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO);

    /**
     * 委托
     * @param procinst
     */
    abstract void assignor(ProcinstBO procinst, TaskBO taskBO, UndoBO oldUndo, UndoBO newUndo);

    /**
     * 撤回
     * @param procinst
     */
    abstract void recall(ProcinstBO procinst);

    /**
     * 等待处理
     * @param procinst
     */
    abstract void waitHandler(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO);

}
