package com.nd.fastdp.workflow.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.db.mybatis.wrapper.QueryWrapperX;
import com.nd.fastdp.workflow.model.entity.Task;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskMapper extends BaseMapper<Task> {

    default List<Task> findByProcinstId(String procinstId){

        return selectList(new QueryWrapperX<Task>().eq("procinst_id", procinstId).orderByAsc("create_time"));
    }
}
