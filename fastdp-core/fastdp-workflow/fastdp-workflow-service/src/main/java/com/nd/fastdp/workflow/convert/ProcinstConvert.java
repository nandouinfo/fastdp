package com.nd.fastdp.workflow.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.workflow.model.bo.procinst.ProcinstBO;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstModifyParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstStartParam;
import com.nd.fastdp.workflow.model.entity.Procinst;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProcinstConvert {

    ProcinstConvert INSTANCE = Mappers.getMapper(ProcinstConvert.class);

    Procinst from(ProcinstStartParam param);

    Procinst from(ProcinstModifyParam param);

    ProcinstBO to(Procinst procinst);

    Page<ProcinstBO> to(IPage<Procinst> page);
}
