package com.nd.fastdp.workflow.convert;

import com.nd.fastdp.workflow.model.bo.undo.UndoBO;
import com.nd.fastdp.workflow.model.dto.undo.*;
import com.nd.fastdp.workflow.model.entity.Undo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UndoConvert {

    UndoConvert INSTANCE = Mappers.getMapper(UndoConvert.class);

    Undo from(UndoAddParam param);

    Undo from(UndoReadParam param);

    Undo from(UndoAgreeParam param);

    Undo from(UndoAssignorParam param);

    Undo from(UndoRefuseParam param);

    UndoBO to(Undo undo);

    List<UndoBO> to(List<Undo> undoList);
}
