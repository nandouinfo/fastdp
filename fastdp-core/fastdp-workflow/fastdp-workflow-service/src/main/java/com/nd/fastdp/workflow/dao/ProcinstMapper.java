package com.nd.fastdp.workflow.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.db.mybatis.wrapper.QueryWrapperX;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstQueryParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstStatisticsParam;
import com.nd.fastdp.workflow.model.entity.Procinst;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcinstMapper extends BaseMapper<Procinst> {

    default IPage<Procinst> mySendPage(IPage<Procinst> page, ProcinstQueryParam param){

        return selectPage(page, new QueryWrapperX<Procinst>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                             .eq("create_user", param.getUserId())
                                                             .eq(StringUtils.isNotEmpty(param.getGroupId()), "group_id", param.getGroupId())
                                                             .eq(StringUtils.isNotEmpty(param.getProcdefId()), "procdef_id", param.getProcdefId())
                                                             .ge(param.getStartTime() != null, "start_time", param.getStartTime())
                                                             .le(param.getEndTime() != null, "start_time", param.getEndTime())
                                                             .ge(param.getCompleteStartTime() != null, "end_time", param.getCompleteStartTime())
                                                             .le(param.getCompleteEndTime() != null, "end_time", param.getCompleteEndTime())
                                                             .eq(param.getState() != null , "state", param.getState())
                                                             .eq(param.getFinished() != null , "finished", param.getFinished()).orderByDesc("create_time"));
    }

    default IPage<Procinst> page(IPage<Procinst> page, ProcinstQueryParam param){

        return selectPage(page, new QueryWrapperX<Procinst>().eq("del", DelStatusEnum.NORMAL.getValue())
                .eq(StringUtils.isNotEmpty(param.getGroupId()), "group_id", param.getGroupId())
                .eq(StringUtils.isNotEmpty(param.getProcdefId()), "procdef_id", param.getProcdefId())
                .ge(param.getStartTime() != null, "start_time", param.getStartTime())
                .le(param.getEndTime() != null, "start_time", param.getEndTime())
                .ge(param.getCompleteStartTime() != null, "end_time", param.getCompleteStartTime())
                .le(param.getCompleteEndTime() != null, "end_time", param.getCompleteEndTime())
                .eq(param.getState() != null , "state", param.getState())
                .eq(param.getFinished() != null , "finished", param.getFinished()).orderByDesc("create_time"));
    }

    IPage<Procinst> waitApprovePage(IPage<Procinst> page, @Param("param") ProcinstQueryParam param);


    IPage<Procinst> alreadyApprovePage(IPage<Procinst> page, @Param("param") ProcinstQueryParam param);

    default void del(DelParam param) {

        Procinst procinst = null;
        for (String id : param.getIds()) {

            procinst = new Procinst();

            procinst.setId(id);
            procinst.setDel(DelStatusEnum.DEL.getValue());

            updateById(procinst);
        }
    }

    Integer statistics(@Param("param") ProcinstStatisticsParam param);

}
