package com.nd.fastdp.workflow.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.db.mybatis.wrapper.QueryWrapperX;
import com.nd.fastdp.workflow.model.entity.Undo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UndoMapper extends BaseMapper<Undo> {

    default List<Undo> findByProcinst(String procinstId){

        return selectList(new QueryWrapperX<Undo>().eq("procinst_id", procinstId).orderByAsc("create_time"));
    }

    default List<Undo> findByTaskId(String taskId){

        return selectList(new QueryWrapperX<Undo>().eq("task_id", taskId).orderByAsc("create_time"));
    }

    default List<Undo> findByUserId(String userId){

        return selectList(new QueryWrapperX<Undo>().eq("user_id", userId).orderByDesc("create_time"));
    }
}
