package com.nd.fastdp.workflow.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.workflow.model.bo.group.GroupBO;
import com.nd.fastdp.workflow.model.dto.group.GroupAddParam;
import com.nd.fastdp.workflow.model.dto.group.GroupModifyParam;
import com.nd.fastdp.workflow.model.entity.Group;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface GroupConvert {

    GroupConvert INSTANCE = Mappers.getMapper(GroupConvert.class);

    Group from(GroupAddParam param);

    Group from(GroupModifyParam param);

    GroupBO to(Group group);

    List<GroupBO> to(List<Group> groups);

    Page<GroupBO> to(IPage<Group> groups);
}
