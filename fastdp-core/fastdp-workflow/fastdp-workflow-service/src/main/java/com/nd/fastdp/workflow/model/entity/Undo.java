package com.nd.fastdp.workflow.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用于保存任务待办
 *
 */
@TableName("fdp_flow_undo")
@Data
@Accessors(chain = true)
public class Undo extends FdpEntity {

    private String userId; //表示当前审批的用户
    private String userName; //当前审批的用户名称
    private String deptId;//部门
    private String deptName;//部门名称

    private String procinstId;//流程实例ID
    private String taskId;//任务表TaskId

    private Integer result;//处理结果

    private String assignorUndoId;//委托待办ID

    private Integer finished;

    private Long createTime;//创建时间
    private Long assertionTime;//断言时间

    private String suggest;//意见

}
