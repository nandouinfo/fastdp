package com.nd.fastdp.workflow.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用于保存任务
 */
@TableName("fdp_flow_task")
@Data
@Accessors(chain = true)
public class Task extends FdpEntity {

    private String nodeName;//节点名称
    private String nodeId;//表示节点，如："主管审批"结点
    private String nodeCode;//节点编码
    private String nodeType;//节点类型
    private String procinstId; //流程实例ID

    private Long createTime;
    private Long claimTime;//断言时间、通过审批或者拒绝审批

    private Integer memberCount;//表示当前任务需要多少人审批之后才能结束，默认是 1
    private Integer unCompleteNum;//表示还有多少人没有审批，默认是1
    private Integer agreeNum;//表示通过的人数,抄送时数量为已阅知用户数量

    private Boolean counterSign;//是否会签

    private Integer state;//同意拒绝

    private Integer finished;

}
