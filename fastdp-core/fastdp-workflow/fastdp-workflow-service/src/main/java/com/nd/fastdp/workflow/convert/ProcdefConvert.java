package com.nd.fastdp.workflow.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.workflow.model.bo.procdef.ProcdefBO;
import com.nd.fastdp.workflow.model.dto.procdef.ProcdefAddParam;
import com.nd.fastdp.workflow.model.dto.procdef.ProcdefModifyParam;
import com.nd.fastdp.workflow.model.entity.Procdef;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProcdefConvert {

    ProcdefConvert INSTANCE = Mappers.getMapper(ProcdefConvert.class);

    Procdef from(ProcdefAddParam param);

    Procdef from(ProcdefModifyParam param);

    ProcdefBO to(Procdef procdef);

    List<ProcdefBO> to(List<Procdef> procdefs);

    Page<ProcdefBO> to(IPage<Procdef> procdefs);
}
