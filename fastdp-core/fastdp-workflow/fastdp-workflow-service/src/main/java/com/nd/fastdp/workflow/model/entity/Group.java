package com.nd.fastdp.workflow.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_flow_group")
@Data
@Accessors(chain = true)
public class Group extends BaseEntity {

    private String name;

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    private Integer fdpCore;

}
