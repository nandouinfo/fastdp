package com.nd.fastdp.workflow.service;

import com.nd.fastdp.sys.constant.MsgTypeEnum;
import com.nd.fastdp.sys.model.dto.msg.MsgAddParam;
import com.nd.fastdp.sys.service.MsgService;
import com.nd.fastdp.workflow.constant.NodeTypes;
import com.nd.fastdp.workflow.model.bo.procinst.ProcinstBO;
import com.nd.fastdp.workflow.model.bo.task.TaskBO;
import com.nd.fastdp.workflow.model.bo.undo.UndoBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("defaultWorkFlowListener")
public class DefaultWorkFlowListener extends WorkFlowListener{

    protected static final  String MSG_LINK = "/static/workflow/procdesign/index.html?id={ID}#/view";
    protected static final  String UNDO_MSG_LINK = "/static/workflow/procdesign/index.html?undoId={ID}#/view";

    @Autowired
    protected MsgService msgService;

    /**
     * 审批通过
     * @param procinst
     */
    public void pass(ProcinstBO procinst){

        MsgAddParam param = new MsgAddParam();

        param.setBusId(procinst.getId());
        param.setType(MsgTypeEnum.OA_APPROVE.getValue());
        param.setReceiveUser(procinst.getCreateUser());
        param.setReceiveUserName(procinst.getCreateUserName());
        param.setReceiveTime(System.currentTimeMillis());
        param.setReceiveDept(procinst.getCreateDept());
        param.setReceiveDeptName(procinst.getCreateDeptName());
        param.setContent(procinst.getProcdefName() + ",审批通过");
        param.setTitle(procinst.getProcdefName() + ",审批通过");

        msgService.add(param);
    }

    /**
     * 审批驳回
     * @param procinst
     */
    public void reject(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO){

        MsgAddParam param = new MsgAddParam();

        param.setBusId(procinst.getId());
        param.setType(MsgTypeEnum.OA_APPROVE.getValue());
        param.setReceiveUser(procinst.getCreateUser());
        param.setReceiveUserName(procinst.getCreateUserName());
        param.setReceiveTime(System.currentTimeMillis());
        param.setReceiveDept(procinst.getCreateDept());
        param.setReceiveDeptName(procinst.getCreateDeptName());
        param.setContent(procinst.getProcdefName() + ",被驳回; 原因:" + undoBO.getSuggest());
        param.setTitle(procinst.getProcdefName() + ",被驳回");

        msgService.add(param);

    }

    /**
     * 同意
     * @param procinst
     */
    public void agree(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO){

    }

    /**
     * 已读
     * @param procinst
     */
    public void read(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO){

    }

    /**
     * 委托
     * @param procinst
     */
    public void assignor(ProcinstBO procinst, TaskBO taskBO, UndoBO oldUndo, UndoBO newUndo){

        MsgAddParam param = new MsgAddParam();

        param.setBusId(newUndo.getId());
        param.setType(MsgTypeEnum.OA_APPROVE.getValue());
        param.setReceiveUser(newUndo.getUserId());
        param.setReceiveUserName(newUndo.getUserName());
        param.setReceiveTime(System.currentTimeMillis());
        param.setReceiveDept(newUndo.getDeptId());
        param.setReceiveDeptName(newUndo.getDeptName());
        param.setContent(oldUndo.getUserName() + "委托你代理审批," + procinst.getTitle());
        param.setTitle(oldUndo.getUserName() + "委托你代理审批," + procinst.getTitle());

        msgService.add(param);

    }

    /**
     * 撤回
     * @param procinst
     */
    public void recall(ProcinstBO procinst){

    }

    /**
     * 等待处理
     * @param procinst
     */
    public void waitHandler(ProcinstBO procinst, TaskBO taskBO, UndoBO undoBO){

        MsgAddParam param = new MsgAddParam();

        param.setBusId(undoBO.getId());
        param.setType(MsgTypeEnum.OA_APPROVE.getValue());
        param.setReceiveUser(undoBO.getUserId());
        param.setReceiveUserName(undoBO.getUserName());
        param.setReceiveTime(System.currentTimeMillis());
        param.setReceiveDept(undoBO.getDeptId());
        param.setReceiveDeptName(undoBO.getDeptName());

        param.setContent(procinst.getTitle() + ",待处理");
        param.setTitle(procinst.getTitle() + ",待处理");

        if(NodeTypes.NOTIFIER.getValue().equals(taskBO.getNodeType())){

            param.setContent(procinst.getTitle() + ",待查阅");
            param.setTitle(procinst.getTitle() + ",待查阅");

            msgService.add(param);

        }/*else if(NodeTypes.APPROVER.getValue().equals(taskBO.getNodeType())){

            param.setContent(procinst.getTitle() + ",待审批");
            param.setTitle(procinst.getTitle() + ",待审批");
        }
*/

    }

}
