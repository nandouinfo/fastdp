package com.nd.fastdp.workflow.convert;

import com.nd.fastdp.workflow.model.bo.task.TaskBO;
import com.nd.fastdp.workflow.model.entity.Task;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TaskConvert {

    TaskConvert INSTANCE = Mappers.getMapper(TaskConvert.class);

    TaskBO to(Task task);

    List<TaskBO> to(List<Task> task);
}
