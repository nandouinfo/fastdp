package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.model.dto.dict.DictListParam;
import com.nd.fastdp.sys.model.dto.dict.DictQueryParam;
import com.nd.fastdp.sys.model.entity.Dict;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface DictMapper extends BaseMapper<Dict> {

    default Dict getByCode(String code){

        return selectOne(new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                  .eq("code", code));
    }

    default boolean isExist(String code){

        Dict dict = selectOne(new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                        .eq("code", code));

        return dict != null;
    }

    default boolean isExist(String id, String code){

        Dict dict = selectOne(new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .ne("id", id)
                                                      .eq("code", code));

        return dict != null;
    }

    default List<Dict> listAllEnable(){

        return selectList(new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                  .eq("state", StatusEnum.ENABLE.getValue())
                                                  .orderByAsc("sort"));
    }

    default List<Dict> listAll(){

        return selectList(new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }

    default List<Dict> list(DictListParam param){

        return selectList(new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                       .like(StringUtils.isNotEmpty(param.getCode()), "code", param.getCode())
                                                       .eq(null != param.getType(), "type", param.getType())
                                                       .eq(StringUtils.isNotEmpty(param.getParentCode()), "parent_code", param.getParentCode())
                                                       .eq(null != param.getState(), "state", param.getState())
                                                       .orderByAsc("sort"));
    }

    default IPage<Dict> page(IPage<Dict> page, DictQueryParam param){

        return selectPage(page, new QueryWrapper<Dict>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                            .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                            .like(StringUtils.isNotEmpty(param.getCode()), "code", param.getCode())
                                                            .eq(null != param.getType(), "type", param.getType())
                                                            .eq(StringUtils.isNotEmpty(param.getParentCode()), "parent_code", param.getParentCode())
                                                            .eq(null != param.getState(), "state", param.getState())
                                                            .orderByAsc("sort"));
    }

    default void del(DelParam param) {

        Dict dict = null;
        for (String id : param.getIds()) {

            dict = new Dict();

            dict.setId(id);
            dict.init(param);
            dict.setDel(DelStatusEnum.DEL.getValue());

            updateById(dict);
        }
    }

    default void updateState(StateParam param) {

        Dict dict = null;
        for (String id : param.getIds()) {

            dict = new Dict();

            dict.setId(id);
            dict.init(param);
            dict.setState(param.getState());

            updateById(dict);
        }
    }
}
