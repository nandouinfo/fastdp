package com.nd.fastdp.sys.convert;

import com.nd.fastdp.sys.model.bo.resource.ResourceDataScopeBO;
import com.nd.fastdp.sys.model.dto.resource.DataScopeParam;
import com.nd.fastdp.sys.model.entity.ResourceDataScope;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ResourceDataScopeConvert {

    ResourceDataScopeConvert INSTANCE = Mappers.getMapper(ResourceDataScopeConvert.class);

    ResourceDataScope from(DataScopeParam param);

    ResourceDataScopeBO to(ResourceDataScope dataScope);

    List<ResourceDataScopeBO> to(List<ResourceDataScope> dataScopes);

}
