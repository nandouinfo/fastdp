package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunListParam;
import com.nd.fastdp.sys.model.entity.CommonFun;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommonFunMapper extends BaseMapper<CommonFun> {

    default List<CommonFun> list(CommonFunListParam param){

        return selectList(new QueryWrapper<CommonFun>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .eq(StringUtils.isNotEmpty(param.getType()), "type", param.getType())
                                                       .eq(StringUtils.isNotEmpty(param.getLoginUser().getUserId()), "user_id", param.getLoginUser().getUserId())
                                                       .orderByDesc("times"));
    }

    default CommonFun getByUserIdAndBusId(String userId, String busId){

        return selectOne(new QueryWrapper<CommonFun>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .eq("bus_id", busId)
                                                      .eq("user_id", userId));
    }

    default void del(DelParam param) {

        CommonFun commonFun = null;
        for (String id : param.getIds()) {

            commonFun = new CommonFun();

            commonFun.setId(id);
            commonFun.setDel(DelStatusEnum.DEL.getValue());

            updateById(commonFun);
        }
    }
}
