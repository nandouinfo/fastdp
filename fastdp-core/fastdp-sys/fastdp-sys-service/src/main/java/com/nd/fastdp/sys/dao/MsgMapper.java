package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.constant.MsgStateEnum;
import com.nd.fastdp.sys.model.dto.msg.MsgListParam;
import com.nd.fastdp.sys.model.dto.msg.MsgQueryParam;
import com.nd.fastdp.sys.model.entity.Msg;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MsgMapper extends BaseMapper<Msg> {

    default List<Msg> list(MsgListParam param){

        return selectList(new QueryWrapper<Msg>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .lt(param.getStartTime() != null, "receive_time", param.getStartTime())
                                                       .gt(param.getEndTime() != null, "receive_time", param.getEndTime())
                                                       .eq(StringUtils.isNotEmpty(param.getType()), "type", param.getType())
                                                       .eq("receive_user", param.getLoginUser().getUserId())
                                                       .eq(null != param.getState(), "state", param.getState())
                                                       .orderByDesc("receive_time"));
    }

    default Integer unReadCount(String receiveUser){
        return selectCount(new QueryWrapper<Msg>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                    .eq("receive_user", receiveUser)
                                                    .eq("state", MsgStateEnum.UN_READ.getValue()));
    }

    default IPage<Msg> page(IPage<Msg> page, MsgQueryParam param){

        return selectPage(page, new QueryWrapper<Msg>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                        .lt(param.getStartTime() != null, "receive_time", param.getStartTime())
                                                        .gt(param.getEndTime() != null, "receive_time", param.getEndTime())
                                                        .eq(StringUtils.isNotEmpty(param.getType()), "type", param.getType())
                                                        .eq("receive_user", param.getLoginUser().getUserId())
                                                        .eq(null != param.getState(), "state", param.getState())
                                                        .orderByDesc("receive_time"));
    }

    default void del(DelParam param) {

        Msg msg = null;
        for (String id : param.getIds()) {

            msg = new Msg();

            msg.setId(id);
            msg.setDel(DelStatusEnum.DEL.getValue());

            updateById(msg);
        }
    }

    void readByReceiveUser(@Param("receiveUser") String receiveUser);

    default void updateState(StateParam param) {

        Msg msg = null;
        for (String id : param.getIds()) {

            msg = new Msg();

            msg.setId(id);
            msg.setState(param.getState());

            updateById(msg);
        }
    }
}
