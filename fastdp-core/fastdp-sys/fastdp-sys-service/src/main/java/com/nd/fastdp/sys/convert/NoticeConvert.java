package com.nd.fastdp.sys.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.sys.model.bo.notice.NoticeBO;
import com.nd.fastdp.sys.model.bo.notice.NoticeReceiverBO;
import com.nd.fastdp.sys.model.bo.notice.NoticeReceiverVBO;
import com.nd.fastdp.sys.model.dto.notice.NoticeAddParam;
import com.nd.fastdp.sys.model.dto.notice.NoticeModifyParam;
import com.nd.fastdp.sys.model.entity.Notice;
import com.nd.fastdp.sys.model.entity.NoticeReceiver;
import com.nd.fastdp.sys.model.view.NoticeReceiverV;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface NoticeConvert {

    NoticeConvert INSTANCE = Mappers.getMapper(NoticeConvert.class);

    Notice from(NoticeAddParam param);

    Notice from(NoticeModifyParam param);

    NoticeBO to(Notice notice);

    List<NoticeBO> to(List<Notice> notices);

    List<NoticeReceiverBO> toReceiver(List<NoticeReceiver> receivers);

    Page<NoticeReceiverVBO> toReceiverV(IPage<NoticeReceiverV> receivers);

    NoticeReceiverVBO to(NoticeReceiverV receiver);

    Page<NoticeBO> to(IPage<Notice> notices);
}
