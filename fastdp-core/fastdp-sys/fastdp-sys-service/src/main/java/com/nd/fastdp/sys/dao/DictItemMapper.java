package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemListParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemQueryParam;
import com.nd.fastdp.sys.model.entity.DictItem;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictItemMapper extends BaseMapper<DictItem> {

    default DictItem getByCode(String dictCode, String value){

        return selectOne(new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .eq("dict_code", dictCode)
                                                      .eq("value", value));
    }

    default boolean isExist(String dictCode, String value){

        DictItem dictItem = selectOne(new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                                  .eq("dict_code", dictCode)
                                                                  .eq("value", value));

        return dictItem != null;
    }

    default boolean isExist(String id, String dictCode, String value){

        DictItem dictItem = selectOne(new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                                  .ne("id", id)
                                                                  .eq("dict_code", dictCode)
                                                                  .eq("value", value));

        return dictItem != null;
    }

    default List<DictItem> listAllEnable(String dictCode){

        return selectList(new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .eq("dict_code", dictCode)
                                                      .eq("state", StatusEnum.ENABLE.getValue())
                                                      .orderByAsc("sort"));
    }

    default List<DictItem> listAll(String dictCode){

        return selectList(new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .eq("dict_code", dictCode)
                                                      .orderByAsc("sort"));
    }

    default List<DictItem> list(DictItemListParam param){

        return selectList(new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .eq("dict_code", param.getDictCode())
                                                       .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                       .like(StringUtils.isNotEmpty(param.getValue()), "value", param.getValue())
                                                       .eq(null != param.getState(), "state", param.getState())
                                                       .orderByAsc("sort"));
    }

    default IPage<DictItem> page(IPage<DictItem> page, DictItemQueryParam param){

        return selectPage(page, new QueryWrapper<DictItem>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                            .eq("dict_code", param.getDictCode())
                                                            .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                            .like(StringUtils.isNotEmpty(param.getValue()), "value", param.getValue())
                                                            .eq(null != param.getState(), "state", param.getState())
                                                            .orderByAsc("sort"));
    }

    default void del(DelParam param) {

        DictItem dictItem = null;
        for (String id : param.getIds()) {

            dictItem = new DictItem();

            dictItem.setId(id);
            dictItem.init(param);
            dictItem.setDel(DelStatusEnum.DEL.getValue());

            updateById(dictItem);
        }
    }

    default void updateState(StateParam param) {

        DictItem dictItem = null;
        for (String id : param.getIds()) {

            dictItem = new DictItem();

            dictItem.setId(id);
            dictItem.init(param);
            dictItem.setState(param.getState());

            updateById(dictItem);
        }
    }
}
