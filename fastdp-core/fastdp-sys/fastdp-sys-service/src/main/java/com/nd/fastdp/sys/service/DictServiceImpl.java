package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.constant.VerifyRespCodeEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.convert.DictConvert;
import com.nd.fastdp.sys.dao.DictMapper;
import com.nd.fastdp.sys.model.bo.dict.DictBO;
import com.nd.fastdp.sys.model.dto.dict.DictAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictListParam;
import com.nd.fastdp.sys.model.dto.dict.DictModifyParam;
import com.nd.fastdp.sys.model.dto.dict.DictQueryParam;
import com.nd.fastdp.sys.model.entity.Dict;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictServiceImpl implements DictService {

    @Autowired
    private DictMapper dictMapper;

    @Override
    public DictBO add(DictAddParam param) {

        Dict dict = DictConvert.INSTANCE.from(param);

        if(dictMapper.isExist(dict.getCode())){
            throw new BusinessException(VerifyRespCodeEnum.CODE_EXIST);
        }

        if(StringUtils.isEmpty(dict.getParentCode()))
            dict.setParentCode("");

        dictMapper.insert(dict);

        return DictConvert.INSTANCE.to(dict);
    }

    @Override
    public void del(DelParam param) {
        dictMapper.del(param);
    }

    @Override
    public DictBO modify(DictModifyParam param) {

        Dict dict = DictConvert.INSTANCE.from(param);

        if(dictMapper.isExist(dict.getId(), dict.getCode())){
            throw new BusinessException(VerifyRespCodeEnum.CODE_EXIST);
        }

        dictMapper.updateById(dict);

        return get(dict.getId());
    }

    @Override
    public void modifyState(StateParam param) {
        dictMapper.updateState(param);
    }

    @Override
    public DictBO get(String id) {

        Dict dict = dictMapper.selectById(id);

        return DictConvert.INSTANCE.to(dict);
    }

    @Override
    public DictBO getByCode(String code) {

        Dict dict = dictMapper.getByCode(code);

        return DictConvert.INSTANCE.to(dict);
    }


    @Override
    public List<DictBO> listAllEnable() {

        List<Dict> dicts =  dictMapper.listAllEnable();

        return DictConvert.INSTANCE.to(dicts);
    }

    @Override
    public List<DictBO> listAll() {

        List<Dict> dicts =  dictMapper.listAll();

        return DictConvert.INSTANCE.to(dicts);
    }

    @Override
    public List<DictBO> list(DictListParam param) {

        List<Dict> dicts =  dictMapper.list(param);

        return DictConvert.INSTANCE.to(dicts);
    }

    @Override
    public Page<DictBO> page(DictQueryParam param) {

        IPage<Dict> page =  dictMapper.page(param.getPage(), param);

        return DictConvert.INSTANCE.to(page);
    }
}
