package com.nd.fastdp.sys.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.sys.model.bo.msg.MsgBO;
import com.nd.fastdp.sys.model.dto.msg.MsgAddParam;
import com.nd.fastdp.sys.model.entity.Msg;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MsgConvert {

    MsgConvert INSTANCE = Mappers.getMapper(MsgConvert.class);

    Msg from(MsgAddParam param);

    MsgBO to(Msg Msg);

    List<MsgBO> to(List<Msg> Msgs);

    Page<MsgBO> to(IPage<Msg> Msgs);
}
