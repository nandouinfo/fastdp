package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_resource_data_scope")
@Data
@Accessors(chain = true)
public class ResourceDataScope extends BaseEntity {

    private String name;
    private String code;
    private Integer type;
    private String resourceCode;

    private String authSql;

    private Integer fdpCore;
}
