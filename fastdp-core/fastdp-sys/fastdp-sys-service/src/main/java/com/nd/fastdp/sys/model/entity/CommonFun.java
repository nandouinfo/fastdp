package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_common_fun")
@Data
@Accessors(chain = true)
public class CommonFun extends FdpEntity {

    private String busId;
    private String userId;
    private Integer type;

    private Integer times;

    private Integer sort;
    private Integer state;
    private Integer del;

    private Integer fdpCore;
}
