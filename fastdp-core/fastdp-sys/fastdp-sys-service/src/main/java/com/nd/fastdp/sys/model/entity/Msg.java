package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_msg")
@Data
@Accessors(chain = true)
public class Msg extends FdpEntity {

    private String title;
    private String content;
    private String busId;
    private Integer type;

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    private Long receiveTime;
    private String receiveUser;
    private String receiveUserName;
    private String receiveDept;
    private String receiveDeptName;

    private Long readTime;

    private Integer fdpCore;

}
