package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceListParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceQueryParam;
import com.nd.fastdp.sys.model.entity.Resource;
import com.nd.fastdp.sys.model.entity.RoleDataResource;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourceMapper extends BaseMapper<Resource> {

    default void del(DelParam param) {

        Resource resource = null;
        for (String id : param.getIds()) {

            resource = new Resource();

            resource.setId(id);
            resource.init(param);
            resource.setDel(DelStatusEnum.DEL.getValue());

            updateById(resource);
        }
    }

    List<Resource> findByUserId(@Param("userId") String userId);

    default Resource getByCode(String code){

        return selectOne(new QueryWrapper<Resource>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                     .eq("code", code));
    }

    default boolean isExist(String code){

        return getByCode(code) != null;
    }

    default boolean isExist(String id, String code){

        Resource resource = selectOne(new QueryWrapper<Resource>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                                   .ne("id", id)
                                                                   .eq("code", code));

        return resource != null;
    }

    default List<Resource> listAllEnable(){

        return selectList(new QueryWrapper<Resource>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                      .eq("state", StatusEnum.ENABLE.getValue())
                                                      .orderByAsc("sort"));
    }

    default List<Resource> listAll(){

        return selectList(new QueryWrapper<Resource>().eq("del", DelStatusEnum.NORMAL.getValue()).orderByAsc("sort"));
    }

    default List<Resource> list(ResourceListParam param){

        return selectList(new QueryWrapper<Resource>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                       .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                       .like(StringUtils.isNotEmpty(param.getCode()), "code", param.getCode())
                                                       .eq(null != param.getType(), "type", param.getType())
                                                       .eq(StringUtils.isNotEmpty(param.getParentCode()), "parent_code", param.getParentCode())
                                                       .eq(null != param.getState(), "state", param.getState())
                                                       .eq(null != param.getOnlineDesignFlag(), "online_design_flag", param.getOnlineDesignFlag())
                                                       .orderByAsc("sort"));
    }

    default List<Resource> findByParentCode(String parentCode){

        ResourceListParam param = new ResourceListParam();
        param.setParentCode(parentCode);

        return list(param);
    }

    default IPage<Resource> page(IPage<Resource> page, ResourceQueryParam param){

        return selectPage(page, new QueryWrapper<Resource>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                            .like(StringUtils.isNotEmpty(param.getName()), "name", param.getName())
                                                            .like(StringUtils.isNotEmpty(param.getCode()), "code", param.getCode())
                                                            .eq(null != param.getType(), "type", param.getType())
                                                            .eq(StringUtils.isNotEmpty(param.getParentCode()), "parent_code", param.getParentCode())
                                                            .eq(null != param.getState(), "state", param.getState())
                                                            .eq(null != param.getOnlineDesignFlag(), "online_design_flag", param.getOnlineDesignFlag())
                                                            .orderByAsc("sort"));
    }
}
