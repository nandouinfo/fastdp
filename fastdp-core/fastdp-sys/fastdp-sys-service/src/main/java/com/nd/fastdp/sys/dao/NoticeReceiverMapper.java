package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nd.fastdp.db.mybatis.mapper.FdpBaseMapper;
import com.nd.fastdp.sys.model.entity.NoticeReceiver;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeReceiverMapper extends FdpBaseMapper<NoticeReceiver> {

    void delByNoticeId(@Param("noticeId") String noticeId);

    default List<NoticeReceiver> listByNoticeId(String noticeId){
        return selectList(new QueryWrapper<NoticeReceiver>().eq("notice_id", noticeId).orderByAsc("read_time"));
    }
}
