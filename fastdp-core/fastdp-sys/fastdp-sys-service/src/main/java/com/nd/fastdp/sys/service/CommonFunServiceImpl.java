package com.nd.fastdp.sys.service;

import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.convert.CommonFunConvert;
import com.nd.fastdp.sys.dao.CommonFunMapper;
import com.nd.fastdp.sys.model.bo.commonFun.CommonFunBO;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunAddParam;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunListParam;
import com.nd.fastdp.sys.model.entity.CommonFun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonFunServiceImpl implements CommonFunService {

    @Autowired
    private CommonFunMapper commonFunMapper;

    @Override
    public void add(CommonFunAddParam param) {

        CommonFun target = commonFunMapper.getByUserIdAndBusId(param.getLoginUser().getUserId(), param.getBusId());

        if(target != null){

            CommonFun commonFun = new CommonFun();
            commonFun.setId(target.getId());
            commonFun.setTimes(target.getTimes() + 1);

            commonFunMapper.updateById(commonFun);

        }else{
            CommonFun commonFun = CommonFunConvert.INSTANCE.from(param);
            commonFun.setState(StatusEnum.ENABLE.getValue());
            commonFun.setDel(DelStatusEnum.NORMAL_VAL);
            commonFun.setUserId(param.getLoginUser().getUserId());

            commonFunMapper.insert(commonFun);
        }
    }

    @Override
    public void del(DelParam param) {
        commonFunMapper.del(param);
    }

    @Override
    public List<CommonFunBO> list(CommonFunListParam param) {

        List<CommonFun> commonFuns =  commonFunMapper.list(param);

        return CommonFunConvert.INSTANCE.to(commonFuns);
    }
}
