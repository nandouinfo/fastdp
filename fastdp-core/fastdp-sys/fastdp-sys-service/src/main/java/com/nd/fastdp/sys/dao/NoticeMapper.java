package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.db.mybatis.mapper.FdpBaseMapper;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.model.dto.notice.NoticeListParam;
import com.nd.fastdp.sys.model.dto.notice.NoticeQueryParam;
import com.nd.fastdp.sys.model.entity.Notice;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeMapper extends FdpBaseMapper<Notice> {

    String MODULE_CODE = "NOTICE_MANG";

    default List<Notice> list(NoticeListParam param){

        return selectList(queryWrapper(param));
    }

    default IPage<Notice> page(IPage<Notice> page, NoticeQueryParam param){

        return selectPage(page, queryWrapper(param));
    }

    default QueryWrapper queryWrapper(NoticeListParam param){

        QueryWrapper queryWrapper = new QueryWrapper<Notice>().eq("del", DelStatusEnum.NORMAL.getValue())
                                                                .like(StringUtils.isNotEmpty(param.getTitle()), "title", param.getTitle())
                                                                .eq(StringUtils.isNotEmpty(param.getType()), "type", param.getType())
                                                                .eq(null != param.getState(), "state", param.getState())
                                                                .orderByDesc("create_time");

        filterDataAuthority(queryWrapper, MODULE_CODE, param.getLoginUser());

        return queryWrapper;
    }

    default void del(DelParam param) {

        Notice msg = null;
        for (String id : param.getIds()) {

            msg = new Notice();

            msg.setId(id);
            msg.setDel(DelStatusEnum.DEL.getValue());

            updateById(msg);
        }
    }

    default void updateState(StateParam param) {

        Notice msg = null;
        for (String id : param.getIds()) {

            msg = new Notice();

            msg.setId(id);
            msg.setState(param.getState());

            updateById(msg);
        }
    }

    void unRelease(@Param("id") String id);

}
