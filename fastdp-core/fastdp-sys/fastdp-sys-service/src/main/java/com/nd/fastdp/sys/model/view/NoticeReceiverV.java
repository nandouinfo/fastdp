package com.nd.fastdp.sys.model.view;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_notice_receiver_v")
@Data
@Accessors(chain = true)
public class NoticeReceiverV extends FdpEntity {

    private String noticeId;

    private Long readTime;
    private Long receiverTime;
    private String receiverUser;
    private String receiverUserName;
    private Integer receiverState;

    private String title;
    private String content;
    private Long releaseTime;//发布时间

    private String createDeptName;

}
