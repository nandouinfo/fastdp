package com.nd.fastdp.sys.convert;

import com.nd.fastdp.sys.model.bo.commonFun.CommonFunBO;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunAddParam;
import com.nd.fastdp.sys.model.entity.CommonFun;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CommonFunConvert {

    CommonFunConvert INSTANCE = Mappers.getMapper(CommonFunConvert.class);

    CommonFun from(CommonFunAddParam param);

    List<CommonFunBO> to(List<CommonFun> commonFuns);
}
