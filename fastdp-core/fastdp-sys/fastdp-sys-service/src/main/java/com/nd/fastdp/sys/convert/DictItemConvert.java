package com.nd.fastdp.sys.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.sys.model.bo.dict.DictItemBO;
import com.nd.fastdp.sys.model.dto.dict.DictItemAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemModifyParam;
import com.nd.fastdp.sys.model.entity.DictItem;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DictItemConvert {

    DictItemConvert INSTANCE = Mappers.getMapper(DictItemConvert.class);

    DictItem from(DictItemAddParam param);

    DictItem from(DictItemModifyParam param);

    DictItemBO to(DictItem dictItem);

    List<DictItemBO> to(List<DictItem> dictItems);

    Page<DictItemBO> to(IPage<DictItem> dictItems);
}
