package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.constant.DelStatusEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.constant.MsgStateEnum;
import com.nd.fastdp.sys.convert.MsgConvert;
import com.nd.fastdp.sys.dao.MsgMapper;
import com.nd.fastdp.sys.model.bo.msg.MsgBO;
import com.nd.fastdp.sys.model.dto.msg.MsgAddParam;
import com.nd.fastdp.sys.model.dto.msg.MsgListParam;
import com.nd.fastdp.sys.model.dto.msg.MsgQueryParam;
import com.nd.fastdp.sys.model.entity.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsgServiceImpl implements MsgService {

    @Autowired
    private MsgMapper msgMapper;

    @Override
    public void add(MsgAddParam param) {

        Msg msg = MsgConvert.INSTANCE.from(param);
        msg.setState(MsgStateEnum.UN_READ.getValue());
        msg.setDel(DelStatusEnum.NORMAL_VAL);

        msgMapper.insert(msg);
    }

    @Override
    public void del(DelParam param) {
        msgMapper.del(param);
    }

    @Override
    public void read(String id) {

        Msg msg = new Msg();

        msg.setId(id);
        msg.setState(MsgStateEnum.READ.getValue());

        msgMapper.updateById(msg);
    }

    @Override
    public Integer unReadCount(String receiveUser) {
        return msgMapper.unReadCount(receiveUser);
    }

    @Override
    public void readByUser(String receiveUser) {
        msgMapper.readByReceiveUser(receiveUser);
    }

    @Override
    public MsgBO get(String id) {

        Msg msg = msgMapper.selectById(id);

        return MsgConvert.INSTANCE.to(msg);
    }

    @Override
    public List<MsgBO> list(MsgListParam param) {

        List<Msg> msgs =  msgMapper.list(param);

        return MsgConvert.INSTANCE.to(msgs);
    }

    @Override
    public Page<MsgBO> page(MsgQueryParam param) {

        IPage<Msg> page =  msgMapper.page(param.getPage(), param);

        return MsgConvert.INSTANCE.to(page);
    }
}
