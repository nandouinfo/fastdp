package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_notice")
@Data
@Accessors(chain = true)
public class Notice extends BaseEntity {

    private String title;

    private String recivers;

    private String content;

    private Integer type;

    private Integer sort;

    private Integer state;
    private Integer del;

    private Integer topState;//置顶状态

    private Long releaseTime;//发布时间
    private String releaseUser;//发布人编码
    private String releaseUserName;//发布人名称
    private String releaseDept;//发布部门编码
    private String releaseDeptName;//发布部门名称

    private Integer fdpCore;
}
