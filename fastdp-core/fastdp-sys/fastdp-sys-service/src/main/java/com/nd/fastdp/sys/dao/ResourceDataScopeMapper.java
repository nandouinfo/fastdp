package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nd.fastdp.sys.model.entity.ResourceDataScope;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourceDataScopeMapper extends BaseMapper<ResourceDataScope> {

    default void delByResourceCode(String resourceCode) {

        delete(new QueryWrapper<ResourceDataScope>().eq("resource_code", resourceCode));
    }

    default List<ResourceDataScope> listByResourceCode(String resourceCode){

        return selectList(new QueryWrapper<ResourceDataScope>().eq("resource_code", resourceCode));
    }

    List<ResourceDataScope> listAll();

    List<ResourceDataScope> findByUserId(@Param("userId") String userId);
}
