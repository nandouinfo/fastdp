package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.FdpEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_notice_receiver")
@Data
@Accessors(chain = true)
public class NoticeReceiver extends FdpEntity {

    private String noticeId;

    private Integer state;

    private Long receiverTime;
    private String receiverUser;
    private String receiverUserName;
    private String receiverDept;
    private String receiverDeptName;

    private Long readTime;

    private Integer fdpCore;
}
