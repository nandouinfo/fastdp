package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_resource")
@Data
@Accessors(chain = true)
public class RoleDataResource extends BaseEntity {

    private String roleId;
    private Integer dataScope;
    private String name;
    private String code;
    private Integer type;
    private String parentCode;
    private String icon;

    private String path;
    private String perms;

    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    /**
     * 是否在线设计 0不是、 1是
     */
    private Integer onlineDesignFlag;

    private Integer fdpCore;
}

