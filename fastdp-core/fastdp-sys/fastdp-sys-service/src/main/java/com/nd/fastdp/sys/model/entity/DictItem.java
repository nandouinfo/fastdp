package com.nd.fastdp.sys.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.nd.fastdp.db.mybatis.model.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("fdp_sys_dict_item")
@Data
@Accessors(chain = true)
public class DictItem extends BaseEntity {

    private String name;
    private String dictCode;
    private String value;


    private Integer sort;
    private String remark;

    private Integer state;
    private Integer del;

    private Integer fdpCore;

}
