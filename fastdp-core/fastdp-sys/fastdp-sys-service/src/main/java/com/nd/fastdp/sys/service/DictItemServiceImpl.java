package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.constant.VerifyRespCodeEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.convert.DictItemConvert;
import com.nd.fastdp.sys.dao.DictItemMapper;
import com.nd.fastdp.sys.model.bo.dict.DictItemBO;
import com.nd.fastdp.sys.model.dto.dict.DictItemAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemListParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemModifyParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemQueryParam;
import com.nd.fastdp.sys.model.entity.DictItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictItemServiceImpl implements DictItemService {

    @Autowired
    private DictItemMapper dictItemMapper;

    @Override
    public DictItemBO add(DictItemAddParam param) {

        DictItem dictItem = DictItemConvert.INSTANCE.from(param);

        if(dictItemMapper.isExist(dictItem.getDictCode(), dictItem.getValue())){
            throw new BusinessException(VerifyRespCodeEnum.CODE_EXIST);
        }

        dictItemMapper.insert(dictItem);

        return DictItemConvert.INSTANCE.to(dictItem);
    }

    @Override
    public void del(DelParam param) {
        dictItemMapper.del(param);
    }

    @Override
    public DictItemBO modify(DictItemModifyParam param) {

        DictItem dictItem = DictItemConvert.INSTANCE.from(param);

        if(dictItemMapper.isExist(dictItem.getId(), dictItem.getValue())){
            throw new BusinessException(VerifyRespCodeEnum.CODE_EXIST);
        }

        dictItemMapper.updateById(dictItem);

        return DictItemConvert.INSTANCE.to(dictItem);
    }

    @Override
    public void modifyState(StateParam param) {
        dictItemMapper.updateState(param);
    }

    @Override
    public DictItemBO get(String id) {

        DictItem dictItem = dictItemMapper.selectById(id);

        return DictItemConvert.INSTANCE.to(dictItem);
    }

    @Override
    public DictItemBO getByCode(String dictCode, String value) {

        DictItem dictItem = dictItemMapper.getByCode(dictCode, value);

        return DictItemConvert.INSTANCE.to(dictItem);
    }


    @Override
    public List<DictItemBO> listAllEnable(String dictCode) {

        List<DictItem> dictItems =  dictItemMapper.listAllEnable(dictCode);

        return DictItemConvert.INSTANCE.to(dictItems);
    }

    @Override
    public List<DictItemBO> listAll(String dictCode) {

        List<DictItem> dictItems =  dictItemMapper.listAll(dictCode);

        return DictItemConvert.INSTANCE.to(dictItems);
    }

    @Override
    public List<DictItemBO> list(DictItemListParam param) {

        List<DictItem> dictItems =  dictItemMapper.list(param);

        return DictItemConvert.INSTANCE.to(dictItems);
    }

    @Override
    public Page<DictItemBO> page(DictItemQueryParam param) {

        IPage<DictItem> page =  dictItemMapper.page(param.getPage(), param);

        return DictItemConvert.INSTANCE.to(page);
    }
}
