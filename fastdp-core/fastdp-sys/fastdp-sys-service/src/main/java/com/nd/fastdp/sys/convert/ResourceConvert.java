package com.nd.fastdp.sys.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.sys.model.bo.resource.ResourceBO;
import com.nd.fastdp.sys.model.bo.resource.ResourceDataScopeBO;
import com.nd.fastdp.sys.model.dto.resource.ResourceAddParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceModifyParam;
import com.nd.fastdp.sys.model.entity.Resource;
import com.nd.fastdp.sys.model.entity.ResourceDataScope;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ResourceConvert {

    ResourceConvert INSTANCE = Mappers.getMapper(ResourceConvert.class);

    Resource from(ResourceAddParam resourceBO);

    Resource from(ResourceModifyParam resourceBO);

    ResourceBO to(Resource resource);

    List<ResourceBO> to(List<Resource> resources);

    Page<ResourceBO> to(IPage<Resource> resources);

    List<ResourceDataScopeBO> toDataScope(List<ResourceDataScope> dataScopeBOS);
}
