package com.nd.fastdp.sys.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nd.fastdp.db.mybatis.mapper.FdpBaseMapper;
import com.nd.fastdp.sys.model.dto.notice.NoticeListParam;
import com.nd.fastdp.sys.model.dto.notice.NoticeQueryParam;
import com.nd.fastdp.sys.model.view.NoticeReceiverV;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

@Repository
public interface NoticeReceiverVMapper extends FdpBaseMapper<NoticeReceiverV> {

    default IPage<NoticeReceiverV> page(IPage<NoticeReceiverV> page, NoticeQueryParam param){

        return selectPage(page, queryWrapper(param));
    }

    default QueryWrapper queryWrapper(NoticeListParam param){

        QueryWrapper queryWrapper = new QueryWrapper<NoticeReceiverV>()
                .eq("receiver_user", param.getLoginUser().getUserId())
                .eq(null != param.getReceiverState(), "receiver_state", param.getReceiverState())
                .orderByDesc("release_time");

        return queryWrapper;
    }

}
