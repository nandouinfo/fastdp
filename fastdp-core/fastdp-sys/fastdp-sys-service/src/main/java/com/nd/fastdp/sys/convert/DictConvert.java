package com.nd.fastdp.sys.convert;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.sys.model.bo.dict.DictBO;
import com.nd.fastdp.sys.model.dto.dict.DictAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictModifyParam;
import com.nd.fastdp.sys.model.entity.Dict;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DictConvert {

    DictConvert INSTANCE = Mappers.getMapper(DictConvert.class);

    Dict from(DictAddParam param);

    Dict from(DictModifyParam param);

    DictBO to(Dict dict);

    List<DictBO> to(List<Dict> dicts);

    Page<DictBO> to(IPage<Dict> dicts);
}
