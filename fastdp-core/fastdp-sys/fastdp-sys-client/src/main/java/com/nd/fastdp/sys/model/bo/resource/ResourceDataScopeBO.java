package com.nd.fastdp.sys.model.bo.resource;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ResourceDataScopeBO extends BaseBO implements Serializable {

    private String id;
    private String name;
    private String code;
    private Integer type;
    private String resourceCode;

    private String authSql;

    private Integer fdpCore;
}
