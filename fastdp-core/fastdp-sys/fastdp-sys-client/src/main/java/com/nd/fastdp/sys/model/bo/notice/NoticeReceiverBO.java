package com.nd.fastdp.sys.model.bo.notice;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class NoticeReceiverBO extends BaseBO implements Serializable {

    private String id;

    private String noticeId;

    private Integer state;

    private Long receiverTime;
    private String receiverUser;
    private String receiverUserName;
    private String receiverDept;
    private String receiverDeptName;

    private Long readTime;

    private Integer fdpCore;
}
