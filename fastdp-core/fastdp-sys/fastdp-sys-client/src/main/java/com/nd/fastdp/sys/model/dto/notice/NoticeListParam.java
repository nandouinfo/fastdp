package com.nd.fastdp.sys.model.dto.notice;

import com.nd.fastdp.framework.pojo.dto.ListParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("公告列表参数")
@Data
@Accessors(chain = true)
public class NoticeListParam extends ListParam implements Serializable {

    private String title;
    private String type;
    private Integer state;

    private String receiverUser;
    private Integer receiverState;
}
