package com.nd.fastdp.sys.model.dto.commonFun;

import com.nd.fastdp.framework.pojo.dto.ListParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("常用功能列表参数")
@Data
@Accessors(chain = true)
public class CommonFunListParam extends ListParam implements Serializable {

    private String type;
}
