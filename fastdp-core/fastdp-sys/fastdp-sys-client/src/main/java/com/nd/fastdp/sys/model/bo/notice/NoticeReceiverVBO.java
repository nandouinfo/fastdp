package com.nd.fastdp.sys.model.bo.notice;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class NoticeReceiverVBO extends BaseBO implements Serializable {

    private String noticeId;

    private Long readTime;
    private Long receiverTime;
    private String receiverUser;
    private String receiverUserName;
    private Integer receiverState;

    private String title;
    private String content;
    private Long releaseTime;//发布时间

}
