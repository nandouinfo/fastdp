package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 公告接收人类型枚举
 */
@Getter
@AllArgsConstructor
public enum NoticeReceiverTypeEnum implements BaseEnum {

    ALL(0, "所有"),
    DEPT(1, "指定部门"),
    EMPLOYEE(2, "指定人员");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

}
