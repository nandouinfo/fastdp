package com.nd.fastdp.sys.model.dto.notice;

import com.nd.fastdp.framework.pojo.dto.Param;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("公告发布参数")
@Data
@Accessors(chain = true)
public class NoticeReleaseParam extends Param implements Serializable {

    private String id;

    @Override
    public void init() {

    }
}
