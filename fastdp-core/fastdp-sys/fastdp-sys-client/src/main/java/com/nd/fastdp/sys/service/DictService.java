package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.model.bo.dict.DictBO;
import com.nd.fastdp.sys.model.dto.dict.DictAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictListParam;
import com.nd.fastdp.sys.model.dto.dict.DictModifyParam;
import com.nd.fastdp.sys.model.dto.dict.DictQueryParam;

import java.util.List;

public interface DictService {

    DictBO add(DictAddParam param);

    void del(DelParam param);

    DictBO modify(DictModifyParam param);

    void modifyState(StateParam param);

    DictBO get(String id);

    DictBO getByCode(String code);

    List<DictBO> listAllEnable();

    List<DictBO> listAll();

    List<DictBO> list(DictListParam param);

    Page<DictBO> page(DictQueryParam param);
}
