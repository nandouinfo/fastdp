package com.nd.fastdp.sys.model.dto.commonFun;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("常用功能添加参数")
@Data
@Accessors(chain = true)
public class CommonFunAddParam extends AddParam implements Serializable {

    private String busId;
    private Integer type;
}
