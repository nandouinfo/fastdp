package com.nd.fastdp.sys.model.dto.notice;

import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@ApiModel("公告修改参数")
@Data
@Accessors(chain = true)
public class NoticeModifyParam extends ModifyParam implements Serializable {

    private String id;
    private Integer type;
    private String title;
    private String recivers;
    private String content;
    private List<String> files;

    private Integer topState;//置顶状态
}
