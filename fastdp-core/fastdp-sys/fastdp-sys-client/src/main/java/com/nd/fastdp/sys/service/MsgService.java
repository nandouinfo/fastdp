package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.model.bo.msg.MsgBO;
import com.nd.fastdp.sys.model.dto.msg.MsgAddParam;
import com.nd.fastdp.sys.model.dto.msg.MsgListParam;
import com.nd.fastdp.sys.model.dto.msg.MsgQueryParam;

import java.util.List;

public interface MsgService {

    void add(MsgAddParam param);

    void del(DelParam param);

    void read(String id);

    Integer unReadCount(String receiveUser);

    void readByUser(String receiveUser);

    MsgBO get(String id);

    List<MsgBO> list(MsgListParam param);

    Page<MsgBO> page(MsgQueryParam param);
}
