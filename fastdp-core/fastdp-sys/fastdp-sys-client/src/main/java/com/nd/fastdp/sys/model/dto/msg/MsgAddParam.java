package com.nd.fastdp.sys.model.dto.msg;

import com.nd.fastdp.framework.pojo.dto.Param;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("消息添加参数")
@Data
@Accessors(chain = true)
public class MsgAddParam extends Param implements Serializable {

    private String title;
    private String content;
    private String busId;
    private Integer type;

    private Integer state;

    private Long receiveTime;
    private String receiveUser;
    private String receiveUserName;
    private String receiveDept;
    private String receiveDeptName;

    @Override
    public void init() {

    }
}
