package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.model.bo.notice.NoticeBO;
import com.nd.fastdp.sys.model.bo.notice.NoticeReceiverBO;
import com.nd.fastdp.sys.model.bo.notice.NoticeReceiverVBO;
import com.nd.fastdp.sys.model.dto.notice.*;

import java.util.List;

public interface NoticeService {

    NoticeBO add(NoticeAddParam param);

    NoticeBO modify(NoticeModifyParam param);

    void del(DelParam param);

    NoticeBO get(String id);

    List<NoticeBO> list(NoticeListParam param);

    Page<NoticeBO> page(NoticeQueryParam param);

    Page<NoticeReceiverVBO> pageReceiver(NoticeQueryParam param);

    NoticeReceiverVBO getReceiverNotice(String id);

    void release(NoticeReleaseParam param);

    void unRelease(NoticeReleaseParam param);

    List<NoticeReceiverBO> listReceiver(String id);

    void read(String id);
}
