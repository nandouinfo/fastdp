package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 公告置顶状态枚举
 */
@Getter
@AllArgsConstructor
public enum NoticeTopStateEnum implements BaseEnum {

    NORMAL(0, "正常"),
    TOP(1, "置顶");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

}
