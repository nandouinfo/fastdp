package com.nd.fastdp.sys.model.dto.msg;

import com.nd.fastdp.framework.pojo.dto.ListParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("消息列表参数")
@Data
@Accessors(chain = true)
public class MsgListParam extends ListParam implements Serializable {

    private String type;
    private Integer state;
    private Long startTime;
    private Long endTime;
}
