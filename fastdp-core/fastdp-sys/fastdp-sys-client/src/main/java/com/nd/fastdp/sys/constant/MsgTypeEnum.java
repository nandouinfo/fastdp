package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 消息类型枚举
 */
@Getter
@AllArgsConstructor
public enum MsgTypeEnum implements IntArrayValuable, BaseEnum {

    OA_APPROVE(0, "OA审批"),
    REMODEL_APPLY(1, "装修申请"),
    REMODEL_ACCEPTANCE_APPLY(2, "装修验收申请"),
    CONTRACT_EXPIRE_NOTICE(3, "合同过期提醒"),
    CONTRACT_WAIT_PAY(4, "合同待缴费消息"),
    CONTRACT_BILL_FAILD(5, "合同账单作废提醒"),
    CONTRACT_PAY_SUCCESS(6, "合同账单支付成功提醒"),
    CONTRACT_BILL_CREATE_SUCCESS(7, "合同账单生成成功提醒"),
    EQUIPMENT_EXCEPTION(8, "设备异常"),
    REMODEL_INSPECTION(9, "装修巡查");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(MsgTypeEnum::getValue).toArray();

    @Override
    public int[] array() {
        return ARRAYS;
    }

}
