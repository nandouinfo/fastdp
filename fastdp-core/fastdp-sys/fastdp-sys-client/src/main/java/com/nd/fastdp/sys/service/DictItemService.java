package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.sys.model.bo.dict.DictBO;
import com.nd.fastdp.sys.model.bo.dict.DictItemBO;
import com.nd.fastdp.sys.model.dto.dict.DictItemAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemListParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemModifyParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemQueryParam;

import java.util.List;

public interface DictItemService {

    DictItemBO add(DictItemAddParam param);

    void del(DelParam param);

    DictItemBO modify(DictItemModifyParam param);

    void modifyState(StateParam param);

    DictItemBO get(String id);

    DictItemBO getByCode(String dictCode, String value);

    List<DictItemBO> listAllEnable(String dictCode);

    List<DictItemBO> listAll(String dictCode);

    List<DictItemBO> list(DictItemListParam param);

    Page<DictItemBO> page(DictItemQueryParam param);

}
