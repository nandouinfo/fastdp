package com.nd.fastdp.sys.model.dto.resource;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@ApiModel("资源数据权限参数")
@Data
@Accessors(chain = true)
public class DataScopeParam extends AddParam implements Serializable {

    private String name;
    private String code;
    private Integer type;
    private String resourceCode;

    private String authSql;
}
