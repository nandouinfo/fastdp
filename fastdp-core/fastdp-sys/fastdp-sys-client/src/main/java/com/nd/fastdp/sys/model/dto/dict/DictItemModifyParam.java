package com.nd.fastdp.sys.model.dto.dict;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ModifyParam;
import com.nd.fastdp.framework.validator.InEnum;
import com.nd.fastdp.sys.constant.DictTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("字典项修改参数")
@Data
@Accessors(chain = true)
public class DictItemModifyParam extends ModifyParam implements Serializable {

    @ApiModelProperty(value = "ID", required = true, example = "C4CA4238A0B923820DCC509A6F75849B")
    @NotEmpty(message = "ID不能为空")
    private String id;

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "值", required = true, example = "TEST")
    @NotEmpty(message = "值不能为空")
    private String value;

    @ApiModelProperty(value = "类型编号", example = "PARENT", required = true)
    private String dictCode;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序", required = true, example = "1")
    private Integer sort;

    @ApiModelProperty(value = "备注", required = true, example = "测试备注")
    private String remark;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", required = true, example = "1")
    private Integer state;
}
