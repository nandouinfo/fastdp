package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 资源类型枚举
 */
@Getter
@AllArgsConstructor
public enum ResourceDesignTypeEnum implements IntArrayValuable, BaseEnum {

    ONLINE(1, "在线设计"),
    CODE(2, "编码研发");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(ResourceDesignTypeEnum::getValue).toArray();

    @Override
    public int[] array() {
        return ARRAYS;
    }

}
