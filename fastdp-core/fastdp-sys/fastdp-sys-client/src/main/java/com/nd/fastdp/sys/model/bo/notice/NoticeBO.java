package com.nd.fastdp.sys.model.bo.notice;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class NoticeBO extends BaseBO implements Serializable {

    private String id;

    private String title;
    private String content;

    private String recivers;

    private Integer sort;

    private Integer type;

    private Integer state;

    private Integer topState;//置顶状态

    private Long releaseTime;//发布时间
    private String releaseUser;//发布人编码
    private String releaseUserName;//发布人名称
    private String releaseDept;//发布部门编码
    private String releaseDeptName;//发布部门名称

    private Integer fdpCore;
}
