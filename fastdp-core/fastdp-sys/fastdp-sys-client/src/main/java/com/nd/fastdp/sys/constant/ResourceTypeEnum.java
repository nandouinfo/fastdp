package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 资源类型枚举
 */
@Getter
@AllArgsConstructor
public enum ResourceTypeEnum implements IntArrayValuable, BaseEnum {

    MODULE(1, "模块"),
    MENU(2, "菜单"),
    ACTION(3, "动作");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(ResourceTypeEnum::getValue).toArray();

    @Override
    public int[] array() {
        return ARRAYS;
    }

}
