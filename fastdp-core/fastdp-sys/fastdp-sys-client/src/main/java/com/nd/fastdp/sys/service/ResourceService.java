package com.nd.fastdp.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.model.bo.resource.ResourceBO;
import com.nd.fastdp.sys.model.bo.resource.ResourceDataScopeBO;
import com.nd.fastdp.sys.model.dto.resource.ResourceAddParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceListParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceModifyParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceQueryParam;

import java.util.List;

public interface ResourceService {

    ResourceBO add(ResourceAddParam param);

    void del(DelParam param);

    ResourceBO modify(ResourceModifyParam param);

    ResourceBO get(String id);

    ResourceBO getByCode(String code);

    List<ResourceBO> findByUserId(String userId);

    List<ResourceBO> listAllEnable();

    List<ResourceBO> listAll();

    List<ResourceBO> list(ResourceListParam param);

    Page<ResourceBO> page(ResourceQueryParam param);

    List<ResourceDataScopeBO> findDataScopeByUserId(String userId);

    List<ResourceDataScopeBO> listAllDataScope();
}
