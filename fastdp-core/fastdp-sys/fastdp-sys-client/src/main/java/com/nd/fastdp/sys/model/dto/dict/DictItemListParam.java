package com.nd.fastdp.sys.model.dto.dict;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ListParam;
import com.nd.fastdp.framework.validator.InEnum;
import com.nd.fastdp.sys.constant.DictTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("字典项列表参数")
@Data
@Accessors(chain = true)
public class DictItemListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "值", required = true, example = "TEST")
    @NotEmpty(message = "值不能为空")
    private String value;

    @ApiModelProperty(value = "父级编号", example = "PARENT", required = true)
    private String dictCode;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", required = true, example = "1")
    private Integer state;
}
