package com.nd.fastdp.sys.service;

import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.sys.model.bo.commonFun.CommonFunBO;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunAddParam;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunListParam;

import java.util.List;

public interface CommonFunService {

    void add(CommonFunAddParam param);

    void del(DelParam param);

    List<CommonFunBO> list(CommonFunListParam param);
}
