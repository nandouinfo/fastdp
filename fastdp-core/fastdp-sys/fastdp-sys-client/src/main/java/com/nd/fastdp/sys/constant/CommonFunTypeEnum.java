package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 常用资源类型枚举
 */
@Getter
@AllArgsConstructor
public enum CommonFunTypeEnum implements IntArrayValuable, BaseEnum {

    RESOURCE(0, "资源"),
    OA_FORM(1, "OA表单");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(CommonFunTypeEnum::getValue).toArray();

    @Override
    public int[] array() {
        return ARRAYS;
    }

}
