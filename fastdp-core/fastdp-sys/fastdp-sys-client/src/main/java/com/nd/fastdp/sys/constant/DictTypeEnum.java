package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import com.nd.fastdp.framework.validator.IntArrayValuable;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 字典类型枚举
 */
@Getter
@AllArgsConstructor
public enum DictTypeEnum implements IntArrayValuable, BaseEnum {

    LIST(1, "列表"),
    TREE(2, "树形");

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(DictTypeEnum::getValue).toArray();

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

    @Override
    public int[] array() {
        return ARRAYS;
    }

}
