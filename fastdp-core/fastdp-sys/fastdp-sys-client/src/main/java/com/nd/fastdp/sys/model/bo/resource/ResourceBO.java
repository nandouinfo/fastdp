package com.nd.fastdp.sys.model.bo.resource;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import com.nd.fastdp.sys.model.dto.resource.DataScopeParam;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class ResourceBO extends BaseBO implements Serializable {

    private String id;
    private String name;
    private String code;
    private Integer type;
    private String parentCode;
    private String icon;

    private String path;
    private String perms;

    private Integer sort;
    private String remark;

    private Integer state;

    private Integer onlineDesignFlag;

    List<ResourceDataScopeBO> dataScopes;

    private Integer fdpCore;
}
