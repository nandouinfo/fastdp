package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 公告状态枚举
 */
@Getter
@AllArgsConstructor
public enum NoticeStateEnum implements BaseEnum {

    BOOKED(0, "录入"),
    RELEASE(1, "发布");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

}
