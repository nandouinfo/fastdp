package com.nd.fastdp.sys.model.bo.dict;

import com.nd.fastdp.framework.pojo.bo.BaseBO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class DictItemBO extends BaseBO implements Serializable {

    private String id;

    private String name;
    private String dictCode;
    private String value;


    private Integer sort;
    private String remark;

    private Integer state;

    private Integer fdpCore;
}
