package com.nd.fastdp.sys.model.dto.notice;

import com.nd.fastdp.framework.pojo.dto.AddParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@ApiModel("公告添加参数")
@Data
@Accessors(chain = true)
public class NoticeAddParam extends AddParam implements Serializable {

    private String title;
    private String recivers;
    private Integer type;

    private String content;

    private Integer topState;//置顶状态

    private List<String> files;
}
