package com.nd.fastdp.sys.model.bo.msg;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class MsgBO implements Serializable {

    private String id;

    private String title;
    private String content;
    private String busId;
    private Integer type;

    private Integer sort;
    private String remark;

    private Integer state;

    private Long receiveTime;
    private String receiveUser;
    private String receiveUserName;
    private String receiveDept;
    private String receiveDeptName;

    private Long readTime;

    private Integer fdpCore;
}
