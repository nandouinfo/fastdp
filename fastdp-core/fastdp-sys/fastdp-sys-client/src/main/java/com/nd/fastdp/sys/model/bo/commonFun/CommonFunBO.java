package com.nd.fastdp.sys.model.bo.commonFun;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class CommonFunBO implements Serializable {

    private String id;

    private String busId;
    private String userId;
    private Integer type;

    private Integer times;

    private Integer sort;
    private Integer state;
    private Integer del;

    private Integer fdpCore;
}
