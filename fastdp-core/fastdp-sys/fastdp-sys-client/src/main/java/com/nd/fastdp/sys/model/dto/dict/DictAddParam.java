package com.nd.fastdp.sys.model.dto.dict;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.AddParam;
import com.nd.fastdp.framework.validator.InEnum;
import com.nd.fastdp.sys.constant.DictTypeEnum;
import com.nd.fastdp.sys.constant.ResourceDesignTypeEnum;
import com.nd.fastdp.sys.constant.ResourceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("字典类型添加参数")
@Data
@Accessors(chain = true)
public class DictAddParam extends AddParam implements Serializable {

    @ApiModelProperty(value = "名称", required = true, example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "编码", required = true, example = "TEST")
    @NotEmpty(message = "编码不能为空")
    private String code;

    @NotEmpty(message = "类型不能为空")
    @InEnum(value = DictTypeEnum.class, message = "字典类型必须是 {value}")
    @ApiModelProperty(value = "类型。1 代表【列表】；2 代表【树形】；", required = true, example = "1")
    private String type;

    @ApiModelProperty(value = "父级编码", example = "PARENT")
    private String parentCode;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序", required = true, example = "1")
    private Integer sort;

    @ApiModelProperty(value = "备注", required = true, example = "测试备注")
    private String remark;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", required = true, example = "1")
    private Integer state;
}
