package com.nd.fastdp.sys.model.dto.resource;

import com.nd.fastdp.framework.pojo.constant.StatusEnum;
import com.nd.fastdp.framework.pojo.dto.ListParam;
import com.nd.fastdp.framework.validator.InEnum;
import com.nd.fastdp.sys.constant.ResourceDesignTypeEnum;
import com.nd.fastdp.sys.constant.ResourceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@ApiModel("资源列表参数")
@Data
@Accessors(chain = true)
public class ResourceListParam extends ListParam implements Serializable {

    @ApiModelProperty(value = "名称", example = "测试")
    @NotEmpty(message = "名称不能为空")
    private String name;

    @ApiModelProperty(value = "编码", example = "TEST")
    @NotEmpty(message = "编码不能为空")
    private String code;

    @NotEmpty(message = "类型不能为空")
    @InEnum(value = ResourceTypeEnum.class, message = "资源类型必须是 {value}")
    @ApiModelProperty(value = "资源类型。1 代表【模块】；2 代表【菜单】；3 代表【动作】", example = "1")
    private Integer type;

    @ApiModelProperty(value = "父级编码", example = "PARENT")
    private String parentCode;

    @NotEmpty(message = "状态不能为空")
    @InEnum(value = StatusEnum.class, message = "状态必须是 {value}")
    @ApiModelProperty(value = "状态。0 代表【启用】；1 代表【停用】", example = "1")
    private Integer state;

    @NotEmpty(message = "设计类型不能为空")
    @InEnum(value = ResourceDesignTypeEnum.class, message = "设计类型必须是 {value}")
    @ApiModelProperty(value = "设计类型。1 代表【在线设计】；2 代表【编码研发】；", example = "1")
    private Integer onlineDesignFlag;
}
