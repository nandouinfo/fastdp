package com.nd.fastdp.sys.constant;

import com.nd.fastdp.framework.pojo.constant.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息状态枚举
 */
@Getter
@AllArgsConstructor
public enum MsgStateEnum implements BaseEnum {

    UN_READ(0, "未读"),
    READ(1, "已读");

    /**
     * 资源类型
     */
    private Integer value;
    /**
     * 资源类型名
     */
    private String desc;

}
