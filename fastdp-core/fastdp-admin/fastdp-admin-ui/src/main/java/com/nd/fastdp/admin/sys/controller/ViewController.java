package com.nd.fastdp.admin.sys.controller;

import com.nd.fastdp.framework.log.annotation.OperationLogIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

@Controller
@Api(tags={"统一页面请求"})
public class ViewController{

    @OperationLogIgnore
    @ApiOperation(value = "请求地址")
    @GetMapping("/view/**")
    public String login(@ApiIgnore ModelMap map,
                        @ApiIgnore HttpServletRequest request) {

        String[] requestURI = request.getRequestURI().split("/view/");

        String parentCode = request.getParameter("parentCode");

        String path = requestURI.length > 1 && StringUtils.isNotEmpty(requestURI[1]) ? "/" + requestURI[1] : "/login";

        return path;
    }

}
