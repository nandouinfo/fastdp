<!DOCTYPE html>
<html>
<head>
    <title>南斗 · 快速开发平台</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <style>
        .layuiadmin-card-team .layui-team-img {
            width: 40px!important;
            height: 40px!important;
            line-height: 40px!important;
        }

        .layuiadmin-page-table td span{
            color: #878787!important;
        }

        .layuiadmin-card-list p.layuiadmin-big-font{
            font-size: 32px!important;
        }

    </style>

</head>
<body class="layui-layout-body" style="overflow-y: auto;">

<div class="layui-fluid" id="container">

    <div class="layui-row layui-col-space15">

        <div class="layui-col-sm6 layui-col-md4">
            <notice-panel></notice-panel>
        </div>

        <div class="layui-col-sm6 layui-col-md4">
            <undo-panel></undo-panel>
        </div>

        <div class="layui-col-sm6 layui-col-md4">
            <msg-panel></msg-panel>
        </div>

    </div>

</div>

<script src="https://cdn.staticfile.org/echarts/4.3.0/echarts.min.js"></script>

<script>
    layui.use(['FdpUtil', 'laytpl', 'form', 'jquery', 'element', 'laydate', 'http', 'jform'], function () {

        var $ = layui.jquery,
            http = layui.http,
            element = layui.element;

        var noticePanel = http.getTpl({url: "${context.contextPath}/view/index/notice"});
        $("notice-panel").replaceWith(noticePanel);

        var undoPanel = http.getTpl({url: "${context.contextPath}/view/index/undo"});
        $("undo-panel").replaceWith(undoPanel);

        var msgPanel = http.getTpl({url: "${context.contextPath}/view/index/msg"});
        $("msg-panel").replaceWith(msgPanel);

    });
</script>


</body>
</html>


