<div class="layui-card" id="msg-container" v-cloak style="height: 400px;margin-bottom:0px;">
    <div class="layui-card-header">消息<div style="display: inline-block;float: right;" ><a style="color:#dddddd;" href="javascript:;" @click="more()">更多&gt;&gt;</a></div></div>
    <div class="layui-card-body">
        <table class="layui-table layuiadmin-page-table" lay-skin="nob" v-if="msgs.length > 0">
            <tbody>
                <tr v-for="(msg, i) in msgs" @click="open(msg)">
                    <td style="width: 70%">
                        <span>{{msg.title}}</span>
                    </td>
                    <td style="width: 30%">
                        <span class="right">{{ formatUndoDate(msg.receiveTime) }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
        <van-empty description="暂无消息" v-else="msgs.length <= 0"/>
    </div>
</div>

<link href="${context.contextPath}/static/lib/vant/index.css" media="all" rel="stylesheet" />
<script src="${context.contextPath}/static/lib/vant/vant.min.js"></script>

<script>
    layui.use(['FdpUtil', 'laytpl', 'jquery', 'element', 'http'], function () {

        var $ = layui.jquery,
            http = layui.http,
            element = layui.element;

        new Vue({

            el: "#msg-container",

            data(){
                return {
                    msgs: []
                }
            },

            created(){

                var that = this;

                that.loadMsg();
                setInterval(function(){

                    if(that.isShow()){
                        that.loadMsg();
                    }

                }, 60 * 1000 * 3);
            },

            methods: {
                isShow(){
                    return $("#container").is(':visible');
                },
                formatUndoDate(date){
                    return DateUtil.format(date, "MM-dd HH:mm");
                },
                loadMsg(){

                    var that = this;

                    http.post({
                        url: GLOBAL.path.adminApi + '/sys/msg/page',
                        async: true,
                        hideLoading: true,
                        data: {
                            state: 0,
                            page: {
                                current: 1,
                                size: 8
                            }
                        },
                        backFun: function(result){
                            that.msgs = result.records;
                        }});
                },

                open(msg){

                    var that = this;

                    var winOptions = {};

                    winOptions.url = '${context.contextPath}/view/sys/msg/view?id=' + msg.id;
                    winOptions.title = '查看';
                    winOptions.type = 1;
                    winOptions.content = winOptions.url;

                    Win.open(winOptions)

                    for(var i = 0; that.msgs.length; i++){
                        if(that.msgs[i].id == msg.id){
                            that.msgs.splice(i,1);
                            break;
                        }
                    }
                },

                more(){
                    top.layui.$("#message_center").click();
                }
            },

        })

    })

</script>