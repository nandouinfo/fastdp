<div class="layui-card" id="undo-container" v-cloak style="height: 400px;margin-bottom:0px;">
    <div class="layui-card-header">待办<template v-if="procinstsTotal>0">({{procinstsTotal}})</template><div style="display: inline-block;float: right;" id="" ><a style="color:#dddddd;" href="javascript:;" @click="moreUndos()">更多&gt;&gt;</a></div></div>
    <div class="layui-card-body">
        <table class="layui-table layuiadmin-page-table" lay-skin="nob" v-if="procinsts.length > 0">
            <tbody>
                <tr v-for="(procinst, i) in procinsts" @click="openUndo(procinst)">
                    <td style="width: 70%">
                        <span>{{procinst.title}}</span>
                    </td>
                    <td style="width: 30%">
                        <span class="right">{{ formatUndoDate(procinst.createTime) }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
        <van-empty description="暂无待办" v-else="procinsts.length <= 0"/>
    </div>
</div>

<link href="${context.contextPath}/static/lib/vant/index.css" media="all" rel="stylesheet" />
<script src="${context.contextPath}/static/lib/vant/vant.min.js"></script>

<script>
    layui.use(['FdpUtil', 'laytpl', 'jquery', 'element', 'http'], function () {

        var $ = layui.jquery,
            http = layui.http,
            element = layui.element;

        new Vue({

            el: "#undo-container",

            data(){
                return {
                    procinsts: [],
                    procinstsTotal: 0
                }
            },

            created(){

                var that = this;

                that.loadUndo();
                setInterval(function(){

                    if(that.isShow()){
                        that.loadUndo()
                    }

                }, 60 * 1000 * 5);
            },

            methods: {
                isShow(){
                    return $("#container").is(':visible');
                },
                formatUndoDate(date){
                    return DateUtil.format(date, "MM-dd HH:mm");
                },
                loadUndo(){

                    var that = this;

                    http.post({
                        url: GLOBAL.path.adminApi + '/workflow/procinst/waitApprovePage',
                        async: true,
                        hideLoading: true,
                        data: {
                            page: {
                                current: 1,
                                size: 8
                            }
                        },
                        backFun: function(result){
                            that.procinsts = result.records;
                            that.procinstsTotal = result.total;
                        }});
                },

                openUndo(procinst){

                    var winOptions = {}

                    winOptions.url = GLOBAL.path.basePath + '/static/workflow/procdesign/index.html';
                    winOptions.title ='详情';
                    winOptions.type = 2;
                    winOptions.content = winOptions.url;
                    winOptions.isAppendToken = false;

                    var undo = null;
                    http.get({
                        url: GLOBAL.path.adminApi + '/workflow/undo/getLoginUserByProcinstId/' + procinst.id,
                        async: false,
                        backFun: function(result){
                            undo = result;
                        }});

                    if(undo != null){
                        winOptions.url = winOptions.url + "?undoId=" + undo.id + "#/view"
                        winOptions.content = winOptions.url;
                    }else{
                        winOptions.url = winOptions.url + "?id=" + procinst.id + "#/view"
                        winOptions.content = winOptions.url;
                    }

                    Win.open(winOptions);
                },

                moreUndos(){
                    top.layui.$("a[resource_code='WAIT_APPROVE']").click();
                }
            },

        })

    })

</script>