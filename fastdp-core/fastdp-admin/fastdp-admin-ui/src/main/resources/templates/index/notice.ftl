<div class="layui-card" id="notice-container" v-cloak style="height: 400px;margin-bottom:0px;">
    <div class="layui-card-header">公告<div style="display: inline-block;float: right;" id="" ><a style="color:#dddddd;" href="javascript:;" @click="moreNotice()">更多&gt;&gt;</a></div></div>
    <div class="layui-card-body">
        <table class="layui-table layuiadmin-page-table" lay-skin="nob" v-if="notices.length > 0">
            <tbody>
                <tr v-for="(notice, i) in notices" @click="openNotice(notice)">
                    <td style="width: 70%">
                        <span>{{notice.title}}</span>
                        <span class="layui-badge-dot" style="margin-left: 2px;" v-if="notice.receiverState == 0"></span>
                    </td>
                    <td style="width: 30%">
                        <span class="right">{{ formatUndoDate(notice.releaseTime) }}</span>
                    </td>
                </tr>
            </tbody>
        </table>
        <van-empty description="暂无公告" v-else="notices.length <= 0"/>
    </div>
</div>

<link href="${context.contextPath}/static/lib/vant/index.css" media="all" rel="stylesheet" />
<script src="${context.contextPath}/static/lib/vant/vant.min.js"></script>

<script>
    layui.use(['FdpUtil', 'laytpl', 'jquery', 'element', 'http'], function () {

        var $ = layui.jquery,
            http = layui.http,
            element = layui.element;

        new Vue({

            el: "#notice-container",

            data(){
                return {
                    notices: []
                }
            },

            created(){

                var that = this;

                that.loadNotice();
                setInterval(function(){

                    if(that.isShow()){
                        that.loadNotice()
                    }

                }, 60 * 1000 * 5);
            },

            methods: {
                isShow(){
                    return $("#container").is(':visible');
                },
                formatUndoDate(date){
                    return DateUtil.format(date, "MM-dd HH:mm");
                },
                loadNotice(){

                    var that = this;

                    http.post({
                        url: GLOBAL.path.adminApi + '/sys/notice/pageReceiver',
                        async: true,
                        hideLoading: true,
                        data: {
                            page: {
                                current: 1,
                                size: 8
                            }
                        },
                        backFun: function(result){
                            that.notices = result.records;
                        }});
                },

                openNotice(notice){

                    http.get({
                        url: GLOBAL.path.adminApi + '/sys/notice/read/' + notice.id,
                        async: true,
                        hideLoading: true,
                        backFun: function(result){
                            notice.state = GLOBAL.constant.Dict.NOTICE_READ_STATE.READ.value;
                        }});

                    Win.open({
                        url: GLOBAL.path.basePath + "/view/sys/notice/read?id=" + notice.id,
                        width: '50%',
                        height: '60%',
                        type: 1,
                        offset: 't',
                        anim: 0,
                        skin: '-1',
                        isOutAnim: false
                    })
                },

                moreNotice(){
                    top.layui.$("a[resource_code='NOTICE_VIEW']").click();
                }
            },

        })

    })

</script>