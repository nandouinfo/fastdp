<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <div id="userRoleTree" class="eleTree" lay-filter="userRoleTree" style="margin-top: 15px;"></div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'authtree', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            authtree = layui.authtree,
            util = layui.util;

        var MODULE = "ORG-USER";

        var userId = '${RequestParameters.id!}';
        var userRoles = null;
        var userRoleTree = '#userRoleTree';

        http.get({
            url: GLOBAL.path.adminApi + '/org/user/findRole/' + userId,
            async: false,
            backFun: function(result){
                userRoles =  result;
            }});

        var checkeds = [];
        for(var i = 0; i < userRoles.length; i++){
            checkeds.push(userRoles[i].roleId);
        }

        var roles = SessionUtils.getLoginUser().roles;
        for(var i = 0; i < roles.length; i++){
            roles[i].parentId = '';
        }
        var treeDatas = [{
            name: "系统角色",
            list: DataConvert.listToTreeConvert(roles, {
                childKey: 'list',
                nameKey: 'name',
                valueKey: 'id',
                labelKey: 'name',
                checkedKey: checkeds,
                parentKey: 'parentId'
            }),
            value: "",
            data: {
                name: "系统角色",
                value: ""
            },
            checked: checkeds && checkeds.length > 0 ? true : false
        }]

        console.info(treeDatas);

        authtree.render(userRoleTree, treeDatas, {
            inputname: 'roleIds[]',
            layfilter: 'userRoleTree',
            autowidth: true,
            openchecked: false,
            autochecked: true
        });

        authtree.showDept(userRoleTree, 3);//默认展开到第三层

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            var userRoleChecked = authtree.getChecked(userRoleTree)
            var roleIds = [];

            for (var i = 0; i < userRoleChecked.length; i++){
                if(userRoleChecked[i] && userRoleChecked[i].length > 0){
                    roleIds.push(userRoleChecked[i])
                }
            }

            var params = {
                userId: userId,
                roleIds: roleIds
            };

            http.post({'url': GLOBAL.path.adminApi + '/org/user/addRole',
                'data': params,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });


    });
</script>


