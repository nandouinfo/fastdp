<form class="layui-form" onsubmit="return false" id="form" lay-filter="form">

    <div class="layui-tab layui-tab-brief" lay-filter="" style="height: 100%;">


        <ul class="layui-tab-title">
            <li class="layui-this">资源权限</li>
            <li>数据权限</li>
        </ul>
        <div class="layui-tab-content" style="height: 100%">
            <div class="layui-tab-item layui-show" style="height: 100%">
                <div style="padding: 20px 0px;height: 100%; overflow-x: hidden;">

                    <div class="layui-row layui-col-space15 layui-form-item">

                        <div class="layui-col-xs1">
                        </div>

                        <div class="layui-col-xs9">
                            <div id="roleResourceTree" class="eleTree" lay-filter="roleResourceTree"
                                 style="margin-top: 15px;"></div>
                        </div>

                        <div class="layui-col-xs2">
                        </div>

                    </div>

                </div>
            </div>
            <div class="layui-tab-item " style="height: 100%;">
                <div style="padding: 20px 0px; height: 100%; overflow-x: hidden;">

                    <div class="layui-row layui-col-space15 layui-form-item">

                        <div class="layui-col-xs1">
                        </div>

                        <div class="layui-col-xs9">
                            <div id="dataScopeTree" class="eleTree" lay-filter="dataScopeTree"
                                 style="margin-top: 15px;"></div>
                        </div>

                        <div class="layui-col-xs2">
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存
                </button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消
                </button>
            </div>
        </div>

    </div>
</form>

<style>
    #dataScopeTree .layui-disabled{
        padding-left: 0px!important;
    }
    #dataScopeTree .layui-disabled i{
        display: none;
    }
</style>

<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'authtree', 'util'], function () {

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            authtree = layui.authtree,
            util = layui.util;

        var MODULE = "ORG-ROLE";

        var role = null;
        var roleResources = null;
        var roleResourceTree = '#roleResourceTree';

        http.get({
            url: GLOBAL.path.adminApi + '/org/role/get/${RequestParameters.id!}',
            async: false,
            backFun: function (result) {

                role = result;

                role.dataScope = role.dataScope ? role.dataScope + "" : "";

                setTimeout(function () {

                    form.val("form", role);

                    form.render('');

                }, 500)
            }
        });

        http.get({
            url: GLOBAL.path.adminApi + '/org/role/findResource/' + role.id,
            async: false,
            backFun: function (result) {
                roleResources = result;
            }
        });

        var checkeds = [];
        for (var i = 0; i < roleResources.length; i++) {
            checkeds.push(roleResources[i].resourceCode);
        }

        console.info(checkeds);

        var sources = SessionUtils.getLoginUser().resources;
        var treeDatas = [{
            name: "系统资源",
            list: DataConvert.listToTreeConvert(sources, {
                childKey: 'list',
                nameKey: 'name',
                valueKey: 'code',
                labelKey: 'name',
                checkedKey: checkeds
            }),
            value: "",
            data: {
                name: "系统资源",
                value: ""
            },
            checked: checkeds && checkeds.length > 0 ? true : false
        }]

        console.info(treeDatas);

        authtree.render(roleResourceTree, treeDatas, {
            inputname: 'resourceCodes[]',
            layfilter: 'roleResourceTree',
            autowidth: true,
            openchecked: false,
            autochecked: true,
            autoclose: false
        });

        authtree.showDept(roleResourceTree, 3);//默认展开到第三层

        //----------------------------------------数据权限-------------------------------------------------
        var dataScopeTree = '#dataScopeTree';

        var dataResource = [];
        var sources = JSON.parse(JSON.stringify(sources));
        for(var i = 0; i < sources.length; i++){
            if(sources[i].type != GLOBAL.constant.Dict.RESOURCE_TYPE.ACTION.value){
                sources[i].disabled = true;
                dataResource.push(sources[i]);
            }
        }

        var loginUserDataScopeMap = SessionUtils.getLoginUser().dataScopeMap || {};

        http.post({
            url: GLOBAL.path.adminApi + '/sys/resource/listAllDataScope',
            async: false,
            backFun: function (result) {
                if(result){

                    for(var i = 0; i < result.length; i++){

                        result[i].parentCode = result[i].resourceCode;

                        if(loginUserDataScopeMap[result[i].resourceCode]){
                            if(parseInt(result[i].code) <= parseInt(loginUserDataScopeMap[result[i].resourceCode].code)){
                                dataResource.push(result[i]);
                            }
                        }else{
                            if(SessionUtils.getLoginUser().superAdmin){
                                dataResource.push(result[i]);
                            }
                        }
                    }
                }
            }
        });

        console.info('resourceDataScopeMap:' + dataResource);

        var dataResourceChecked = [];
        if(role.dataScopes){
            for(var i = 0; i < role.dataScopes.length; i++){
                dataResourceChecked.push(role.dataScopes[i].dataScopeId);
            }
        }

        var dataScopeTreeDatas = [{
            name: "系统资源",
            list: DataConvert.listToTreeConvert(dataResource, {
                childKey: 'list',
                nameKey: 'name',
                valueKey: 'id',
                labelKey: 'name',
                checkedKey: dataResourceChecked
            }),
            value: "",
            data: {
                name: "系统资源",
                value: ""
            },
            disabled: true,
            checked: false
        }]

        authtree.render(dataScopeTree, dataScopeTreeDatas, {
            inputname: 'dataScopes[]',
            layfilter: 'dataScopeTree',
            autowidth: true,
            openchecked: false,
            autochecked: true
        });

        authtree.showDept(dataScopeTree, 4);//默认展开到第三层

        //----------------------------------------数据权限-------------------------------------------------

        //监听提交
        form.on('submit(submit_btn)', function (data) {

            var roleResourceChecked = authtree.getChecked(roleResourceTree)
            var resourceCodes = [];

            for (var i = 0; i < roleResourceChecked.length; i++) {
                resourceCodes.push(roleResourceChecked[i])
            }


            var roleDataScopeChecked = authtree.getChecked(dataScopeTree)
            var dataScopeIds = [];

            for (var i = 0; i < roleDataScopeChecked.length; i++) {
                dataScopeIds.push(roleDataScopeChecked[i])
            }

            var params = {
                roleId: role.id,
                resourceCodes: resourceCodes,
                dataScopeIds: dataScopeIds
            };

            http.post({
                'url': GLOBAL.path.adminApi + '/org/role/addResource',
                'data': params,
                'backFun': function (result) {

                    top.layui.layer.msg("操作成功！");

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }
            })

            return false;
        });

    });
</script>


