<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <div id="userMangDeptTree" class="eleTree" lay-filter="userMangDeptTree" style="margin-top: 15px;"></div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>

<style>
    #userMangDeptTree .eleTree-checkbox-disabled{
        display: none;
    }
</style>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'eleTree', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            eleTree = layui.eleTree,
            util = layui.util;

        var MODULE = "ORG-USER";

        var userId = '${RequestParameters.id!}';
        var userMangDepts = null;
        var userMangDeptTree = '#userMangDeptTree';
        var deptTree = null;

        http.get({
            url: GLOBAL.path.adminApi + '/org/user/findMangDept/' + userId,
            async: false,
            backFun: function(result){
                userMangDepts =  result;
            }});

        var checkeds = [];
        for(var i = 0; i < userMangDepts.length; i++){
            checkeds.push(userMangDepts[i].id);
        }

        console.info(checkeds);

        var mangDepts = SessionUtils.getLoginUser().depts;
        var mangDeptMap = {};
        for(var i = 0; i < mangDepts.length; i++){
            mangDeptMap[mangDepts[i].id] = mangDepts[i];
        }

        var allDetps = [];
        http.post({
            url: GLOBAL.path.adminApi + '/org/dept/list',
            async: false,
            backFun: function(result){
                allDetps =  result;
                for(var i = 0; i < allDetps.length; i++){

                    allDetps[i].label = allDetps[i].name;

                    if(!mangDeptMap[allDetps[i].id]){
                        allDetps[i].disabled = true;
                    }
                }
            }});

        var treeDatas = DataConvert.listToTreeConvert(allDetps, {
            primaryKey: 'id',
            parentKey: 'parentId',
            valueKey: 'id',
            checkedKey: checkeds
        })

        console.info(treeDatas);

        deptTree = eleTree.render({
            elem: userMangDeptTree,
            data: treeDatas,
            highlightCurrent: true,
            showLine: true,
            showCheckbox: true,
            defaultExpandedKeys: [''],
            accordion: true,
            expandOnClickNode: true,
            request: {
                name: 'label',
                key: 'value'
            }
        });

        deptTree.expandAll();

        $("#userMangDeptTree").find(".eleTree-disabled").each(function () {
            $(this).parent().find(".eleTree-node-content-label").css("color", "#eee");
        })

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            var deptChecked = deptTree.getChecked(false, false);
            var deptIds = [];

            for (var i = 0; i < deptChecked.length; i++){
                deptIds.push(deptChecked[i].value)
            }

            var params = {
                userId: userId,
                deptIds: deptIds
            };

            http.post({'url': GLOBAL.path.adminApi + '/org/user/addMangDept',
                'data': params,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });


    });
</script>


