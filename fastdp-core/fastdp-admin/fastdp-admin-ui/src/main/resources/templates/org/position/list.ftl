<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>职务管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
</head>
<body>
    <div class="content">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-card">
                    <div class="layui-card-body">

                        <form class="layui-form" action="" onsubmit="return false">
                            <div class="layui-form-item">

                                <div class="layui-inline">
                                    <input type="text" name="name" placeholder="职务名称" autocomplete="off" class="layui-input">
                                </div>

                                <div class="layui-inline">
                                    <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>

                                <div class="layui-inline tool-btn right">
                                    <@shiro.hasPermission name="position:manage:add">
                                    <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add" data-options="{url: '/view/org/position/edit', title: '添加'}"><i class="layui-icon">&#xe654;</i></button>
                                    </@shiro.hasPermission>
                                    <@shiro.hasPermission name="position:manage:del">
                                    <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del" data-options="{url:'/org/position/del'}"><i class="layui-icon">&#xe640;</i></button>
                                    </@shiro.hasPermission>
                                </div>
                            </div>
                        </form>

                        <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                        <script type="text/html" id="operation_btn">
                            <@shiro.hasPermission name="position:manage:modify">
                            <a class="text-blue edit-btn" lay-event="edit" data-options="{url: '/view/org/position/edit'}">编辑</a> |
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="position:manage:del">
                            <a class="text-danger" lay-event="del" data-options="{url: '/org/position/del'}">删除</a>
                            </@shiro.hasPermission>

                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        layui.use(['laytpl', 'table', 'http', 'eleTree', 'jquery', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                util = layui.util;

            var MODULE = "ORG-POSITION";

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/org/position/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'name', title: '名称', width:150},
                    {field:'code', title: '编码', width:200},
                    {field:'sort', title: '排序', width:80},
                    {field:'createTime', title: '创建时间', width:180, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:150},
                    {field:'stateName', title: '状态', width:80, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.STATE.ENABLE.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }
                    }},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', minWidth:125}
                ]],
                id: 'lay_table',
                page: true
            });

            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', null);
            });

            //刷新列表
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

        });
    </script>
</body>
</html>
