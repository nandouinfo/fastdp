<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>部门管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">
</head>
<body>
    <div class="content">

        <div class="layui-row layui-col-space15">

            <div class="layui-col-md2">
                <div class="layui-card" lay-height="100px">
                    <div class="layui-card-header">组织树</div>
                    <div class="layui-card-body">
                        <input type="text" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search">
                        <div id="orgTree" class="eleTree" lay-filter="orgTree" style="margin-top: 15px;"></div>
                    </div>
                </div>
            </div>

            <div class="layui-col-md10">

                        <div class="layui-card">
                            <div class="layui-card-body">

                                <form class="layui-form" action="" onsubmit="return false">
                                    <input type="hidden" name="parentId" value="" id="parentId">
                                    <div class="layui-form-item">

                                        <div class="layui-inline">
                                            <input type="text" name="name" placeholder="组织名称" autocomplete="off" class="layui-input">
                                        </div>

                                        <div class="layui-inline">
                                            <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                        </div>

                                        <div class="layui-inline tool-btn right">
                                            <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add" data-options="{url: '/view/org/dept/edit?parentId=1', title: '添加'}"><i class="layui-icon">&#xe654;</i></button>
                                            <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del" data-options="{url:'/org/dept/del', callBack:{module: 'ORG-DEPT', cmdName: 'batchDel'}}"><i class="layui-icon">&#xe640;</i></button>
                                        </div>
                                    </div>
                                </form>

                                <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                                <script type="text/html" id="operation_btn">

                                    <a class="text-blue view-btn" lay-event="view" data-options="{url: '/view/org/dept/view'}">查看</a> |
                                    <a class="text-blue edit-btn" lay-event="edit" data-options="{url: '/view/org/dept/edit'}">编辑</a> |
                                    <a class="text-danger" lay-event="del" data-options="{url: '/org/dept/del', callBack:{module: 'ORG-DEPT', cmdName: 'del'}}">删除</a>

                                </script>
                            </div>
                        </div>

            </div>
        </div>
    </div>

    <script>
        layui.use(['laytpl', 'table', 'http', 'jquery', 'eleTree', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            var MODULE = "ORG-DEPT";

            var orgTree = null;
            http.post({
                url: GLOBAL.path.adminApi + '/org/dept/list',
                async: false,
                backFun: function(result){

                    if(result){
                        for(var i = 0; i < result.length; i++){
                            if(result[i].type == GLOBAL.constant.Dict.ORG_TYPE.COMPANY.value){
                                result[i].label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                            }else if(result[i].type == GLOBAL.constant.Dict.ORG_TYPE.DEPT.value){
                                result[i].label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                            }

                            if(result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                                result[i].label += result[i].name;
                            }else{
                                result[i].label += '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                            }
                        }
                    }

                    /*var treeData = [{
                        label: "<i class='layui-icon layui-icon-tree'></i>&nbsp;&nbsp;组织机构",
                        children: DataConvert.listToTreeConvert(result,{
                            primaryKey: 'id',
                            parentKey: 'parentId',
                            valueKey: 'id'
                        }),
                        value: "",
                        data: {
                            name: "组织机构"
                        }
                    }]*/

                    var treeData =  DataConvert.listToTreeConvert(result,{
                        primaryKey: 'id',
                        parentKey: 'parentId',
                        valueKey: 'id'
                    })

                    orgTree = eleTree.render({
                        elem: '#orgTree',
                        data: treeData,
                        highlightCurrent: true,
                        showLine: true,
                        showCheckbox: false,
                        defaultExpandedKeys: [''],
                        accordion: false,
                        expandOnClickNode: false,
                        request: {
                            name: 'label',
                            key: 'value'
                        },
                        searchNodeMethod: function(value,data) {
                            if (!value) return true;
                            return data.label.indexOf(value) !== -1;
                        }
                    });

                    // 节点点击事件
                    eleTree.on("nodeClick(orgTree)",function(d) {
                        console.log(d.data);    // 点击节点对应的数据
                        $("input[name='parentId']").val(d.data.currentData.value);

                        var options = Utils.parseOptions($(".addBtn").attr("data-options"));
                        var dataUrl = options.url.split("?")[0];
                        options.url = dataUrl + "?parentId=" + d.data.currentData.value;
                        $(".addBtn").attr("data-options", JSON.stringify(options));

                        $("#search_btn").click();
                    })

                    $(".eleTree-search").on("change",function() {
                        orgTree.search($(this).val());
                    })

                    orgTree.expandNode(['1']);
                }});

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/org/dept/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            parentId: $("input[name='parentId']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'name', title: '名称', width:170},
                    {field:'simpleName', title: '简称', width:150},
                    {field:'code', title: '编码', width:80},
                    {field:'typeName', title: '类型', width:70, templet: function (d) {
                        return Dict.getItemName(Dict.Type.ORG_TYPE, d.type);
                    }},
                    {field:'phone', title: '电话', width:120},
                    {field:'leaderName', title: '负责人', width:150, templet: function (d) {
                        return Org.getUserName(d.leader)
                    }},
                    {field:'stateName', title: '状态', width:70, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.STATE.ENABLE.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }

                        return itemName;
                    }},
                    {field:'sort', title: '排序', width:70},
                    {field:'createTime', title: '创建时间', width:160, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:120},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', width:150}
                ]],
                id: 'lay_table',
                page: true
            });

            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });

            //添加资源回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "addNode",
                callBack: function (options) {

                    var label = null;

                    if(options.type == GLOBAL.constant.Dict.ORG_TYPE.COMPANY.value){
                        label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                    }else if(options.type == GLOBAL.constant.Dict.ORG_TYPE.DEPT.value){
                        label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                    }

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        label += options.name;
                    }else{
                        label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    orgTree.append(options.parentId, {
                        label: label,
                        value: options.code,
                        data: options
                    })
                }
            })

            //修改资源回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "updateNode",
                callBack: function (options) {

                    var label = null;

                    if(options.type == GLOBAL.constant.Dict.ORG_TYPE.COMPANY.value){
                        label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                    }else if(options.type == GLOBAL.constant.Dict.ORG_TYPE.DEPT.value){
                        label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                    }

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        label += options.name;
                    }else{
                        label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    orgTree.updateKeySelf(options.id, {
                        label: label,
                        value: options.code,
                        data: options
                    })
                }
            })

            //头批量删除回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "batchDel",
                callBack: function (options) {
                    $('#search_btn').click();
                    for(var i = 0; i < options.data.length; i++){
                        orgTree.remove(options.data[i].id)
                    }
                }
            })

            //行删除回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "del",
                callBack: function (options) {
                    options.rowObj.del();
                    orgTree.remove(options.data.id)
                }
            })

            //刷新列表
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

        });
    </script>
</body>
</html>


