<div class="content">
    <div class="layui-row layui-col-space15">

        <div class="layui-col-md12">

            <div class="layui-card">
                <div class="layui-card-body">

                    <form class="layui-form" action="" onsubmit="return false">

                        <input type="hidden" name="roleId" value="${RequestParameters.roleId!}" id="roleId">

                        <div class="layui-form-item">

                            <div class="layui-inline tool-btn right">
                                <button id="search_btn" class="layui-btn" lay-submit="search"><i
                                            class="layui-icon">&#xe669;</i></button>
                            </div>

                        </div>
                    </form>

                    <table class="layui-hide" id="role_user_lay_table_content" lay-filter="table"></table>

                    <script type="text/html" id="operation_btn">

                        <@shiro.hasPermission name="user:del">
                            <a class="text-danger" lay-event="del" data-options="{url: '/org/user/delRole?userId={{d.id}}&roleId=${RequestParameters.roleId!}'}">删除</a>
                        </@shiro.hasPermission>
                    </script>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    layui.use(['laytpl', 'table', 'http', 'jquery', 'eleTree', 'FdpUtil', 'util'], function () {

        var laytpl = layui.laytpl,
            table = layui.table,
            layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            eleTree = layui.eleTree,
            util = layui.util;

        var MODULE = "ORG-ROLE-USER";

        //方法级渲染
        table.render({
            elem: '#role_user_lay_table_content',
            requestFun: function (page, size, renderFun) {

                http.get({
                    url: GLOBAL.path.adminApi + '/org/user/findByRoleId/' + $("input[name='roleId']").val(),
                    async: false,
                    data: {
                    },
                    backFun: function (result) {

                        renderFun({
                            data: result,
                            count: result.length
                        });
                    }
                });
            },
            cols: [[
                {type: 'numbers'},
                {field: 'name', title: '姓名', width: 100},
                {field: 'code', title: '工号', width: 120},
                {field: 'deptName', title: '部门', width: 150, templet: function (d) {
                    return Org.getDeptName(d.dept);
                }},
                {field: 'employeePositionName', title: '职务', width: 120, templet: function (d) {
                    return Org.getPositionName(d.employeePosition);
                }},
                {field: 'employeeTypeName', title: '类型', width: 120, templet: function (d) {
                    return Dict.getItemName(Dict.Type.EMPLOYEE_TYPE, d.employeeType);
                }},
                {field: 'sexName', title: '性别', width: 70, templet: function (d) {
                    return Dict.getItemName(Dict.Type.SEX, d.sex);
                }},
                {field: 'stateName', title: '状态', width: 70, templet: function (d) {

                    var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                    if (d.state == GLOBAL.constant.Dict.STATE.ENABLE.value) {

                        return '<span style="color: #5FB878;">' + itemName + '</span>';

                    } else {

                        return '<span style="color:#FF5722;">' + itemName + '</span>';
                    }
                }},
                {fixed: 'right', title: '操作', toolbar: '#operation_btn', width: 100}
            ]],
            id: 'role_user_lay_table',
            page: false
        });

        $('#search_btn').on('click', function () {

            //执行重载
            table.reload('role_user_lay_table', {});
        });

        //刷新列表
        FdpCommand.register({
            module: MODULE,
            cmdName: "refreshTable",
            callBack: function (options) {
                $('#search_btn').click();
            }
        })

    });

</script>


