<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>
        <input type="hidden" name="parentId" value="${RequestParameters.parentId!}"/>
        <input type="hidden" name="leader"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>基础信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">上级:</label>
                <div class="layui-input-block">
                    <input type="text" id="parentName" name="parentName" placeholder="默认顶级组织" autocomplete="off" class="layui-input" disabled>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">简称:</label>
                <div class="layui-input-block">
                    <input type="text" id="simpleName" name="simpleName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>编码:</label>
                <div class="layui-input-block">
                    <input type="text" id="code" name="code" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>类型:</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-dict="ORG_TYPE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">负责人:</label>
                <div class="layui-input-block">
                    <input type="text" id="leaderName" name="leaderName" autocomplete="off" class="layui-input" readonly="readonly">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">电话:</label>
                <div class="layui-input-block">
                    <input type="text" id="phone" name="phone" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">邮箱:</label>
                <div class="layui-input-block">
                    <input type="text" id="email" name="email" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">地址:</label>
                <div class="layui-input-block">
                    <input type="text" id="address" name="address" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>状态:</label>
                <div class="layui-input-block">
                    <select name="state" id="state" lay-dict="STATE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>排序</label>
                <div class="layui-input-block">
                    <input type="text" id="sort" name="sort" value="0" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>维护人员信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">创建时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="createTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">创建人:</label>
                <div class="layui-input-block">
                    <input type="text" name="createUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="createDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">修改时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">修改人:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            util = layui.util;

        var MODULE = "ORG-DEPT";

        var id = $("#form").find("input[name='id']").val();

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/org/dept/get/' + id,
                async: false,
                backFun: function(result){

                    result.createTime = util.toDateString(result.createTime);
                    result.modifyTime = util.toDateString(result.modifyTime);

                    form.val("form", result);

                    $("#form").find("input[name='code']").attr("readonly", "readonly");
                    $("#form").find("input[name='leaderName']").val(Org.getUserName(result.leader));
                }});
        }

        var parentId = $("#form").find("input[name='parentId']").val();
        if(parentId != null && parentId.length > 0){
            $("#form").find("input[name='parentName']").val(Org.getDeptName(parentId));
        }

        $("#leaderName").click(function () {

            //设置部门
            FdpCommand.register({
                module: MODULE,
                cmdName: "setLeader",
                callBack: function (options) {

                    $("#form").find("input[name='leaderName']").val("");
                    $("#form").find("input[name='leader']").val("");

                    if(options.length> 0){
                        $("#form").find("input[name='leaderName']").val($.trim(options[0].name));
                        $("#form").find("input[name='leader']").val(options[0].id);
                    }
                }
            })

            var checked = $("#form").find("input[name='leader']").val();

            Org.selectUser({
                callBack: {
                    module: MODULE,
                    cmdName: "setLeader",
                },
                checked: checked && checked.length > 0 ? [checked] : null,
                singleFlag: true
            })
        })

        form.disabled("form");//禁止编辑表单
    });
</script>


