<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>
        <input type="hidden" name="dept" value="${RequestParameters.dept!}"/>
        <input type="hidden" name="type" value="2"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>主要信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>姓名:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <#--<div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">简称:</label>
                <div class="layui-input-block">
                    <input type="text" id="simpleName" name="simpleName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>-->

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>工号:</label>
                <div class="layui-input-block">
                    <input type="text" id="code" name="code" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>部门:</label>
                <div class="layui-input-block">
                    <input type="text" id="deptName" name="deptName" autocomplete="off" required="" lay-verify="required" class="layui-input" readonly="readonly">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">职务:</label>
                <div class="layui-input-block">
                    <select name="employeePosition" id="employeePosition" lay-dict="{valField: 'id', url: '${context.contextPath}/org/position/list'}">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">员工类型:</label>
                <div class="layui-input-block">
                    <select name="employeeType" id="employeeType" lay-dict="EMPLOYEE_TYPE" >

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>状态:</label>
                <div class="layui-input-block">
                    <select name="state" id="state" lay-dict="STATE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>排序</label>
                <div class="layui-input-block">
                    <input type="text" id="sort" name="sort" value="0" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>基本信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">分机号:</label>
                <div class="layui-input-block">
                    <input type="text" id="landline" name="landline" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">电话:</label>
                <div class="layui-input-block">
                    <input type="text" id="phone" name="phone" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">邮箱:</label>
                <div class="layui-input-block">
                    <input type="text" id="email" name="email" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">办公地点:</label>
                <div class="layui-input-block">
                    <input type="text" id="address" name="address" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>个人信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">性别:</label>
                <div class="layui-input-block">
                    <select name="sex" id="sex" lay-dict="SEX">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">出生日期:</label>
                <div class="layui-input-block">
                    <input type="text" id="birthday" name="birthday" autocomplete="off" class="layui-input" placeholder="yyyy-MM-dd" readonly="readonly">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">身份证号:</label>
                <div class="layui-input-block">
                    <input type="text" id="idcard" name="idcard" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">民族:</label>
                <div class="layui-input-block">
                    <select name="nation" id="nation" lay-dict="NATION">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">籍贯:</label>
                <div class="layui-input-block">
                    <input type="text" id="nativeplace" name="nativeplace" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">文化程度:</label>
                <div class="layui-input-block">
                    <select name="degree" id="degree" lay-dict="DEGREE">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">婚姻状态:</label>
                <div class="layui-input-block">
                    <select name="married" id="married" lay-dict="MARRIE_STATE">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <input id="remark" name="remark" autocomplete="off" class="layui-input"></input>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>入职信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">入职时间:</label>
                <div class="layui-input-block">
                    <input type="text" id="entryTime" name="entryTime" autocomplete="off" class="layui-input" placeholder="yyyy-MM-dd" readonly="readonly">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">离职时间:</label>
                <div class="layui-input-block">
                    <input type="text" id="leaveTime" name="leaveTime" autocomplete="off" class="layui-input" placeholder="yyyy-MM-dd" readonly="readonly">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>紧急联系人</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">姓名:</label>
                <div class="layui-input-block">
                    <input type="text" id="contact" name="contact" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">电话:</label>
                <div class="layui-input-block">
                    <input type="text" id="contactPhone" name="contactPhone" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>维护人员信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">创建时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="createTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">创建人:</label>
                <div class="layui-input-block">
                    <input type="text" name="createUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="createDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">修改时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">修改人:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'laydate', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            laydate = layui.laydate;
            util = layui.util;

        var MODULE = "ORG-USER";

        var id = $("#form").find("input[name='id']").val();

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/org/user/get/' + id,
                async: false,
                backFun: function(result){

                    result.createTime = util.toDateString(result.createTime);
                    result.modifyTime = util.toDateString(result.modifyTime);

                    form.val("form", result);

                    $("#form").find("input[name='code']").attr("readonly", "readonly");
                }});
        }

        var dept = $("#form").find("input[name='dept']").val();
        if(dept != null && dept.length > 0){
              $("#form").find("input[name='deptName']").val(Org.getDeptName(dept));
        }

        form.disabled("form");//禁止编辑表单
    });
</script>


