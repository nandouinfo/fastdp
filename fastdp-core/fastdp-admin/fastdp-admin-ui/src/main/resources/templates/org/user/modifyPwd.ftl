<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改密码</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">

</head>
<body>
<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false" lay-filter="pwd-form">

        <div class=" layui-form-item">

            <label class="layui-form-label">原始密码:</label>
            <div class="layui-input-inline">
                <input type="password" name="pwd" autocomplete="off" class="layui-input" lay-verify="required"
                />
            </div>

        </div>
        <div class=" layui-form-item">
            <label class="layui-form-label">新密码:</label>
            <div class="layui-input-inline">
                <input type="password" name="newPwd" autocomplete="off" class="layui-input" lay-verify="required|pwd"
                />
            </div>
        </div>
        <div class=" layui-form-item">
            <label class="layui-form-label">确认密码:</label>
            <div class="layui-input-inline">
                <input type="password" name="confirmPwd" autocomplete="off" class="layui-input"
                       lay-verify="required|pwd"
                />
            </div>

        </div>
        <div class=" layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="user-pwd">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </form>

</div>

<script>
    layui.use(['form', 'http', 'jquery', 'laydate', 'FdpUtil', 'util'], function () {

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            util = layui.util;

        let path = {
            page: {
                pc: {}
            },
            req: {
                pc: {
                    modifyPwd: GLOBAL.path.adminApi + '/org/user/modifyPwd',
                }
            }
        };

        var MODULE = "SYS-USER-PWD";

        form.verify({
            pwd: function (value, item) { //value：表单的值、item：表单的DOM对象
                // if (!new RegExp("^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$").test(value)) {
                //     return '用户名不能有特殊字符';
                // }
                if (value.length < 6) {
                    return '密码最小6位';
                }
                if (value.length > 18) {
                    return '密码最大18位';
                }
            }
        });
        form.on("submit(user-pwd)", function (data) {
            let param = data.field;
            if (param.newPwd != param.confirmPwd) {
                // 密码有问题
                layer.msg("密码不一致");
                return;
            }
            http.post({
                url: path.req.pc.modifyPwd,
                data: param,
                async: false,
                backFun: function (result) {

                    layer.msg("修改成功");

                    setTimeout(function (){
                        SessionUtils.loginOut();
                        let contextPath = "${context.contextPath}";
                        top.window.location.href = DataVerify.isNotBlank(contextPath) ? contextPath : '/';
                    }, 3000);
                }
            });
        })
        // 获取房子信息

    });
</script>
</body>
</html>


