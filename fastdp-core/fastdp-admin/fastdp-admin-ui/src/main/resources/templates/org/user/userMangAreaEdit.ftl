<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <div id="userMangAreaTree" class="eleTree" lay-filter="userMangAreaTree" style="margin-top: 15px;"></div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>

<style>
    #userMangAreaTree .eleTree-checkbox-disabled{
        display: none;
    }
</style>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'eleTree', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            eleTree = layui.eleTree,
            util = layui.util;

        var MODULE = "ORG-USER";

        var userId = '${RequestParameters.id!}';
        var userMangAreas = null;
        var userMangAreaTree = '#userMangAreaTree';
        var areaTree = null;

        http.get({
            url: GLOBAL.path.adminApi + '/org/user/findMangArea/' + userId,
            async: false,
            backFun: function(result){
                userMangAreas =  result;
            }});

        var checkeds = [];
        for(var i = 0; i < userMangAreas.length; i++){
            checkeds.push(userMangAreas[i].id);
        }

        console.info(checkeds);

        var mangAreas = SessionUtils.getLoginUser().areas;
        var mangAreasMap = {};
        for(var i = 0; i < mangAreas.length; i++){
            mangAreasMap[mangAreas[i].id] = mangAreas[i];
        }

        var allAreas = [];
        http.post({
            url: GLOBAL.path.adminApi + '/cmstimp/base/area/list',
            async: false,
            backFun: function(result){
                allAreas =  result;
                for(var i = 0; i < allAreas.length; i++){

                    allAreas[i].label = allAreas[i].name;

                    if(!mangAreasMap[allAreas[i].id]){
                        allAreas[i].disabled = true;
                    }
                }
            }});

        var treeDatas = [{
            label: "<i class='layui-icon layui-icon-tree'></i>&nbsp;&nbsp;市场区域",
            children: DataConvert.listToTreeConvert(allAreas, {
                primaryKey: 'id',
                parentKey: 'parentId',
                valueKey: 'id',
                checkedKey: checkeds
            }),
            value: "",
            data: {
                name: "市场区域"
            }
        }];

        console.info(treeDatas);

        areaTree = eleTree.render({
            elem: userMangAreaTree,
            data: treeDatas,
            highlightCurrent: true,
            showLine: true,
            showCheckbox: true,
            defaultExpandedKeys: [''],
            accordion: true,
            expandOnClickNode: true,
            request: {
                name: 'label',
                key: 'value'
            }
        });

        areaTree.expandAll();

        $("#userMangAreaTree").find(".eleTree-disabled").each(function () {
            $(this).parent().find(".eleTree-node-content-label").css("color", "#eee");
        })

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            var areaChecked = areaTree.getChecked(false, false);
            var areaIds = [];

            for (var i = 0; i < areaChecked.length; i++){
                areaIds.push(areaChecked[i].value)
            }

            var params = {
                userId: userId,
                areaIds: areaIds
            };

            http.post({'url': GLOBAL.path.adminApi + '/org/user/addMangArea',
                'data': params,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });


    });
</script>


