<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>用户管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

    <style>
        #auth_tools {
            height: 25px;
            display: none;
            padding: 2px;
            padding-top: 5px;
            position: absolute;
            border: 2px dotted #e6e6e6;
            border-radius: 3px;
            background-color: white;
            box-shadow: 0px 15px 10px -15px #000;
            z-index: 19911128;
        }

        #auth_tools a {
            color: #878787 !important;
        }
    </style>
</head>
<body>
<div class="content">
    <div class="layui-row layui-col-space15">

        <div class="layui-col-md2">
            <div class="layui-card" lay-height="100px">
                <div class="layui-card-header">组织树</div>
                <div class="layui-card-body">
                    <input type="text" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search">
                    <div id="orgTree" class="eleTree" lay-filter="orgTree" style="margin-top: 15px;"></div>
                </div>
            </div>
        </div>

        <div class="layui-col-md10">

                    <div class="layui-card">
                        <div class="layui-card-body">

                            <form class="layui-form" action="" onsubmit="return false">
                                <input type="hidden" name="dept" value="" id="dept">
                                <input type="hidden" name="state" value="${RequestParameters.state!}" id="state">
                                <div class="layui-form-item">

                                    <div class="layui-inline">
                                        <input type="text" name="name" placeholder="用户姓名/工号" autocomplete="off"
                                               class="layui-input">
                                    </div>

                                    <div class="layui-inline">
                                        <button id="search_btn" class="layui-btn" lay-submit="search"><i
                                                    class="layui-icon">&#xe615;</i></button>
                                    </div>

                                    <div class="layui-inline tool-btn right">
                                        <@shiro.hasPermission name="user:add">
                                        <button class="layui-btn layui-btn-small layui-btn-normal addBtn"
                                                lay-event="add"
                                                data-options="{url: '/view/org/user/edit?dept=', title: '添加'}"><i
                                                    class="layui-icon">&#xe654;</i></button>
                                        </@shiro.hasPermission>

                                        <@shiro.hasPermission name="user:del">
                                        <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs"
                                                lay-event="del" data-options="{url:'/org/user/del'}"><i
                                                    class="layui-icon">&#xe640;</i></button>
                                        </@shiro.hasPermission>
                                    </div>
                                </div>
                            </form>

                            <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                            <script type="text/html" id="operation_btn">

                                <a class="text-blue edit-btn" lay-event="edit"
                                   data-options="{url: '/view/org/user/view'}">查看</a> |
                                <@shiro.hasPermission name="user:modify">
                                    <a class="text-blue edit-btn" lay-event="edit"
                                       data-options="{url: '/view/org/user/edit'}">编辑</a> |
                                </@shiro.hasPermission>

                                <@shiro.hasPermission name="user:authorization">
                                <a class="text-blue edit-btn"
                                   onmouseenter="showAuthTools('{{d.id}}', '{{d.name}}', event)"
                                   onmouseout="hideAuthTools('{{d.id}}')">分配权限</a> |
                                </@shiro.hasPermission>

                                <@shiro.hasPermission name="user:account:reset:pwd">
                                    <a class="text-blue edit-btn" lay-event="resetPwd"
                                       data-options="{url: '/org/user/resetPwd?userCode={{d.code}}'}">重置密码</a> |
                                </@shiro.hasPermission>

                                <@shiro.hasPermission name="user:del">
                                    <a class="text-danger" lay-event="del" data-options="{url: '/org/user/del'}">删除</a>
                                </@shiro.hasPermission>
                            </script>

                        </div>
                    </div>
        </div>
    </div>
</div>

<div id="auth_tools" onmouseenter="entryAuthTools()" onmouseover="entryAuthTools()" onmouseout="levelAuthTools()">
    <a class="text-blue edit-btn" href="javascript:;" id="userRoleEditBtn">所属角色</a> |<#--
    <a class="text-blue edit-btn" href="javascript:;" id="userMangAreaEditBtn">管理区域</a> |-->
    <a class="text-blue edit-btn" href="javascript:;" id="userMangDeptEditBtn">管理部门</a>
</div>

<script>
    layui.use(['laytpl', 'table', 'http', 'jquery', 'eleTree', 'FdpUtil', 'util'], function () {

        var laytpl = layui.laytpl,
            table = layui.table,
            layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            eleTree = layui.eleTree,
            util = layui.util;

        var MODULE = "ORG-USER";

        var orgTree = null;
        http.post({
            url: GLOBAL.path.adminApi + '/org/dept/list',
            async: true,
            backFun: function (result) {

                if (result) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].type == GLOBAL.constant.Dict.ORG_TYPE.COMPANY.value) {
                            result[i].label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                        } else if (result[i].type == GLOBAL.constant.Dict.ORG_TYPE.DEPT.value) {
                            result[i].label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                        }

                        if (result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value) {
                            result[i].label += result[i].name;
                        } else {
                            result[i].label += '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                        }
                    }
                }

                var treeData = DataConvert.listToTreeConvert(result, {
                    primaryKey: 'id',
                    parentKey: 'parentId',
                    valueKey: 'id'
                });

                orgTree = eleTree.render({
                    elem: '#orgTree',
                    data: treeData,
                    highlightCurrent: true,
                    showLine: true,
                    showCheckbox: false,
                    defaultExpandedKeys: ['1'],
                    accordion: false,
                    expandOnClickNode: false,
                    request: {
                        name: 'label',
                        key: 'value'
                    },
                    searchNodeMethod: function (value, data) {
                        if (!value) return true;
                        return data.label.indexOf(value) !== -1;
                    }
                });

                orgTree.expandNode(['1']);

                // 节点点击事件
                eleTree.on("nodeClick(orgTree)", function (d) {
                    //console.log(d.data);    // 点击节点对应的数据
                    $("input[name='dept']").val(d.data.currentData.value);

                    <@shiro.hasPermission name="user:add">
                    var options = Utils.parseOptions($(".addBtn").attr("data-options"));
                    var dataUrl = options.url.split("?")[0];
                    options.url = dataUrl + "?dept=" + d.data.currentData.value;
                    $(".addBtn").attr("data-options", JSON.stringify(options));
                    </@shiro.hasPermission>

                    $("#search_btn").click();
                })

                $(".eleTree-search").on("change", function () {
                    orgTree.search($(this).val());
                })
            }
        });

        //方法级渲染
        table.render({
            elem: '#lay_table_content',
            requestFun: function (page, size, renderFun) {

                http.post({
                    url: GLOBAL.path.adminApi + '/org/user/page',
                    async: false,
                    data: {
                        name: $("input[name='name']").val(),
                        dept: $("input[name='dept']").val(),
                        state: $("input[name='state']").val(),
                        page: {
                            current: page,
                            size: size ? size : GLOBAL.page.size
                        }
                    },
                    backFun: function (result) {

                        renderFun({
                            data: result.records,
                            count: result.total
                        });
                    }
                });
            },
            cols: [[
                {checkbox: true, fixed: true},
                {type: 'numbers'},
                {field: 'name', title: '姓名', width: 100},
                {field: 'code', title: '工号', width: 120},
                {field: 'deptName', title: '部门', width: 150, templet: function (d) {
                    return Org.getDeptName(d.dept);
                }},
                {field: 'employeePositionName', title: '职务', width: 120, templet: function (d) {
                    return Org.getPositionName(d.employeePosition);
                }},
                {field: 'employeeTypeName', title: '类型', width: 120, templet: function (d) {
                    return Dict.getItemName(Dict.Type.EMPLOYEE_TYPE, d.employeeType);
                }},
                {field: 'sexName', title: '性别', width: 70, templet: function (d) {
                    return Dict.getItemName(Dict.Type.SEX, d.sex);
                }},
                {field: 'stateName', title: '状态', width: 70, templet: function (d) {

                    var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                    if (d.state == GLOBAL.constant.Dict.STATE.ENABLE.value) {

                        return '<span style="color: #5FB878;">' + itemName + '</span>';

                    } else {

                        return '<span style="color:#FF5722;">' + itemName + '</span>';
                    }
                }},
                {field: 'sort', title: '排序', width: 70},
                {field: 'phone', title: '电话', width: 120},
                {field: 'email', title: '邮箱', width: 150},
                /*{field: 'createTime', title: '创建时间', width: 180, templet: function (d) {
                    return util.toDateString(d.createTime);
                }},
                {field: 'createUserName', title: '创建人', width: 150},*/
                {fixed: 'right', title: '操作', toolbar: '#operation_btn', width: 300}
            ]],
            id: 'lay_table',
            page: true
        });

        $('#search_btn').on('click', function () {

            //执行重载
            table.reload('lay_table', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        //刷新列表
        FdpCommand.register({
            module: MODULE,
            cmdName: "refreshTable",
            callBack: function (options) {
                $('#search_btn').click();
            }
        })

        $("#userRoleEditBtn").click(function () {
            Win.open({
                url: GLOBAL.path.basePath + "/view/org/user/userRoleEdit?id=" + curRowUserId,
                title: "所属角色--" + curRowUserName,
                type: 1
            })
        })

        $("#userMangAreaEditBtn").click(function () {
            Win.open({
                url: GLOBAL.path.basePath + "/view/org/user/userMangAreaEdit?id=" + curRowUserId,
                title: "管理区域--" + curRowUserName,
                type: 1
            })
        })

        $("#userMangDeptEditBtn").click(function () {
            Win.open({
                url: GLOBAL.path.basePath + "/view/org/user/userMangDeptEdit?id=" + curRowUserId,
                title: "管理部门--" + curRowUserName,
                type: 1
            })
        })

    });

    var onAuthToolsPanel = false;
    var curRowUserId = null;
    var curRowUserName = null;

    function showAuthTools(id, name, event) {

        curRowUserId = id;
        curRowUserName = name;

        var event = event || window.event;

        onAuthToolsPanel = true;

        layui.$("#auth_tools").hide();
        layui.$("#auth_tools").show().css("top", event.pageY + 15).css("left", event.pageX - 70);
    }

    function hideAuthTools(id) {

        onAuthToolsPanel = false;

        setTimeout(function () {
            if (!onAuthToolsPanel) {

                console.info("setTimeout")

                layui.$("#auth_tools").hide();
            }
        }, 1500);
    }

    function entryAuthTools() {
        onAuthToolsPanel = true;

        console.info("entryAuthTools")

        console.info(onAuthToolsPanel);
    }

    function levelAuthTools() {
        console.info("levelAuthTools")
        onAuthToolsPanel = false;
        setTimeout(function () {
            if (!onAuthToolsPanel) {

                console.info("setTimeout")

                layui.$("#auth_tools").hide();
            }
        }, 1500);
    }
</script>
</body>
</html>


