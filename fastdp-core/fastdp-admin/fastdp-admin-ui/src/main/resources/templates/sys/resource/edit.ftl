<style>
    #dataScopeSel .layui-form-checkbox span{
        width: 110px;
    }
</style>
<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>
        <input type="hidden" name="parentCode" value="${RequestParameters.parentCode!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>基础信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">父级:</label>
                <div class="layui-input-block">
                    <input type="text" id="parentName" name="parentName" placeholder="系统资源" autocomplete="off" class="layui-input" disabled>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>编码:</label>
                <div class="layui-input-block">
                    <input type="text" id="code" name="code" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>类型:</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-dict="RESOURCE_TYPE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>状态:</label>
                <div class="layui-input-block">
                    <select name="state" id="state" lay-dict="STATE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>排序</label>
                <div class="layui-input-block">
                    <input type="text" id="sort" name="sort" value="0" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>图标:</label>
                <div class="layui-input-block">
                    <input type="text" id="icon" name="icon" value="layui-icon-file-b" required="" lay-filter="icon" lay-verify="required" autocomplete="off" class="layui-input" style="display:none;">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">路径:</label>
                <div class="layui-input-block">
                    <input type="text" id="path" name="path" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">权限:</label>
                <div class="layui-input-block">
                    <textarea id="perms" name="perms" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
                <span class="tips">*多个权限分号隔开</span>
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>可选数据权限</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div id="dataScopeSel">

            <div class="layui-row layui-col-space15 layui-form-item layui-form-text" v-if="options.length > 0">

                <div class="layui-col-xs10">
                    <div class="layui-input-block">

                        <fieldset class="layui-elem-field">
                            <legend>标准权限</legend>
                            <div class="layui-field-box">
                                <input type="checkbox" lay-dict="DATA_SCOPE" name="dataScopes">
                            </div>
                        </fieldset>

                    </div>
                </div>

                <div class="layui-col-xs2">

                </div>

            </div>

        </div>

        <div id="dataScopeSel">

            <div class="layui-row layui-col-space15 layui-form-item layui-form-text" v-if="options.length > 0">

                <div class="layui-col-xs10">
                    <div class="layui-input-block">

                        <fieldset class="layui-elem-field">
                            <legend>
                                自定义权限
                                <button type="button" id="dataScopeAddBtn" style="border: 0px;" class="layui-btn layui-btn-primary layui-btn-sm">
                                    <i class="layui-icon">&#xe654;</i>
                                </button>
                            </legend>
                            <div class="layui-field-box" id="custom_dataScope_list">
                            </div>
                        </fieldset>

                    </div>
                </div>

                <div class="layui-col-xs2">

                </div>

            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>备注信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>


        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'util', 'iconPicker'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            util = layui.util,
            iconPicker = layui.iconPicker;

        var MODULE = "SYS-RESOURCE";

        var id = $("#form").find("input[name='id']").val();

        form.render();//渲染表单

        var dataScopeMap = {};
        var dataScopeAdd = function (options) {

            var id = options.id || UUID();

            options.id = id;

            dataScopeMap[id] = options;

            if($("#" + id).length > 0){
                $("#" + id).remove()
            }

            $("#custom_dataScope_list").append("<div class='layui-unselect layui-form-checkbox layui-form-checked' id='" + id + "'><span name='dataScope_detail_btn'>" + options.name + "(" + options.code + ")" + "</span><i name='del_dataScope_btn' class='layui-icon layui-icon-close'></i></div>");

            $("span[name='dataScope_detail_btn']").unbind().click(function(){

                var id = $(this).parent().attr("id");

                Win.open({
                    url: "${context.contextPath}/view/sys/resource/dataScope?id=" + id,
                    title: "数据权限(500~999)",
                    offset: 'c',
                    height: '500px',
                    width: '600px;',
                    type: 1
                });

            })

            $("i[name='del_dataScope_btn']").unbind().click(function(){

                var id = $(this).parent().attr("id");

                dataScopeMap[id] = null;

                $(this).parent().remove();
            })
        }

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/sys/resource/get/' + id,
                async: false,
                backFun: function(result){

                    form.val("form", result);

                    $("#form").find("input[name='code']").attr("readonly", "readonly");

                    $("input[name='dataScopes']").each(function(){
                        $(this).prop('checked', false);
                    })

                    if(result.dataScopes){
                        for(var i = 0; i < result.dataScopes.length; i++){
                            if(result.dataScopes[i].type == GLOBAL.constant.Dict.DATA_SCOPE_TYPE.CUSTOMER.value){
                                dataScopeAdd(result.dataScopes[i]);
                            }else{
                                $("input[name='dataScopes'][value='" + result.dataScopes[i].code + "']").prop('checked', true);
                            }
                        }
                    }

                    form.render('checkbox');//渲染表单

                }});
        }

        iconPicker.render({
            // 选择器，推荐使用input
            elem: '#icon',
            // 数据类型：fontClass/unicode，推荐使用fontClass
            type: 'fontClass',
            // 是否开启搜索：true/false，默认true
            search: true,
            // 是否开启分页：true/false，默认true
            page: false,
            // 每页显示数量，默认12
            limit: 12,
            // 每个图标格子的宽度：'43px'或'20%'
            cellWidth: '43px',
            // 点击回调
            click: function (data) {
                console.log(data);
            },
            // 渲染成功后的回调
            success: function(d) {
                console.log(d);
            }
        });

        var parentCode = $("#form").find("input[name='parentCode']").val();
        if(parentCode != null && parentCode.length > 0){

            http.get({
                url: GLOBAL.path.adminApi + '/sys/resource/getByCode/' + parentCode,
                async: false,
                backFun: function(result){
                    $("#form").find("input[name='parentName']").val(result.name);
                }});
        }

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            var params = data.field;
            params.dataScopes = [];

            $("input[name='dataScopes']").each(function (){
                if($(this).is(":checked")){
                    params.dataScopes.push({
                        code: $(this).val(),
                        name: $(this).attr('title'),
                        type: GLOBAL.constant.Dict.DATA_SCOPE_TYPE.SYS.value
                    })
                }
            })

            for(var key in dataScopeMap){
                if(dataScopeMap[key] != null){

                    dataScopeMap[key].type = GLOBAL.constant.Dict.DATA_SCOPE_TYPE.CUSTOMER.value

                    params.dataScopes.push( dataScopeMap[key]);
                }
            }

            console.info(data.field);

            http.post({'url': GLOBAL.path.adminApi + '/sys/resource/' + (id ? 'modify': 'add'),
                'data': data.field,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    FdpCommand.execute({
                        module: MODULE,
                        cmdName: (id ? "updateNode" : "addNode"),
                        options: result
                    });

                    FdpCommand.execute({
                        module: MODULE,
                        cmdName: "refreshTable",
                        options: result
                    })

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });

        $("#dataScopeAddBtn").click(function () {

            Win.open({
                url: "${context.contextPath}/view/sys/resource/dataScope",
                title: "数据权限(500~999)",
                offset: 'c',
                height: '500px',
                width: '600px;',
                type: 1
            });
        })



        /**
         * 添加数据权限事件
         */
        FdpCommand.register({
            module: MODULE,
            cmdName: "dataScopeAddEvent",
            callBack: function (options) {
                console.info(options);

                dataScopeAdd(options);
            }
        })

        FdpCommand.register({
            module: MODULE,
            cmdName: "getDataScope",
            callBack: function (id) {
                return dataScopeMap[id];
            }
        })

    });
</script>


