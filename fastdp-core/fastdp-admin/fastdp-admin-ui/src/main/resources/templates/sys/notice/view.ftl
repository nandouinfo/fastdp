<style>
    .file-container {
        width: 100%;
        height: 500px;
        border-width: 0px;
        border-style: solid;
        background-color: #fff;
        border-radius: 2px;
        border-color: #e6e6e6;
        overflow: auto;
    }

    .file-container .file {
        height: 170px;
        width: 170px;
        margin-left: 10px;
        margin-top: 10px;
        margin-bottom: 0px;
        margin-right: 0px;
        border-width: 0px;
        border-style: solid;
        background-color: #fff;
        border-radius: 2px;
        border-color: #e6e6e6;
    }

    .operation-btn {
        height: 20px;
        width: 178px;
        text-align: center;
    }

    .file-name{
        text-align: center;
    }

    #notice-form .text {
        /*border: 1px solid #ccc;*/
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .layui-form-tool{
        z-index: 19911128;
    }

    .layui-anim-upbit{
        z-index: 999999999999999!important;
    }
</style>
<div style="padding: 20px 0px; overflow-x: hidden;" id="notice-form">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>接收者:</label>
                <div class="layui-input-block">
                    <div style="width:15%; display: inline-block;">
                        <select name="type" id="type" lay-dict="NOTICE_RECEIVER_TYPE" required="" lay-verify="required" lay-filter="type">

                        </select>
                    </div>
                    <input type="text" id="recivers" name="recivers" required="" lay-verify="required" autocomplete="off" class="layui-input" readonly="readonly" style="display: inline-block;width:84%">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>标题:</label>
                <div class="layui-input-block">
                    <input type="text" id="title" name="title" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>正文:</label>
                <div class="layui-input-block">
                    <textarea name="content" style="display: none;"></textarea>
                    <div id="content" class="text"></div>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">
            <div class="layui-col-xs1">
            </div>
            <div class="layui-col-xs9" style="text-align: right;">
                <a href="javascript:;" style="color: #c2c2c2; font-size: 12px;" id="read_details_btn">
                    已读：<span style="color: #5FB878" id="read_count">0</span> 未读：<span style="color: #FFB800" id="unread_count">0</span>
                </a>
            </div>
            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text" id="file_list_panel"
             style="display: none;">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <div class="layui-input-block">
                    <div class="file-container">

                    </div>
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>维护人员信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs2">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">创建时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="createTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">创建人:</label>
                <div class="layui-input-block">
                    <input type="text" name="createUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="createDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">修改时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">修改人:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">发布时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="releaseTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">发布人:</label>
                <div class="layui-input-block">
                    <input type="text" name="releaseUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="releaseDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn layui-btn-warm" lay-submit="" lay-filter="unRelease_btn" id="unRelease_btn" style="display: none;">取消发布</button>
                <button type="button" class="layui-btn layui-btn-primary" id="view_btn">预览</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
            </div>
        </div>

    </form>

</div>

<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'upload', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            upload = layui.upload,
            util = layui.util;

        var MODULE = "SYS-NOTICE";

        var id = $("#form").find("input[name='id']").val();
        var recivers = [];

        var files = [];

        var appendFile = function(fileObj){

            if (FileType.isImg(fileObj.originalName)) {

                $(".file-container").append("<div id='file-" + fileObj.id + "' style='display: inline-block;' name='img-div' title='" + fileObj.originalName + "'><img class='file' src='" + Appendix.getDownLoadUrl(fileObj.id) + "'/><div class='file-name' title='" + fileObj.originalName + "'>" + fileObj.originalName + "</div><div class='operation-btn'><a name='file-download-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>下载</a></div></div>");

            } else {

                $(".file-container").append("<div id='file-" + fileObj.id + "' style='display: inline-block;' name='pdf-div' title='" + fileObj.originalName + "'><img file-id='" + fileObj.id + "' isPdf='" + (fileObj.originalName.indexOf('.pdf') >= 0) + "' class='file' src='" + (fileObj.originalName.indexOf('.pdf') > 0 ? '${context.contextPath}/static/img/pdf-default.png' : '${context.contextPath}/static/img/file-default.png') + "'/><div class='file-name' title='" + fileObj.originalName + "'>" + fileObj.originalName + "</div><div class='operation-btn'><a name='file-download-btn' file-id='" +fileObj.id + "' class='text-blue' href='javascript:;'>下载</a></div></div>");
            }

            files.push(fileObj.id);

            $("a[name='file-download-btn']").unbind().click(function () {

                var id = $(this).attr("file-id");

                top.window.open(Appendix.getDownLoadUrl(id));
            });

            Appendix.imgPreview("div[name='img-div']");

            $("div[name='pdf-div'] img").unbind().click(function () {

                var id = $(this).attr("file-id");
                var isPdf = $(this).attr("isPdf");

                if (isPdf == "true") {

                    Appendix.pdfPreview(id);

                } else {
                    layer.msg("不支持在线浏览!");
                }
            })

            if (files.length > 0) {
                $("#file_list_panel").show();
            } else {
                $("#file_list_panel").hide();
            }
        }

        form.render();//渲染表单

        var fillReciverNames = function (options) {

            recivers = options;

            if(recivers.length > 0){

                var reciverNames = [];
                for (var i = 0; i < recivers.length; i++){
                    reciverNames.push(recivers[i].name);
                }

                $("#form").find("input[name='recivers']").val(reciverNames.join("、"));

            }else{
                $("#form").find("input[name='recivers']").val("");
            }
        }

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/sys/notice/get/' + id,
                async: false,
                backFun: function(result){

                    result.createTime = util.toDateString(result.createTime);
                    result.modifyTime = util.toDateString(result.modifyTime);

                    if(result.releaseTime){
                        result.releaseTime = util.toDateString(result.releaseTime);
                    }

                    form.val("form", result);

                    form.render("select");//渲染表单

                    var fileObjs = Appendix.find(result.id, Appendix.Type.DefaultType);

                    if(fileObjs){
                        for(var i = 0; i < fileObjs.length; i++){
                            appendFile(fileObjs[i]);
                        }
                    }

                    fillReciverNames(JSON.parse(result.recivers));

                    if(result.state == GLOBAL.constant.Dict.NOTICE_STATE.RELEASE.value){
                        $("#form").find("#unRelease_btn").show();
                    }
                }});
        }

        Editor.init({
            container: [$("#form").find("#content")[0]],
            syncTextArea: $("#form").find("textarea[name='content']"),
            contenteditable: false,
            content: $("#form").find("textarea[name='content']").val(),
            showMenus: false,
            height: 600,
            showFullScreen: false
        });

        $("#view_btn").click(function(){

            Win.open({
                url: GLOBAL.path.basePath + "/view/sys/notice/preView?id=" + id,
                width: '50%',
                height: '60%',
                type: 1,
                offset: 't',
                anim: 0,
                skin: '-1',
                isOutAnim: false
            })
        })

        form.disabled("form");//禁止编辑表单

        $("#unRelease_btn").click(function(){

            layer.confirm('取消后将清空所有已读、未读记录，请确认!', function (index) {

                http.post({
                    url: GLOBAL.path.adminApi + '/sys/notice/unRelease?id=' + id,
                    async: false,
                    backFun: function (result) {

                        layer.msg("已取消");

                        FdpCommand.execute({
                            module: MODULE,
                            cmdName: "refreshTable",
                            options: result
                        })

                        setTimeout(function () {

                            $("button[lay-filter='cancle_btn']").click();
                        }, 500);
                    }
                });
            });

        })

        var reads = [];
        var unReads = [];
        http.post({
            url: GLOBAL.path.wechatApi + '/sys/notice/recivers?id=' + id,
            async: true,
            backFun: function(result){

                for (var i = 0; i < result.length; i++){

                    if(result[i].state == GLOBAL.constant.Dict.NOTICE_READ_STATE.READ.value){
                        reads.push(result[i]);
                    }else{
                        unReads.push(result[i]);
                    }
                }

                $("#read_count").html(reads.length);
                $("#unread_count").html(unReads.length);
            }});

        $("#read_details_btn").unbind().click(function () {
            Win.open({
                url: GLOBAL.path.basePath + '/view/sys/notice/read_detail?id=' + id,
                title: "查阅详情(" + (reads.length + unReads.length) + ")",
                type: 1
            });
        })


    });
</script>


