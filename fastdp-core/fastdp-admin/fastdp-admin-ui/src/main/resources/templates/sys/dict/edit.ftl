<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>
        <input type="hidden" name="parentCode" value="${RequestParameters.parentCode!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">父级:</label>
                <div class="layui-input-block">
                    <input type="text" id="parentName" name="parentName" placeholder="默认为顶级类型" autocomplete="off" class="layui-input" disabled>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>编码:</label>
                <div class="layui-input-block">
                    <input type="text" id="code" name="code" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>


        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>类型:</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-dict="DICT_TYPE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>状态:</label>
                <div class="layui-input-block">
                    <select name="state" id="state" lay-dict="STATE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>排序</label>
                <div class="layui-input-block">
                    <input type="text" id="sort" name="sort" value="0" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>


        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            util = layui.util;

        var MODULE = "SYS-DICT";

        var id = $("#form").find("input[name='id']").val();

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/sys/dict/get/' + id,
                async: false,
                backFun: function(result){
                    form.val("form", result);
                }});

            $("#form").find("input[name='code']").attr("readonly", "readonly");
        }

        var parentCode = $("#form").find("input[name='parentCode']").val();
        if(parentCode != null && parentCode.length > 0){
            $("#form").find("input[name='parentName']").val(top.Dict.getDictName(parentCode));
        }

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            http.post({'url': GLOBAL.path.adminApi + '/sys/dict/' + (id ? 'modify': 'add'),
                'data': data.field,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    FdpCommand.execute({
                        module: MODULE,
                        cmdName: (id ? "updateDictNode" : "addDictNode"),
                        options: result
                    });

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });


    });
</script>


