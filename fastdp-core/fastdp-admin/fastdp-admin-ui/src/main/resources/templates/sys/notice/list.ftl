<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>通知公告</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">

</head>
<body>
<div class="content">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">

                    <form class="layui-form" action="" onsubmit="return false">
                        <div class="layui-form-item">

                            <div class="layui-inline">
                                <input type="text" name="title" placeholder="标题" autocomplete="off" class="layui-input">
                            </div>

                            <div class="layui-inline">
                                <select name="state" id="state" lay-dict="NOTICE_STATE" placeholder="发布状态">

                                </select>
                            </div>

                            <div class="layui-inline">
                                <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                            </div>

                            <div class="layui-inline tool-btn right">
                                <@shiro.hasPermission name="notice:manage:add">
                                <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add"  data-options="{url: '/view/sys/notice/edit', title: '添加', width: '1200px'}"><i class="layui-icon">&#xe654;</i></button>
                                </@shiro.hasPermission>
                                <@shiro.hasPermission name="notice:manage:del">
                                <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del"  data-options="{url: '/sys/notice/del', title: '删除'}"><i class="layui-icon">&#xe640;</i></button>
                                </@shiro.hasPermission>
                            </div>
                        </div>
                    </form>

                    <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                    <script type="text/html" id="operation_btn">

                        <a class="text-blue" lay-event="view" data-options="{url: '/view/sys/notice/view', title: '详情', width: '1200px'}"> 详情 </a> |

                        <@shiro.hasPermission name="notice:manage:modify">
                            {{# if(d.state == GLOBAL.constant.Dict.NOTICE_STATE.BOOKED.value) { }}
                                <a class="text-blue" lay-event="edit" data-options="{url: '/view/sys/notice/edit', title: '编辑', width: '1200px'}"> 编辑 </a> |
                            {{# } }}
                        </@shiro.hasPermission>

                        <@shiro.hasPermission name="notice:manage:del">
                            <a class="text-danger" lay-event="del"  data-options="{url: '/sys/notice/del', title: '删除'}"> 删除 </a>
                        </@shiro.hasPermission>

                    </script>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    layui.use(['laytpl', 'table', 'http', 'jquery', 'FdpUtil', 'util'], function(){

        var laytpl = layui.laytpl,
            table = layui.table,
            layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            util = layui.util;

        //模块名称
        var MODULE = "SYS-NOTICE";

        //方法级渲染
        table.render({
            elem: '#lay_table_content',
            requestFun: function(page, size, renderFun) {

                http.post({
                    url: GLOBAL.path.adminApi + '/sys/notice/page',
                    async: false,
                    data: {
                        title: $("input[name='title']").val(),
                        state: $("select[name='state']").val(),
                        page: {
                            current: page,
                            size: size ? size : GLOBAL.page.size
                        }
                    },
                    backFun: function(result){

                        renderFun({
                            data: result.records,
                            count: result.total
                        });
                    }});
            },
            cols: [[
                {checkbox: true, fixed: true},
                {type: 'numbers'},
                {field:'title', title: '标题', width:300},
                /*{field:'topState', title: '置顶', width:80, templet: function (d) {
                    return Dict.getItemName(Dict.Type.NOTICE_TOP_STATE, d.topState);
                }},*/
                {field:'type', title: '接收者', width:100, templet: function (d) {
                    return Dict.getItemName(Dict.Type.NOTICE_RECEIVER_TYPE, d.type);
                }},
                {field:'createUserName', title: '创建人', width:150},
                {field:'createTime', title: '创建时间',width:180, templet: function (d) {
                    return util.toDateString(d.createTime);
                }},
                {field:'stateName', title: '状态', width:80, templet: function (d) {

                    var itemName = Dict.getItemName(Dict.Type.NOTICE_STATE, d.state);

                    if(d.state == GLOBAL.constant.Dict.NOTICE_STATE.RELEASE.value){

                        return '<span style="color: #5FB878;">' + itemName  + '</span>';

                    }else{

                        return '<span style="color:#FF5722;">' + itemName  + '</span>';
                    }
                }},
                {field:'releaseTime', title: '发布时间',width:180, templet: function (d) {
                    return d.releaseTime ? util.toDateString(d.releaseTime) : "";
                }},
                {fixed: 'right', title:'操作', toolbar: '#operation_btn', minWidth:125}
            ]],
            id: 'lay_table',
            page: true
        });

        $('#search_btn').on('click', function(){

            //执行重载
            table.reload('lay_table', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        FdpCommand.register({
            module: MODULE,
            cmdName: "refreshTable",
            callBack: function (options) {
                $('#search_btn').click();
            }
        })

    });
</script>
</body>
</html>


