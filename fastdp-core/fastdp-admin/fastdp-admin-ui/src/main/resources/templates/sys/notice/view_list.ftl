<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>通知公告</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">

</head>
<body>
<div class="content">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">

                    <form class="layui-form" action="" onsubmit="return false">
                        <div class="layui-form-item">

                            <div class="layui-inline">
                                <input type="text" name="title" placeholder="标题" autocomplete="off" class="layui-input">
                            </div>

                            <div class="layui-inline">
                                <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                            </div>
                        </div>
                    </form>

                    <table id="lay_table_content" lay-filter="lay_table_content"></table>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    layui.use(['laytpl', 'table', 'http', 'jquery', 'FdpUtil', 'util'], function(){

        var laytpl = layui.laytpl,
            table = layui.table,
            layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            util = layui.util;

        //模块名称
        var MODULE = "SYS-NOTICE";

        //方法级渲染
        table.render({
            elem: '#lay_table_content',
            requestFun: function(page, size, renderFun) {

                http.post({
                    url: GLOBAL.path.adminApi + '/sys/notice/pageReceiver',
                    async: false,
                    data: {
                        title: $("input[name='title']").val(),
                        page: {
                            current: page,
                            size: size ? size : GLOBAL.page.size
                        }
                    },
                    backFun: function(result){

                        renderFun({
                            data: result.records,
                            count: result.total
                        });
                    }});
            },
            cols: [[
                {type: 'numbers'},
                {field:'title', title: '标题', templet: function (d) {

                    if(d.receiverState == GLOBAL.constant.Dict.NOTICE_READ_STATE.READ.value){
                        return d.title;
                    }else{
                        return d.title + "<span class='layui-badge-dot' style='margin-left: 2px;' id='unread-" + d.id + "'></span>";
                    }
                }},
                {field:'createTime', title: '发布时间', width:180, templet: function (d) {
                    return util.toDateString(d.releaseTime);
                }},
            ]],
            id: 'lay_table',
            page: true
        });

        table.on('row(lay_table_content)', function(obj){

            obj.update({
                state: GLOBAL.constant.Dict.NOTICE_READ_STATE.READ.value,
                title: obj.data.title
            });

            $("#unread-" + obj.data.id).remove();

            http.get({
                url: GLOBAL.path.adminApi + '/sys/notice/read/' + obj.data.id,
                async: true,
                hideLoading: true,
                backFun: function(result){

                }});

            Win.open({
                url: GLOBAL.path.basePath + "/view/sys/notice/read?id=" + obj.data.id,
                width: '50%',
                height: '60%',
                type: 1,
                offset: 't',
                anim: 0,
                skin: '-1',
                isOutAnim: false
            })
        });

        $('#search_btn').on('click', function(){

            //执行重载
            table.reload('lay_table', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

    });
</script>
</body>
</html>


