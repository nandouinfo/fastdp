<style>

    .operation-btn {
        height: 20px;
        width: 178px;
        text-align: center;
    }

    #msg-form #content{
        font-size: 16px;
    }
</style>
<div style="padding: 20px 0px; overflow-x: hidden;">

    <img id="state_img" src="" style="position: absolute; right: 50px;z-index: 19911128;">

    <form class="layui-form" onsubmit="return false"  id="msg-form" lay-filter="msg-form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs2">
            </div>

            <div class="layui-col-xs8">

                <h2 id="title" style="text-align: center;"></h2>

            </div>

            <div class="layui-col-xs2">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">
                <p id="content"></p>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">
                <p id="receiveTime" style="text-align: right"></p>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="button" class="layui-btn layui-btn-primary" id="cancle_btn" lay-filter="cancle_btn">返回</button>
                <button type="button" class="layui-btn" lay-submit="" id="detail_btn" lay-filter="detail_btn">详情</button>
            </div>
        </div>

    </form>

</div>

<#--<script src="${context.contextPath}/static/config/cmstimp-config.js"></script>-->

<script>

    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'laydate', 'upload', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            laydate = layui.laydate;
            upload = layui.upload,
            util = layui.util;

        var MODULE = "SYS-MSG";

        var id = $("#msg-form").find("input[name='id']").val();

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/sys/msg/get/' + id,
                async: false,
                backFun: function(result){
                    $("#msg-form").find("#title").html(result.title);
                    $("#msg-form").find("#content").html(result.content);
                    $("#msg-form").find("#receiveTime").html(util.toDateString(result.receiveTime));

                    $("#msg-form").find("#detail_btn").click(function () {

                        if(GLOBAL.constant.Dict.MSG_TYPE.CONTRACT_EXPIRE_NOTICE.value == result.type){

                            Win.open({
                                url: GLOBAL.path.basePath + "/view/cmstimp/contractBook/view_index?id=" + result.busId,
                                title: "合同详情",
                                type: 1,
                                width: '1200px'
                            })

                        }else if(GLOBAL.constant.Dict.MSG_TYPE.CONTRACT_BILL_FAILD.value == result.type){

                            Win.open({
                                url: GLOBAL.path.basePath + "/view/cmstimp/contractBook/bill?id=" + result.busId,
                                title: "缴费单",
                                type: 1
                            })

                        }else if(GLOBAL.constant.Dict.MSG_TYPE.REMODEL_APPLY.value == result.type){

                            var procinst = null;
                            http.get({
                                url: GLOBAL.path.adminApi + '/workflow/procinst/get/' + result.busId,
                                async: false,
                                backFun: function (result) {

                                    procinst = result;
                                }
                            });

                            var url = "";

                            if(procinst){
                                url = GLOBAL.path.basePath + "/static/workflow/procdesign/index.html?id=" + result.busId + "#/view";
                            }else{
                                url = GLOBAL.path.basePath + "/static/workflow/procdesign/index.html?undoId=" + result.busId + "#/view";
                            }

                            Win.open({
                                content: url,
                                title: "详情",
                                type: 2,
                                isAppendToken: false,
                                enablePrint: true
                            })

                        }else if(GLOBAL.constant.Dict.MSG_TYPE.CONTRACT_WAIT_PAY.value == result.type){

                            http.get({
                                url: GLOBAL.path.adminApi + '/cmstimp/pay/bill/get/' + result.busId,
                                async: false,
                                backFun: function (bill) {

                                    if(bill.state != GLOBAL.constant.Dict.BILL_STATE.WAIT_PAY){

                                        Win.open({
                                            url: GLOBAL.path.basePath + "/view/cmstimp/pay/contract/view?id=" + result.busId,
                                            title: "缴费单",
                                            type: 1
                                        })

                                    }else{

                                        Win.open({
                                            url: GLOBAL.path.basePath + "/view/cmstimp/pay/contract/pay?id=" + result.busId,
                                            title: "缴费单",
                                            type: 1
                                        })
                                    }
                                }
                            });

                        }else if(GLOBAL.constant.Dict.MSG_TYPE.CONTRACT_BILL_CREATE_SUCCESS.value == result.type){

                            Win.open({
                                url: GLOBAL.path.basePath + "/view/cmstimp/contractBook/bill?id=" + result.busId,
                                title: "缴费单",
                                type: 1
                            })
                        }else if(GLOBAL.constant.Dict.MSG_TYPE.EQUIPMENT_EXCEPTION.value == result.type){

                            Win.open({
                                url: GLOBAL.path.basePath + "/view/cmstimp/equipment/equipment/inspection/view?id=" + result.busId,
                                title: "设备异常",
                                type: 1
                            })
                        }else if(GLOBAL.constant.Dict.MSG_TYPE.REMODEL_INSPECTION.value == result.type){

                            Win.open({
                                url: GLOBAL.path.basePath + "/view/cmstimp/remodel/form/inspection/edit?id=" + result.busId,
                                title: "巡查记录",
                                type: 1
                            })
                        }

                        $("#msg-form").find("#cancle_btn").click();
                    })

                }});

            http.post({
                url: GLOBAL.path.adminApi + '/sys/msg/read?id=' + id,
                async: false,
                hideLoading: true,
                backFun: function(result){
                }});
        }

    });
</script>


