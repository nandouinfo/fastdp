<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>消息列表</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

</head>
<body>
    <div class="content">
        <div class="layui-row layui-col-space15">

            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <input type="hidden" name="state" id="state" value="${RequestParameters.state!}">

                        <table id="lay_table_content" lay-filter="lay_table_content"></table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="${context.contextPath}/static/config/cmstimp-config.js"></script>

    <script>
        layui.use(['laytpl', 'table', 'http', 'jquery', 'eleTree', 'FdpUtil', 'laydate', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                laydate = layui.laydate,
                util = layui.util;

            var MODULE = "SYS-MSG";

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    var param = {
                        state: $("input[name='state']").val(),
                        page: {
                            current: page,
                            size: size ? size : 30
                        }
                    }

                    http.post({
                        url: GLOBAL.path.adminApi + '/sys/msg/page',
                        async: false,
                        data: param,
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {field:'title', title: '标题'},
                    {field:'receiveTime', title: '时间',width:180, templet: function (d) {
                        return util.toDateString(d.receiveTime);
                    }},
                ]],
                id: 'lay_table',
                page: true
            });

            table.on('row(lay_table_content)', function(obj){

                var winOptions = {};

                winOptions.url = '${context.contextPath}/view/sys/msg/view?id=' + obj.data.id;
                winOptions.title = '查看';
                winOptions.type = 1;
                winOptions.content = winOptions.url;

                Win.open(winOptions)

                if(obj.data.state == 0){
                    setTimeout(function () {

                        obj.del();

                    }, 1000);
                }
            });

        });
    </script>
</body>
</html>


