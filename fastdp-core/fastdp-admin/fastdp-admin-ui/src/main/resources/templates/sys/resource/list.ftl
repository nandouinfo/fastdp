<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>资源管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

</head>
<body>
    <div class="content">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md2">
                <div class="layui-card" lay-height="100px">
                    <div class="layui-card-header">资源树</div>
                    <div class="layui-card-body">
                        <input type="text" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search">
                        <div id="resourceTree" class="eleTree" lay-filter="resourceTree" style="margin-top: 15px;"></div>
                    </div>
                </div>
            </div>
            <div class="layui-col-md10">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <form class="layui-form" action="" onsubmit="return false">
                            <input type="hidden" name="parentCode" value="" id="parentCode">
                            <div class="layui-form-item">

                                <div class="layui-inline">
                                    <input type="text" name="name" placeholder="资源名称" autocomplete="off" class="layui-input">
                                </div>

                                <div class="layui-inline">
                                    <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>

                                <div class="layui-inline tool-btn right">
                                    <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add" data-options="{url: '/view/sys/resource/edit?parentCode=', title: '添加'}"><i class="layui-icon">&#xe654;</i></button>
                                    <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del" data-options="{url:'/sys/resource/del', callBack:{module: 'SYS-RESOURCE', cmdName: 'batchDel'}}"><i class="layui-icon">&#xe640;</i></button>
                                </div>
                            </div>
                        </form>

                        <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                        <script type="text/html" id="operation_btn">

                            <a class="text-blue edit-btn" lay-event="edit" data-options="{url: '/view/sys/resource/edit'}">编辑</a> |
                            <a class="text-danger" lay-event="del" data-options="{url: '/sys/resource/del', callBack:{module: 'SYS-RESOURCE', cmdName: 'del'}}">删除</a>

                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        layui.use(['laytpl', 'table', 'http', 'eleTree', 'jquery', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            var MODULE = "SYS-RESOURCE";

            var resourceTree = null;
            http.post({
                url: GLOBAL.path.adminApi + '/sys/resource/list',
                async: true,
                backFun: function(result){

                    if(result){
                        for(var i = 0; i < result.length; i++){
                            if(result[i].type == GLOBAL.constant.Dict.RESOURCE_TYPE.MODULE.value){
                                result[i].label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                            }else if(result[i].type == GLOBAL.constant.Dict.RESOURCE_TYPE.MENU.value){
                                result[i].label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                            }else if(result[i].type == GLOBAL.constant.Dict.RESOURCE_TYPE.ACTION.value){
                                result[i].label = '<i class="layui-icon layui-icon-star"></i>&nbsp;&nbsp;';
                            }

                            if(result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                                result[i].label += result[i].name;
                            }else{
                                result[i].label += '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                            }
                        }
                    }

                    var treeData = [{
                        label: "<i class='layui-icon layui-icon-tree'></i>&nbsp;&nbsp;系统资源",
                        children: DataConvert.listToTreeConvert(result),
                        value: "",
                        data: {
                            name: "系统资源"
                        }
                    }]

                    resourceTree = eleTree.render({
                        elem: '#resourceTree',
                        data: treeData,
                        highlightCurrent: true,
                        showLine: true,
                        showCheckbox: false,
                        defaultExpandedKeys: [''],
                        accordion: true,
                        expandOnClickNode: true,
                        request: {
                            name: 'label',
                            key: 'value'
                        },
                        searchNodeMethod: function(value,data) {
                            if (!value) return true;
                            return data.label.indexOf(value) !== -1;
                        }
                    });

                    // 节点点击事件
                    eleTree.on("nodeClick(resourceTree)",function(d) {
                        console.log(d.data);    // 点击节点对应的数据
                        $("input[name='parentCode']").val(d.data.currentData.value);

                        var options = Utils.parseOptions($(".addBtn").attr("data-options"));
                        var dataUrl = options.url.split("?")[0];
                        options.url = dataUrl + "?parentCode=" + d.data.currentData.value;
                        $(".addBtn").attr("data-options", JSON.stringify(options));

                        $("#search_btn").click();
                    })

                    $(".eleTree-search").on("change",function() {
                        resourceTree.search($(this).val());
                    })
                }});

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/sys/resource/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            parentCode: $("input[name='parentCode']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'name', title: '名称', width:150},
                    {field:'code', title: '编码', width:200},
                    {field:'typeName', title: '类型', width:80, templet: function (d) {
                        return Dict.getItemName(Dict.Type.RESOURCE_TYPE, d.type);
                    }},
                    {field:'sort', title: '排序', width:80},
                    {field:'createTime', title: '创建时间', width:180, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:150},
                    {field:'stateName', title: '状态', width:80, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.STATE.ENABLE.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }
                    }},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', minWidth:125}
                ]],
                id: 'lay_table',
                page: true
            });

            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });

            //添加资源回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "addNode",
                callBack: function (options) {

                    var label = null;

                    if(options.type == GLOBAL.constant.Dict.RESOURCE_TYPE.MODULE.value){
                        label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                    }else if(options.type == GLOBAL.constant.Dict.RESOURCE_TYPE.MENU.value){
                        label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                    }else if(options.type == GLOBAL.constant.Dict.RESOURCE_TYPE.ACTION.value){
                        label = '<i class="layui-icon layui-icon-star"></i>&nbsp;&nbsp;';
                    }

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        label += options.name;
                    }else{
                        label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    resourceTree.append(options.parentCode, {
                        label: label,
                        value: options.code,
                        data: options
                    })
                }
            })

            //修改资源回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "updateNode",
                callBack: function (options) {

                    var label = null;

                    if(options.type == GLOBAL.constant.Dict.RESOURCE_TYPE.MODULE.value){
                        label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                    }else if(options.type == GLOBAL.constant.Dict.RESOURCE_TYPE.MENU.value){
                        label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                    }else if(options.type == GLOBAL.constant.Dict.RESOURCE_TYPE.ACTION.value){
                        label = '<i class="layui-icon layui-icon-star"></i>&nbsp;&nbsp;';
                    }

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        label += options.name;
                    }else{
                        label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    resourceTree.updateKeySelf(options.code, {
                        label: label,
                        value: options.code,
                        data: options
                    })
                }
            })

            //头批量删除回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "batchDel",
                callBack: function (options) {
                    $('#search_btn').click();
                    for(var i = 0; i < options.data.length; i++){
                        resourceTree.remove(options.data[i].code)
                    }
                }
            })

            //行删除回调
            FdpCommand.register({
                module: MODULE,
                cmdName: "del",
                callBack: function (options) {
                    options.rowObj.del();
                    resourceTree.remove(options.data.code)
                }
            })


            //刷新列表
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

        });
    </script>
</body>
</html>


