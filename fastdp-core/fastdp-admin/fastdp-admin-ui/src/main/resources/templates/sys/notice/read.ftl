<style>

    #notice-form .file-container {
        width: 100%;
        height: auto!important;
        border-width: 0px;
        border-style: solid;
        background-color: #fff;
        border-radius: 2px;
        border-color: #e6e6e6;
        overflow: auto;
        margin-left: 10px;
    }

    #notice-form .file-container .file {
        height: 170px;
        width: 170px;
        margin-left: 10px;
        margin-top: 10px;
        margin-bottom: 0px;
        margin-right: 0px;
        border-width: 0px;
        border-style: solid;
        background-color: #fff;
        border-radius: 2px;
        border-color: #e6e6e6;
    }

    #notice-form .operation-btn {
        height: 20px;
        text-align: left;
        display: inline-block;
    }

    #notice-form .file-name{
        text-align: left
    }

</style>
<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="notice-form" lay-filter="notice-form">

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs2">
            </div>

            <div class="layui-col-xs8">

                <h2 id="title" style="text-align: center;font-weight: bold;"></h2>

            </div>

            <div class="layui-col-xs2">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item" style="height: 30px;">

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">
                <p id="content"></p>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item" style="height: 60px;">

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">
                <div style="min-width: 200px;float: right;">
                    <p id="receiveTime" style="text-align: left"><span style="font-size: 14px;font-weight: bold">发布时间：</span></p>
                    <br/>
                    <p id="receiveDeot" style="text-align: left"><span style="font-size: 14px;font-weight: bold">发布部门：</span></p>
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item" id="file_list_panel_title" style="display: none;">
            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">
                附件<i class="layui-icon layui-icon-link"></i>
            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text" id="file_list_panel"
             style="display: none;">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <div class="file-container">

                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

    </form>

</div>

<script>

    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            util = layui.util;

        var MODULE = "SYS-NOTICE";

        var id = "${RequestParameters.id!}";

        var files = [];

        var appendFile = function(fileObj){

            $("#notice-form").find(".file-container").append("<div id='file-" + fileObj.id + "' style='display: block;text-align: left' name='img-div' title='" + fileObj.originalName + "'><div class='file-name' title='" + fileObj.originalName + "'>" + fileObj.originalName + "<div class='operation-btn'>&nbsp;&nbsp;&nbsp;<a name='file-download-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>下载</a></div></div></div>");

            files.push(fileObj.id);

            $("#notice-form").find("a[name='file-download-btn']").unbind().click(function () {

                var id = $(this).attr("file-id");

                top.window.open(Appendix.getDownLoadUrl(id));
            });

            if (files.length > 0) {
                $("#notice-form").find("#file_list_panel").show();
                $("#notice-form").find("#file_list_panel_title").show();
            } else {
                $("#notice-form").find("#file_list_panel").hide();
                $("#notice-form").find("#file_list_panel_title").hide();
            }

        }

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/sys/notice/getReceiver/' + id,
                async: false,
                backFun: function(result){

                    $("#notice-form").find("#title").html(result.title);
                    $("#notice-form").find("#content").html(result.content);
                    $("#notice-form").find("#receiveTime").append(util.toDateString(result.releaseTime));

                    $("#notice-form").find("#receiveDeot").append(result.createDeptName);

                    var fileObjs = Appendix.find(result.id, Appendix.Type.DefaultType);

                    if(fileObjs){
                        for(var i = 0; i < fileObjs.length; i++){
                            appendFile(fileObjs[i]);
                        }
                    }

                }});
        }

    });
</script>


