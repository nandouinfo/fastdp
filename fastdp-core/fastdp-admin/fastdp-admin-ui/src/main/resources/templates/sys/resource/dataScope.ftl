<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="datascope_form" lay-filter="datascope_form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>编码:</label>
                <div class="layui-input-block">
                    <input type="text" id="code" name="code" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">授权脚本</label>
                <div class="layui-input-block">
                    <textarea id="authSql" name="authSql" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="datascope_submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            util = layui.util;

        var MODULE = "SYS-RESOURCE";

        var id = $("#datascope_form").find("input[name='id']").val();

        var data = FdpCommand.execute({
            module: MODULE,
            cmdName: "getDataScope",
            options: id
        });

        form.val("datascope_form", data);

        //监听提交
        form.on('submit(datascope_submit_btn)', function(data) {

            FdpCommand.execute({
                module: MODULE,
                cmdName: "dataScopeAddEvent",
                options: data.field
            })

            $("#datascope_form").find("button[lay-filter='cancle_btn']").click();

            return false;
        });
    });
</script>


