<style>
    #notice-read-form .avatar{
        display: inline-block;
        height: 60px;
        width: 50px;
        text-align: center;
        margin: 5px;
    }

    #notice-read-form .avatar-content{
        display: inline-block;
        height: 40px;
        width: 40px;
        line-height: 40px;
        border-radius: 100%;
        text-align: center;
    }

    #notice-read-form .read{
        background-color: rgb(50, 150, 250);
        color: #FFFFFF;
    }

    #notice-read-form .un_read{
        background-color: #c2c2c2;
        color: #FFFFFF;
    }

    #notice-read-form .avatar-name{

        font-size: 12px;
        color: #c2c2c2;
        margin-top: 5px;
    }
</style>
<div style="padding: 20px 0px; overflow-x: hidden;" id="notice-read-form">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>已读({{reads.length}})</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs2">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <div class="avatar" v-for="(read, i) in reads">
                    <div class="avatar-content read">
                        {{read.receiverUserName.length > 2 ? read.receiverUserName.substring(1, 3) : read.receiverUserName}}
                    </div>
                    <div class="avatar-name">
                        {{read.receiverUserName}}
                    </div>
                </div>

            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>未读({{unReads.length}})</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs2">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <div class="avatar" v-for="(unRead, i) in unReads">
                    <div class="avatar-content un_read">
                        {{unRead.receiverUserName.length > 2 ? unRead.receiverUserName.substring(1, 3) : unRead.receiverUserName}}
                    </div>
                    <div class="avatar-name">
                        {{unRead.receiverUserName}}
                    </div>
                </div>

            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
            </div>
        </div>

    </form>

</div>

<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'upload', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            util = layui.util;

        var MODULE = "SYS-NOTICE";

        new Vue({

            el: "#notice-read-form",

            data(){
                return {
                    noticeId: "${RequestParameters.id!}",
                    reads: [],
                    unReads: []
                }
            },

            created(){
                this.loadReceiverList();
            },

            methods: {

                loadReceiverList(){

                    var that = this;

                    http.post({
                        url: GLOBAL.path.wechatApi + '/sys/notice/recivers?id=' + that.noticeId,
                        async: true,
                        backFun: function(result){

                            for (var i = 0; i < result.length; i++){

                                if(result[i].state == GLOBAL.constant.Dict.NOTICE_READ_STATE.READ.value){
                                    that.reads.push(result[i]);
                                }else{
                                    that.unReads.push(result[i]);
                                }
                            }
                        }});
                }
            }
        })

    });
</script>


