<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>字典管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

</head>
<body>
    <div class="content">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md2">
                <div class="layui-card" lay-height="100px">
                    <div class="layui-card-header">
                        字典分类
                        <a href="javascript:;" title="添加父类型" id="add-type-btn"><i class="layui-icon layui-icon-addition"></i></a>
                    </div>
                    <div class="layui-card-body">
                        <input type="text" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search">
                        <div id="dictTree" class="eleTree ele3" lay-filter="dictTree" style="margin-top: 20px;overflow-y: auto;"></div>
                    </div>
                </div>
            </div>
            <div class="layui-col-md10">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <form class="layui-form" action="" onsubmit="return false">
                            <input type="hidden" name="dictCode" value="" id="dictCode">
                            <div class="layui-form-item">

                                <div class="layui-inline">
                                    <input type="text" name="name" placeholder="项目名称" autocomplete="off" class="layui-input">
                                </div>

                                <div class="layui-inline">
                                    <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>

                                <div class="layui-inline tool-btn right" style="display: none;">
                                    <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add"  data-options="{url: '/view/sys/dict/editItem?dictCode=0', title: '添加'}"><i class="layui-icon">&#xe654;</i></button>
                                    <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del"  data-options="{url: '/sys/dictItem/del', title: '删除'}"><i class="layui-icon">&#xe640;</i></button>
                                </div>
                            </div>
                        </form>

                        <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                        <script type="text/html" id="operation_btn">

                            <a class="text-blue" lay-event="edit" data-options="{url: '/view/sys/dict/editItem', title: '编辑'}"> 编辑 </a> |
                            <a class="text-danger" lay-event="del"  data-options="{url: '/sys/dictItem/del', title: '删除'}"> 删除 </a>

                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        layui.use(['laytpl', 'table', 'http', 'eleTree', 'jquery', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            //模块名称
            var MODULE = "SYS-DICT";

            var dictTree = null;
            $("#dictTree").height($("#dictTree").parent().parent().height() - 120);
            http.post({
                url: GLOBAL.path.adminApi + '/sys/dict/list',
                async: true,
                backFun: function(result){

                    if(result != null){
                        for(var i = 0; i < result.length; i++){
                            if(result[i].type == GLOBAL.constant.Dict.DICT_TYPE.LIST.value){
                                result[i].label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                            }else{
                                result[i].label = '<i class="layui-icon layui-icon-tree"></i>&nbsp;&nbsp;';
                            }

                            if(result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                                result[i].label += result[i].name;
                            }else{
                                result[i].label += '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                            }
                        }
                    }

                    var treeData = DataConvert.listToTreeConvert(result);

                    dictTree = eleTree.render({
                        elem: '#dictTree',
                        data: treeData,
                        highlightCurrent: true,
                        showLine: true,
                        showCheckbox: false,
                        defaultExpandedKeys: [''],
                        contextmenuList: [{eventName: "addChild", text: "添加子分类"},
                                          {eventName: "update", text: "修改"},
                                          {eventName: "del", text: "删除"}],
                        accordion: true,
                        expandOnClickNode: true,
                        request: {
                            name: 'label',
                            key: 'value'
                        },
                        searchNodeMethod: function(value,data) {
                            if (!value) return true;
                            return data.label.indexOf(value) !== -1;
                        }
                    });

                    // 节点点击事件
                    eleTree.on("nodeClick(dictTree)",function(d) {

                        $("input[name='dictCode']").val(d.data.currentData.data.code);

                        var options = Utils.parseOptions($(".addBtn").attr("data-options"));
                        var dataUrl = options.url.split("?")[0];
                        options.url = dataUrl + "?dictCode=" + d.data.currentData.data.code;
                        $(".addBtn").attr("data-options", JSON.stringify(options));

                        $(".tool-btn").show();

                        $("#search_btn").click();
                    })

                    eleTree.on("nodeAddChild(dictTree)",function(d) {

                        Win.open({
                            url: "${context.contextPath}/view/sys/dict/edit?parentCode=" + d.data.value,
                            type: "1",
                            title: "添加分类"
                        })

                        FdpCommand.register({
                            module: MODULE,
                            cmdName: "addDictNode",
                            callBack: function (options) {

                                var label = null;

                                if(options.type == GLOBAL.constant.Dict.DICT_TYPE.LIST.value){
                                    label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                                }else{
                                    label = '<i class="layui-icon layui-icon-tree"></i>&nbsp;&nbsp;';
                                }

                                if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                                    label += options.name;
                                }else{
                                    label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                                }

                                dictTree.append(d.data.value, {
                                    label: label,
                                    value: options.code,
                                    data: options
                                })
                            }
                        })
                    })

                    eleTree.on("nodeUpdate(dictTree)",function(d) {
                        Win.open({
                            url: "${context.contextPath}/view/sys/dict/edit?id=" + d.data.data.id,
                            type: "1",
                            title: "修改分类"
                        })

                        FdpCommand.register({
                            module: MODULE,
                            cmdName: "updateDictNode",
                            callBack: function (options) {

                                var label = null;

                                if(options.type == GLOBAL.constant.Dict.DICT_TYPE.LIST.value){
                                    label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                                }else{
                                    label = '<i class="layui-icon layui-icon-tree"></i>&nbsp;&nbsp;';
                                }

                                if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                                    label += options.name;
                                }else{
                                    label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                                }

                                dictTree.updateKeySelf(d.data.value, {
                                    label: label,
                                    value: options.code,
                                    data: options
                                })
                            }
                        })
                    })

                    eleTree.on("nodeDel(dictTree)",function(d) {

                        top.layer.confirm('删除后不可恢复，请确认!', function (index) {

                            var nodes = DataConvert.treeToListConvert(d.data, {})

                            var ids = [];

                            for (var i = 0; i < nodes.length; i++) {
                                ids.push(nodes[i].data.id);
                            }

                            http.post({
                                url: GLOBAL.path.adminApi + '/sys/dict/del',
                                data: ids,
                                async: false,
                                backFun: function (result) {

                                    dictTree.remove(d.data.value)

                                    layer.close(index);
                                    top.layui.layer.msg("操作成功！");
                                }
                            });
                        });

                    })

                    $(".eleTree-search").on("change",function() {
                        dictTree.search($(this).val());
                    })
                }});

            $("#add-type-btn").click(function () {

                Win.open({
                    url: "${context.contextPath}/view/sys/dict/edit",
                    type: "1",
                    title: "添加分类"
                })
            });

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/sys/dictItem/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            dictCode: $("input[name='dictCode']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'name', title: '名称', width:200},
                    {field:'value', title: '值', width:150},
                    {field:'sort', title: '排序', width:80},
                    {field:'createTime', title: '创建时间', width:180, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:150},
                    {field:'stateName', title: '状态', width:80, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.STATE.ENABLE.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }
                    }},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', minWidth:125}
                ]],
                id: 'lay_table',
                page: true
            });

            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });

            /**
             * 更新字典项列表
             */
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshItemTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

            /**
             * 添加节点
             */
            FdpCommand.register({
                module: MODULE,
                cmdName: "addDictNode",
                callBack: function (options) {

                    var label = null;

                    if(options.type == GLOBAL.constant.Dict.DICT_TYPE.LIST.value){
                        label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                    }else{
                        label = '<i class="layui-icon layui-icon-tree"></i>&nbsp;&nbsp;';
                    }

                    if(options.state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                        label += options.name;
                    }else{
                        label += '<span style="color: #FF5722;text-decoration: line-through;">' + options.name + '</span>';
                    }

                    dictTree.append("", {
                        label: label,
                        value: options.code,
                        data: options
                    })
                }
            })

        });
    </script>
</body>
</html>


