<!DOCTYPE html>
<html>
<head>
    <title>南斗·快速开发平台</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">
    <style>
        .layui-layim-face img {
            vertical-align: bottom;
            height: 22px;
        }

        .layadmin-side-shrink .layui-layout-admin .layui-logo {
            background-image: url('${context.contextPath}/static/img/head_logo.png') !important;
        }

        .layui-nav-tree .layui-nav-bar{
            background-color: #dd2521;
        }
    </style>

    <script>
        /^http(s*):\/\//.test(location.href) || alert('请先部署到 localhost 下再访问');
    </script>
</head>
<body class="layui-layout-body">

<div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layadmin-flexible" lay-unselect>
                    <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                        <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                    </a>
                </li>
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="refresh" lay-text="刷新">
                        <i class="layui-icon layui-icon-refresh"></i>
                    </a>
                </li>

                <li class="layui-nav-item" lay-unselect>
                    <a lay-href="" id="message_center" layadmin-event="message" lay-text="消息中心">
                        <i class="layui-icon layui-icon-notice"></i>

                        <!-- 如果有新消息，则显示小圆点 -->
                        <span id="show-msg-dot" style="display: none;" class="layui-badge-dot"></span>
                    </a>
                </li>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="theme">
                        <i class="layui-icon layui-icon-theme"></i>
                    </a>
                </li>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="fullscreen">
                        <i class="layui-icon layui-icon-screen-full"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;">
                        <cite id="login_user_name"></cite>
                    </a>
                    <dl class="layui-nav-child">
                        <#--                        <dd><a lay-href="/usercenter/info.html">基本资料</a></dd>-->
                        <dd><a id="modifyPwdEle" lay-href="">修改密码</a></dd>
                        <hr>
                        <dd style="text-align: center;" id="logout_btn"><a href="javascript:;">退出</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="about"><i
                                class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
                <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
                    <a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
                </li>


            </ul>
        </div>

        <!-- 侧边菜单 -->
        <div class="layui-side layui-side-menu">
            <div class="layui-side-scroll">
                <div class="layui-logo">
                    <span>快速开发平台</span>
                </div>

                <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu"
                    lay-filter="layadmin-system-side-menu">

                </ul>
            </div>
        </div>

        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li lay-id="welcome.html" lay-attr="welcome.html" class="layui-this"><i
                                class="layui-icon layui-icon-home"></i></li>
                </ul>
            </div>
        </div>


        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body">
            <div class="layadmin-tabsbody-item layui-show">
                <iframe id="home" src="" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>

        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
</div>
<script>
    layui.extend({
        index: 'js/index/index' //主入口模块
    }).use('index');
</script>
<script>
    layui.use(['FdpUtil', 'laytpl', 'form', 'jquery', 'element', 'laydate', 'http', 'jform'], function () {

        var form = layui.form,
            laytpl = layui.laytpl,
            layer = layui.layer,
            element = layui.element,
            $ = layui.jquery,
            http = layui.http;

        Utils.visitRecoder({
            module: "开发平台",
            source: "PC"
        })

        $("#logout_btn").click(function () {
            SessionUtils.loginOut();
            let contextPath = "${context.contextPath}";
            window.location.href = !DataVerify.isBlank(contextPath) ? contextPath : '/';
        })

        if (!SessionUtils.isLogin()) {
            $("#logout_btn").click();
        }

        $("#home").attr("src", "${context.contextPath}/view/welcome" + '?token=' + SessionUtils.getToken())
        $("#message_center").attr("lay-href", "${context.contextPath}/view/sys/msg/index" + '?token=' + SessionUtils.getToken());

        var loginUser = SessionUtils.getLoginUser();

        $("#login_user_name").html(loginUser.name);

        var resourcesList = loginUser.resources;

        var menus = [];
        for (var i = 0; i < resourcesList.length; i++) {

            if (resourcesList[i].type == GLOBAL.constant.Dict.RESOURCE_TYPE.MODULE.value &&
                resourcesList[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value) {

                menus.push('<li data-name="home" class="layui-nav-item">' +
                    '<a href="javascript:;" lay-tips="' + resourcesList[i].name + '" lay-direction="2">' +
                    '<i class="layui-icon ' + resourcesList[i].icon + '"></i>' +
                    '<cite>' + resourcesList[i].name + '</cite>' +
                    '</a>');


                menus.push('<dl class="layui-nav-child">');

                for (var j = 0; j < resourcesList.length; j++) {

                    if (resourcesList[j].parentCode == resourcesList[i].code &&
                        resourcesList[j].type == GLOBAL.constant.Dict.RESOURCE_TYPE.MENU.value &&
                        resourcesList[j].state == GLOBAL.constant.Dict.STATE.ENABLE.value) {

                        if (resourcesList[j].path) {

                            if(resourcesList[j].path.indexOf("http:") < 0){
                                let separator = "?"
                                if (resourcesList[j].path.indexOf("?") >= 0) {
                                    separator = "&";
                                }
                                menus.push('<dd data-name="console">' +
                                    '<a resource_code="' + resourcesList[j].code + '" lay-href="${context.contextPath}' + resourcesList[j].path + separator + 'token=' + SessionUtils.getToken() + '">' + resourcesList[j].name + '</a>' +
                                    '</dd>')
                            }else{
                                menus.push('<dd data-name="console">' +
                                    '<a resource_code="' + resourcesList[j].code + '" href="' + resourcesList[j].path + '" target="_blank">' + resourcesList[j].name + '</a>' +
                                    '</dd>')
                            }

                        }
                    }
                }

                menus.push('</dl>');

                menus.push('</li>');
            }
        }

        $("#LAY-system-side-menu").html(menus.join(""));
        element.render('nav');

        element.on('nav(layadmin-system-side-menu)', function(elem){

            var resourceCode = $(elem).attr("resource_code");

            if(!resourceCode || resourceCode.length <= 0) return;

            CommonFun.add({
                busId: resourceCode,
                type: GLOBAL.constant.Dict.COMMON_FUN_TYPE.RESOURCE.value
            })
        });

        $('#modifyPwdEle').attr('lay-href', '${context.contextPath}/view/org/user/modifyPwd?token=' + SessionUtils.getToken());

        //刷新未读数量
        setInterval(function () {

            http.get({
                url: GLOBAL.path.adminApi + '/sys/msg/unReadCount',
                async: false,
                hideLoading: true,
                backFun: function(result){
                    if(result > 0){
                        $("#show-msg-dot").show();
                    }else{
                        $("#show-msg-dot").hide();
                    }
                }});

        }, 1000 * 60 * 3);


        /*var beforeUseTime = new Date().getTime();
        $(document).click(function () {
            beforeUseTime = new Date().getTime();
        })

        var gameIndex = null;
        setInterval(function () {

            if(new Date().getTime() - beforeUseTime >= 1000 * 60 * 60){

                beforeUseTime = new Date().getTime();
                try{
                    window.open("http://wayou.github.io/t-rex-runner/")
                }catch (e) {

                }
            }

        }, 1000 * 60)*/
    });
</script>
</body>
</html>


