<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>快速开发平台</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" media="screen" href="${context.contextPath}/static/css/login.css">
</head>
<body>
<!-- particles.js container -->
<div id="particles-js" style="display: flex;align-items: center;justify-content: center">
    <canvas class="particles-js-canvas-el" style="width: 100%; height: 100%;" width="472" height="625"></canvas>
</div>

<div class="apTitle">南斗 · 快速开发平台</div>

<div class="logcon" onsubmit="return false" lay-filter="form">
    <input type="text" name="account" placeholder="账号">
    <input type="password" name="password" placeholder="密码">
    <button type="submit" id="login_btn" lay-submit="" lay-filter="login_btn">登录</button>
</div>

<!-- scripts -->
<script src="${context.contextPath}/static/js/login.js"></script>
<script src="${context.contextPath}/static/js/loginApp.js"></script>

<script>
    layui.use(['FdpUtil', 'form', 'jquery', 'element', 'http'], function () {

        var form = layui.form,
            layer = layui.layer,
            element = layui.element,
            $ = layui.jquery,
            http = layui.http;

        //已登录的用户直接跳转至首页
        if(SessionUtils.isLogin()){
            window.location.href = "${context.contextPath}/view/index" + '?token=' + SessionUtils.getToken();
        }

        //监听提交
        form.on('submit(login_btn)', function(data) {

            var account = $("input[name='account']").val();
            var password = $("input[name='password']").val();

            if(!account || account.length <= 0){
                layer.tips("请输入登录账户!", "#login_btn", {tips: [1, '#3595CC']});
                return;
            }

            if(!password || password.length <= 0){
                layer.tips("请输入登录密码!", "#login_btn", {tips: [1, '#3595CC']});
                return;
            }

            http.post({ 'url': GLOBAL.path.adminApi + '/login',
                'data': {
                    account: account,
                    password: password
                },
                'backFun': function(result){

                    SessionUtils.setToken(result)

                    window.location.href = "${context.contextPath}/view/index" + '?token=' + SessionUtils.getToken();
                },
                'failHandle': function (msg) {
                    layer.tips(msg + "!", "#login_btn", {tips: [1, '#3595CC']});
                }})

            return false;
        });

        $(document).keydown(function(){
            if (event.keyCode == 13) {//回车键的键值为13
                $("#login_btn").click();
            }
        });

    });
</script>

</body>
</html>
