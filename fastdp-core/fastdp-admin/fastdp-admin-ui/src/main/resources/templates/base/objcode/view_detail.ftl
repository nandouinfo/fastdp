<div style="padding: 20px 0px; overflow-x: hidden;">

    <div class="layui-row" id="objcode_detail_panel">

        <div class="layui-card">
            <div class="layui-card-body">

                <div class="layui-fluid" style="background-color: #F4F4F4;padding-bottom: 20px;">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-sm4 layui-col-md3">
                            <div class="layui-card">
                                <div class="layui-card-body layuiadmin-card-list">
                                    <p class="layuiadmin-big-font" id="count">--</p>
                                    <p>总数</p>
                                </div>
                            </div>
                        </div>

                        <div class="layui-col-sm4 layui-col-md3">
                            <div class="layui-card">
                                <div class="layui-card-body layuiadmin-card-list">
                                    <p class="layuiadmin-big-font" id="used_total" style="color: #01AAED;">--</p>
                                    <p style="display: inline-block">已使用</p>
                                </div>
                            </div>
                        </div>

                        <div class="layui-col-sm4 layui-col-md3">
                            <div class="layui-card">
                                <div class="layui-card-body layuiadmin-card-list">
                                    <p class="layuiadmin-big-font" id="surplus_total"  style="color: #FF5722;">--</p>
                                    <p>剩余</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br/>

                <form class="layui-form" action="" onsubmit="return false">
                    <div class="layui-form-item">

                        <div class="layui-inline">
                            <input type="text" name="no" placeholder="编号" autocomplete="off" class="layui-input">
                        </div>

                        <div class="layui-inline">
                            <select name="state" id="state" lay-dict="OBJ_CODE_DETAIL_STATE" placeholder="状态">

                            </select>
                        </div>

                        <div class="layui-inline">
                            <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                        </div>

                        <div class="layui-inline tool-btn right">
                            <button class="layui-btn layui-btn-small layui-btn-primary"
                                    title="导出"
                                    lay-event="export"
                                    data-options="{busType:'objcodeDetail', callBack:{module: 'OBJCODE_DETAIL', cmdName: 'searchParam'}}">
                                <i class="layui-icon">&#xe601;</i>
                            </button>
                        </div>
                    </div>
                </form>

                <table class="layui-hide" id="objcode_detail_layui" lay-filter="table"></table>

                <script type="text/html" id="operation_btn">

                    <a class="text-blue view-btn" lay-event="qrcode" data-options="{url: '/base/objcode/singleDowmload/{{d.id}}', width: '300px', height: '{{d.height}}'}">二维码</a>

                </script>
            </div>
        </div>
    </div>

    <div class="layui-row">
        <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
            <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
        </div>
    </div>

    </form>

</div>

<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'laydate', 'table', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            laydate = layui.laydate,
            table = layui.table,
            util = layui.util;

        var MODULE = "OBJCODE_DETAIL";
        var $from = $("#objcode_detail_panel");

        var id = "${RequestParameters.id!}";

        form.render();//渲染表单

        var objCode = null;
        http.get({
            url: GLOBAL.path.adminApi + '/base/objcode/get/' + id,
            async: false,
            backFun: function(result){
                objCode = result;
            }});

        function getSearchParam() {

            let param = {
                no: $from.find("input[name='no']").val(),
                state: $from.find("select[name='state']").val(),
                objCodeId: id
            };

            return param;
        }

        //方法级渲染
        table.render({
            elem: '#objcode_detail_layui',
            requestFun: function(page, size, renderFun) {

                http.post({
                    url: GLOBAL.path.adminApi + '/base/objcode/listDetail',
                    async: false,
                    data: getSearchParam(),
                    backFun: function(result){

                        for(var i = 0 ;i < result.length; i++){

                            if(objCode.hasDesc == GLOBAL.constant.Dict.YES_NO.YES.value){
                                result[i].height = '400px';
                            }else{
                                result[i].height = '300px';
                            }
                        }

                        renderFun({
                            data: result,
                            count: result.length
                        });
                    }});
            },
            cols: [[
                {checkbox: true, fixed: true},
                {type: 'numbers'},
                {field:'no', title: '编号', width:250},
                {field:'stateName', title: '状态', width:100, templet: function (d) {

                    var itemName = Dict.getItemName(Dict.Type.OBJ_CODE_DETAIL_STATE, d.state);

                    if(d.state == GLOBAL.constant.Dict.OBJ_CODE_DETAIL_STATE.WAIT_USE.value){

                        return '<span style="color: #1E9FFF;">' + itemName  + '</span>';

                    }else if (d.state == GLOBAL.constant.Dict.OBJ_CODE_DETAIL_STATE.USED.value){

                        return '<span style="color: #5FB878;">' + itemName  + '</span>';
                    }

                    return itemName;
                }},

                {field:'startVal', title: '激活时间', width:200, templet: function (d) {
                    return util.toDateString(d.createTime);
                }},
                {field:'createUserName', title: '激活人', width:150},
                {fixed: 'right', title:'操作', toolbar: '#operation_btn', width:300}
            ]],
            id: 'objcode_detail_layui',
            page: false
        });

        loadCount();
        $from.find('#search_btn').on('click', function(){

            //执行重载
            table.reload('objcode_detail_layui', {});

            loadCount();
        });

        //查询参数
        FdpCommand.register({
            module: MODULE,
            cmdName: "searchParam",
            callBack: function (options) {
                let result = getSearchParam();
                return result;
            }
        })

        /**
         * 加载数量
         */
        function loadCount(){

            var getDetailCount = function (state){

                var count = 0;

                http.post({
                    url: GLOBAL.path.adminApi + '/base/objcode/detailCount',
                    async: false,
                    data: {objCodeId: id, state: state},
                    backFun: function(result){

                        count = result;
                    }});

                return count;
            }

            var count = getDetailCount("");
            $from.find("#count").html(count);

            var usedCount = getDetailCount(GLOBAL.constant.Dict.OBJ_CODE_DETAIL_STATE.USED.value);
            $from.find("#used_total").html(usedCount);

            $from.find("#surplus_total").html(count - usedCount);
        }
    });
</script>


