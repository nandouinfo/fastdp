<style>
    .file-container {
        width: 100%;
        height: 350px;
        border-width: 0px;
        border-style: solid;
        background-color: #fff;
        border-radius: 2px;
        border-color: #e6e6e6;
        overflow: auto;
    }

    .file-container .file {
        /*height: 300px;*/
        width: 95%;
        margin-left: 10px;
        margin-top: 10px;
        margin-bottom: 5px;
        margin-right: 0px;
        border-width: 0px;
        border-style: solid;
        background-color: #fff;
        border-radius: 2px;
        border-color: #e6e6e6;
    }

    .operation-btn {
        height: 20px;
        width: 95%;
        text-align: center;
    }
</style>
<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>
        <input type="hidden" name="type" value="${RequestParameters.type!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>状态:</label>
                <div class="layui-input-block">
                    <select name="state" id="state" lay-dict="STATE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>素材</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="select-file-btn">
                        <i class="layui-icon">&#xe681;</i>选择文件
                    </button>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text" id="file_list_panel"
             style="display: none;">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <div class="layui-input-block">
                    <div class="file-container">

                    </div>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>

</div>

<script src="${context.contextPath}/static/config/cmstimp-config.js"></script>

<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'upload', 'laydate', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            laydate = layui.laydate,
            upload = layui.upload,
            util = layui.util;

        var MODULE = "CMSTIMP-MATERIAL-${RequestParameters.type!}";

        var id = $("#form").find("input[name='id']").val();

        var file = "";

        var appendFile = function(fileObj){

            console.info(fileObj);

            if (FileType.isImg(fileObj.originalName)) {

                $(".file-container").html("<div id='file-" + fileObj.id + "' style='display: inline-block;' name='img-div' title='" + fileObj.originalName + "'><img class='file' src='" + Appendix.getDownLoadUrl(fileObj.id) + "'/><div class='operation-btn'><a name='file-del-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>删除</a> | <a name='file-download-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>下载</a></div></div>");

            } else if(FileType.isVideo(fileObj.originalName)){

                $(".file-container").html("<div id='file-" + fileObj.id + "' style='display: inline-block;' name='video-div' title='" + fileObj.originalName + "'><video class='file' src='" + Appendix.getDownLoadUrl(fileObj.id) + "' controls='controls'/><div class='operation-btn'><a name='file-del-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>删除</a> | <a name='file-download-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>下载</a></div></div>");

            }else {

                $(".file-container").html("<div id='file-" + fileObj.id + "' style='display: inline-block;' name='pdf-div' title='" + fileObj.originalName + "'><img file-id='" + fileObj.id + "' isPdf='" + (fileObj.originalName.indexOf('.pdf') >= 0) + "' class='file' src='" + (fileObj.originalName.indexOf('.pdf') > 0 ? '${context.contextPath}/static/img/pdf-default.png' : '${context.contextPath}/static/img/file-default.png') + "'/><div class='operation-btn'><a name='file-del-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>删除</a> | <a name='file-download-btn' file-id='" + fileObj.id + "' class='text-blue' href='javascript:;'>下载</a></div></div>");
            }

            file = fileObj.id;

            $("a[name='file-del-btn']").unbind().click(function () {

                var id = $(this).attr("file-id");

                file = "";

                $("#file-" + id).remove();

                if (file.length > 0) {
                    $("#file_list_panel").show();
                } else {
                    $("#file_list_panel").hide();
                }
            });

            $("a[name='file-download-btn']").unbind().click(function () {

                var id = $(this).attr("file-id");

                top.window.open(Appendix.getDownLoadUrl(id));
            });

            $("div[name='pdf-div'] img").unbind().click(function () {

                var id = $(this).attr("file-id");
                var isPdf = $(this).attr("isPdf");

                if (isPdf == "true") {

                    Appendix.pdfPreview(id);

                } else {

                    layer.msg("不支持在线浏览!");
                }
            })

            if (file.length > 0) {
                $("#file_list_panel").show();
            } else {
                $("#file_list_panel").hide();
            }
        }

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/base/material/get/' + id,
                async: false,
                backFun: function(result){
                    form.val("form", result);
                }});

            var fileObjs = Appendix.find(id, Appendix.Type.DefaultType);

            if(fileObjs){
                for(var i = 0; i < fileObjs.length; i++){
                    appendFile(fileObjs[i]);
                }
            }
        }

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            if(file.length <= 0){
                top.layui.layer.msg("请上传素材文件！");
                return;
            }

            params = data.field;
            params.file = file;

            http.post({'url': GLOBAL.path.adminApi + '/base/material/' + (id ? 'modify': 'add'),
                'data': params,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    FdpCommand.execute({
                        module: MODULE,
                        cmdName: "refreshTable",
                        options: result
                    })

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });

        var uploadFile = upload.render({
            elem: '#select-file-btn'
            , url: Appendix.getUploadUrl()
            , accept: "${RequestParameters.type!}" == GLOBAL.constant.Dict.MATERIAL_TYPE.IMG.value ? "image" : "video"
            , before: function (obj) {
            }
            , done: function (res) {

                if (res.code == GLOBAL.returnCode.OK) {

                    appendFile(res.data);

                } else {
                    return layer.msg('上传失败');
                }
            }
            , error: function () {
                layer.msg("上传失败");
            }
        });
    });
</script>


