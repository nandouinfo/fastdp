<div style="overflow-x: hidden;">
    <div class="layui-tab layui-tab-brief" style="margin-top: 0px;"  lay-filter="layui-tab-view">
        <ul class="layui-tab-title">

        </ul>
        <div class="layui-tab-content">

        </div>
    </div>
</div>

<script>
    layui.use(['element', 'jquery', 'FdpUtil', 'http'], function(){

        var $ = layui.jquery,
            element = layui.element,
            http = layui.http;

        var tabMap = {};

        var defaultTab = addTab("基本信息", "${context.contextPath}/view/base/objcode/view?id=${RequestParameters.id!}");
        element.tabChange('layui-tab-view', defaultTab);
        addTab("明细详情", "${context.contextPath}/view/base/objcode/view_detail?id=${RequestParameters.id!}");

        function addTab(title, url){

            var id = UUID();

            if(url.split("?").length > 1){
                url = url + "&t_=" + new Date().getTime() + "&token=" + SessionUtils.getToken()
            }else{
                url = url + "?t_=" + new Date().getTime() + "&token=" + SessionUtils.getToken()
            }
            url += "&token=" + SessionUtils.getToken();

            element.tabAdd('layui-tab-view', {
                title: title
                ,content: http.getTpl({
                    url: url
                }) //支持传入html
                ,id: id
            });

            return id;
        }

    })
</script>

