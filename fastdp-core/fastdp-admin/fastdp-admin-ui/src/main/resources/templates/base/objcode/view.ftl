<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="obj-view-form" lay-filter="obj-view-form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>
        <input type="hidden" name="type" value="2"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>主要信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>类型:</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-dict="OBJ_CODE_TYPE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>编码前缀:</label>
                <div class="layui-input-block">
                    <input type="text" id="prefix" name="prefix" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>起始值:</label>
                <div class="layui-input-block">
                    <input type="text" id="startVal" name="startVal" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>总数:</label>
                <div class="layui-input-block">
                    <input type="text" id="total" name="total" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>是否包描述信息:</label>
                <div class="layui-input-block">
                    <select name="hasDesc" id="hasDesc" lay-dict="YES_NO" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs5" id="descBackColor_panel">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>描述信息背景色:</label>
                <div class="layui-input-block">
                    <input type="text" id="descBackColor" name="descBackColor" style="width: 338px;display: inline-block;" class="layui-input">
                    <input style="width: 36px;height: 38px;display: inline-block;" class="layui-input" id="descBackColor_input"></input>
                </div>
            </div>

            <div class="layui-col-xs5">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>状态:</label>
                <div class="layui-input-block">
                    <select name="state" id="state" lay-dict="OBJ_CODE_CREATE_STATE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs10">

                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                    <legend>维护人员信息</legend>
                </fieldset>

            </div>

            <div class="layui-col-xs1">
            </div>
        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">创建时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="createTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">创建人:</label>
                <div class="layui-input-block">
                    <input type="text" name="createUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="createDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">修改时间:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyTime" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">修改人:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyUserName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs3">
                <label class="layui-form-label">所属组织:</label>
                <div class="layui-input-block">
                    <input type="text" name="modifyDeptName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs1">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
            </div>
        </div>

    </form>

</div>



<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'laydate', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            laydate = layui.laydate;
            util = layui.util;

        var MODULE = "OBJCODE_CREATE_MANG";
        var $form = $("#obj-view-form");

        var id = "${RequestParameters.id!}";

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/base/objcode/get/' + id,
                async: false,
                backFun: function(result){

                    result.createTime = util.toDateString(result.createTime);
                    result.modifyTime = util.toDateString(result.modifyTime);

                    form.val("obj-view-form", result);

                    if(GLOBAL.constant.Dict.YES_NO.YES.value == result.hasDesc){
                        $form.find("#descBackColor_panel").show();
                        $form.find("#descBackColor_input").css({"background-color": result.descBackColor});
                    }else{
                        $form.find("#descBackColor_input").hide();
                    }

                }});
        }

        form.disabled("obj-view-form");//禁止编辑表单
    });
</script>


