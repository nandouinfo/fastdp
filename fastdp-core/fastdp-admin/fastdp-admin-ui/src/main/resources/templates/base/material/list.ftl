<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>素材管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

    <style>
        .layui-table-body .layui-table-cell{
            height:135px;
            line-height: 135px;
        }
    </style>
</head>
<body>
    <div class="content">
        <div class="layui-row layui-col-space15">

            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <div class="layui-card">
                            <div class="layui-card-body">

                                <form class="layui-form" action="" onsubmit="return false">
                                    <input type="hidden" name="type" id="type" value="${RequestParameters.type!}">
                                    <div class="layui-form-item">

                                        <div class="layui-inline">
                                            <input type="text" name="name" placeholder="素材名称" autocomplete="off" class="layui-input">
                                        </div>

                                        <div class="layui-inline">
                                            <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                        </div>

                                        <div class="layui-inline tool-btn right">
                                            <@shiro.hasPermission name="material:add">
                                            <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add" data-options="{url: '/view/base/material/edit?type=${RequestParameters.type!}', title: '添加'}"><i class="layui-icon">&#xe654;</i></button>
                                            </@shiro.hasPermission>
                                            <@shiro.hasPermission name="material:del">
                                            <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del" data-options="{url:'/base/material/del'}"><i class="layui-icon">&#xe640;</i></button>
                                            </@shiro.hasPermission>
                                        </div>
                                    </div>
                                </form>

                                <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                                <script type="text/html" id="file_cell">

                                    {{# if(d.type == GLOBAL.constant.Dict.MATERIAL_TYPE.IMG.value) { }}
                                        <img src="{{ Appendix.getPathByBusIdAndType(d.id) }}" style="max-height: 140px; max-width: 280px;"></img>
                                    {{# } }}

                                    {{# if(d.type == GLOBAL.constant.Dict.MATERIAL_TYPE.VIDEO.value) { }}
                                        <video src="{{ Appendix.getPathByBusIdAndType(d.id) }}" controls="controls" style="max-height: 140px; max-width: 280px;"></video>
                                    {{# } }}

                                </script>

                                <script type="text/html" id="operation_btn">

                                    <@shiro.hasPermission name="material:modify">
                                    <a class="text-blue edit-btn" lay-event="edit" data-options="{url: '/view/base/material/edit?type={{d.type}}'}">编辑</a> |
                                    </@shiro.hasPermission>
                                    <@shiro.hasPermission name="material:del">
                                    <a class="text-danger" lay-event="del" data-options="{url: '/base/material/del'}">删除</a>
                                    </@shiro.hasPermission>

                                </script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="${context.contextPath}/static/config/cmstimp-config.js"></script>

    <script>
        layui.use(['laytpl', 'table', 'http', 'jquery', 'eleTree', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            var MODULE = "CMSTIMP-MATERIAL-${RequestParameters.type!}";

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/base/material/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            type: $("input[name='type']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'file', align: 'center', title: '素材', width:320, templet: '#file_cell'},
                    {field:'name', title: '名称', width:250},
                    {field:'stateName', title: '状态', width:80, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.STATE.ENABLE.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }
                    }},
                    {field:'createTime', title: '创建时间', width:180, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:150},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', width:300}
                ]],
                id: 'lay_table',
                limit: 5,
                page: true
            });

            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });

            //刷新列表
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

        });
    </script>
</body>
</html>


