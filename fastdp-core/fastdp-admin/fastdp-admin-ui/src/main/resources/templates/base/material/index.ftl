<!DOCTYPE html>
<html style="overflow-y: hidden;">
<head>
    <meta charset="utf-8">
    <title>素材管理</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <style>
        .layui-tab-title{
            background: white;
        }
    </style>
</head>
<body>
<div>
    <div class="layui-tab layui-tab-brief" style="margin-top: 0px;"  lay-filter="layui-tab">
        <ul class="layui-tab-title">

        </ul>
        <div class="layui-tab-content">

        </div>
    </div>
</div>

<script>
    layui.use(['element', 'jquery', 'FdpUtil'], function(){

        var $ = layui.jquery,
            element = layui.element;

        var tabMap = {};

        var savedTab = addTab("视频", "${context.contextPath}/view/base/material/list?type=2", false);
        element.tabChange('layui-tab', savedTab);
        addTab("图片", "${context.contextPath}/view/base/material/list?type=1", true);

        function addTab(title, url, lazy){

            var id = UUID();

            if(url.split("?").length > 1){
                url = url + "&t_=" + new Date().getTime() + "&token=" + SessionUtils.getToken()
            }else{
                url = url + "?t_=" + new Date().getTime() + "&token=" + SessionUtils.getToken()
            }
            url += "&token=" + SessionUtils.getToken();
            tabMap[id] = {
                id: id,
                title: title,
                url: url,
                loaded: false
            }

            var content = '';

            if(!lazy){
                content = '<iframe src="' + url + '" width="' + $(".layui-tab-content").width() + '" height="' + ($(window).height() - 100) + '" frameBorder="0" scrolling="auto" frameborder="0"  allowtransparency="true"></iframe>';
                tabMap[id].loaded = true;
            }

            element.tabAdd('layui-tab', {
                title: title
                ,content: content //支持传入html
                ,id: id
            });

            return id;
        }

        element.on('tab(layui-tab)', function(data){

            var id = $(".layui-this").attr("lay-id");

            if(!tabMap[id].loaded){

                tabMap[id].loaded = true;

                $(".layui-show").html('<iframe src="' + tabMap[id].url + '" width="' + $(".layui-tab-content").width() + '" height="' + ($(window).height() - 60) + '" frameBorder="0" scrolling="auto" frameborder="0"  allowtransparency="true"></iframe>')
            }

        });

    })
</script>

</body>
</html>


