<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>一物一码</title>
    <#include "*/lib/head.ftl" />
    <link rel="stylesheet" href="${context.contextPath}/static/layui/css/admin.css" media="all">
    <link rel="stylesheet" type="text/css" href="${context.contextPath}/static/layui/plugins/eleTree/eleTree.css" media="all">

    <style>

    </style>
</head>
<body>
    <div class="content" id="objcode-list">
        <div class="layui-row layui-col-space15">

            <div class="layui-col-md12">

                <div class="layui-card">
                    <div class="layui-card-body">

                        <div class="layui-fluid" style="background-color: #F4F4F4;padding-bottom: 20px;">
                            <div class="layui-row layui-col-space15">
                                <div class="layui-col-sm4 layui-col-md3">
                                    <div class="layui-card">
                                        <div class="layui-card-body layuiadmin-card-list">
                                            <p class="layuiadmin-big-font" id="count">--</p>
                                            <p>生码次数</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-col-sm4 layui-col-md3">
                                    <div class="layui-card">
                                        <div class="layui-card-body layuiadmin-card-list">
                                            <p class="layuiadmin-big-font" id="code_total" style="color: #01AAED;">--</p>
                                            <p style="display: inline-block">合计生码量</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-col-sm4 layui-col-md3">
                                    <div class="layui-card">
                                        <div class="layui-card-body layuiadmin-card-list">
                                            <p class="layuiadmin-big-font" id="used_total"  style="color: #5FB878;">--</p>
                                            <p>已使用</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <br/>

                        <form class="layui-form" action="" onsubmit="return false">
                            <div class="layui-form-item">

                                <div class="layui-inline">
                                    <input type="text" name="name" placeholder="名称" autocomplete="off" class="layui-input">
                                </div>

                                <div class="layui-inline">
                                    <select name="state" id="state" lay-dict="OBJ_CODE_CREATE_STATE" placeholder="状态">

                                    </select>
                                </div>

                                <div class="layui-inline">
                                    <select name="type" id="type" lay-dict="OBJ_CODE_TYPE" placeholder="类型">

                                    </select>
                                </div>

                                <div class="layui-inline">
                                    <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>

                                <div class="layui-inline tool-btn right">
                                    <@shiro.hasPermission name="objcode:create:add">
                                    <button class="layui-btn layui-btn-small layui-btn-normal addBtn" lay-event="add" data-options="{url: '/view/base/objcode/edit', title: '创建'}" title="创建二维码"><i class="layui-icon">&#xe654;</i></button>
                                    </@shiro.hasPermission>
                                    <@shiro.hasPermission name="objcode:create:del">
                                    <button class="layui-btn layui-btn-small layui-btn-danger delBtn hidden-xs" lay-event="del" data-options="{url:'/base/objcode/del'}"><i class="layui-icon">&#xe640;</i></button>
                                    </@shiro.hasPermission>
                                </div>
                            </div>
                        </form>

                        <table class="layui-hide" id="lay_table_content" lay-filter="table"></table>

                        <script type="text/html" id="operation_btn">

                            <a class="text-blue view-btn" lay-event="view" data-options="{url: '/view/base/objcode/view_index', width: '1200px'}">查看</a>
                            <@shiro.hasPermission name="objcode:create:del">
                            <a class="text-danger" lay-event="del" data-options="{url: '/base/objcode/del'}"> | 删除</a>
                            </@shiro.hasPermission>

                            <@shiro.hasPermission name="objcode:create:export">
                                <a class="text-blue" lay-event="download" data-options="{url: '/base/objcode/dowmload/{{d.id}}'}"> | 导出</a>
                            </@shiro.hasPermission>

                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="${context.contextPath}/static/config/cmstimp-config.js"></script>

    <script>
        layui.use(['laytpl', 'table', 'http', 'jquery', 'eleTree', 'FdpUtil', 'util'], function(){

            var laytpl = layui.laytpl,
                table = layui.table,
                layer = layui.layer,
                $ = layui.jquery,
                http = layui.http,
                eleTree = layui.eleTree,
                util = layui.util;

            var MODULE = "OBJCODE_CREATE_MANG";

            //方法级渲染
            table.render({
                elem: '#lay_table_content',
                requestFun: function(page, size, renderFun) {

                    http.post({
                        url: GLOBAL.path.adminApi + '/base/objcode/page',
                        async: false,
                        data: {
                            name: $("input[name='name']").val(),
                            state: $("select[name='state']").val(),
                            type: $("select[name='type']").val(),
                            page: {
                                current: page,
                                size: size ? size : GLOBAL.page.size
                            }
                        },
                        backFun: function(result){

                            renderFun({
                                data: result.records,
                                count: result.total
                            });
                        }});
                },
                cols: [[
                    {checkbox: true, fixed: true},
                    {type: 'numbers'},
                    {field:'name', title: '名称', width:250},
                    {field:'type', title: '类型', width:100, templet: function (d) {

                        return Dict.getItemName(Dict.Type.OBJ_CODE_TYPE, d.type);
                    }},
                    {field:'prefix', title: '前缀', width:150},
                    {field:'startVal', title: '起始值', width:150},
                    {field:'total', title: '总数', width:150},
                    {field:'usedCount', title: '已使用', width:150},
                    {field:'stateName', title: '状态', width:100, templet: function (d) {

                        var itemName = Dict.getItemName(Dict.Type.OBJ_CODE_CREATE_STATE, d.state);

                        if(d.state == GLOBAL.constant.Dict.OBJ_CODE_CREATE_STATE.CREATED.value){

                            return '<span style="color: #5FB878;">' + itemName  + '</span>';

                        }else if(d.state == GLOBAL.constant.Dict.OBJ_CODE_CREATE_STATE.CREATEING.value){

                            return '<span style="color: #1E9FFF;">' + itemName  + '</span>';

                        }else{

                            return '<span style="color:#FF5722;">' + itemName  + '</span>';
                        }
                    }},
                    {field:'createTime', title: '创建时间', width:180, templet: function (d) {
                        return util.toDateString(d.createTime);
                    }},
                    {field:'createUserName', title: '创建人', width:150},
                    {fixed: 'right', title:'操作', toolbar: '#operation_btn', width:300}
                ]],
                id: 'lay_table',
                limit: 5,
                page: true
            });

            loadCount();
            $('#search_btn').on('click', function(){

                //执行重载
                table.reload('lay_table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });

                loadCount();
            });

            //刷新列表
            FdpCommand.register({
                module: MODULE,
                cmdName: "refreshTable",
                callBack: function (options) {
                    $('#search_btn').click();
                }
            })

            /**
             * 加载数量
             */
            function loadCount(){

                var getCount = function (){

                    var count = 0;

                    http.post({
                        url: GLOBAL.path.adminApi + '/base/objcode/count',
                        async: false,
                        data: {},
                        backFun: function(result){

                            count = Utils.toThousandsNum(result);
                        }});

                    return count;
                }

                var getCodeCount = function (){

                    var count = 0;

                    http.post({
                        url: GLOBAL.path.adminApi + '/base/objcode/codeCount',
                        async: false,
                        data: {},
                        backFun: function(result){
                            count = result;
                        }});

                    return count;
                }

                var getCodeUsedCount = function (){

                    var count = 0;

                    http.post({
                        url: GLOBAL.path.adminApi + '/base/objcode/codeUsedCount',
                        async: false,
                        data: {},
                        backFun: function(result){
                            count = result;
                        }});

                    return count;
                }


                $("#objcode-list").find("#count").html(getCount());

                $("#objcode-list").find("#code_total").html(getCodeCount());

                $("#objcode-list").find("#used_total").html(getCodeUsedCount());
            }


        });
    </script>
</body>
</html>


