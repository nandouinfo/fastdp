<div style="padding: 20px 0px; overflow-x: hidden;">

    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">

        <input type="hidden" name="id" value="${RequestParameters.id!}"/>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>名称:</label>
                <div class="layui-input-block">
                    <input type="text" id="name" name="name" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>类型:</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-dict="OBJ_CODE_TYPE" required="" lay-verify="required">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>编码前缀:</label>
                <div class="layui-input-block">
                    <input type="text" id="prefix" name="prefix" required="" lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>起始值:</label>
                <div class="layui-input-block">
                    <input type="text" id="startVal" name="startVal" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item" style="text-align: center;">

            <div class="layui-col-xs11" style="color: #FF5722;">
                *示例编码: <span id="demo_no"></span>
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>总数:</label>
                <div class="layui-input-block">
                    <input type="text" id="total" name="total" required="" lay-verify="required|number" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item" style="text-align: center;">

            <div class="layui-col-xs11" style="color: #FF5722;">
                *每次最少生成1个, 最多生成<span id="max_count"></span>各
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>是否包描述信息:</label>
                <div class="layui-input-block">
                    <select name="hasDesc" id="hasDesc" lay-dict="YES_NO" required="" lay-verify="required" lay-filter="hasDesc">

                    </select>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item" id="descBackColor_panel">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label"><i class="require">* &nbsp;</i>描述信息背景色:</label>
                <div class="layui-input-block">
                    <div class="layui-input-inline" style="width: 120px;">
                        <input type="text" name="descBackColor" value="" placeholder="请选择颜色" class="layui-input" id="descBackColor">
                    </div>
                    <div class="layui-inline" style="left: -11px;">
                        <div id="descBackColor-sel"></div>
                    </div>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row layui-col-space15 layui-form-item layui-form-text">

            <div class="layui-col-xs1">
            </div>

            <div class="layui-col-xs9">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea id="remark" name="remark" autocomplete="off" class="layui-textarea" style="height: 200px;"></textarea>
                </div>
            </div>

            <div class="layui-col-xs2">
            </div>

        </div>

        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submit_btn">保存</button>
                <button type="button" class="layui-btn layui-btn-primary" id="view_btn">预览</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>

            </div>
        </div>

    </form>

</div>
<#--
<script src="${context.contextPath}/static/config/cmstimp-config.js"></script>-->

<script>
    layui.use(['form', 'http', 'jquery', 'FdpUtil', 'upload', 'laydate', 'util', 'colorpicker'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            http = layui.http,
            laydate = layui.laydate,
            upload = layui.upload,
            util = layui.util,
            colorpicker = layui.colorpicker;

        var MODULE = "OBJCODE_CREATE_MANG";
        var $form = $("#form");

        var id = "${RequestParameters.id!}";

        form.render();//渲染表单

        if(id.length > 0){
            http.get({
                url: GLOBAL.path.adminApi + '/base/objcode/get/' + id,
                async: false,
                backFun: function(result){
                    form.val("form", result);
                }});
        }else{
            form.val("form", {
                size: Dict.getItemValue(Dict.Type.OBJ_CODE_CONFIG, "DEFAULT_SIZE"),
                startVal: DateUtil.format(new Date(), "yyyyMMdd" + "001"),
                total: 100,
                descBackColor: Dict.getItemValue(Dict.Type.OBJ_CODE_CONFIG, "DESC_BACK_COLOR")
            });
        }

        colorpicker.render({
             elem: '#descBackColor-sel'
            ,color: Dict.getItemValue(Dict.Type.OBJ_CODE_CONFIG, "DESC_BACK_COLOR")
            ,predefine: true // 开启预定义颜色
            ,colors: [Dict.getItemValue(Dict.Type.OBJ_CODE_CONFIG, "DESC_BACK_COLOR"),'#5FB878','#FFB800','#FF5722','#1E9FFF','#2F4056'] //自定义预定义颜色项
            ,done: function(color){
                $form.find('#descBackColor').val(color);
            }
        });

        form.on('select(hasDesc)', function(data){

            if(data.value == GLOBAL.constant.Dict.YES_NO.YES.value){
                $form.find("#descBackColor_panel").show();
            }else{
                $form.find("#descBackColor_panel").hide();
            }
        })

        $form.find("#name").unbind().on("input",function(e){

            var name = $("#name").val();

            if(!name || name.length <= 0){
                $("#prefix").val("");
            }else if(name.length > 1){
                $("#prefix").val(HanyuPinyinHelper.getFirstLetters(name));
            }
        });

        function demoNo(){
            var prefix = $form.find("#prefix").val();
            var startVal = $form.find("#startVal").val();

            $form.find("#demo_no").html(prefix + startVal);
        }

        demoNo();
        $form.find("#name, #prefix, #startVal").on("input",function(e){
            demoNo();
        });

        $form.find("#max_count").html(Dict.getItemValue(Dict.Type.OBJ_CODE_CONFIG, "MAX_TOTAL"))

        //监听提交
        form.on('submit(submit_btn)', function(data) {

            http.post({'url': GLOBAL.path.adminApi + '/base/objcode/' + (id ? 'modify': 'add'),
                'data': data.field,
                'backFun': function(result){

                    top.layui.layer.msg("操作成功！");

                    FdpCommand.execute({
                        module: MODULE,
                        cmdName: "refreshTable",
                        options: result
                    })

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                }})

            return false;
        });

        $("#view_btn").click(function(){

            var params = {
                name: $form.find('#name').val(),
                prefix: $form.find('#prefix').val(),
                startVal: $form.find('#startVal').val(),
                hasDesc: $form.find('#hasDesc').val(),
                descBackColor: $form.find('#descBackColor').val()
            };

            if(!params.name){
                params.name = "测试预览"
            }

            if(!params.prefix){
                params.prefix = "CSYL"
            }

            if(!params.startVal){
                params.startVal = DateUtil.format(new Date(), "yyyyMMdd" + "001")
            }

            console.info(JSON.stringify(params));

            Appendix.qrcode({
                hideDownLoad: true,
                url: '/base/objcode/preView?t=' + new Date().getTime() + '&params=' + encodeURI(JSON.stringify(params).replace("#", "")),
                width: '300px',
                height: (params.hasDesc == GLOBAL.constant.Dict.YES_NO.YES.value ? '400px' : '300px')
            })

        })
    });
</script>


