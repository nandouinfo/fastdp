<style>
    #material-sel .layui-table-body .layui-table-cell{
        height:115px;
        line-height: 115px;
    }
</style>
<div style="padding: 20px;" id="material-sel">
    <form id="material-sel-form" class="layui-form" action="" onsubmit="return false">

        <div class="layui-form-item layui-row">
            <div class="layui-col-md1">&nbsp;</div>
            <div class="layui-upload-drag layui-col-md10" id="fileSelect">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <div class="layui-col-md1">&nbsp;</div>
        </div>

    </form>

    <div class="layui-row layui-col-space15 layui-form-item">

        <div class="layui-col-xs1">
        </div>

        <div class="layui-col-xs10">

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>素材库</legend>
            </fieldset>

        </div>

        <div class="layui-col-xs1">
        </div>
    </div>

    <div class="layui-row layui-col-space15 layui-form-item" id="material-sel">

        <div class="layui-col-xs1">
        </div>

        <div class="layui-col-xs10">

            <form id="material-sel-search-form" class="layui-form" action="" onsubmit="return false">
                <input type="hidden" name="type" id="type" value="${RequestParameters.materialType!}">

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <input type="text" name="name" placeholder="素材名称" autocomplete="off" class="layui-input">
                    </div>

                    <div class="layui-inline">
                        <button id="search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                    </div>
                </div>
            </form>

            <table class="layui-hide" id="material-sel_lay_table_content" lay-filter="table"></table>

            <script type="text/html" id="material-sel_file_cell">

                {{# if(d.type == GLOBAL.constant.Dict.MATERIAL_TYPE.IMG.value) { }}
                <img src="{{ Appendix.getPathByBusIdAndType(d.id) }}" style="max-height: 100px; max-width: 200px;"></img>
                {{# } }}

                {{# if(d.type == GLOBAL.constant.Dict.MATERIAL_TYPE.VIDEO.value) { }}
                <video src="{{ Appendix.getPathByBusIdAndType(d.id) }}" controls="controls" style="max-height: 100px; max-width: 200px;"></video>
                {{# } }}

            </script>
        <div>
        <div class="layui-col-xs1">
        </div>
    </div>

    <div class="layui-row">
        <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
            <button id="confirm_btn" class="layui-btn">确定</button>
            <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
        </div>
    </div>
</div>
<script>
    layui.use(['http', 'table', 'jquery', 'FdpUtil', 'util', 'form', 'upload'], function () {

        var layer = layui.layer,
            table = layui.table,
            $ = layui.jquery,
            http = layui.http,
            util = layui.util,
            form = layui.form,
            upload = layui.upload;

        var MODULE = "${RequestParameters.module!}";

        let busType = '${RequestParameters.busType}';
        let materialType = '${RequestParameters.materialType}';

        let fileUpload = http.upload({
            elem: '#fileSelect'
            , url: GLOBAL.path.adminApi + '/file/upload'
            , auto: true
            , accept: materialType == GLOBAL.constant.Dict.MATERIAL_TYPE.IMG.value ? 'image' : "video"
            , data: {}
            , before: function (obj) {
                layer.load(); //上传loading
            }
            , done: function (res, index, upload) {

                layer.closeAll('loading'); //关闭loading

                if (res.code && res.code == 200) {

                    top.layui.layer.msg("上传成功！");

                    //保存文件到素材中
                    http.post({'url': GLOBAL.path.adminApi + '/base/material/add',
                        'data': {
                            name: res.data.originalName,
                            type: materialType,
                            file: res.data.id,
                            state: GLOBAL.constant.Dict.STATE.ENABLE.value
                        },
                        'async': false,
                        'backFun': function(result){
                        }})

                    FdpCommand.execute({
                        module: MODULE,
                        cmdName: materialType == GLOBAL.constant.Dict.MATERIAL_TYPE.IMG.value ? "uploadImg" : "uploadVideo",
                        options: [Appendix.getDownLoadUrl(res.data.id)]
                    })

                    setTimeout(function () {
                        $("#material-sel").find("button[lay-filter='cancle_btn']").click();
                    }, 500);

                } else {
                    top.layui.layer.msg(res.msg);
                }

                return false;
            }
            , error: function (index, upload) {
                layer.closeAll('loading'); //关闭loading
            }
        });

        //方法级渲染
        table.render({
            elem: '#material-sel_lay_table_content',
            requestFun: function(page, size, renderFun) {

                http.post({
                    url: GLOBAL.path.adminApi + '/base/material/page',
                    async: false,
                    data: {
                        name:  $('#material-sel-search-form').find("input[name='name']").val(),
                        type:  $('#material-sel-search-form').find("input[name='type']").val(),
                        page: {
                            current: page,
                            size: size ? size : GLOBAL.page.size
                        }
                    },
                    backFun: function(result){

                        renderFun({
                            data: result.records,
                            count: result.total
                        });
                    }});
            },
            cols: [[
                {checkbox: true, fixed: true},
                {type: 'numbers'},
                {field:'file', align: 'center', title: '素材', templet: '#material-sel_file_cell', width: '270'},
                {field:'name', title: '名称', width: '270'}
            ]],
            id: 'material-sel_lay_table',
            limit: 5,
            page: true
        });

        $('#material-sel-search-form').find('#search_btn').on('click', function(){

            //执行重载
            table.reload('material-sel_lay_table', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        }).click();

        $('#material-sel').find("#confirm_btn").click(function () {

            var checked = table.checkStatus('material-sel_lay_table');

            if(checked.data.length <= 0){

                layer.msg("请上传或选择素材!");

                return;
            }

            var result = [];
            for(var i = 0; i < checked.data.length; i++){
                result.push(Appendix.getPathByBusIdAndType(checked.data[i].id))
            }

            FdpCommand.execute({
                module: MODULE,
                cmdName: materialType == GLOBAL.constant.Dict.MATERIAL_TYPE.IMG.value ? "uploadImg" : "uploadVideo",
                options: result
            })

            setTimeout(function () {
                $("#material-sel").find("button[lay-filter='cancle_btn']").click();
            }, 500);
        })

    });
</script>