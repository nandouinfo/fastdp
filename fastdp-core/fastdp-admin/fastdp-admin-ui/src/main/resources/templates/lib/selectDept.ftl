<style>
    #select_dept_win .layadmin-caller .caller-contar .caller-item {
        padding: 5px 0;
        overflow: hidden;
    }
</style>
<div class="select-win" id="select_dept_win" style="height: 573px;overflow-y: hidden;">
    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">
        <div class="layui-row layui-col-space15">

            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">选择</div>
                    <div class="layui-card-body">

                        <form class="layui-form" action="" onsubmit="return false">

                            <div class="layui-form-item"  style="margin-bottom: 0px;">

                                <div class="layui-inline">
                                    <input type="text" name="deptName" placeholder="请输入关键字进行搜索" autocomplete="off" class="layui-input eleTree-search" style="width: 270px;">
                                </div>
                                <div class="layui-inline">
                                    <button id="selDeptTreeSearch_btn"  class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </div>
                        </form>

                        <div id="selDeptTree" class="eleTree" lay-filter="selDeptTree" style="margin-top: 9px; height: 460px;overflow-y: auto;"></div>
                    </div>
                </div>
            </div>

            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">已选择</div>
                    <div class="layui-card-body layadmin-caller">
                        <div class="caller-contar" style="height: 513px" id="selectedList">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-layer-btn layui-layer-btn-c layui-form-tool" style="border-top: 0px;">
                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="sel_dept_submit_btn">确定</button>
                    <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
                </div>
            </div>
        </div>

    </form>
</div>



<script>
    layui.use(['http', 'form', 'jquery', 'FdpUtil', 'eleTree', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            form = layui.form,
            eleTree = layui.eleTree,
            util = layui.util;

        var options = ${RequestParameters.options!};

        var selectDeptWin = $("#select_dept_win");
        var selectedList = selectDeptWin.find("#selectedList");
        var selDeptTree = null;
        http.post({
            url: GLOBAL.path.adminApi + '/org/dept/list',
            async: true,
            backFun: function(result){

                if(result){
                    for(var i = 0; i < result.length; i++){
                        if(result[i].type == GLOBAL.constant.Dict.ORG_TYPE.COMPANY.value){
                            result[i].label = '<i class="layui-icon layui-icon-app"></i>&nbsp;&nbsp;';
                        }else if(result[i].type == GLOBAL.constant.Dict.ORG_TYPE.DEPT.value){
                            result[i].label = '<i class="layui-icon layui-icon-list"></i>&nbsp;&nbsp;';
                        }

                        if(result[i].state == GLOBAL.constant.Dict.STATE.ENABLE.value){
                            result[i].label += result[i].name;
                        }else{
                            result[i].label += '<span style="color: #FF5722;text-decoration: line-through;">' + result[i].name + '</span>';
                        }
                    }
                }

                var treeData = DataConvert.listToTreeConvert(result, {
                    primaryKey: 'id',
                    parentKey: 'parentId',
                    valueKey: 'id'
                });

                selDeptTree = eleTree.render({
                    elem: '#selDeptTree',
                    data: treeData,
                    highlightCurrent: true,
                    checkOnClickNode: false,
                    showLine: true,
                    showCheckbox: true,
                    defaultExpandedKeys: options.checked ? options.checked : ['1'],
                    checkStrictly: true,
                    accordion: false,
                    expandOnClickNode: true,
                    request: {
                        name: 'label',
                        key: 'value'
                    },
                    searchNodeMethod: function(value,data) {
                        if (!value) return true;
                        return data.label.indexOf(value) !== -1;
                    }
                });

                selDeptTree.expandNode(['1']);

                selDeptTree.unCheckNodes();

                if(options.checked){

                    console.info("checked:" + JSON.stringify(options.checked));

                    selDeptTree.setChecked(options.checked, true);

                    var selDeptNodeList = selDeptTree.getChecked(false, false);

                    console.info("selDeptNodeList:" + JSON.stringify(selDeptNodeList))

                    for(var i = 0; i < selDeptNodeList.length; i++){
                        selectedList.append("<div class='caller-item' sel_dept_id='" + selDeptNodeList[i].value + "'>" +
                                                "<div class='caller-main caller-fl'>" +
                                                    "<p class='caller-adds'>" +
                                                        selDeptNodeList[i].label +
                                                        "<a href='javascript:;' dept_id='" + selDeptNodeList[i].value + "' class='right'>" +
                                                            "<i class='layui-icon layui-icon-delete'></i>" +
                                                        "</a>" +
                                                    "</p>" +
                                                "</div>" +
                                            "</div>")
                    }
                }

                // 节点点击事件
                eleTree.on("nodeChecked(selDeptTree)",function(d) {
                    console.log(d.data);    // 点击节点对应的数据

                    if(!d.isChecked){

                        selectedList.find("div[sel_dept_id='" + d.data.currentData.data.id + "']").remove();

                    }else{

                        //单选时
                        if(options.singleFlag){
                            selectedList.html("");
                            selDeptTree.unCheckNodes();
                            selDeptTree.setChecked([d.data.currentData.data.id],true);
                        }

                        selectedList.find("div[sel_dept_id='" + d.data.currentData.data.id + "']").remove();

                        selectedList.append("<div class='caller-item' sel_dept_id='" + d.data.currentData.data.id + "'>" +
                                                "<div class='caller-main caller-fl'>" +
                                                    "<p class='caller-adds'>" +
                                                        d.data.currentData.data.name +
                                                        "<a href='javascript:;' dept_id='" + d.data.currentData.data.id + "' class='right'>" +
                                                            "<i class='layui-icon layui-icon-delete'></i>" +
                                                        "</a>" +
                                                    "</p>" +
                                                "</div>" +
                                                "</div>")
                    }
                })

                selectedList.on('click', "a", function () {

                    var dept_code = $(this).attr("dept_id");

                    selectedList.find("div[sel_dept_id='" + dept_code + "']").remove();

                    selDeptTree.unCheckArrNodes([dept_code])
                });

                $('#selDeptTreeSearch_btn').click(function () {
                    selDeptTree.search(selectDeptWin.find("input[name='deptName']").val());
                })
            }});

        //监听确认
        form.on('submit(sel_dept_submit_btn)', function(data) {

            var selDeptNodeList = selDeptTree.getChecked(false, false);

            options.callBack.options = [];
            for(var i = 0; i < selDeptNodeList.length; i++){
                options.callBack.options.push({
                    name: selDeptNodeList[i].label,
                    id: selDeptNodeList[i].value
                })
            }

            FdpCommand.execute(options.callBack);

            $("#select_dept_win").find("button[lay-filter='cancle_btn']").click();

            return false;
        });

    });
</script>


