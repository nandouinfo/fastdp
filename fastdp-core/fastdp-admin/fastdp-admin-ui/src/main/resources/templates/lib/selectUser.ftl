<style>
    #select_dept_win .layadmin-caller .caller-contar .caller-item {
        padding: 5px 0;
        overflow: hidden;
    }
</style>
<div class="select-win" id="select_user_win" style="height: 573px;overflow-y: hidden;">
    <form class="layui-form" onsubmit="return false"  id="form" lay-filter="form">
        <div class="layui-row layui-col-space15">

            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">选择</div>
                    <div class="layui-card-body" style="height: 513px">

                        <form class="layui-form" action="" onsubmit="return false">
                            <div class="layui-form-item" style="margin-bottom: 5px;">

                                <#--<div class="layui-inline">
                                    <input type="text" name="dept" placeholder="所属组织" autocomplete="off" class="layui-input" readonly="readonly" style="width: 310px;">
                                </div>-->

                                <div class="layui-inline">
                                    <input type="text" name="name" placeholder="用户名称/工号" autocomplete="off" class="layui-input">
                                </div>

                                <div class="layui-inline">
                                    <button id="sel_user_search_btn" class="layui-btn" lay-submit="search"><i class="layui-icon">&#xe615;</i></button>
                                </div>

                            </div>
                        </form>

                        <table class="layui-hide" id="lay_sel_user_table_content" lay-filter="lay_sel_user_table" ></table>

                    </div>
                </div>
            </div>

            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">已选择</div>
                    <div class="layui-card-body layadmin-caller">
                        <div class="caller-contar" style="height: 513px; overflow-y: auto;" id="selectedUserList">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-layer-btn layui-layer-btn-c layui-form-tool" style="border-top: 0px;">
                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="sel_user_submit_btn">确定</button>
                    <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
                </div>
            </div>
        </div>

    </form>
</div>



<script>
    layui.use(['http', 'form', 'jquery', 'FdpUtil', 'table', 'util'], function(){

        var layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            form = layui.form,
            table = layui.table,
            util = layui.util;

        var options = ${RequestParameters.options!};

        var selectUserWin = $("#select_user_win");
        var selectedList = selectUserWin.find("#selectedUserList");
        var selecedUsers = [];
        var selecedUserMap = {};

        if(!options.checked){
            options.checked = [];
        }

        var addSelectedUserNode = function(userObj){

            if(options.singleFlag){
                selectedList.html("");
            }

            if(!selecedUserMap[userObj.code]){

                selectedList.find("div[sel_user_code='" + userObj.code + "']").remove();

                selectedList.append("<div class='caller-item' style='border-bottom: 1px solid #e0e0e0;margin-bottom: 15px;' sel_user_code='" + userObj.code + "'>" +
                    "<div class='caller-main caller-fl'>" +
                    "<p class='caller-adds'>" +
                    "<strong>工号: </strong>"+ userObj.code +
                    "</p>" +
                    "<p class='caller-adds'>" +
                    "<strong>姓名: </strong>" + userObj.name +
                    "<a href='javascript:;' user_code='" + userObj.code + "' class='right'>" +
                    "<i class='layui-icon layui-icon-delete'></i>" +
                    "</a>" +
                    "</p>" +
                    "<p class='caller-adds'>" +
                    "<strong>所属组织: </strong>" + Org.getDeptName(userObj.dept) +
                    "</p>" +
                    "</div>" +
                    "</div>");

                selecedUsers.push(userObj);

                selecedUserMap[userObj.code ] = userObj;
            }
        }

        for(var i = 0; i < options.checked.length; i++){
            addSelectedUserNode(Org.getUser(options.checked[i]));
        }

        var cols = [];

        if(options.singleFlag){
            cols.push({type: 'radio', fixed: true});
        }else{
            cols.push({checkbox: true, fixed: true});
        }

        cols.push({type: 'numbers'});
        cols.push({field:'code', title: '工号', width:150});
        cols.push({field:'name', title: '名称', width:100});
        cols.push({field:'deptName', title: '部门', minWidth:250, templet: function (d) {
                        return Org.getDeptName(d.dept);
                  }});

        //方法级渲染
        table =  $.extend(table, {config: {checkName: 'checked'}});
        var rows = null;
        table.render({
            elem: '#lay_sel_user_table_content',
            requestFun: function(page, size, renderFun) {

                http.post({
                    url: GLOBAL.path.adminApi + '/org/user/page',
                    async: false,
                    data: {
                        name: selectUserWin.find("input[name='name']").val(),
                        dept: selectUserWin.find("input[name='dept']").val(),
                        page: {
                            current: page,
                            size: size ? size : GLOBAL.page.size
                        }
                    },
                    backFun: function(result){

                        rows = result.records;

                        for(var i = 0; i < rows.length; i++){
                            rows[i].checked = selecedUserMap[rows[i].code] ? true : false;
                        }

                        renderFun({
                            data: rows,
                            count: result.total
                        });
                    }});
            },
            cols: [cols],
            id: 'lay_sel_user_table',
            page: true,
            limits: [10]
        });

        $('#sel_user_search_btn').on('click', function(){

            //执行重载
            table.reload('lay_sel_user_table', null);
        });


        table.on('checkbox(lay_sel_user_table)', function(obj){
            console.log(obj.checked); //当前是否选中状态
            console.log(obj.data); //选中行的相关数据
            console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one

            var rowDatas = [];

            if(obj.type == "all"){//是否全部
                if(obj.checked){
                    rowDatas = rowDatas.concat(table.checkStatus('lay_sel_user_table').data);
                }else{
                    rowDatas = rowDatas.concat(rows);
                }
            }else{
                rowDatas.push(obj.data);
            }

            for(var i = 0; i < rowDatas.length; i++){

                if(obj.checked){
                    addSelectedUserNode(rowDatas[i]);
                }
            }
        });

        table.on('radio(lay_sel_user_table)', function(obj){
            console.log(obj.checked); //当前是否选中状态
            console.log(obj.data); //选中行的相关数据

            //单选时
            selecedUsers = [];
            selecedUserMap = {};

            addSelectedUserNode(obj.data);
        });

        selectedList.on('click', "a", function () {

            var user_code = $(this).attr("user_code");

            selectedList.find("div[sel_user_code='" + user_code + "']").remove();

            selecedUsers.splice($.inArray(selecedUserMap[user_code], selecedUsers), 1);
            selecedUserMap[user_code] = null;

            for(var i = 0; i < rows.length; i++){
                rows[i].checked = selecedUserMap[rows[i].code] ? true : false;
            }

            table.reload('lay_sel_user_table', {
                data: rows
            });
        });

        //监听确认
        form.on('submit(sel_user_submit_btn)', function(data) {

            /*options.callBack.options = [];
            for(var i = 0; i < selDeptNodeList.length; i++){
                options.callBack.options.push({
                    name: selDeptNodeList[i].label,
                    code: selDeptNodeList[i].value
                })
            }*/

            options.callBack.options = selecedUsers;

            FdpCommand.execute(options.callBack);

            selectUserWin.find("button[lay-filter='cancle_btn']").click();

            return false;
        });

    });
</script>


