<script>
    var basePath = "${context.contextPath}";
</script>
<meta charset="utf-8" />
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- 防止大部分浏览器缓存： -->
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
<meta http-equiv="expires" content="-1">

<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="shortcut icon" href="${context.contextPath}/static/img/favicon.ico" type="image/x-icon"/>

<link rel="stylesheet" href="${context.contextPath}/static/layui/css/layui.css" media="all">

<script src="${context.contextPath}/static/config/config.js"></script>
<script src="${context.contextPath}/static/layui/layui.js"></script>

<!-- element-ui -->
<#--<link href="${context.contextPath}/static/lib/element-ui/2.15.0/index.css" rel="stylesheet" media="all">
<script src="${context.contextPath}/static/lib/element-ui/2.15.0/index.js"></script>-->

<script src="${context.contextPath}/static/lib/vue/vue.js"></script>

<style>
    .content{
        position: absolute;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        padding: 15px;
    }

    .content .layui-row{
        height: 100%;
    }

    .content .layui-row > div{
        height: 100%;
        padding-bottom: 0px;
    }

    .content .layui-card{
        height: 100%;
    }

    /*2、在样式表里设置：只要是有 v-cloak 属性的标签，我都让它隐藏。
    直到 Vue实例化完毕以后，v-cloak 会自动消失，那么对应的css样式就会失去作用，最终将span中的内容呈现给用户 */
    [v-cloak] {
        display: none;
    }

    @page {
        margin-bottom: 0mm;
        margin-top: 0mm
    }

</style>
