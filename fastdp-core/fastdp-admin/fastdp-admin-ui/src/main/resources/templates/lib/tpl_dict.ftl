<div style="padding: 10px;" id="tpl-dict-container">

        <div class="layui-form-item layui-row">
            <div class="layui-col-md1">&nbsp;</div>
            <div class="layui-col-md10" style="color: #FF5722">
                模板中使用方式: &#36;{字典名称}, 花括号为输入法英文状态下
            </div>
            <div class="layui-col-md1">&nbsp;</div>
        </div>

        <div class="layui-form-item layui-row">
            <div class="layui-col-md1">&nbsp;</div>
            <div class="layui-col-md10">
                <input type="text" v-model="searchName" autocomplete="off" class="layui-input" placeholder="请输入查找内容">
            </div>
            <div class="layui-col-md1">&nbsp;</div>
        </div>
        <div class="layui-form-item layui-row">
            <div class="layui-col-md1">&nbsp;</div>
                <div class="layui-col-md10">

                    <div class="layui-card">
                        <div class="layui-card-body">
                            <dl class="layuiadmin-card-status">
                                <dd v-for="(dict, index) in list">
                                    <div>
                                        <p>{{index + 1}}、{{dict.name}}</p>
                                        <span>{{dict.remark}}</span>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>

                </div>
            <div class="layui-col-md1">&nbsp;</div>
        </div>
        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">返回</button>
            </div>
        </div>

</div>
<script>
    layui.use(['http', 'jquery', 'FdpUtil', 'util', 'form', 'upload'], function () {

        var layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            util = layui.util,
            form = layui.form,
            upload = layui.upload;

        var vue = new Vue({

            el: "#tpl-dict-container",

            data(){
                return {
                    dictItems: [],
                    searchName: "",
                    list: []
                }
            },

            created(){
                this.dictItems = Dict.findItemByDictCode("COMPANY_INFO");
                this.dictItems = this.dictItems.concat(Dict.findItemByDictCode("${RequestParameters.dictCode!}"))
                this.list = this.dictItems;
            },

            computed:{

            },

            watch: {
                'searchName': function (value) {

                    if(value.length <= 0){
                        this.list = this.dictItems;
                    }

                    var result = [];
                    for (var i = 0; i < this.dictItems.length; i++){
                        if(this.dictItems[i].name.indexOf(value) >= 0){
                            result.push(this.dictItems[i])
                        }
                    }

                    this.list = result;
                }
            },

            methods: {

            }
        })

    });
</script>


