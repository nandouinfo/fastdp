<div style="padding: 20px;">
    <form id="import-form" class="layui-form" action="" onsubmit="return false">

        <div class="layui-form-item layui-row">
            <div class="layui-col-md1">&nbsp;</div>
            <div class="layui-upload-drag layui-col-md10" id="fileSelect">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <div class="layui-col-md1">&nbsp;</div>
        </div>
        <div class="layui-form-item layui-row">
            <div class="layui-col-md1">&nbsp;</div>
                <div class="layui-col-md10" style="text-align: center;">
                    <p>
                        注意事项： 导入数据请严格按照
                        <a id="template" style="color: #0e90d2;"
                                          href="">导入模板</a>进行导入
                    </p>

                </div>
            <div class="layui-col-md1">&nbsp;</div>
        </div>
        <div class="layui-row">
            <div class="layui-layer-btn layui-layer-btn-c layui-form-tool">
                <button id="import_btn" class="layui-btn">导入</button>
                <button type="button" class="layui-btn layui-btn-primary" lay-filter="cancle_btn">取消</button>
            </div>
        </div>

    </form>
</div>
<script>
    layui.use(['http', 'jquery', 'FdpUtil', 'util', 'form', 'upload'], function () {

        var layer = layui.layer,
            $ = layui.jquery,
            http = layui.http,
            util = layui.util,
            form = layui.form,
            upload = layui.upload;

        let options = JSON.parse(decodeURIComponent('${RequestParameters.options}'));

        $("#template").attr("href", options.tpl);

        let fileUpload = http.upload({
            elem: '#fileSelect'
            , url: GLOBAL.path.adminApi + '/public/import'
            , auto: false
            , accept: 'file'
            , bindAction: '#import_btn'
            , data: {
                busType: function () {
                    return options.busType;
                }
            }
            , before: function (obj) {
                layer.load(); //上传loading
            }
            , done: function (res, index, upload) {
                layer.closeAll('loading'); //关闭loading

                if (res.code && res.code == 200) {
                    top.layui.layer.msg("操作成功！");

                    FdpCommand.execute(options.callBack)

                    setTimeout(function () {
                        $("button[lay-filter='cancle_btn']").click();
                    }, 500);

                } else {
                    top.layui.layer.msg(res.msg);
                }

                return false;
            }
            , error: function (index, upload) {
                layer.closeAll('loading'); //关闭loading
            }
        });

    });
</script>


