<!DOCTYPE html>
<html>
<head>
    <title>加载中</title>
    <#include "*/lib/head.ftl" />
</head>
<body>

<script type="text/javascript" src="http://wap.cmread.com/rbc/t/content/repository/ues/js/s109/vconsole.js"></script>
<script type="text/javascript">
    var vConsole=new VConsole();
</script>

<script>
    layui.use(['FdpUtil','http'], function () {

        var layer = layui.layer,
            http = layui.http;

        var params = {
            codeId : "${RequestParameters.codeId!}",
            timeStamp : "${RequestParameters.timeStamp!}",
            ciphertext : "${RequestParameters.ciphertext!}"
        }

        var path = "";

        http.get({
            url: "${context.contextPath}/wx/base/objcode/get/" + params.codeId,
            async: false,
            backFun: function(result){

                if(result){
                    path = result.path + "?id=" + result.id;
                }else{

                    layer.msg("无效二维码!");

                    setTimeout(function (){
                        try{
                            WeixinJSBridge.call('closeWindow');
                        }catch (e) {
                            window.close();
                        }
                    }, 2000)
                }
            }});

        if(path && path.length > 0){
            window.location.href = "${context.contextPath}" + path;
        }
    });
</script>

</body>
</html>
