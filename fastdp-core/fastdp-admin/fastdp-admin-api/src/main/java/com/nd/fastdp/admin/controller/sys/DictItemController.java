package com.nd.fastdp.admin.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.sys.model.bo.dict.DictItemBO;
import com.nd.fastdp.sys.model.dto.dict.DictItemAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemListParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemModifyParam;
import com.nd.fastdp.sys.model.dto.dict.DictItemQueryParam;
import com.nd.fastdp.sys.service.DictItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/sys/dictItem")
@Api(tags={"字典项管理"})
public class DictItemController {

    @Autowired
    private DictItemService dictItemService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody DictItemAddParam param) {

        param.setLoginUser(loginUser);

        DictItemBO dictItemBO = dictItemService.add(param);

        return Result.OK(dictItemBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        dictItemService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody DictItemModifyParam param) {

        param.setLoginUser(loginUser);

        DictItemBO dictItemBO = dictItemService.modify(param);

        return Result.OK(dictItemBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        dictItemService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        DictItemBO dict = dictItemService.get(id);

        return Result.OK(dict);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) DictItemListParam param){

        param.setLoginUser(loginUser);

        List<DictItemBO> list = dictItemService.list(param);

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) DictItemQueryParam param) {

        param.setLoginUser(loginUser);

        Page<DictItemBO> page =  dictItemService.page(param);

        return Result.OK(page);
    }
}
