package com.nd.fastdp.admin.controller.workflow;

import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.org.service.DeptService;
import com.nd.fastdp.workflow.constant.UndoFinishedFlagEnum;
import com.nd.fastdp.workflow.model.bo.procinst.ProcinstBO;
import com.nd.fastdp.workflow.model.bo.undo.UndoBO;
import com.nd.fastdp.workflow.model.dto.undo.UndoAgreeParam;
import com.nd.fastdp.workflow.model.dto.undo.UndoAssignorParam;
import com.nd.fastdp.workflow.model.dto.undo.UndoReadParam;
import com.nd.fastdp.workflow.model.dto.undo.UndoRefuseParam;
import com.nd.fastdp.workflow.serivce.ProcinstService;
import com.nd.fastdp.workflow.serivce.UndoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/workflow/undo")
@Api(tags={"流程待办管理"})
public class UndoController {

    @Autowired
    private UndoService UndoService;

    @Autowired
    private DeptService deptService;

    @Autowired
    private ProcinstService procinstService;

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        UndoBO undoBO = UndoService.get(id);

        return Result.OK(undoBO);
    }

    @GetMapping("/getLoginUserByProcinstId/{id}")
    @ApiOperation(value = "当前登录用户待办")
    public Result getLoginUserByProcinstId(@ApiIgnore @CurrentUser LoginUser loginUser,  @PathVariable("id") String id){

        UndoBO result = null;

        ProcinstBO procinstBO = procinstService.get(id);

        if(procinstBO == null){
            return Result.OK(null);
        }

        List<UndoBO> list = UndoService.listByProcinstId(procinstBO.getId());
        for (UndoBO undoBO : list){
            if(undoBO.getUserId().equals(loginUser.getUserId()) && UndoFinishedFlagEnum.PROCESSING.getValue() == undoBO.getFinished()){
                result = undoBO;
                break;
            }
        }

        return Result.OK(result);
    }

    @PostMapping("/readed")
    @ApiOperation(value = "已读")
    public Result readed(@ApiIgnore @CurrentUser LoginUser loginUser,  @RequestBody UndoReadParam param) {

        UndoService.read(param);

       return Result.OK();
    }

    @PostMapping("/agree")
    @ApiOperation(value = "同意")
    public Result agree(@ApiIgnore @CurrentUser LoginUser loginUser, String taskId, @RequestBody UndoAgreeParam param) {

        UndoService.agree(param);

        return Result.OK();
    }

    @PostMapping("/refuse")
    @ApiOperation(value = "拒绝")
    public Result refuse(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UndoRefuseParam param) {

        UndoService.refuse(param);

        return Result.OK();
    }

    @PostMapping("/assignor")
    @ApiOperation(value = "委托")
    public Result assignor(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UndoAssignorParam param) {

        param.setDeptName(deptService.get(param.getDeptId()).getName());

        UndoService.assignor(param);

        return Result.OK();
    }

    @PostMapping("/undos")
    @ApiOperation(value = "代办")
    public Result assignor(@ApiIgnore @CurrentUser LoginUser loginUser) {

        return Result.OK();
    }

}
