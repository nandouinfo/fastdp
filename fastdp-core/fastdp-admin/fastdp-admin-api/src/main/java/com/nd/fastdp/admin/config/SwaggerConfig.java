package com.nd.fastdp.admin.config;

import com.nd.fastdp.framework.swagger.Swagger2Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Slf4j
@Configuration
@EnableSwagger2
@ConditionalOnProperty(value = {"fastdp.swagger.enable"}, matchIfMissing = true)
public class SwaggerConfig extends Swagger2Config {

    @Bean
    public Docket devApi(){

        Docket defaultDocket = createRestApi(PathSelectors.ant("/dev/**"), "开发平台接口文档");

        return defaultDocket;
    }

    @Bean
    public Docket orgApi(){

        Docket defaultDocket = createRestApi(PathSelectors.ant("/org/**"), "组织结构接口文档");

        return defaultDocket;
    }

    @Bean
    public Docket sysApi(){

        Docket defaultDocket = createRestApi(PathSelectors.ant("/sys/**"), "系统管理接口文档");

        return defaultDocket;
    }

    @Bean
    public Docket fileApi(){

        Docket defaultDocket = createRestApi(PathSelectors.ant("/file/**"), "文件管理接口文档");

        return defaultDocket;
    }
}
