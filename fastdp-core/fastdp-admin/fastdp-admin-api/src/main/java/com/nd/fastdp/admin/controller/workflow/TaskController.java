package com.nd.fastdp.admin.controller.workflow;

import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.workflow.model.bo.task.TaskBO;
import com.nd.fastdp.workflow.serivce.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/workflow/task")
@Api(tags={"流程任务管理"})
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        TaskBO taskBO = taskService.get(id);

        return Result.OK(taskBO);
    }

    @GetMapping("/listByProcinst/{procinstId}")
    @ApiOperation(value = "根据流程实例ID获取任务节点")
    public Result listByProcinst(@PathVariable("procinstId") String procinstId){

        List<TaskBO> result = taskService.list(procinstId);

        return Result.OK(result);
    }

}
