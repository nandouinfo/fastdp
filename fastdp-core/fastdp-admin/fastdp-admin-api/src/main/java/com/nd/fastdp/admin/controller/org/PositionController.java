package com.nd.fastdp.admin.controller.org;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.org.model.bo.position.PositionBO;
import com.nd.fastdp.org.model.dto.position.PositionAddParam;
import com.nd.fastdp.org.model.dto.position.PositionListParam;
import com.nd.fastdp.org.model.dto.position.PositionModifyParam;
import com.nd.fastdp.org.model.dto.position.PositionQueryParam;
import com.nd.fastdp.org.service.PositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/org/position")
@Api(tags={"职务管理"})
public class PositionController {

    @Autowired
    private PositionService positionService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody PositionAddParam param) {

        param.setLoginUser(loginUser);

        PositionBO positionBO = positionService.add(param);

        return Result.OK(positionBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        positionService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody PositionModifyParam param) {

        param.setLoginUser(loginUser);

        PositionBO positionBO = positionService.modify(param);

        return Result.OK(positionBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        positionService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        PositionBO position = positionService.get(id);

        return Result.OK(position);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) PositionListParam param){

        List<PositionBO> list = null;

        if(null == param){

            list = positionService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = positionService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) PositionQueryParam param) {

        param.setLoginUser(loginUser);

        Page<PositionBO> page =  positionService.page(param);

        return Result.OK(page);
    }
}
