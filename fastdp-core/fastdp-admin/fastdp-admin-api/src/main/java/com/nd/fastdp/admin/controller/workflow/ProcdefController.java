package com.nd.fastdp.admin.controller.workflow;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.workflow.model.bo.procdef.ProcdefBO;
import com.nd.fastdp.workflow.model.dto.procdef.*;
import com.nd.fastdp.workflow.model.pojo.Node;
import com.nd.fastdp.workflow.serivce.ProcdefService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/workflow/procdef")
@Api(tags={"流程定义管理"})
public class ProcdefController {

    @Autowired
    private ProcdefService procdefService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody ProcdefAddParam param) {

        param.setLoginUser(loginUser);

        ProcdefBO procdefBO = procdefService.add(param);

        return Result.OK(procdefBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        procdefService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody ProcdefModifyParam param) {

        param.setLoginUser(loginUser);

        ProcdefBO procdefBO = procdefService.modify(param);

        return Result.OK(procdefBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        procdefService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        ProcdefBO procdef = procdefService.get(id);

        return Result.OK(procdef);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcdefListParam param){

        List<ProcdefBO> list = null;

        if(null == param){

            list = procdefService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = procdefService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcdefQueryParam param) {

        param.setLoginUser(loginUser);

        Page<ProcdefBO> page =  procdefService.page(param);

        return Result.OK(page);
    }

    @PostMapping("/flowLine")
    @ApiOperation(value = "流程线")
    public Result flowLine(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) ProcdefFlowLineParam param){

        param.setLoginUser(loginUser);

        List<Node> list = procdefService.flowLine(param);

        return Result.OK(list);

    }


}
