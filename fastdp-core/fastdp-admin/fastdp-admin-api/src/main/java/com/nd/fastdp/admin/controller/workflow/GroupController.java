package com.nd.fastdp.admin.controller.workflow;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.workflow.model.bo.group.GroupBO;
import com.nd.fastdp.workflow.model.dto.group.GroupAddParam;
import com.nd.fastdp.workflow.model.dto.group.GroupListParam;
import com.nd.fastdp.workflow.model.dto.group.GroupModifyParam;
import com.nd.fastdp.workflow.model.dto.group.GroupQueryParam;
import com.nd.fastdp.workflow.serivce.GroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/workflow/group")
@Api(tags={"流程分组管理"})
public class GroupController {

    @Autowired
    private GroupService groupService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody GroupAddParam param) {

        param.setLoginUser(loginUser);

        GroupBO groupBO = groupService.add(param);

        return Result.OK(groupBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        groupService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody GroupModifyParam param) {

        param.setLoginUser(loginUser);

        GroupBO groupBO = groupService.modify(param);

        return Result.OK(groupBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        groupService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        GroupBO group = groupService.get(id);

        return Result.OK(group);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) GroupListParam param){

        List<GroupBO> list = null;

        if(null == param){

            list = groupService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = groupService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) GroupQueryParam param) {

        param.setLoginUser(loginUser);

        Page<GroupBO> page =  groupService.page(param);

        return Result.OK(page);
    }
}
