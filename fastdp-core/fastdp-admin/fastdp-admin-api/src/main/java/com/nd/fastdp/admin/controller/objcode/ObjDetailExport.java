package com.nd.fastdp.admin.controller.objcode;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ObjDetailExport implements Serializable {

    @ExcelProperty(value = "唯一标识", index = 0)
    private String id;

    @ExcelProperty(value = "名称", index = 1)
    private String name;

    @ExcelProperty(value = "编号", index = 2)
    private String no;

    @ExcelProperty(value = "状态", index = 3)
    private String stateName;

    @ExcelProperty(value = "使用时间", index = 4)
    private String usedTimeStr;

    @ExcelProperty(value = "使用人", index = 5)
    private String usedUserName;

    @ExcelProperty(value = "使用部门", index = 6)
    private String usedDeptName;
}
