package com.nd.fastdp.admin.controller.objcode;

import com.alibaba.fastjson.JSON;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.service.ExcelExportHandler;
import com.nd.fastdp.objcode.constant.ObjCodeItemStateEnum;
import com.nd.fastdp.objcode.model.bo.ObjCodeDetailBO;
import com.nd.fastdp.objcode.model.dto.ObjCodeDetailListParam;
import com.nd.fastdp.objcode.service.ObjCodeService;
import com.nd.fastdp.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component("exportService_objcodeDetail")
public class DetailExportHandler extends ExcelExportHandler {

    @Autowired
    private ObjCodeService objCodeService;

    @Override
    protected List getData(LoginUser loginUser, Map<String, Object> param, HttpServletRequest request) {

        ObjCodeDetailListParam listParam = JSON.parseObject(JSON.toJSONString(param), ObjCodeDetailListParam.class);
        listParam.setLoginUser(loginUser);

        List<ObjCodeDetailBO> list = objCodeService.list(listParam);

        List<ObjDetailExport> datas = new ArrayList<ObjDetailExport>();

        list.forEach(v -> {

            ObjDetailExport row = new ObjDetailExport();

            row.setId(v.getId());
            row.setName(v.getName());
            row.setNo(v.getNo());
            row.setStateName(ObjCodeItemStateEnum.getDesc(v.getState()));
            if(v.getUsedTime() != null){
                row.setUsedTimeStr(DateUtil.format(new Date(v.getUsedTime()), "yyyy-MM-dd HH:mm:ss"));
                row.setUsedUserName(row.getUsedUserName());
                row.setUsedDeptName(row.getUsedDeptName());
            }


            datas.add(row);
        });

        return datas;
    }

    @Override
    protected Class getTemplate() {
        return ObjDetailExport.class;
    }

    @Override
    protected String getFileName() {

        String fileName = "一物一码";
        long currentTimeMillis = System.currentTimeMillis();
        fileName = fileName + "-" + currentTimeMillis + ".xlsx";

        return fileName;
    }
}
