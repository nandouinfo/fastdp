package com.nd.fastdp.admin.controller.sys;

import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.sys.model.bo.commonFun.CommonFunBO;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunAddParam;
import com.nd.fastdp.sys.model.dto.commonFun.CommonFunListParam;
import com.nd.fastdp.sys.service.CommonFunService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/sys/commonFun")
@Api(tags={"常用功能管理"})
public class CommonFunController {

    @Autowired
    private CommonFunService commonFunService;

    @PostMapping("/add")
    @ApiOperation(value = "添加")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody CommonFunAddParam param) {

        param.setLoginUser(loginUser);

        commonFunService.add(param);

        return Result.OK();
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        commonFunService.del(param);

        return Result.OK();
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) CommonFunListParam param){

        param.setLoginUser(loginUser);

        List<CommonFunBO> list = commonFunService.list(param);

        return Result.OK(list);
    }
}
