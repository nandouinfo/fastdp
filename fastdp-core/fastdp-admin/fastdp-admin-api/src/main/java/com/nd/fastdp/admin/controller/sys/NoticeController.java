package com.nd.fastdp.admin.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.sys.model.bo.notice.NoticeBO;
import com.nd.fastdp.sys.model.bo.notice.NoticeReceiverBO;
import com.nd.fastdp.sys.model.bo.notice.NoticeReceiverVBO;
import com.nd.fastdp.sys.model.dto.notice.*;
import com.nd.fastdp.sys.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/sys/notice")
@Api(tags={"公告消息管理"})
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody NoticeAddParam param) {

        param.setLoginUser(loginUser);

        NoticeBO noticeBO = noticeService.add(param);

        return Result.OK(noticeBO);
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody NoticeModifyParam param) {

        param.setLoginUser(loginUser);

        NoticeBO noticeBO = noticeService.modify(param);

        return Result.OK(noticeBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        noticeService.del(param);

        return Result.OK();
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        NoticeBO notice = noticeService.get(id);

        return Result.OK(notice);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) NoticeListParam param){

        param.setLoginUser(loginUser);

        List<NoticeBO> list = noticeService.list(param);

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) NoticeQueryParam param) {

        param.setLoginUser(loginUser);

        Page<NoticeBO> page =  noticeService.page(param);

        return Result.OK(page);
    }

    @PostMapping("/release")
    @ApiOperation(value = "发布")
    public Result release(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestParam(value="id",required = true) String id){

        NoticeReleaseParam param = new NoticeReleaseParam();
        param.setLoginUser(loginUser);
        param.setId(id);

        noticeService.release(param);

        return Result.OK();
    }

    @PostMapping("/unRelease")
    @ApiOperation(value = "取消发布")
    public Result unRelease(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestParam(value="id",required = true) String id){

        NoticeReleaseParam param = new NoticeReleaseParam();
        param.setLoginUser(loginUser);
        param.setId(id);

        noticeService.unRelease(param);

        return Result.OK();
    }

    @PostMapping("/recivers")
    @ApiOperation(value = "接收者")
    public Result recivers(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestParam(value="id",required = true) String id){

        List<NoticeReceiverBO> receiverS = noticeService.listReceiver(id);

        return Result.OK(receiverS);
    }

    @PostMapping("/pageReceiver")
    @ApiOperation(value = "查阅分页")
    public Result pageReceiver(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) NoticeQueryParam param) {

        param.setLoginUser(loginUser);

        Page<NoticeReceiverVBO> page =  noticeService.pageReceiver(param);

        return Result.OK(page);
    }

    @GetMapping("/getReceiver/{id}")
    @ApiOperation(value = "接收人详情")
    public Result getReceiver(@PathVariable("id") String id){

        NoticeReceiverVBO receiverNotice = noticeService.getReceiverNotice(id);

        return Result.OK(receiverNotice);
    }

    @GetMapping("/read/{id}")
    @ApiOperation(value = "已读")
    public Result read(@PathVariable("id") String id){

        noticeService.read(id);

        return Result.OK();
    }

}
