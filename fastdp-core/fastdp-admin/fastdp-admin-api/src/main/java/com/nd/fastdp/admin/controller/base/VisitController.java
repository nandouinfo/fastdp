package com.nd.fastdp.admin.controller.base;

import com.alibaba.fastjson.JSON;
import com.nd.fastdp.base.model.dto.visit.VisitAddParam;
import com.nd.fastdp.base.model.service.VisitService;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.utils.TerminalInfo;
import com.nd.fastdp.utils.TerminalInfoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/public/")
@Api(tags={"访问记录"})
public class VisitController {

    @Autowired
    private VisitService visitService;

    @PostMapping("/visit")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody VisitAddParam param, HttpServletRequest request) {

        param.setLoginUser(loginUser);

        TerminalInfo terminalInfo = TerminalInfoUtil.get(request);
        param.setIp(terminalInfo.getIp());
        param.setViewTime(System.currentTimeMillis());
        param.setTerminalInfo(JSON.toJSONString(terminalInfo));

        visitService.add(param);

        return Result.OK();
    }
}
