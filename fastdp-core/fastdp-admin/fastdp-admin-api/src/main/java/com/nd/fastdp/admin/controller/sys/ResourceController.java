package com.nd.fastdp.admin.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.sys.model.bo.resource.ResourceBO;
import com.nd.fastdp.sys.model.bo.resource.ResourceDataScopeBO;
import com.nd.fastdp.sys.model.dto.resource.ResourceAddParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceListParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceModifyParam;
import com.nd.fastdp.sys.model.dto.resource.ResourceQueryParam;
import com.nd.fastdp.sys.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/sys/resource")
@Api(tags={"资源管理"})
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody ResourceAddParam param) {

        param.setLoginUser(loginUser);

        ResourceBO resourceBO = resourceService.add(param);

        return Result.OK(resourceBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        resourceService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody ResourceModifyParam param) {

        param.setLoginUser(loginUser);

        ResourceBO resource = resourceService.modify(param);

        return Result.OK(resource);
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        ResourceBO resource = resourceService.get(id);

        return Result.OK(resource);
    }

    @GetMapping("/getByCode/{code}")
    @ApiOperation(value = "详情")
    public Result getByCode(@PathVariable("code") String code){

        ResourceBO resource = resourceService.getByCode(code);

        return Result.OK(resource);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ResourceListParam param){

        List<ResourceBO> list = null;

        if(null == param){

            list = resourceService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = resourceService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ResourceQueryParam param) {

        param.setLoginUser(loginUser);

        Page<ResourceBO> page =  resourceService.page(param);

        return Result.OK(page);
    }

    @PostMapping("/listAllDataScope")
    @ApiOperation(value = "数据权限列表")
    public Result listAllDataScope(@ApiIgnore @CurrentUser LoginUser loginUser){

        List<ResourceDataScopeBO> result = resourceService.listAllDataScope();

        return Result.OK(result);
    }
}
