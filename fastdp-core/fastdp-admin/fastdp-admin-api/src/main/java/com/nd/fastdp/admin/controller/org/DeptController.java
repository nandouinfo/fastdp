package com.nd.fastdp.admin.controller.org;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import com.nd.fastdp.org.model.dto.dept.DeptAddParam;
import com.nd.fastdp.org.model.dto.dept.DeptListParam;
import com.nd.fastdp.org.model.dto.dept.DeptModifyParam;
import com.nd.fastdp.org.model.dto.dept.DeptQueryParam;
import com.nd.fastdp.org.service.DeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/org/dept")
@Api(tags={"部门管理"})
public class DeptController {

    @Autowired
    private DeptService deptService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody DeptAddParam param) {

        param.setLoginUser(loginUser);

        DeptBO deptBO = deptService.add(param);

        return Result.OK(deptBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        deptService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody DeptModifyParam param) {

        param.setLoginUser(loginUser);

        DeptBO deptBO = deptService.modify(param);

        return Result.OK(deptBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        deptService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        DeptBO dept = deptService.get(id);

        return Result.OK(dept);
    }

    @GetMapping("/getByCode/{code}")
    @ApiOperation(value = "详情")
    public Result getByCode(@PathVariable("code") String code){

        DeptBO dept = deptService.getByCode(code);

        return Result.OK(dept);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) DeptListParam param){

        List<DeptBO> list = null;

        if(null == param){

            list = deptService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = deptService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) DeptQueryParam param) {

        param.setLoginUser(loginUser);

        Page<DeptBO> page =  deptService.page(param);

        return Result.OK(page);
    }
}
