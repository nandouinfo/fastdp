package com.nd.fastdp.admin.controller.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.base.model.bo.material.MaterialBO;
import com.nd.fastdp.base.model.dto.material.MaterialAddParam;
import com.nd.fastdp.base.model.dto.material.MaterialListParam;
import com.nd.fastdp.base.model.dto.material.MaterialModifyParam;
import com.nd.fastdp.base.model.dto.material.MaterialQueryParam;
import com.nd.fastdp.base.model.service.MaterialService;
import com.nd.fastdp.file.convert.FileConvert;
import com.nd.fastdp.file.model.bo.FileBO;
import com.nd.fastdp.file.model.dto.FileAddParam;
import com.nd.fastdp.file.vo.FileVO;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/base/material")
@Api(tags={"素材管理"})
public class MaterialController {

    @Autowired
    private MaterialService materialService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody MaterialAddParam param) {

        param.setLoginUser(loginUser);

        materialService.add(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody MaterialModifyParam param) {

        param.setLoginUser(loginUser);

        materialService.modify(param);

        return Result.OK();
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        materialService.del(param);

        return Result.OK();
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        MaterialBO material = materialService.get(id);

        return Result.OK(material);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) MaterialListParam param){

        param.setLoginUser(loginUser);

        List<MaterialBO> list = materialService.list(param);

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) MaterialQueryParam param) {

        param.setLoginUser(loginUser);

        Page<MaterialBO> page =  materialService.page(param);

        return Result.OK(page);
    }
}
