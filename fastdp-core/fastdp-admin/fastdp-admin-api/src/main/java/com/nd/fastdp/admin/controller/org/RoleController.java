package com.nd.fastdp.admin.controller.org;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.org.model.bo.role.RoleBO;
import com.nd.fastdp.org.model.bo.role.RoleResourceBO;
import com.nd.fastdp.org.model.dto.role.*;
import com.nd.fastdp.org.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/org/role")
@Api(tags={"角色管理"})
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody RoleAddParam param) {

        param.setLoginUser(loginUser);

        RoleBO roleBO = roleService.add(param);

        return Result.OK(roleBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        roleService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody RoleModifyParam param) {

        param.setLoginUser(loginUser);

        RoleBO roleBO = roleService.modify(param);

        return Result.OK(roleBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        roleService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        RoleBO role = roleService.get(id);

        return Result.OK(role);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) RoleListParam param){

        List<RoleBO> list = null;

        if(null == param){

            list = roleService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = roleService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) RoleQueryParam param) {

        param.setLoginUser(loginUser);

        Page<RoleBO> page =  roleService.page(param);

        return Result.OK(page);
    }

    @PostMapping("/addResource")
    @ApiOperation(value = "添加资源")
    public Result addResource(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody RoleResourceAddParam param) {

        param.setLoginUser(loginUser);

        roleService.add(param);

        return Result.OK();
    }

    @GetMapping("/findResource/{roleId}")
    @ApiOperation(value = "获取资源")
    public Result findResource(@PathVariable("roleId") String roleId) {

        List<RoleResourceBO> roleResourceBOs = roleService.findRoleResourcesByRoleId(roleId);

        return Result.OK(roleResourceBOs);
    }
}
