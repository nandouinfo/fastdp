package com.nd.fastdp.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = {"com.nd.fastdp"})
public class FastdpAdminApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(FastdpAdminApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FastdpAdminApplication.class);
    }
}
