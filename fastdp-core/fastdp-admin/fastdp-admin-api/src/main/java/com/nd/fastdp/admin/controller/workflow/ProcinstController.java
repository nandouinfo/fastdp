package com.nd.fastdp.admin.controller.workflow;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.workflow.model.bo.procinst.ProcinstBO;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstQueryParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstStartParam;
import com.nd.fastdp.workflow.model.dto.procinst.ProcinstStatisticsParam;
import com.nd.fastdp.workflow.serivce.ProcinstService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/workflow/procinst")
@Api(tags={"流程实例管理"})
public class ProcinstController {

    @Autowired
    private ProcinstService procinstService;

    @PostMapping("/start")
    @ApiOperation(value = "启动")
    public Result start(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcinstStartParam param) {

        param.setLoginUser(loginUser);

        procinstService.start(param);

        return Result.OK();
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        ProcinstBO procinstBO = procinstService.get(id);

        return Result.OK(procinstBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        procinstService.del(param);

        return Result.OK();
    }

    @PostMapping("/recall")
    @ApiOperation(value = "撤回")
    public Result recall(String id){

        procinstService.recall(id);

        return Result.OK();
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcinstQueryParam param) {

        param.setLoginUser(loginUser);

        Page<ProcinstBO> page =  procinstService.page(param);

        return Result.OK(page);
    }

    @PostMapping("/mySendPage")
    @ApiOperation(value = "已发送")
    public Result mySendPage(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcinstQueryParam param) {

        param.setLoginUser(loginUser);

        Page<ProcinstBO> page =  procinstService.mySendPage(param);

        return Result.OK(page);
    }

    @PostMapping("/waitApprovePage")
    @ApiOperation(value = "待处理")
    public Result waitApprovePage(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcinstQueryParam param) {

        param.setLoginUser(loginUser);

        Page<ProcinstBO> page =  procinstService.waitApprovePage(param);

        return Result.OK(page);
    }

    @PostMapping("/alreadyApprovePage")
    @ApiOperation(value = "已处理")
    public Result alreadyApprovePage(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcinstQueryParam param) {

        param.setLoginUser(loginUser);

        Page<ProcinstBO> page =  procinstService.alreadyApprovePage(param);

        return Result.OK(page);
    }

    @PostMapping("/statistics")
    @ApiOperation(value = "统计总数")
    public Result statistics(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) ProcinstStatisticsParam param) {

        param.setLoginUser(loginUser);

        return Result.OK(procinstService.statistics(param));
    }

}
