package com.nd.fastdp.admin.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.sys.model.bo.msg.MsgBO;
import com.nd.fastdp.sys.model.dto.msg.MsgAddParam;
import com.nd.fastdp.sys.model.dto.msg.MsgListParam;
import com.nd.fastdp.sys.model.dto.msg.MsgQueryParam;
import com.nd.fastdp.sys.service.MsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/sys/msg")
@Api(tags={"系统消息管理"})
public class MsgController {

    @Autowired
    private MsgService msgService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody MsgAddParam param) {

        param.setLoginUser(loginUser);

        msgService.add(param);

        return Result.OK();
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        msgService.del(param);

        return Result.OK();
    }

    @PostMapping("/read")
    @ApiOperation(value = "已读")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, String id) {

        msgService.read(id);

        return Result.OK();
    }

    @PostMapping("/readAll")
    @ApiOperation(value = "全部已读")
    public Result readAll(@ApiIgnore @CurrentUser LoginUser loginUser) {

        msgService.readByUser(loginUser.getUserId());

        return Result.OK();
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        MsgBO msg = msgService.get(id);

        return Result.OK(msg);
    }

    @GetMapping("/unReadCount")
    @ApiOperation(value = "未读数量")
    public Result unReadCount(@ApiIgnore @CurrentUser LoginUser loginUser){

        Integer count = msgService.unReadCount(loginUser.getUserId());

        return Result.OK(count);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) MsgListParam param){

        param.setLoginUser(loginUser);

        List<MsgBO> list = msgService.list(param);

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) MsgQueryParam param) {

        param.setLoginUser(loginUser);

        Page<MsgBO> page =  msgService.page(param);

        return Result.OK(page);
    }
}
