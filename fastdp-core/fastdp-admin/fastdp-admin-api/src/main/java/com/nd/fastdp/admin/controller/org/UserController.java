package com.nd.fastdp.admin.controller.org;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.exception.BusinessException;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.framework.properties.FdpProperties;
import com.nd.fastdp.org.model.bo.account.AccountBO;
import com.nd.fastdp.org.model.bo.org.DeptBO;
import com.nd.fastdp.org.model.bo.user.UserBO;
import com.nd.fastdp.org.model.bo.user.UserRoleBO;
import com.nd.fastdp.org.model.dto.account.AccountAddParam;
import com.nd.fastdp.org.model.dto.account.AccountUpdatePwdParam;
import com.nd.fastdp.org.model.dto.user.*;
import com.nd.fastdp.org.service.AccountService;
import com.nd.fastdp.org.service.RoleService;
import com.nd.fastdp.org.service.UserService;
import com.nd.fastdp.security.util.PasswordUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/org/user")
@Api(tags={"用户管理"})
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private FdpProperties fdpProperties;

    @Autowired
    private AccountService accountService;

    @Autowired
    private RoleService roleService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UserAddParam param) {

        param.setLoginUser(loginUser);

        UserBO userBO = userService.add(param);

        //自动创建账号
        AccountAddParam accountAddParam = new AccountAddParam();
        accountAddParam.setAccount(userBO.getCode());
        accountAddParam.setUserId(userBO.getId());
        accountAddParam.setPassword(PasswordUtil.encrypt(fdpProperties.getLoginInitPassword(), fdpProperties.getLoginInitSalt()));
        accountAddParam.setSalt(fdpProperties.getLoginInitSalt());
        accountAddParam.setLoginUser(param.getLoginUser());

        accountService.add(accountAddParam);

        return Result.OK(userBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        userService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UserModifyParam param) {

        param.setLoginUser(loginUser);

        UserBO userBO = userService.modify(param);

        return Result.OK(userBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        userService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        UserBO user = userService.get(id);

        return Result.OK(user);
    }

    @GetMapping("/getByCode/{code}")
    @ApiOperation(value = "详情")
    public Result getByCode(@PathVariable("code") String code){

        UserBO user = userService.getByCode(code);

        return Result.OK(user);
    }

    @GetMapping("/findByRoleId/{roleId}")
    @ApiOperation(value = "根据角色获取")
    public Result findByRoleId(@PathVariable("roleId") String roleId){

        List<UserBO> list = userService.findByRoleId(roleId);

        return Result.OK(list);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) UserListParam param){

        List<UserBO> list = null;

        if(null == param){

            list = userService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = userService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) UserQueryParam param) {

        param.setLoginUser(loginUser);

        Page<UserBO> page =  userService.page(param);

        return Result.OK(page);
    }

    @PostMapping("/addRole")
    @ApiOperation(value = "添加角色")
    public Result addRole(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UserRoleAddParam param) {

        param.setLoginUser(loginUser);

        userService.add(param);

        return Result.OK();
    }

    @PostMapping("/delRole")
    @ApiOperation(value = "删除用户指定角色")
    public Result addRole(@ApiIgnore @CurrentUser LoginUser loginUser, String userId, String roleId) {

        userService.del(userId, roleId);

        return Result.OK();
    }

    @GetMapping("/findRole/{userId}")
    @ApiOperation(value = "获取角色")
    public Result findRole(@PathVariable("userId") String userId) {

        List<UserRoleBO> userRoleBOS =  userService.findRoleByUserId(userId);

        return Result.OK(userRoleBOS);
    }

    @PostMapping("/addMangArea")
    @ApiOperation(value = "添加管理区域")
    public Result addMangArea(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UserMangAreaAddParam param) {

        param.setLoginUser(loginUser);

        userService.add(param);

        return Result.OK();
    }

//    @GetMapping("/findMangArea/{userId}")
//    @ApiOperation(value = "获取管理区域")
//    public Result findMangArea(@PathVariable("userId") String userId) {
//
//        List<AreaBO> userMangAreaBOS =  userService.findMangAreaByUserId(userId);
//
//        return Result.OK(userMangAreaBOS);
//    }

    @PostMapping("/addMangDept")
    @ApiOperation(value = "添加管理部门")
    public Result addMangDept(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UserMangDeptAddParam param) {

        param.setLoginUser(loginUser);

        userService.add(param);

        return Result.OK();
    }

    @GetMapping("/findMangDept/{userId}")
    @ApiOperation(value = "获取管理部门")
    public Result findMangDept(@PathVariable("userId") String userId) {

        List<DeptBO> userMangDeptBOS =  userService.findMangDeptByUserId(userId);

        return Result.OK(userMangDeptBOS);
    }

    @PostMapping("/resetPwd")
    @ApiOperation(value = "重置密码")
    public Result resetPwd(@ApiIgnore @CurrentUser LoginUser loginUser, String userCode) {

        AccountUpdatePwdParam param = new AccountUpdatePwdParam();

        param.setLoginUser(loginUser);
        param.setAccount(userCode);
        param.setPassword(PasswordUtil.encrypt(fdpProperties.getLoginInitPassword(), fdpProperties.getLoginInitSalt()));

        accountService.modify(param);

        return Result.OK(fdpProperties.getLoginInitPassword());
    }

    @PostMapping("/modifyPwd")
    @ApiOperation(value = "修改密码")
    public Result modifyPwd(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody UserModifyPwdParam userModifyPwdParam) {

        AccountUpdatePwdParam param = new AccountUpdatePwdParam();

        String account = loginUser.getAccount();

        AccountBO loginAccount = accountService.getByAccount(account);
        if (null == loginAccount) {
            throw new BusinessException("账户不存在！");
        }
        String enPwd = PasswordUtil.encrypt(userModifyPwdParam.getPwd(), loginAccount.getSalt());
        if (!enPwd.equals(loginAccount.getPassword())) {
            throw new BusinessException("密码错误！");
        }
        if (!userModifyPwdParam.getConfirmPwd().equals(userModifyPwdParam.getNewPwd())) {
            throw new BusinessException("密码不一致！");
        }

        param.setLoginUser(loginUser);
        param.setAccount(account);
        param.setPassword(PasswordUtil.encrypt(userModifyPwdParam.getNewPwd(), fdpProperties.getLoginInitSalt()));
        param.setSalt(fdpProperties.getLoginInitSalt());

        accountService.modify(param);

        return Result.OK();
    }
}
