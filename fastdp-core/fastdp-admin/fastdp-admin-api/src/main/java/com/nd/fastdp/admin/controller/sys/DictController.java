package com.nd.fastdp.admin.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nd.fastdp.framework.annotation.CurrentUser;
import com.nd.fastdp.framework.pojo.bo.LoginUser;
import com.nd.fastdp.framework.pojo.constant.VerifyRespCodeEnum;
import com.nd.fastdp.framework.pojo.dto.DelParam;
import com.nd.fastdp.framework.pojo.dto.StateParam;
import com.nd.fastdp.framework.pojo.vo.Result;
import com.nd.fastdp.security.exception.VerificationCodeException;
import com.nd.fastdp.sys.model.bo.dict.DictBO;
import com.nd.fastdp.sys.model.dto.dict.DictAddParam;
import com.nd.fastdp.sys.model.dto.dict.DictListParam;
import com.nd.fastdp.sys.model.dto.dict.DictModifyParam;
import com.nd.fastdp.sys.model.dto.dict.DictQueryParam;
import com.nd.fastdp.sys.service.DictService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/sys/dict")
@Api(tags={"字典类型管理"})
public class DictController {

    @Autowired
    private DictService dictService;

    @PostMapping("/add")
    @ApiOperation(value = "创建")
    public Result add(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody DictAddParam param) {

        param.setLoginUser(loginUser);

        DictBO dictBO = dictService.add(param);

        return Result.OK(dictBO);
    }

    @PostMapping("/del")
    @ApiOperation(value = "删除")
    public Result del(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = true) List<String> ids) {

        DelParam param = new DelParam();

        param.setIds(ids);
        param.setLoginUser(loginUser);

        dictService.del(param);

        return Result.OK();
    }

    @PostMapping("/modify")
    @ApiOperation(value = "修改")
    public Result modify(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody DictModifyParam param) {

        param.setLoginUser(loginUser);

        DictBO dictBO = dictService.modify(param);

        return Result.OK(dictBO);
    }

    @PostMapping("/modifyState")
    @ApiOperation(value = "启停")
    public Result modifyState(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody StateParam param) {

        param.setLoginUser(loginUser);

        dictService.modifyState(param);

        return Result.OK();
    }


    @GetMapping("/get/{id}")
    @ApiOperation(value = "详情")
    public Result get(@PathVariable("id") String id){

        DictBO dict = dictService.get(id);

        return Result.OK(dict);
    }

    @GetMapping("/getByCode/{code}")
    @ApiOperation(value = "详情")
    public Result getByCode(@PathVariable("code") String code){

        DictBO dict = dictService.getByCode(code);

        return Result.OK(dict);
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表")
    public Result list(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) DictListParam param){

        List<DictBO> list = null;

        if(null == param){

            list = dictService.listAll();

        }else{

            param.setLoginUser(loginUser);

            list = dictService.list(param);
        }

        return Result.OK(list);
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询")
    public Result page(@ApiIgnore @CurrentUser LoginUser loginUser, @RequestBody(required = false) DictQueryParam param) {

        param.setLoginUser(loginUser);

        Page<DictBO> page =  dictService.page(param);

        return Result.OK(page);
    }
}
